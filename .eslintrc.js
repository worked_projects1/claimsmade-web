module.exports = {
    "parser": 'babel-eslint',
    "extends": ['airbnb', 'prettier', 'prettier/react'],
    "plugins": ['prettier', 'redux-saga', 'react', 'react-hooks', 'jsx-a11y'],
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2021,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    // "eslintIgnore": "**/*.js",
    "rules": {
        "no-console": "off",
        "jsx-a11y/anchor-is-valid": "off",
        "no-control-regex": 0
     },
    "settings": {
        'import/resolver': {
          "webpack": {
            "config": './internals/webpack/webpack.prod.babel.js',
          },
        },
      },
}