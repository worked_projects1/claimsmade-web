import { makeStyles } from '@mui/styles';
const Styles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  marginTopMedium: {
    marginTop: theme.spacing(4),
    padding: '10px',
    opacity: '0.65',
    fontSize: '14px'
  },
  btnVerify: {
    width: '205px',
    height: '37px',
    margin: '10px',
    textTransform: 'none',
    marginTop: '20px',
  },
  fieldColor: {
    width: '205px',
    marginTop: '24px',
    marginBottom: '10px',
    '& :after': {
      borderBottomColor: '#9dbcdc',
    },
    '& :before': {
      borderBottomColor: '#9dbcdc',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#9dbcdc',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#9dbcdc',
    },

  },
  linkColor: {
    color: '#0077c5',
    textDecoration: 'none',
    boxShadow: 'none',
    background: 'transparent !important',
    border: 'none',
    textTransform: 'capitalize',
    '&:hover': {
      textDecoration: 'none',
      boxShadow: 'none',
      background: 'transparent',
      border: 'none'
    }
  },
  error: {
    fontSize: '14px',
    color: 'red',
    marginTop: '5px'
  }
}));
export default Styles;
