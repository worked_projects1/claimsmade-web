/**
 * 
 * Verification Form
 * 
 */



import React, { useEffect, useState } from 'react';
import Styles from './styles';
import { Field, reduxForm } from 'redux-form';
import { Grid, Button, Typography } from '@mui/material';
import Error from '../Error';
import ButtonSpinner from 'components/ButtonSpinner';
import TextField from '@mui/material/TextField';
import { resendOtp } from 'blocks/session/actions';

const TextAreaField = ({ input, classes, errorStyle }) => {// eslint-disable-line
    return <div>
        <TextField
            name={input.name}
            className={classes.fieldColor}
            variant="filled"
            autoFocus
            onChange={(e) => input.onChange(e.target.value)}
            inputProps={
                {
                    min: 0,
                    style:
                    {
                        textAlign: 'center',
                        fontSize: '16px',
                        padding: '17px 12px 10px'
                    }
                }} />
        <div style={errorStyle || {}} className={classes.error}>
        </div>
    </div>;
}

/**
 * 
 * @param {object} props 
 * @returns 
 */
function VerificationForm(props) {
    const { handleSubmit, pristine, submitting, errorMessage, clearCache, destroy, locationState = {}, dispatch, success } = props;// eslint-disable-line
    const classes = Styles();
    const [buttonSpinner, setButtonSpinner] = useState(false);

    useEffect(() => {
        return () => destroy();
    }, []);

    const handleResendCode = () => {
        setButtonSpinner(true);
        dispatch(resendOtp(locationState.identifier, locationState.secret, setButtonSpinner));
    }

    return (
        <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container direction="row" justifyContent="center" alignItems="center" style={{ marginBottom: '20px' }}>
                <img src={require('images/home/vettedclaims_color.png')} style={{ width: '80%' }} />
            </Grid>
            <Grid container spacing={3} justifyContent="center" alignItems="center">
                <Typography variant="body2" className={classes.marginTopMedium}>
                    A secret code has been sent to your email address
                 </Typography>
                <Grid>
                    <Field
                        name="verifyInput"
                        classes={classes}
                        component={TextAreaField}
                        type="text"
                    />
                </Grid>
            </Grid>
            <Grid container spacing={3} justifyContent="center" alignItems="center">
                <Button
                    className={classes.btnVerify}
                    type="submit"
                    variant="contained"
                    disabled={pristine}
                    color="primary">
                    {submitting && <ButtonSpinner /> || 'Verify'}
                </Button>
            </Grid>
            <Grid container spacing={3} justifyContent="center" style={{ marginTop: '10px' }}>
                <Button className={classes.linkColor}
                    type="button"
                    variant="contained"
                    onClick={handleResendCode}
                    color="primary">
                    {buttonSpinner && <ButtonSpinner color='#9dbcdc' /> || 'Resend Verification Code'}
                </Button>
            </Grid>
            {errorMessage && <Error errorMessage={errorMessage} style={{ marginTop: '35px' }} onClose={clearCache} /> || null}
            {/* {success && success['verifyOtp'] && <Success successMessage={success['verifyOtp']} style={{ marginTop: '35px' }} onClose={clearCache} /> || null} */}
        </form>
    )
}

const validate = (values) => {
    const errors = {}

    const requiredFields = ['verifyInput'];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })

    if (values.verifyInput && values.verifyInput.length < 4) {
        errors.verifyInput = 'OTP must be 4 characters'
    }

    return errors
}

export default reduxForm({
    form: 'verifyForm',
    validate,
    touchOnChange: true,
})(VerificationForm);
