
/**
 * 
 * Checkbox Field
 * 
 */

import React from 'react';
import { FormControlLabel, FormControl, Grid } from '@mui/material'
import Checkbox from '@mui/material/Checkbox';
import styles from './styles';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

export default function CheckboxField({ input, options, meta: { touched, error }, disabled }) {// eslint-disable-line

  const classes = styles();
  const isPreDefinedSet = Array.isArray(options);
  let inputValue = (input && input.value != "" && Object.keys(JSON.parse(input.value)).length > 0 && input.value) || "";
  const theme = useTheme();
  const xs = useMediaQuery(theme.breakpoints.up('xs'));

  const handleChange = (val, checked) => {
    const changeValue = inputValue != "" && JSON.parse(inputValue) || {}
    if (checked) {
      changeValue[val] = true
    } else {
      delete changeValue[val];

    }
    if (Object.keys(changeValue).length == 0) {
      input.onChange("")
    } else {
      input.onChange(JSON.stringify(changeValue))
    }
    //  }
  }

  return (<div style={{ width: '100%', padding: '5px 20px 0px 20px', maxWidth: '100%' }}>
    <FormControl component="fieldset" className={classes.fieldSet} error={touched && !!error}>
      {isPreDefinedSet ? (options || []).map((opt, index) =>// eslint-disable-line

        <Grid container key={index} className={classes.checkContainer}>
          <Grid item style={{ marginRight: '10px' }}>
            <FormControlLabel
              control={
                <Checkbox

                  style={{ color: "#3c89c9" }}
                  defaultChecked={input.value && JSON.parse(input.value)[opt.value] || ''}
                  className={classes.checkBox}
                  disabled={disabled}
                  onChange={(e) => handleChange(opt.value, e.target.checked)} />
              }
              labelPlacement="right"
              label={<span className={classes.textSize} dangerouslySetInnerHTML={{ __html: '' }} />}
              className={classes.checkboxLabel}
            />
          </Grid>
          <Grid item style={{ maxWidth: xs ? '85%' : '92%' }}>
            <div style={{ display: 'inline-block' }}><span className={classes.checkLabel}>{opt.label}</span></div>
          </Grid>
        </Grid>
      )
        : null}
    </FormControl>
  </div>)
}