

import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/material/styles';
const useStyles = makeStyles((theme) => ({
    checkboxLabel: {
        marginRight: '3px',
        '& span': {
            fontSize: '1rem',
            '& span': {
                marginTop: theme.spacing(0.5),
            }
        },
    },
    fieldSet: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
    },
    textSize: {
        fontSize: '14px',
    },
    label: {
        fontSize: '18px',
        marginTop: '1em',
        fontWeight: '400'
    },
    formControlLabel: {
        marginTop: useTheme().spacing(2),
        '& .MuiFormControlLabel-root.Mui-disabled': {
            opacity: 0.5
        }
    },
    error: {
        fontSize: '14px',
        color: 'red'
    },
    signUpForm: {
        '& .MuiTypography-root': {
            paddingLeft: '5px',
        }
    },
    checkBox: {
        '&.Mui-disabled': {
            opacity: 0.5,
        },
        padding: '0px',
    },
    checkLabel: {
        fontSize: '1rem'
    },
    checkContainer: {
        padding: '3px 0px'
    }

}));


export default useStyles;