import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
    root: {
        height: '100vh',
        flexGrow: 1
    },
    submitBtn: {
        fontWeight: 'bold',
        borderRadius: '3px',
        fontFamily: 'MyriadPro-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        height: '35px',
        marginRight: '15px',
        paddingTop: '10px'
    },
    
}));


export default useStyles;
