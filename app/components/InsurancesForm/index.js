
/**
 * 
 * Insurances Form
 * 
 */

 import React, { useEffect } from 'react';
 import { Field, FieldArray, reduxForm } from 'redux-form';
 import { ImplementationFor } from 'components/CreateRecordForm/utils';
 import { Grid, Button } from '@mui/material';
 import useStyles from './styles';
 import ButtonSpinner from 'components/ButtonSpinner';
 import Error from '../Error';
 import { normalize } from 'utils/tools';
 
 /**
  * 
  * @param {object} props 
  * @returns 
  */
 
 function InsurancesForm(props) {
 
     const classes = useStyles();
     const { handleSubmit, submitting, fields, error, metaData, destroy, spinner, btnDisabled } = props;// eslint-disable-line
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     return (<div>
         <form className={classes.form} onSubmit={handleSubmit.bind(this)} noValidate >
             <Grid container spacing={3} name="insurancesForm">
                 {(fields || []).map((field, index) => {
                     const InputComponent = ImplementationFor[field.type];
                     return <Grid key={index} item xs={12}>
                         {field.type !== 'fieldArray' ?
                             <Field
                                 name={field.value}
                                 label={field.label}
                                 type="text"
                                 metaData={metaData}
                                 component={InputComponent}
                                 required={field.required}
                                 disabled={field.disabled}
                                 normalize={normalize(field)}
                                 {...field} /> :
                             <FieldArray
                                 name={field.value}
                                 label={field.label}
                                 type="text"
                                 fieldArray={field.fieldArray}
                                 metaData={metaData}
                                 component={InputComponent}
                                 required={field.required}
                                 normalize={normalize(field)}
                                 ImplementationFor={ImplementationFor}
                                 disabled={field.disabled}
                                 {...field} />}
                     </Grid>
                 })}
                 <Grid item xs={12}>
                     {error && <Error errorMessage={error} /> || ''}
                 </Grid>
             </Grid>
             <Grid className={classes.footer}>
                 <Button
                     type="submit"
                     disabled={btnDisabled ? false : true}
                     variant="contained"
                     color="primary"
                     className={classes.submitBtn}>
                     {(submitting || spinner) && <ButtonSpinner /> || 'Submit'}
                 </Button>
             </Grid>
         </form>
     </div>);
 }
 
 const validate = (values) => {
     const errors = {}
 
     const requiredFields = ['guidelines', 'chapters', 'diagnosis', 'services']
 
     requiredFields.forEach(field => {
         if (!values[field]) {
             errors[field] = 'Required'
         }
     })
     return errors
 }
 
 export default reduxForm({
     form: 'Insurances',
     enableReinitialize: true,
     validate,
     touchOnChange: true,
 })(InsurancesForm);
 