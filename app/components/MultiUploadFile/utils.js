
/**
 * 
 * Utils
 * 
 */
 export function getContentType(name) {
  
    const extension = name && name.substring(name.lastIndexOf('.') + 1).toLowerCase() || '';
    if (extension === 'pdf') {
      return 'application/pdf'
    } else if (extension === 'mp4') {
      return 'video/mp4'
    } else if (extension === 'gif') {
      return 'image/gif'
    } else if (extension === 'png') {
      return 'image/png'
    } else if (extension === 'ico') {
      return 'image/x-icon'
    } else if (extension === 'jng') {
      return 'image/x-jng'
    } else if (extension === 'bmp') {
      return 'image/x-ms-bmp'
    } else if (extension === 'webp') {
      return 'image/webp'
    } else if (extension === 'wbmp') {
      return 'image/vnd.wap.wbmp'
    } else if (extension === 'jpeg' || extension === 'jpg') {
      return 'image/jpeg'
    } else if (extension === 'svg' || extension === 'svgz') {
      return 'image/svg+xml'
    } else if (extension === 'tif' || extension === 'tiff') {
      return 'image/tiff'
    } else if (extension === '3gpp' || extension === '3gp') {
      return 'video/3gpp'
    } else if (extension === 'mpeg' || extension === 'mpg') {
      return 'video/mpeg'
    } else if (extension === 'asx' || extension === 'asf') {
      return 'video/x-ms-asf'
    } else if (extension === 'mov') {
      return 'video/quicktime'
    } else if (extension === 'ogg') {
      return 'video/ogg'
    } else if (extension === 'wmv') {
      return 'video/x-ms-wmv'
    } else if (extension === 'webm') {
      return 'video/webm'
    } else if (extension === 'flv') {
      return 'video/x-flv'
    } else if (extension === 'avi') {
      return 'video/x-msvideo'
    } else if (extension === 'm4v') {
      return 'video/x-m4v'
    } else if (extension === 'mng') {
      return 'video/x-mng'
    } else if (extension === 'doc' || extension === 'dot') {
      return 'application/msword'
    } else if (extension === 'docx') {
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    } else if (extension === 'dotx') {
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
    } else if (extension === 'docm') {
      return 'application/vnd.ms-word.document.macroEnabled.12'
    } else {
      return extension;
    }
  }