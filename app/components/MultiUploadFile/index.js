
/**
 * 
 * UploadField
 * 
 * 
 */
import React, { useState, useCallback, useEffect, useRef } from 'react';
import { useDropzone } from 'react-dropzone';
import { Grid, Button } from '@mui/material';
import { getContentType } from './utils';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import styles from './styles';
import { uploadFile } from 'utils/api';
import Snackbar from '../Snackbar';
import CircularProgress from '@mui/material/CircularProgress';
import { uploadingAttachment, getFileContent, openUrl, getUploadPdfUrl } from 'blocks/poc2/remotes.js';// eslint-disable-line
import { capitalizeFirstLetterOnly } from 'utils/tools';


function UploadField({ input, contentType = '*', meta: { error, warning }, uploadName, rfaFormId, max, label, fileloading, lastValue }) {// eslint-disable-line
  const inputValue = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];
  const componentMounted = useRef(true);
  const [uploadError, setuploadError] = useState(false);
  const [loading, setLoading] = useState(false);
  const classes = styles();
  const labelValue = capitalizeFirstLetterOnly(label);
  const accept = contentType;

  useEffect(() => {
    fileloading(true);
    if (inputValue.length != 0) {
      const promises = inputValue.map(async (e, i) => {
        return await Promise.resolve(await openUrl(Object.assign({}, { s3_file_key: e.s3_file_key }))
          .then((res) => {
            inputValue[i].base64 = res.base64_data;
            inputValue[i].publicUrl = res.publicUrl;
            return inputValue
          })
          .catch((err) => { setuploadError('Failed to load base64 data') })// eslint-disable-line
        )
      })
      Promise.all(promises).then((data, i) => {// eslint-disable-line
        if (input.name == lastValue) {
          fileloading(false);
        }
      });
    } else {
      if (lastValue == "") {
        fileloading(false);
      }
      if (input.name == lastValue) {
        fileloading(false);
      }
    }
    return () => { // This code runs when component is unmounted
      componentMounted.current = false; // (4) set it to false when we leave the page
    }
  }, []);
  const openPdf = (s3_file_key) => {
    (async () => {
      await openUrl(Object.assign({}, { s3_file_key: s3_file_key }))
        .then(res => window.open(res.publicUrl, '_blank'))
        .catch(err => setuploadError('Open Pdf Failed'))// eslint-disable-line
    })();
  }

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file)
      fileReader.onload = () => {
        resolve(fileReader.result);
      }
      fileReader.onerror = (error) => {
        reject(error);
      }
    })
  }

  const onUpload = async (files) => {
    let uploadLimit = files && files.length + inputValue.length <= max;
    if (uploadLimit) {
      setLoading(true);
      const promises = await files.map(async (file, i) => {// eslint-disable-line
        const Upload = uploadingAttachment;
        const fileName = file.name;
        const fileContentType = getContentType(fileName);
        let base64 = '';
        var reader = new FileReader();
        (async () => {
          // const file = file;
          base64 = await convertBase64(file)
        })();

        reader.readAsArrayBuffer(file);
        return await Promise.resolve(Upload(Object.assign({}, {
          "file_name": fileName,
          "path_identifier": uploadName,
          "content_type": fileContentType,
          "rfa_form_id": rfaFormId
        }))
          .then(async (result) => {
            if (result) {
              return await uploadFile(result.uploadURL, file, fileContentType).then(async () => {
                return await Object.assign({}, result, { base64: base64 }, { publicUrl: result.publicUrl }, { fileName: fileName });
              }).catch(err => setuploadError('Failed to upload files'))// eslint-disable-line
            }
          }))

      });

      Promise.all(promises).then((data) => {
        input.onChange(inputValue.concat(data));
        setLoading(false);
      });

    } else if (files) {
      setuploadError(`You can upload upto ${max} ${max <= 1 ? 'file' : 'files'}`);
    }


  }

  const onDrop = useCallback(async (uploadedFiles) => {

    const acceptedFiles = uploadedFiles.map(file => new File([file], file.name.replace(/[^a-zA-Z.0-9]/g, ""), { type: file.type, lastModified: file.lastModified }));

    await onUpload(acceptedFiles)
  }, [onUpload]);

  const onDropRejected = useCallback((error) => {
    if (error[0].errors[0].code == "file-invalid-type" && contentType == 'application/pdf') {
      setuploadError("File type must be pdf");
    } else {
      setuploadError(error[0].errors[0].message);
    }
  }, []);

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, open } = useDropzone({ accept: accept, multiple: true, onDrop, onDropRejected, noClick: true, noDrag: true });// eslint-disable-line


  const thumbs = inputValue.map((file, i) => {
    const file_name_default = file.fileName && file.fileName || file.s3_file_key.substring(file.s3_file_key.lastIndexOf('/') + 1, file.length);
    return (
      <Grid item key={i} className={classes.thumbnaile}>
        <Grid className={classes.thumb} key={file}>
          <span className={classes.remove}>
            <HighlightOffIcon style={{ fill: "#fb0404" }} onClick={(e) => {
              e.stopPropagation();
              input.onChange(inputValue.filter((f, key) => key !== i));
            }} />
          </span>
          <div className={classes.thumbInner} onClick={(e) => file.s3_file_key && openPdf(file.s3_file_key) || e.stopPropagation()}>
            {["image/png", "image/jpeg"].includes(getContentType(file.s3_file_key)) ? (
              <div>
                <img src={file.public_url} alt="No image" className={classes.img} />
              </div>
            ) : getContentType(file.s3_file_key) === "application/pdf" ? (
              <div>
                <img src={require('images/icons/pdf1.svg')} className={classes.img} />
              </div>

            ) : null}
          </div>
        </Grid>
        <div className={classes.fileNameDiv} ><span style={{ fontSize: '11px' }}>{(file_name_default.substring(0, file_name_default.lastIndexOf('.')).length > 4 ? `${file_name_default.substring(0, 3)}.. .pdf` : file_name_default)}</span></div>
      </Grid>
    );
  });

  return (
    <Grid container className={classes.root} onClick={open}>
      {thumbs.length != 0 ? <Grid className={classes.documentPreview}>
        <Grid style={{ outline: "none" }}>
          <aside className={classes.thumbsContainer}>{thumbs}</aside>
        </Grid>
      </Grid> : null}
      {thumbs.length < max ? <Grid className={classes.paper} style={thumbs.length == max ? { opacity: 0.5 } : {}}>
        <Grid className={classes.container}>

          <Grid className={classes.firstDropzone} style={{
            outline: "none", display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            width: '100%',
            height: 'auto',
            cursor: 'pointer'
          }} {...getRootProps({ className: 'dropzone' })}>
            <input style={{ display: 'none' }} {...getInputProps()} />
            {isDragAccept && <p>jpg, png, pdf only accepted</p>}
            {isDragReject && <p>Some files will be rejected</p>}
            <div style={{ textAlign: 'center' }}>
              {thumbs.length != max ? <img src={require('images/icons/upload.png')} className={classes.uploadimg} />
                : <img src={require('images/icons/cloud.svg.png')} className={classes.uploadimg} />}
            </div>
            <p style={{ marginTop: '0px', marginBottom: '0px', paddingTop: '0px' }} >{inputValue.length == 0 ? `You can upload upto ${max} ${max <= 1 ? 'file' : 'files'}` : `You can upload ${max - thumbs.length} more ${max - thumbs.length <= 1 ? 'file' : 'files'}`}</p>
            <Grid style={{ flexDirection: 'row', paddingBottom: '7px' }}>
              <span>{`Drag ${labelValue} to upload, or`}</span>
            </Grid>
            <div style={{ textAlign: 'center' }}>
              <Button type="button" variant="contained" color="primary" className={classes.btn1} style={thumbs.length != max ? { background: '#009cd2' } : { background: 'gray', cursor: 'not-allowed' }}>Select File
              </Button></div>
          </Grid>
        </Grid>

        {loading && <span className={classes.loader}><CircularProgress style={{ fill: "#2ca01c" }} /></span>}
      </Grid> : null}
      {error && <Grid item xs={12} className={classes.error}>
        {(error && <span>{error}</span>) || (warning && <span>{warning}</span>)}
      </Grid> || null}
      <Snackbar show={uploadError ? true : false} text={uploadError} severity={'error'} handleClose={() => setuploadError(false)} />
    </Grid>
  );
}

export default UploadField;

