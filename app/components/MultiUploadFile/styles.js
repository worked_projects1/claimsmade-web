import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  root: {
    '@global': {
      '.MuiTypography-h6': {
        fontWeight: 600
      }
    }
  },
  paper: {
    position: 'relative',
    height: 'auto',
    marginBottom: '10px',
    padding: '10px 8px',
    border: '1px dashed #b9bdc1cc',
    boxSizing: 'border-box',
    borderRadius: '3px',
    marginTop: '10px',
    flexBasis: '100%',
    background: '#f5f7f9cc'
  },
  container: {
    display: 'flex',
    justifyContent: 'flex-end',
    borderBottom: '0.1px lightgray',
    borderBottomStyle: 'insut'
  },
  left: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
 
  thumbsContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: '8px',
  },
  thumb: {
    display: 'flex',
    borderRadius: 3,
    marginBottom: 5,
    boxShadow: '0px 2px 9px -1px rgb(0 0 0 / 59%)',
    width: 59,
    height: 71,
    padding: 4,
    boxSizing: 'border-box',
  },
  thumbInner: {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden',
    cursor: 'pointer'
  },
  img: {
    display: 'block',
    width: '100%',
    height: '100%'
  },
  remove: {
    position: 'relative',
    top: '-16px',
    width: '0',
    height: '0',
    right: '-45px',
    cursor: 'pointer',
    '& svg': {
      width: '18px',
      height: '18px',
      background: '#fff',
      borderRadius: '50%'
    }
  },
  btnContinue: {
    width: '30vw',
    height: '38px',
    textTransform: 'none',
    fontSize: '14px',
    padding: '5px 20px',
    fontWeight: 'bold',
    backgroundColor: '#2ca01c',
    '&:hover': {
      background: '#2ca01c',
    },
  },
  documentPreview: {
    margin: '15px',
    boxSizing: 'border-box',
  },
  firstDropzone: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    height: 'auto',
    cursor:'pointer'
  },
  loader: {
    position: 'absolute',
    textAlign: 'center',
    left: '47%',
    top: '32%'
  },
  upload: {
    fill: '#2ca01c',
    cursor:'pointer'
  },
  error: {
      color: 'red',
      marginTop: '10px',
      marginBottom: '10px'
  },
  btn1: {
    height: '27px',
    lineHeight: '27px',
    paddingTop: '8px',
    background: '#009cd2',
    textTransform: 'capitalize',
  },
  button: {
    height: '35px',
    paddingTop: '9px',
    lineHeight: '35px',
    textTransform: 'capitalize'
  },
  fileNameDiv: {
    textAlign: 'center', width: '55px', lineHeight: '0.9 !important'
  },
  thumbnaile: {
    flexDirection: 'column', marginRight: '15px', marginTop: '5px'
  },
  uploadimg: {
    width: '100%',
    height: '75%',
  }
}));

export default useStyles;
