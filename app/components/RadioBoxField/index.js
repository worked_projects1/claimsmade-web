/**
 * 
 * Radio Box
 * 
 */

import React, { useEffect, useRef, useState } from 'react';
import Styles from './styles';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import { FormControlLabel, FormLabel } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import { Grid } from '@mui/material';


export default function ({ input, label, options, disabled, meta: { touched, error, warning }, defaultValue, submitButtonName }) {// eslint-disable-line
  const classes = Styles();

  const { name, value, } = input;// eslint-disable-line
  const isPreDefinedSet = Array.isArray(options);
  const componentMounted = useRef(true);
  const [selectedValue, setSelectedValue] = useState(defaultValue || value || "")

  useEffect(() => {
    if (defaultValue) {
      input.onChange(defaultValue)
    }
    return () => { // This code runs when component is unmounted
      componentMounted.current = false; // (4) set it to false when we leave the page
  }
  }, []);

  return (
    <Grid className={classes.checkboxField}>
      <FormControl component="fieldset">
        <FormLabel key="demo-customized-radios" name="sample" style={{ marginRight: label == "Entry" ? '0px' : '20px' }}>{label == "Entry" ? "" : label}</FormLabel>
        <RadioGroup
          row
          name={name}
          // value={value || ""}
          defaultValue={defaultValue}
          onChange={(e) => {
            setSelectedValue(e.target.value)
            input.onChange(e.target.value)}}>
          {isPreDefinedSet ? (options || []).map((opt, index) =>
            <FormControlLabel key={index} value={opt.value} name={opt.value} label={(opt.label).replace(/-/g, ' ').replace(/(?: |\b)(\w)/g, function (key) { return key.toUpperCase() })} control={<Radio checked={submitButtonName != undefined ? opt.value == defaultValue : opt.value == selectedValue}
              classes={{ root: classes.radio, checked: classes.checked }}
            />} disabled={disabled ? true : false} labelPlacement="end" className={classes.radioLabel} />
          ) : null}
        </RadioGroup>
      </FormControl>
      <div className={classes.error}>
        {touched && ((error && <span style={{ color: "red", fontSize: '14px' }}>{error}</span>) || (warning && <span style={{ color: "red", fontSize: '14px' }}>{warning}</span>))}
      </div>
    </Grid>
  );
}
