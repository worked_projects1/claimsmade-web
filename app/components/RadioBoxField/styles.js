
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  checkboxField: {
    width: '100%',
    '& .MuiFormControl-root': {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      '& .MuiFormLabel-root.Mui-focused': { color: 'black', },
      '& .MuiFormLabel-root': { color: 'black', marginRight: '20px' },
    },
  },
  radioLabel: {
    fontSize: '18px',
    marginRight: '14px!important',
    '&.MuiFormLabel-root.Mui-focused': { color: '#3c89c9', },
    '& span:first-of-type': {
      fontSize: '18px',
      color: '#3c89c9',
    },
    '& .MuiTypography-root': {
      marginTop: '4px'
    }
  },
}));


export default useStyles;