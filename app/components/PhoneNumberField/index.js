
/***
 *
 * Phone Number Field
 *
 */


import React from 'react';
import TextField from '@mui/material/TextField';
import Styles from './styles';
import { AsYouType } from 'libphonenumber-js';

export default function PhoneNumberField({ input, label, autoFocus, type, disabled, variant, className, errorStyle, meta: { touched, error, warning } }) {// eslint-disable-line

    const classes = Styles();
    const { name, value, onChange } = input;

    const phoneType = new AsYouType('US');

    return (
        <div>
            <TextField
                name={name}
                type={type}
                label={<span className={classes.textSize} >{label}</span>}
                className={className || classes.fieldColor}
                disabled={disabled}
                fullWidth
                variant={variant || "standard"}
                onChange={(e) => onChange(e.target.value && (e.target.value.replace(/[()\ \s-]+/g, '').length == 10) && phoneType.input(e.target.value) || e.target.value.replace(/[()\ \s-]+/g, '') || '')}// eslint-disable-line
                value={value && (value.replace(/[()\ \s-]+/g, '').length == 10) && phoneType.input(value) || value.replace(/[()\ \s-]+/g, '') || ''}// eslint-disable-line
                InputProps={{ classes: { input: classes.input } }}
                autoFocus={autoFocus} />
            <div style={errorStyle || {}} className={classes.error}>
                {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
            </div>
        </div>

    )
}
