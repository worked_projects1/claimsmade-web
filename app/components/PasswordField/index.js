/**
 * 
 * Password Field
 * 
 */

import React, { useState } from 'react';
import { InputAdornment, IconButton } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Styles from './styles';
import TextField from '@mui/material/TextField';

export default function ({ input, label, placeholder, autoFocus, disabled, style, variant, multiline, rows, className, errorStyle, defaultBlur, meta: { touched, error, warning } }) {// eslint-disable-line
  const classes = Styles();
  const { name, value, onChange } = input; ``
  const [showPassword, setshowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setshowPassword(!showPassword);
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const InputChange = defaultBlur ? Object.assign({}, {
    onBlur: (e) => onChange(e.target.value),
    defaultValue: value || ''
  }) : Object.assign({}, {
    onChange: (e) => onChange(e.target.value),
    value: value || ''
  });

  return (
    <div style={style || {}}>
      <TextField
        name={name}
        type={showPassword ? 'text' : 'password'}
        label={<span className={classes.textSize} >{label}</span>}
        placeholder={placeholder}
        disabled={disabled}
        className={className || classes.fieldColor}
        fullWidth
        variant={variant || "standard"}
        {...InputChange}
        InputProps={{ // <-- This is where the toggle button is added.
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
          classes: { input: classes.input }
        }}
        autoFocus={autoFocus}
        multiline={multiline || false}
        rows={rows || 1} />
      <div style={errorStyle || {}} className={classes.error}>
        {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
      </div>
    </div>
  )

}