/**
 * 
 *  DialogForm
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Grid, Typography, Button, Dialog, DialogContent, DialogTitle, Divider, DialogActions, Snackbar, Alert } from '@mui/material';//eslint-disable-line
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@mui/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { normalize } from 'utils/tools';
import Styles from './styles';
import Spinner from 'components/Spinner';


function DialogForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, fields, metaData, btnLabel, onOpen, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, enableSubmitBtn, disableCancelBtn, progress, footerBtn, confirmPopUpClose, enableScroll, page, loading, form, physicianDetailChange, initialValues, formError, closeAlert, user, AdminstratorDetailChange, updateField, record } = props;// eslint-disable-line

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [fileLoad, setFileLoad] = useState(false);
    const [lastValue, setLastValue] = useState("");
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));// eslint-disable-line
    let errorMessage = "";
    if (typeof (formError) == "string") {
        errorMessage = formError
    }
    else {
        if (formError && !formError.request_type && !formError.claims_admin_id && user && user.role == "staff" && !formError.assignee_id) {
            errorMessage = "Required: Request Type, Physician Assigned, Claims Administrator Assigned"
        }
        else if (formError && !formError.request_type && !formError.claims_admin_id && !formError.signature && user && user.role != "staff") {
            errorMessage = "Required: Request Type, Claims Administrator Assigned, Signature"
        }
        else if (formError && !formError.request_type && !formError.signature && user && user.role != "staff") {
            errorMessage = "Required: Request Type, Signature";
        } else if (formError && !formError.request_type && user && user.role == "staff" && !formError.assignee_id) {
            errorMessage = "Required: Request Type, Physician Assigned";
        }
        else if (formError && !formError.request_type && !formError.claims_admin_id) {
            errorMessage = "Required: Request Type, Claims Administrator Assigned"
        }
        else if (formError && !formError.assignee_id && !formError.claims_admin_id && user && user.role == "staff") {
            errorMessage = "Required: Physician Assigned, Claims Administrator Assigned"
        }
        else if (formError && !formError.claims_admin_id && !formError.signature && user && user.role != "staff") {
            errorMessage = "Required: Claims Administrator Assigned, Signature"
        }

        else if (formError) {
            errorMessage = !formError.claims_admin_id && `Required: Claims Administrator Assigned
         ` || !formError.request_type && `Required: Request Type` || user && user.role != "staff" && (!formError.signature && `Required: Signature`) || user && user.role == "staff" && !formError.assignee_id && `Required: Physician Assigned` || '';
        }
    }

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        if(record && record.attachments){
                if(record.attachments.cover_page.length != 0){
                    setLastValue("cover_page");
                }
                if(record.attachments.clinical_notes.length != 0){
                    setLastValue("clinical_notes");
                }
                if(record.attachments.imaging_reports.length != 0){
                    setLastValue("imaging_reports");
                }
                if(record.attachments.supporting_docs.length != 0){
                    setLastValue("supporting_docs");
                }
                if(record.attachments.non_confirming_claims.length != 0){
                    setLastValue("non_confirming_claims");
                }
        }
        if (initialValues && initialValues.claims_admin_id) {
            AdminstratorDetailChange(initialValues.claims_admin_id);
            updateField("dialogForm", "claim_admin_details", initialValues.claim_admin_details)
        }
        if (user && user.role == "staff" && initialValues && initialValues.assignee_id) {
            physicianDetailChange(initialValues.assignee_id);
        }
        return () => destroy();
    }, []);

    return <Dialog
        open={showModal || show || false}
        //   maxWidth={"md"}
        sx={{ '& .MuiDialog-paper': { width: 850, height: '550px', maxHeight: '570px' } }}
        maxWidth="md"
        // scroll={"body"}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle  >
            <Grid container justifyContent="space-between" className={classes.header}>
                <Grid item >
                    <span dangerouslySetInnerHTML={{ __html: title }} className={classes.title} />
                </Grid>
                <Grid item style={{ textAlign: 'end', alignSelf: 'center' }}>
                    {<CloseIcon onClick={closeModal} className={classes.closeIcon} />}
                </Grid>
            </Grid>
        </DialogTitle>
        <Divider />
        <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
            {fileLoad ? <Grid className={classes.spinGrid}><Spinner className={classes.spinner} /></Grid> : null}
            <DialogContent dividers className={classes.dialog}>

                {message ?
                    <Grid className={classes.messageGrid}>
                        <Typography component="span" className={classes.message}>{message || ''}</Typography>
                    </Grid> : null}
                {messageRegular ?
                    <Grid className={classes.messageGrid}>
                        <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                    </Grid> : null}
                <Grid container className={enableScroll ? enableScroll : null}>
                    <Grid item xs={12} className={classes.body}>
                        <Grid container spacing={3}>
                            {(fields || []).map((field, index) => {
                                const InputComponent = ImplementationFor[field.type];
                                return (field.visible ? <Grid key={index} item xs={12} style={{ paddingTop: '15px' }}>
                                    {field.nodeValue && field.visible ? <h3 style={{ margin: '0px', fontSize: '18px' }}>{field.nodeValue}</h3> : null}
                                    {field.type !== 'fieldArray' ?
                                        <Field
                                            name={field.value}
                                            label={field.label}
                                            type="text"
                                            metaData={metaData}
                                            component={InputComponent}
                                            required={field.required}
                                            normalize={normalize(field)}
                                            onChange={field.value == 'assignee_id' ? (e) => physicianDetailChange(e) : field.value == 'claims_admin_id' ? (e) => AdminstratorDetailChange(e) : null}
                                            disabled={field.disableOptons && field.disableOptons.edit}
                                            fileloading={(e) => setFileLoad(e)}
                                            lastValue={lastValue}
                                            {...field} /> :
                                        <FieldArray
                                            name={field.value}
                                            label={field.label}
                                            type="text"
                                            defaultValue={initialValues.claim_admin_details}
                                            fieldArray={field.fieldArray}
                                            metaData={metaData}
                                            component={InputComponent}
                                            required={field.required}
                                            normalize={normalize(field)}
                                            ImplementationFor={ImplementationFor}
                                            disabled={field.disableOptons && field.disableOptons.edit}
                                            {...field} />}
                                </Grid> : null)
                            })}
                        </Grid>
                    </Grid>
                    {error ? <Grid item xs={12} className={classes.error}> <Error errorMessage={error} /></Grid> : null}
                </Grid>
                {notes ? <Grid >
                    <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                </Grid> : null}
                {/* <Divider /> */}


            </DialogContent>
            <Divider />

            {errorMessage != "" ?
                <Snackbar
                    open={errorMessage != "" ? true : false}
                    sx={{ position: 'fixed', width: '60%', top: '95%' }}
                    anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "center"
                    }}
                    autoHideDuration={3000}
                    onClose={closeAlert}
                >
                    <Alert elevation={6} variant="filled" severity={'error'}>
                        {errorMessage}
                    </Alert>
                </Snackbar> : null}

            <DialogActions>

                <Grid container justifyContent="flex-end" style={footerStyle} className={classes.footer}>
                    {footerBtn && React.createElement(footerBtn) || null}
                    {confirmButton ?
                        <AlertDialog
                            description={confirmMessage}
                            onConfirm={() => handleSubmit()}
                            onConfirmPopUpClose={confirmPopUpClose}
                            btnLabel1='Yes'
                            btnLabel2='No' >
                            {(open) => <Button
                                type="button"
                                variant="contained"
                                onClick={open}
                                disabled={page && page === 'casesEdit' ? (pristine || submitting) : !enableSubmitBtn && (pristine || invalid)}
                                color="primary"
                                className={classes.button}>
                                {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                            </Button>}
                        </AlertDialog> :
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            onClick={onSubmitClose ? closeModal : null}>
                            {(submitting || progress || loading) && <ButtonSpinner /> || btnLabel || 'submit'}
                        </Button>
                    }
                    {!disableCancelBtn ? <Button
                        type="button"
                        variant="contained"
                        onClick={closeModal}
                        className={classes.button}>
                        Cancel
                    </Button> : true}
                </Grid>
            </DialogActions>
        </form>
    </Dialog>
}

DialogForm.propTypes = {
    title: PropTypes.string,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'dialogForm',
    validate,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(DialogForm);