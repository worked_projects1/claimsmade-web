import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();// eslint-disable-line
    return ({
        header:{
            fontSize: '17px'
        },
        dialog: {
            border: 'none',
            padding: '25px',
            minWidth: '100%',
            outline: 'none',
            width: '100%',
            overflowY: 'hidden'
        },
        body: {
            padding: '10px'
        },
        footer: {
            marginRight: '15px'
        },
        button: {
            fontWeight: 'bold',
            borderRadius: '4px',
            boxShadow: 'none',
            height: '35px',
            fontFamily: 'MyriadPro-Regular',
            margin: '5px',
            outline: 'none',
            textTransform: 'capitalize',
            marginRight: '12px',
            color: '#fff',
            paddingTop: '8px'
        },
        title: {
            fontFamily: 'MyriadPro-Bold',
            fontSize: '20px',
        },
        message: {
            fontFamily: 'MyriadPro-SemiBold',
            fontSize: '16px',
            paddingTop: '18px',
        },
        messageRegular: {
            fontSize: '16px',
            paddingTop: '18px',
        },
        closeIcon: {
            cursor: 'pointer'
        },
        messageGrid: {
            paddingTop: '10px'
        },
        error: {
            marginTop: '10px'
        },
        note: {
            fontSize: '16px',
        },
        errorMessage: {
            padding: '15px 35px 0px 35px'
        },
        spinGrid: {
            height: "500px",
            zIndex: "1400",
            background: "#dddddd4f",
            width: "100%",
            display: "flex",
            position: "absolute",
            alignItems: "center"
        }
    })
});

export default useStyles;