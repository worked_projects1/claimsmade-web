/**
 * 
 * Icons
 * 
 */

import React from 'react';
import GroupIcon from '@mui/icons-material/Group';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import DashboardIcon from '@mui/icons-material/Dashboard';
import PortraitIcon from '@mui/icons-material/Portrait';
import FilterNoneIcon from '@mui/icons-material/FilterNone';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import UpdateIcon from '@mui/icons-material/Update';
import DeviceHubIcon from '@mui/icons-material/DeviceHub';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import SchoolIcon from '@mui/icons-material/School';
import CategoryIcon from '@mui/icons-material/Category';
import MonetizationOnOutlinedIcon from '@mui/icons-material/MonetizationOnOutlined';
import SubdirectoryArrowRightIcon from '@mui/icons-material/SubdirectoryArrowRight';
import SettingsIcon from '@mui/icons-material/Settings';
import PlayCircleFilledOutlinedIcon from '@mui/icons-material/PlayCircleFilledOutlined';
import ChromeReaderModeIcon from '@mui/icons-material/ChromeReaderMode';
import LocalHospitalIcon from '@mui/icons-material/LocalHospital';
import ContactSupportIcon from '@mui/icons-material/ContactSupport';
import WbIncandescentIcon from '@mui/icons-material/WbIncandescent';
import ChevronRightRoundedIcon from '@mui/icons-material/ChevronRightRounded';
import ChevronLeftRoundedIcon from '@mui/icons-material/ChevronLeftRounded';
import PermPhoneMsgIcon from '@mui/icons-material/PermPhoneMsg';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import DescriptionIcon from '@mui/icons-material/Description';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjects';
import EditIcon from '@mui/icons-material/Edit';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import SearchIcon from '@mui/icons-material/Search';
import DomainIcon from '@mui/icons-material/Domain';
import SecurityIcon from '@mui/icons-material/Security';
import PersonIcon from '@mui/icons-material/Person';
import EnhancedEncryptionIcon from '@mui/icons-material/EnhancedEncryption';
import MedicalInformationOutlinedIcon from '@mui/icons-material/MedicalInformationOutlined';
import VaccinesOutlinedIcon from '@mui/icons-material/VaccinesOutlined';
import BallotOutlinedIcon from '@mui/icons-material/BallotOutlined';
import FactCheckOutlinedIcon from '@mui/icons-material/FactCheckOutlined';
import FontAwesome from 'react-fontawesome';
import { FaMedkit } from 'react-icons/fa';
import BusinessCenterOutlinedIcon from '@mui/icons-material/BusinessCenterOutlined';
import DomainVerificationIcon from '@mui/icons-material/DomainVerification';


/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function Icons(props) {

    switch (props.type) {// eslint-disable-line
        case 'Users':
            return <PersonIcon {...props} />
        case 'insurance':
            return <EnhancedEncryptionIcon {...props} />
        case 'medical':
            return <MedicalInformationOutlinedIcon {...props} />
        case 'poc1':
            return <BallotOutlinedIcon {...props} />
        case 'poc2':
            return <FactCheckOutlinedIcon {...props} />
        case 'poc3':
            return <BusinessCenterOutlinedIcon {...props} />
        case 'rfaForms':
            return <FaMedkit name='suitcase-medical' style={{color: 'white', padding: '3px', fontSize: '24px' }} {...props} />
        case 'Treatment':
            return <VaccinesOutlinedIcon {...props} />
        case 'Diagnosis':
            return <FontAwesome name='stethoscope' style={{color: 'white', padding: '3px', fontSize: '22px' }} {...props}/>
        case 'claims':
            return <DomainVerificationIcon {...props}/>
        case 'Group':
            return <GroupIcon {...props} />
        case 'ShoppingCart':
            return <ShoppingCartIcon {...props} />
        case 'Business':
            return <BusinessCenterIcon {...props} />
        case 'Portrait':
            return <PortraitIcon {...props} />
        case 'FilterNone':
            return <FilterNoneIcon {...props} />
        case 'PeopleAlt':
            return <PeopleAltIcon {...props} />
        case 'Update':
            return <UpdateIcon {...props} />
        case 'DeviceHub':
            return <DeviceHubIcon {...props} />
        case 'ShowChart':
            return <ShowChartIcon {...props} />
        case 'School':
            return <SchoolIcon {...props} />
        case 'Dollar':
            return <MonetizationOnOutlinedIcon {...props} />
        case 'Category':
            return <CategoryIcon {...props} />
        case 'ArrowRight':
            return <SubdirectoryArrowRightIcon {...props} />
        case 'SettingsIcon':
            return <SettingsIcon {...props} />
        case 'PlayCircleFilledOutlinedIcon':
            return <PlayCircleFilledOutlinedIcon {...props} />
        case 'ChromeReaderModeIcon':
            return <ChromeReaderModeIcon {...props} />
        case 'LocalHospitalIcon':
            return <LocalHospitalIcon {...props} />
        case 'ContactSupportIcon':
            return <ContactSupportIcon {...props} />
        case 'WbIncandescentIcon':
            return <WbIncandescentIcon {...props} />
        case 'ChevronRightIcon':
            return <ChevronRightRoundedIcon {...props} />
        case 'ChevronLeftIcon':
            return <ChevronLeftRoundedIcon {...props} />
        case 'PermPhoneMsgIcon':
            return <PermPhoneMsgIcon {...props} />
        case 'AccountCircleIcon':
            return <AccountCircleIcon {...props} />
        case 'Description':
            return <DescriptionIcon {...props} />
        case 'LockOpen':
            return <LockOpenIcon {...props} />
        case 'Edit':
            return <EditIcon {...props} />
        case 'Info':
            return <InfoOutlinedIcon {...props} />
        case 'Emoji':
            return <EmojiObjectsIcon {...props} />
        case 'Back':
            return <ArrowBackIcon {...props}/>
        case 'Duplicate':
            return <FileCopyIcon {...props}/>
        case 'Search':
            return <SearchIcon {...props}/>
        case 'State':
            return <DomainIcon {...props}/>
        case 'FormOtp':
            return <SecurityIcon {...props}/>
        default:
            return <DashboardIcon {...props} />
    }
}