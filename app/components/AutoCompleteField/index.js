/**
 * 
 * Auto Complete Field
 * 
 */


 import React from 'react';
 import Autocomplete from '@mui/material/Autocomplete';
 import Styles from './styles';
 import { FormControl, TextField } from '@mui/material';
 
 export default function ({ input, label, placeholder, metaData, options, disabled, style, meta: { touched, error, warning } }) {// eslint-disable-line
     const classes = Styles();
 
     const { id, name, value, onChange } = input;// eslint-disable-line
     const isPreDefinedSet = Array.isArray(options);
     const Options = isPreDefinedSet ? options : metaData[options] || [];
 
     return (
         <div className={classes.selectField} style={style || {}}>
             <FormControl className={classes.formControl}>
                 <Autocomplete
                     id={id}
                     name={name}
                     options={Options}
                     getOptionLabel={(option) => option.label || ''}
                     value={value && Options.find(_ => _.value.toString() === value.toString()) || null}
                     autoComplete
                     includeInputInList
                     disabled={disabled}
                     onChange={(e, target) => target && target.value && onChange(target.value) || value}
                     renderInput={(params) => (
                         <TextField
                             {...params}
                             variant="standard"
                             className={classes.fieldColor}
                             label={label}
                             placeholder={placeholder}
                         />
                     )}
                 />
             </FormControl>
             <div className={classes.error}>{touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}</div>
         </div>
     )
 }
 
 