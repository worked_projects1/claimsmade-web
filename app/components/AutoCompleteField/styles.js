

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  formControl: {
    width: '100%'
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    fontSize: '14px',
    '& label.Mui-focused': {
      color: '#1d1e1c',
    },
    '& label.Mui-visited': {
      color: '#1d1e1c',
    },
    '&.MuiFormLabel-root.Mui-focused': {
      borderColor: '#ccebf6',
      color: '#1d1e1c'
    }
  },
  selectField: {
    '& .MuiInput-underline:after': {
      borderBottom: '1px solid #ccebf6',
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#ccebf6 !important',
    },
    '& .MuiInputLabel-root': {
      color: '#1d1e1c',
      fontSize: '14px',
    },
    '@global': {
      '.MuiOutlinedInput-notchedOutline': {
        border: 'none'
      }
    }
  },
}));


export default useStyles;