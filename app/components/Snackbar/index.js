
/*
 * 
 * Snack bar 
 * 
 */


import React from 'react';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';


export default function SnackBar({ severity, show, text, handleClose }) {// eslint-disable-line

    return (<div>
        <Snackbar open={show} autoHideDuration={2000} sx={{ position: 'fixed', width: '100%' }} anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }} onClose={handleClose}>
            {show ? <Alert elevation={6} variant="filled" onClose={handleClose} severity={severity}>
                {text}
            </Alert> : null}
        </Snackbar>
    </div>)
}



