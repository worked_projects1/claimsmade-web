

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line

    btnClear: {
        opacity: "0.5",
        color: 'rgba(0, 0, 0, 0.87) !important',
        background: '#e0e0e0',
        borderRadius: '3px',
        fontFamily: 'MyriadPro-Regular',
        height: '35px',
        lineHeight: '35px'
    },
    error: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'red'
    },
    signatureImg: {
        width: '40%',
        height: '40%',
    }, 
    signature: {
        backgroundColor: "rgb(128 128 128 / 38%)", 
        // marginBottom: '10px'
    },
    signLabel: {
        marginRight: '0px'
    },
    radioLabel: {
        fontSize: '18px',
        marginRight: '14px!important',
        '&.MuiFormLabel-root.Mui-focused': { color: '#3c89c9', },
        '& span:first-of-type': {
          fontSize: '18px',
          color: '#3c89c9',
        },
        '& .MuiRadio-root': {
            padding: '3px 9px'
        },
        '& .MuiTypography-root': {
          marginTop: '4px',
          marginLeft: '4px'
        }
    },
    sigCanvas: {
        width: '100%',
        height: '100%'
    },
    radioGroup:{
        padding: '0px 12px'
    }

}));


export default useStyles;
