/**
 * 
 * Custom Signature Form
 * 
 */
import React, { useState, useEffect } from 'react';
import { Grid, Typography, FormControlLabel, Button, FormControl, RadioGroup, Radio } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import SignatureCanvas from 'react-signature-canvas';
import ClearIcon from '@mui/icons-material/Clear';
import Styles from './styles';


export default function CustomSignatureForm({ input, signature }) {// eslint-disable-line

    const classes = Styles()
    const signPad = React.createRef();
    const [width, setWidth] = useState(window.innerWidth);
    const [signValue, setSignValue] = useState('user_signature');
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    useEffect(() => {
        let mounted = true;// eslint-disable-line
        signature && input.onChange(signature);
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);


    const handleChange = (event) => {
        setSignValue(event.target.value);
        if (event.target.value === 'user_signature') {
            input.onChange(signature)
        } else if (event.target.value === 'custom_signature') {
            input.onChange('')
        }
    };

    const handleClear = () => {
        signPad.current.clear();
        input.onChange('');
    }

    return (<Grid container style={{marginTop: '5px'}}>
        {signature ? <Grid item xs={12}>
            <FormControl>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue="user_signature"
                    name="radio-buttons-group"
                    value={signValue}
                    onChange={handleChange}>

                    <Grid container alignItems="center" className={classes.radioGroup}>
                        <Grid item xs={12}>
                            <FormControlLabel value="user_signature" className={classes.radioLabel} control={<Radio />} label={<img className={classes.signatureImg} src={signature} alt='No signature' />} />
                        </Grid>
                    </Grid>
                    <Grid container style={{ marginTop: '3px' }} className={classes.radioGroup}>
                        <FormControlLabel
                            value="custom_signature"
                            className={classes.radioLabel}
                            control={<Radio />}
                            label="Use custom signature" />
                    </Grid>
                </RadioGroup>
            </FormControl>
            {signValue === 'custom_signature' ? <Grid item style={{ color: "#464444" }} className={classes.radioGroup}>
                <Grid container style={{ marginBottom: '12px' }} direction="row" justifyContent="space-between" >
                    <Grid style={{ display: 'inline-grid', maxWidth: '570px', alignContent: 'end' }}>
                        <Typography variant="inherit">Draw your signature here</Typography>
                    </Grid>
                    <Grid>
                        <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                    </Grid>
                </Grid>
                <Grid item xs={12} className={classes.signature}>
                    <SignatureCanvas penColor='black'
                        canvasProps={{ width: !sm ? width - 80 : 745, height: 200, className: 'signCanvas' }}
                        ref={signPad}
                        onEnd={(e) => {// eslint-disable-line
                            input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                        }} />
                </Grid>
            </Grid> : null}
        </Grid> :
            <Grid container className={classes.radioGroup}>
                <Grid item xs={12} style={{ color: "#464444", marginBottom: '12px' }}>
                    <Grid container direction="row" justifyContent={"space-between"}>
                        <Grid style={{ display: 'inline-grid', maxWidth: '570px', alignContent: 'end' }}>
                            <Typography variant="inherit">Draw your signature here</Typography>
                        </Grid>
                        <Grid>
                            <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} className={classes.signature}>
                    <SignatureCanvas penColor='black'
                        canvasProps={{ width: !sm ? width - 80 : 755, height: 200, className: 'signCanvas' }}
                        ref={signPad}
                        onEnd={(e) => {// eslint-disable-line
                            input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                        }} />
                </Grid>
            </Grid>}
    </Grid>)
}
