/**
 * 
 * Field Array
 * 
 */

import React, { useEffect } from 'react';
import { Field } from 'redux-form';
import { Grid } from '@mui/material';
import Styles from './styles';
import { normalize } from 'utils/tools';



function FieldArray(props) { // eslint-disable-line

    const { metaData, fields, fieldArray, ImplementationFor, defaultField } = props; // eslint-disable-line
    const classes = Styles();
    useEffect(() => {
        if(fields && fields.length <= 0 && defaultField) {
            fields.push({})
        }
    }, []);

    return (
        <Grid>
            {fields ? fields.map((el, index) => {
                return <Grid className={classes.container} key={index}>
                    <Grid container className={classes.field}>
                        {(fieldArray || []).map((field, index) => {
                            const InputComponent = ImplementationFor[field.type];
                            return <Grid key={index} item xs={5}  style={field.style || null} className={classes.fieldItem}>
                                <Field
                                    name={`${el}.${field.value}`}
                                    label={field.label}
                                    type={field.type}
                                    metaData={metaData}
                                    component={InputComponent}
                                    normalize={normalize(field)}
                                    disabled={field.disableOptons && field.disableOptons.create}
                                    {...field} />
                            </Grid>
                        })}
                    </Grid>   
                </Grid>
            }): null}
        </Grid>
    )
}

export default FieldArray;
