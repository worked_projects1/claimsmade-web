import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({ // eslint-disable-line
    container: {
        border: '1px solid #eaeaea',
        padding: '24px 10px',
        margin: '8px',
    },
    remove: {
        position: 'relative',
        top: '-18px',
        left: '-13px',
        width: '0',
        height: '0',
        cursor: 'pointer'
    },
    error: {
        color: 'red',
        marginTop: '10px',
        marginBottom: '10px'
    },
    field: {
        justifyContent: "space-between",
    },
    fieldItem: {
        margin: '5px'
    }
  
}));

export default useStyles;
