
/**
 * 
 * Spinner 
 * 
 */
/* eslint-disable */
import React from 'react';
import ScaleLoader from "react-spinners/ScaleLoader";
import Grid from '@mui/material/Grid';

/**
 * 
 * @param {object} props 
 * @returns 
 */
export default function (props) {

    return (<Grid container style={props.style} className={props.className}>
        {props && props.showHeight && <Grid item xs={12} style={{ height: '280px' }} /> || ''}
        <Grid item xs={12} style={{ textAlign: 'center' }}>
            <ScaleLoader
                size={100}
                height={25}
                color={"#00ca1f"}
                loading={true}
            />
        </Grid>
    </Grid>)
}