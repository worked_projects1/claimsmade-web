

/**
 * 
 * Copyright
 * 
 */

import React from 'react';

import Typography from '@mui/material/Typography';

class Copyright extends React.Component {
  render() {
    const { textColor } = this.props;// eslint-disable-line
    
    return (<Typography variant="body2" align="center" style={{ color: 'rgb(127, 127, 127)' || '#000' }}>
        {'© '}  {new Date().getFullYear()}
        {'  VettedClaims. All rights reserved'}

        {'.'}
      </Typography>);
  }
}


export default Copyright;