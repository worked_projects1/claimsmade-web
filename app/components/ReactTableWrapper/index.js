/**
 *
 * ReactTableWrapper
 *
 */

import React, { Component } from 'react';
import Table from './Table';
import PropTypes from 'prop-types';
import { Grid, Button } from '@mui/material';
import { diffString } from 'json-diff';
import AlertDialog from '../AlertDialog';

class ReactTableWrapper extends Component {

    static propTypes = {
        records: PropTypes.array,
        columns: PropTypes.array,
        children: PropTypes.bool,
        path: PropTypes.string,
        locationState: PropTypes.object,
        view: PropTypes.bool
    };


    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    render() {
        const { name, records, columns, children, path, locationState = {}, view, sm, metaData = {}, totalPageCount, onChangeData, headersData, loading, openFile, filterNeed, openDialog, authorize } = this.props;// eslint-disable-line

        const activePath = location.pathname;
        const tableColumns = children ? columns.filter((column) => column.visible && column.viewMode) : columns.filter((column) => column.visible);

        let rows = records.map((record) => Object.assign(
            {},
            record, {
            onClickHandler: view ? this.handleClick.bind(this, record, name, locationState, openFile) : null,
            isActive: activePath === `${path}/${record.id}` || activePath === `${path}/${record.id}/edit`,
        }),
        );

        tableColumns.forEach((column) => {
            switch (column.type) {
                case 'jsonData':
                    rows = rows.map((row) => {
                        return Object.assign(
                            row,
                            {
                                [column.value]:
                                    column.html ? column.html(row, metaData) :
                                        <Grid container>
                                            {row["oldJson"] != undefined && row["newJson"] != undefined ?
                                                <Grid item xs={12}>
                                                    <Grid style={{ height: "150px", overflowY: "scroll", backgroundColor: '#fbfbfb' }}>
                                                        {((diffString(row["oldJson"], row["newJson"])).replace(/(\[32m)/g, "").replace(/(\[31m)/g, "").replace(/(\[39m)/g, "") || '').split(/\n/).map((line, index) => <div key={index}>{line}</div>)}
                                                    </Grid>
                                                    <Grid><Button onClick={() => openDialog(row["oldJson"], row["newJson"])} style={{ textTransform: "capitalize" }}>View details</Button>
                                                    </Grid>
                                                </Grid> : null}
                                        </Grid>
                                ,
                            },
                        )
                    });
                    break;
                case 'upload':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) :
                                    <Grid>
                                        <img
                                            src={`${row[column.value] || ''}`}
                                            role="presentation" style={{ height: 64, padding: 4 }}
                                        />
                                    </Grid>,
                        },
                    ));
                    break;
                case 'download':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value]
                        },
                    ));
                    break;
                case 'checkbox':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                column.html ? column.html(row, metaData) : row[column.value] && 'Yes' || 'No',
                        },
                    ));
                    break;
                case 'select':
                case 'multiSelect':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: column.html ? column.html(row, metaData) : row[column.value],
                        },
                    ));
                    break;
                case 'button':
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]: row.role == "Staff" ?
                                <AlertDialog
                                    description={!row.staff_id ? 'By authorizing, this user will be able to create RFA forms on your behalf with your signature. Are you sure you want to grant this signature authorization?' : 'Are you sure you want to revoke the signature authorization for this staff?'}
                                    onConfirm={() => authorize(row)
                                    }
                                    onConfirmPopUpClose={true}
                                    btnLabel1='YES'
                                    btnLabel2='NO' >
                                    {(open) => <Button variant="outlined"
                                        style={{
                                            textTransform: "none",
                                            fontFamily: "MyriadPro-Regular",
                                            width: "85px",
                                            height: "30px",
                                            color: 'black',
                                            borderColor: 'black',
                                            paddingTop: '8px'
                                        }}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            open();
                                        }}>
                                        {row.staff_id ? 'Revoke' : 'Authorize'}
                                    </Button>}
                                </AlertDialog>
                                : null
                        }
                    ))
                    break;
                default:
                    rows = rows.map((row) => Object.assign(
                        row,
                        {
                            [column.value]:
                                    column.html ? column.value == 'policyDoc' ? <Button onClick={(e) => {
                                        e.stopPropagation(); openFile(row)
                                    }}>{column.html(row, metaData)}</Button> : column.html(row, metaData) : column.limit && row[column.value] && row[column.value].length > 60 ? `${row[column.value].toString().substring(0, 60)}...` : row[column.value] != null ? row[column.value] : '' || '',
                        },
                    ));
                    break;
            }
        });

        return (
            <Grid container>
                <Grid item xs={12} className={sm ? name : null}>
                    <Table
                        columns={tableColumns.map((column) => Object.assign(
                            {},
                            {
                                Header: column.sort ? column.label + ' ⇅' : column.label,
                                accessor: column.value,
                                sort: column.sort,
                                sortType: 'basic',
                                sortColumn: column.sortColumn
                            }
                        ))}
                        data={rows}
                        totalPageCount={totalPageCount || rows.length}
                        onChangeData={onChangeData}
                        headersData={headersData}
                        loading={loading}
                        filterNeed={filterNeed}
                    />
                </Grid>
            </Grid>
        );
    }

    handleClick(record, name, data, openFile, el) {
        if (name == "diagnosis") {
            openFile(record)
        } else {
            const { id } = record || {};
            const targetId = el && el.target && el.target.id || false;
            const { path, history, locationState, schemaId } = this.props;// eslint-disable-line
            if ((id && !targetId) || (id && targetId && targetId !== id))
                history.push({
                    pathname: ['/rfaForms', '/rest/medical-history-summery'].includes(path) ? `${path}/${id}/poc3` : ['/diagnosis'].includes(path) ? `${path}` : `${path}/${id}`,
                    state: ['/rfaForms'].includes(path) ? Object.assign({}, { ...locationState }, { id, caseRecord: record }) : schemaId ? Object.assign({}, { ...locationState }, { id, schemaId }) : ['/diagnosis'].includes(path) ? Object.assign({}, { ...locationState }) : Object.assign({}, { ...locationState }, { id })
                });
        }
    }

}

export default ReactTableWrapper;
