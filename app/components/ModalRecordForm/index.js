/**
 * 
 *  Modal Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Modal, Paper, Fade, Grid, Typography, Button } from '@mui/material';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@mui/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { normalize } from 'utils/tools';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalRecordForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, children, fields, metaData, className, style, btnLabel, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, footerBtn, confirmPopUpClose, paperClassName, enableScroll, page, loading } = props;// eslint-disable-line
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return <Grid container={disableContainer ? false : true} className={className} style={style} id="modal_record">
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={!md ? { width: '90%' } : {}}>
                    <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="space-between">
                            <Grid item xs={10}>
                                <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            </Grid>
                            <Grid item xs={2} style={{ textAlign: 'end' }}>
                                {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                            </Grid>
                        </Grid>
                        {message ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{message || ''}</Typography>
                            </Grid> : null}
                        {messageRegular ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                            </Grid> : null}
                        <Grid container className={classes.body}>
                            <Grid item xs={12} className={enableScroll ? enableScroll : null}>
                                <Grid container spacing={3}>
                                    {(fields || []).map((field, index) => {
                                        const InputComponent = ImplementationFor[field.type];
                                        return <Grid key={index} item xs={12}>
                                            {field.type !== 'fieldArray' ?
                                                <Field
                                                    name={field.value}
                                                    label={field.label}
                                                    type="text"
                                                    metaData={metaData}
                                                    component={InputComponent}
                                                    required={field.required}
                                                    normalize={normalize(field)}
                                                    disabled={field.disableOptons && field.disableOptons.edit}
                                                    {...field} /> :
                                                <FieldArray
                                                    name={field.value}
                                                    label={field.label}
                                                    type="text"
                                                    fieldArray={field.fieldArray}
                                                    metaData={metaData}
                                                    component={InputComponent}
                                                    required={field.required}
                                                    normalize={normalize(field)}
                                                    ImplementationFor={ImplementationFor}
                                                    disabled={field.disableOptons && field.disableOptons.edit}
                                                    {...field} />}
                                        </Grid>
                                    })}
                                </Grid>
                            </Grid>
                            {error ? <Grid item xs={12} className={classes.error}> <Error errorMessage={error} /></Grid> : null}
                        </Grid>
                        {notes ? <Grid >
                            <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                        </Grid> : null}
                        <Grid container justifyContent="flex-end" style={footerStyle} >
                            {footerBtn && React.createElement(footerBtn) || null}
                            {confirmButton ?
                                <AlertDialog
                                    description={confirmMessage}
                                    onConfirm={() => handleSubmit()}
                                    onConfirmPopUpClose={confirmPopUpClose}
                                    btnLabel1='Yes'
                                    btnLabel2='No' >
                                    {(open) => <Button
                                        type="button"
                                        variant="contained"
                                        onClick={open}
                                        disabled={page && page === 'casesEdit' ? (pristine || submitting) : !enableSubmitBtn && (pristine || invalid)}
                                        color="primary"
                                        className={classes.button}>
                                        {(submitting || progress) && <ButtonSpinner /> || btnLabel || 'submit'}
                                    </Button>}
                                </AlertDialog> :
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={page && page === 'casesEdit' ? (pristine || invalid) : !enableSubmitBtn && (pristine || invalid) || (pristine || invalid)}
                                    className={classes.button}
                                    style={{marginRight: '15px'}}
                                    onClick={!invalid && onSubmitClose ? closeModal : null}>
                                    {(submitting || progress || loading) && <ButtonSpinner /> || btnLabel || 'submit'}
                                </Button>
                            }
                            {!disableCancelBtn ? <Button
                                type="button"
                                variant="contained"
                                onClick={closeModal}
                                className={classes.cancel}>
                                Cancel
                            </Button> : true}
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'modalRecord',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(ModalRecordForm);