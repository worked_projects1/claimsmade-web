

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '25%'
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '4px',
        boxShadow: 'none',
        height: '35px',
        fontFamily: 'MyriadPro-Regular',
        // marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        // marginRight: '15px',
        color: '#fff',
        paddingTop: '8px'
    },
    cancel: {
        fontWeight: 'bold',
        borderRadius: '4px',
        boxShadow: 'none',
        height: '35px',
        fontFamily: 'MyriadPro-Regular',
        // marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        color: '#fff',
        paddingTop: '8px'
    },
    title: {
        fontFamily: 'MyriadPro-Bold',
        fontSize: '22px',
    },
    message: {
        fontFamily: 'MyriadPro-SemiBold',
        fontSize: '16px',
        paddingTop: '18px',
    },
    messageRegular: {
        fontSize: '16px',
        paddingTop: '18px',
    },
    closeIcon: {
        cursor: 'pointer'
    },
    messageGrid: {
        paddingTop: '10px'
    },
    error: {
        marginTop: '10px'
    },
    note: {
        fontSize: '16px',
    },
}));


export default useStyles;