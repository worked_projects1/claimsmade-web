

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    root: {
      height: '100vh',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(4),
      flex: 1,
    },
    submit: {
      margin: theme.spacing(6, 0, 2),
      backgroundColor: '#9dbcdc',
      textTransform: 'none',
      height: '37px',
      "&:hover": {
        //you want this to be the same as the backgroundColor above
        backgroundColor: '#9dbcdc',
      },
      fontFamily: 'MyriadPro-Regular',
      color: '#fff',
      paddingTop: '8px'
    },
    div: {
      textAlign: 'center',
      marginTop: theme.spacing(1.3),
    },
    linkColor: {
      color: '#009cd2',
      textDecoration: 'none'
    }
  })
});


export default useStyles;