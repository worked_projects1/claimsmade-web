/***
 *
 * Switch Field
 *
 */

import React from 'react';
import Switch from '@mui/material/Switch';
import Styles from './styles';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import FormControl from '@mui/material/FormControl';

export default function ({ input, label, style, errorStyle, meta: { touched, error, warning } }) {// eslint-disable-line

  const { value, onChange } = input;
  const classes = Styles();

  return (<div style={style || {}}>
    <FormControl component="fieldset" variant="standard">
      <FormGroup>
        <FormControlLabel
          control={
            <Switch
              checked={value || false}
              onChange={e => onChange(e.target.checked)}
              color="primary"
              inputProps={{ "aria-label": "primary checkbox" }}
              classes={{
                // track: classes.switch_track,
                thumb: classes.switch,
                // colorPrimary: classes.switch_primary,
              }}
            />
          }
          label={label}
        />
      </FormGroup>
    </FormControl>
    <div style={errorStyle || {}} className={classes.error}>
      {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  </div>);
}
