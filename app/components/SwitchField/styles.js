import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  error: {
    fontSize: '14px',
    color: 'red',
    marginTop: '5px'
  },
  switch: {
    color: 'inherit',
    "& span": {
      ". MuiSwitch-thumb": {
        color:'inherit',
      },
    }
  },
}));


export default useStyles;
