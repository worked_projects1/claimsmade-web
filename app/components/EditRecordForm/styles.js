

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();
    return ({
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(4)
        },
        forms: {
            width: '100%',
            marginTop: '5px'
        },
        submitBtn: {
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            lineHeight: '35px',
            height: '35px',
            marginRight: '15px',
            paddingTop: '10px'
        },
        cancelBtn: {
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            lineHeight: '35px',
            height: '35px',
            backgroundColor: 'gray !important',
            paddingTop: '10px'
        },
        footer: {
            display: 'flex',
            marginTop: '20px',
        }
    })
});


export default useStyles;