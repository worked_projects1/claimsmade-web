/**
 * 
 * Edit Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { Grid, Button } from '@mui/material';
import Styles from './styles';
import validate from 'utils/validation'
import ButtonSpinner from 'components/ButtonSpinner';
import AlertDialog from 'components/AlertDialog';
import Error from '../Error';
import { normalize } from 'utils/tools';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function editRecordForm(props) {

    const classes = Styles();
    const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, confirmButton, confirmMessage, btnLabel, invalid, destroy, spinner, cancelBtn, form, name } = props;// eslint-disable-line
    const forms = ['serving_pos_date', 'opposing_counsel', 'serving_attorney'];

    useEffect(() => {
        return () => destroy();
    }, []);

    return (<div>
        <form onSubmit={handleSubmit} className={forms.includes(form) ? classes.forms : classes.form} noValidate >
            <Grid container spacing={3} name="editRecordForm">
                {(fields || []).map((field, index) => {
                    const InputComponent = ImplementationFor[field.type];
                    return <Grid key={index} item xs={12}>
                        {field.type !== 'fieldArray' ?
                            <Field
                                name={field.value}
                                label={field.label}
                                type="text"
                                metaData={metaData}
                                component={InputComponent}
                                required={field.required}
                                disabled={field.disableOptons && field.disableOptons.edit}
                                normalize={normalize(field)}
                                {...field} /> :
                            <FieldArray
                                name={field.value}
                                label={field.label}
                                type="text"
                                fieldArray={field.fieldArray}
                                metaData={metaData}
                                component={InputComponent}
                                required={field.required}
                                normalize={normalize(field)}
                                ImplementationFor={ImplementationFor}
                                disabled={field.disableOptons && field.disableOptons.edit}
                                {...field} />}
                    </Grid>
                })}
                {error ? <Grid item xs={12}>
                    {error && <Error errorMessage={error} /> || ''}
                </Grid> : null}
            </Grid>
            <Grid className={classes.footer}>
                {confirmButton ? <AlertDialog
                    description={confirmMessage}
                    onConfirm={() => handleSubmit()}
                    onConfirmPopUpClose={true}
                    btnLabel1='Yes'
                    btnLabel2='No' >
                    {(open) => <Button
                        type="button"
                        disabled={form === 'opposing_counsel' ? (pristine || submitting) : pristine || submitting || (!pristine && invalid)}
                        variant="contained"
                        onClick={!invalid ? open : null}
                        color="primary"
                        className={classes.submitBtn}>
                        {(submitting || spinner) && <ButtonSpinner /> || btnLabel || 'submit'}
                    </Button>}
                </AlertDialog> :
                    <Button
                        type="submit"
                        disabled={submitting || pristine || invalid}
                        variant="contained"
                        color="primary"
                        className={classes.submitBtn}>
                        {submitting ? <ButtonSpinner /> : name == 'treatment' ? 'Upload' : 'Update'}
                    </Button>}
                {cancelBtn && cancelBtn ||
                    <Link to={path && locationState && { pathname: path, state: { ...locationState } } || null}>
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            className={classes.cancelBtn}>
                            Cancel
                        </Button>
                    </Link>}
            </Grid>
        </form>
    </div>)

}


export default reduxForm({
    form: 'EditRecord',
    enableReinitialize: true,
    validate,
    touchOnChange: true,
})(editRecordForm);