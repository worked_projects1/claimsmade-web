/**
 * 
 * Success 
 * 
 */

import React, { useEffect } from 'react';
import Alert from '@mui/lab/Alert';

const errorMessage = ({ style, successMessage, onClose, name }) => {// eslint-disable-line

    useEffect(() => {
        if (onClose) {
            {name && name == "ForgetPassword" ?  setTimeout(onClose,3000) : setTimeout(onClose,  4000)}
        }
    }, []);

    return (
        <div style={style || {}}>
            {successMessage != null ? <Alert severity="success" variant="filled">{successMessage}</Alert> : null}
        </div>

    );
}

export default errorMessage;