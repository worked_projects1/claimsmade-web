

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(4),
      flex: 1
    },
    submit: {
      margin: theme.spacing(6, 0, 2),
      transition: '0.5s',
      backgroundSize: '100% auto',
      textTransform: 'none',
      height: '37px',
      fontFamily: 'MyriadPro-Regular',
      color: '#fff',
      paddingTop: '8px'
    },
    marginTopMedium: {
      fontFamily: 'MyriadPro-Regular',
      marginTop: theme.spacing(4)
    },
    div: {
      textAlign: 'center',
      marginTop: theme.spacing(1.3)
    },
    lockIcon: {
      width: '20px',
      height: '20px',
      marginRight: '4px',
    },
    linkColor: {
      color: '#009cd2',
      textDecoration: 'none'
    },
    check: {
      '& div': {
        '& label': {
          textAlign: 'center'
        }
      }
    }
  })
});


export default useStyles;