import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
    container: {
        paddingLeft: '10px'
    },
    signupBtn: {
        backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
        marginTop: '15px',
        boxShadow: 'none',
        borderRadius: 'initial',
        textTransform: 'none',
        color: '#ffff !important',
        minWidth: '100px',
        fontFamily: 'MyriadPro-Regular',
        height: '40px'
    },
    linkColor: {
        color: '#0077c5',
        textDecoration: 'none',
        paddingLeft: '4px'
    },
}));


export default useStyles;
