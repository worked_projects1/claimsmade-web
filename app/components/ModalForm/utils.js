
const CryptoJS = require("crypto-js");

export const getPolicyJson = (recordsArray, policyArray, currentJson, compare) => {
    if (compare) {
        const copyRecordsArray = recordsArray.map(({ oldJson, newJson, jsonSchema, ...rest }) => ({ ...rest })).sort((a, b) => a['updated_at'].localeCompare(b['updated_at'])).reverse();// eslint-disable-line
        (policyArray || []).map((field, index) => {// eslint-disable-line
            if (copyRecordsArray[index + 1]) {
                let firstBytes = CryptoJS.AES.decrypt((copyRecordsArray[index])["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
                let firstSecret = firstBytes.toString(CryptoJS.enc.Utf8);
                let firstSchema = JSON.parse(firstSecret);
                let secondBytes = CryptoJS.AES.decrypt((copyRecordsArray[index + 1])["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
                let secondSecret = secondBytes.toString(CryptoJS.enc.Utf8);
                let secondSchema = JSON.parse(secondSecret);
                copyRecordsArray[index].oldJson = secondSchema
                copyRecordsArray[index].newJson = firstSchema
            }
        })
        return copyRecordsArray
    } else {
        (policyArray || []).map((field, index) => {// eslint-disable-line
            let bytes = CryptoJS.AES.decrypt(currentJson, 'K6*^)&b=087&H%K!s2A0');
            let secret = bytes.toString(CryptoJS.enc.Utf8);
            let schema = JSON.parse(secret);// eslint-disable-line
            if (recordsArray.length != 0) {
                if (recordsArray[index + 1]) {
                    recordsArray[index].oldJson = policyArray[index + 1].value
                    recordsArray[index].newJson = policyArray[index].value
                }
            }
        })
        return recordsArray
    }
}