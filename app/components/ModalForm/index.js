/**
 * 
 *  Modal Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import { Grid, Typography, Dialog, DialogContent, DialogTitle, Button } from '@mui/material';
import { useTheme } from '@mui/styles';
import { RecordsData } from 'utils/tools';
import Styles from './styles';
import TableWrapper from 'components/ReactTableWrapper';
import { FormControl, MenuItem, TextField } from '@mui/material';
import moment from 'moment';
const CryptoJS = require("crypto-js");
import { getPolicyJson } from './utils';
import ReactJsonViewCompare from 'react-json-view-compare';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function ModalForm(props) {
    const { title, fields, className, style, onClose, show, disableCancelBtn, enableScroll, loading, records, currentJson } = props;// eslint-disable-line

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [compare, setCompare] = useState(false);
    const [firstTreatmentDate, setFirstTreatmentDate] = useState([]);
    const [secTreatmentDate, setSecTreatmentDate] = useState([]);
    const [initialOption, setInitialOption] = useState([]);
    const [source, setSource] = useState("");
    const [target, setTarget] = useState("");
    const [firstDate, setFirstDate] = useState("Source");
    const [secondDate, setSecondDate] = useState("Target");
    const [modalRecord, setModalRecord] = useState([]);
    const [docSchema, setDocSchema] = useState([]);
    const [oldJson, setOldJson] = useState([]);
    const [newJson, setNewJson] = useState([]);
    const theme = useTheme();

    useEffect(() => {
        const treatmentOptions = [];
        const policyDocSchemas = [];
        setModalRecord(records);
        {
            (records || []).map((field, index) => {// eslint-disable-line
                const bytes = CryptoJS.AES.decrypt(field["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
                const secret = bytes.toString(CryptoJS.enc.Utf8);
                const schema = JSON.parse(secret);
                policyDocSchemas.push({ value: schema });
                treatmentOptions.push({ value: field.updated_at, label: moment(field.updated_at).format('MM/DD/YY HH:mm:ss') });
                setFirstTreatmentDate(treatmentOptions);
                setSecTreatmentDate(treatmentOptions);
                setInitialOption(treatmentOptions);
                setDocSchema(policyDocSchemas);
            })
        }
    }, []);

    const closeModal = () => {
        if (onClose)
            onClose();
    }

    const closeDialog = () => {
        setModalOpen(false);
    }

    const openDialog = (oldJs, newJs) => {
        setOldJson(oldJs);
        setNewJson(newJs);
        setModalOpen(true);
    }

    const comparing = () => {
        if (moment(firstDate).isValid() && moment(secondDate).isValid()) {
            setSource(firstDate);
            setTarget(secondDate)
            setCompare(true);
        } else {
            setFirstTreatmentDate(initialOption);
            setSecTreatmentDate(initialOption);
            setCompare(false);
        }
    }

    const clear = () => {
        setFirstDate("Source");
        setSecondDate("Target");
        setFirstTreatmentDate(initialOption);
        setSecTreatmentDate(initialOption);
        setCompare(false);
    }

    const handleFilter = (e, name) => {
        const treatmentOptions = [];
        if (name == "first") {
            setFirstDate(e.target.value)
        } else if (name == "second") {
            setSecondDate(e.target.value)
        }

        if (moment(e.target.value).isValid()) {
            (modalRecord || []).filter((field) => field.updated_at != e.target.value).map((e1) => {
                treatmentOptions.push({ value: e1.updated_at, label: moment(e1.updated_at).format('MM/DD/YY HH:mm:ss') });
                if (name == "first") {
                    setSecTreatmentDate(treatmentOptions)
                } else if (name == "second") {
                    setFirstTreatmentDate(treatmentOptions)
                }
            })
        }
    }

    return <Grid container name="containerForm" className={className} style={style}>
        <Grid>
            <Dialog
                open={show}
                //   maxWidth={"md"}
                sx={{ '& .MuiDialog-paper': { [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                maxWidth="md"
                scroll={"body"}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                        </Grid>
                    </Grid>
                </DialogTitle>
                <DialogContent dividers>
                    <Grid container name={"ModalForm"}>
                        {/* <Grid item xs={12}>
                        <Grid container > */}
                        <Grid item sm={10} xs={12}>
                            <Grid container className={classes.filterContainer}>
                                <Grid className={classes.selectField} >
                                    <FormControl variant="standard" sx={{ m: 0, maxWidth: 220 }} className={classes.formControl}>
                                        <TextField
                                            select
                                            label=" "
                                            size="small"
                                            variant="outlined"
                                            value={firstDate}
                                            defaultValue={firstDate}
                                            margin="normal"
                                            inputProps={{ className: classes.fieldColor }}
                                            InputLabelProps={{ shrink: false }}
                                            // hiddenLabel="true"
                                            name={"name"}
                                            onChange={(e) => handleFilter(e, "first")}
                                            classes={{
                                                select: classes.select
                                            }}
                                            className={classes.fieldColor}
                                        >
                                            {(([{ label: 'Source', value: 'Source' }, ...firstTreatmentDate.sort((a, b) => a['label'].localeCompare(b['label']))] || []).map((field, index) => (
                                                <MenuItem
                                                    key={index}
                                                    label={field['label']}
                                                    value={field['value']}
                                                >
                                                    {field['label']}
                                                </MenuItem>
                                            )))}
                                        </TextField>
                                    </FormControl>
                                </Grid>
                                <Grid className={classes.selectField} style={{ marginLeft: '10px' }}>
                                    <FormControl variant="standard" sx={{ m: 0, maxWidth: 220 }} className={classes.formControl}>
                                        <TextField
                                            select
                                            label=" "
                                            size="small"
                                            // notched
                                            placeholder='Please Select'
                                            id="filled-hidden-label-small"
                                            variant="outlined"
                                            value={secondDate}
                                            defaultValue={secondDate}
                                            margin="normal"
                                            inputProps={{ className: classes.fieldColor }}
                                            InputLabelProps={{ shrink: false }}
                                            // hiddenLabel="true"
                                            name={"name"}
                                            onChange={(e) => handleFilter(e, "second")}
                                            classes={{
                                                select: classes.select
                                            }}
                                            className={classes.fieldColor}
                                        >
                                            {(([{ label: 'Target', value: 'Target' }, ...secTreatmentDate.sort((a, b) => a['label'].localeCompare(b['label']))] || []).map((field, index) => (
                                                <MenuItem
                                                    key={index}
                                                    label={field['label']}
                                                    value={field['value']}
                                                >
                                                    {field['label']}
                                                </MenuItem>
                                            )))}
                                        </TextField>
                                    </FormControl>
                                </Grid>
                                <Grid className={classes.selectField} style={{ marginLeft: '10px', paddingTop: '10px' }}>
                                    {/* <Grid item xs={12}> */}
                                    <Button className={classes.btn} onClick={() => comparing()}>Compare</Button>
                                    {/* </Grid> */}
                                    {/* <Grid item xs={12}> */}
                                    <Button className={classes.btn} style={{ marginLeft: '10px' }} onClick={() => clear()}>Reset</Button>
                                    {/* </Grid> */}
                                </Grid>
                            </Grid>
                        </Grid>
                        {/* </Grid>
            </Grid> */}
                        <Grid container >
                            <Grid item xs={12} className={enableScroll ? enableScroll : null}>
                                <TableWrapper
                                    columns={fields}
                                    records={loading ? RecordsData : compare ? getPolicyJson(modalRecord.filter((e) => e["updated_at"] == source || e["updated_at"] == target), modalRecord.filter((e) => e["updated_at"] == source || e["updated_at"] == target), currentJson, compare) : getPolicyJson(modalRecord, docSchema, currentJson, compare)}
                                    filterNeed={false}
                                    openDialog={(oldJs, newJs) => openDialog(oldJs, newJs)}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent >
            </Dialog>
        </Grid>

        <Grid>
            <Dialog
                open={showModal}
                //   maxWidth={"md"}
                sx={{ '& .MuiDialog-paper': { [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                maxWidth="md"
                scroll={"body"}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle>
                    <Grid container className={classes.header} justify="space-between">
                        <Grid item xs={10}>
                            <Typography component="span" className={classes.title}>{'Json Difference'}</Typography>
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                            {!disableCancelBtn ? <CloseIcon onClick={closeDialog} className={classes.closeIcon} /> : null}
                        </Grid>
                    </Grid>
                </DialogTitle>
                <DialogContent dividers>
                    <Grid>
                        <ReactJsonViewCompare oldData={oldJson} newData={newJson} />
                    </Grid>
                </DialogContent >
            </Dialog>
        </Grid>
    </Grid>
}

ModalForm.propTypes = {
    title: PropTypes.string,
    fields: PropTypes.array,
};

export default ModalForm;