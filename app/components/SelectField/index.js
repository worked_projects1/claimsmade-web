/**
 * 
 * Select Field
 * 
 */

import React from 'react';
import Styles from './styles';
import { FormControl, InputLabel, Select, MenuItem, ListSubheader, TextField, InputAdornment } from '@mui/material';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';


export default function ({ input, label, required, metaData, options, disabled, children, style, meta: { touched, error, warning }, defaultValue }) {// eslint-disable-line
    const classes = Styles();
    const { name, value } = input;
    const isPreDefinedSet = Array.isArray(options);
    
    const [sorting, setSorting] = React.useState(false);
    const [searchText, setSearchText] = React.useState("")// eslint-disable-line

    const changeValue = (e) => {
            input.onChange(e.target.value)
        }
        
    const containsText = (option, searchText) =>
        option.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

    const displayedOptions = React.useMemo(
        () => (name == 'claims_admin_id') && metaData[options] && metaData[options].filter((option) => containsText(option.label, searchText)) || [],
        [searchText]
    );

    return (
        <div className={classes.selectField} style={style || input.name == "assignee_id" && { paddingTop: '5px' } || {}}>
            {children || ''}
            <FormControl variant="standard" sx={{ m: 0, minWidth: 140 }} className={classes.formControl}>
                <InputLabel className={classes.fieldColor} id={`${name}-id`}>{label}</InputLabel>
                <Select
                    name={name}
                    fullWidth
                    disabled={disabled}
                    required={required}
                    MenuProps={{ autoFocus: false }}
                    labelId={`${name}-id`}
                    value={value || ''}
                    defaultValue={defaultValue}
                    classes={{
                        select: classes.select
                    }}
                    onClose={() => {
                        setSearchText("");
                        setSorting(false);
                    }}
                    onChange={(e) => changeValue(e)}>

                    {name == 'claims_admin_id' ? <ListSubheader>
                        <TextField
                            variant="standard"
                            autoFocus
                            sx={{ paddingTop: "3px" }}
                            placeholder="Type to search..."
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <ManageSearchIcon sx={{ color: '#bfbfbf' }} className={classes.search} />
                                    </InputAdornment>
                                )
                            }}
                            onClick={(e) => {
                                e.stopPropagation();
                                e.preventDefault();
                            }}
                            onChange={(e) => {
                                setSearchText(e.target.value);
                                setSorting(true);
                            }}
                            onKeyDown={(e) => {
                                if (e.key !== "Escape") {
                                    e.stopPropagation();
                                }
                            }}
                        />
                    </ListSubheader> : null}

                    {(typeof options === 'string' && metaData[options] === undefined) ? <MenuItem value="">No options</MenuItem> : isPreDefinedSet ? ((sorting && name == 'claims_admin_id') ? displayedOptions : options).map((opt, index) => <MenuItem
                        key={index} disabled={opt.disabled || false} value={opt && opt.value || opt}>{opt && opt.label || opt}</MenuItem>) : (typeof options === 'string' && Object.keys(metaData[options]).length === 0 || (typeof options === 'object' && sorting && displayedOptions.length == 0)) ? <MenuItem value="">No options</MenuItem> : (name == 'claims_admin_id' ? displayedOptions : metaData[options] || []).map((opt, index) => <MenuItem key={index} disabled={opt.disabled || false} value={opt && opt.value != null
                            && opt.value || opt}>{opt && opt.label != null && opt.label || opt}</MenuItem>)
                    }
                </Select>
            </FormControl>
            <div className={classes.error}>
                {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
            </div>
        </div>
    )
}

