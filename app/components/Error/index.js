/**
 * 
 * Error
 * 
 */

import React, { useEffect } from 'react';
import Alert from '@mui/material/Alert';

const errorMessage = ({ style, errorMessage, onClose ,name }) => {// eslint-disable-line
    useEffect(() => {
        if (onClose) {
            {name && name == "ForgetPassword" ?  setTimeout(onClose,  3000) : setTimeout(onClose,  4000)}
        }
    }, []);

    return (<div style={style || {}}>
            {errorMessage != null ? <Alert severity="error" variant="filled">{errorMessage}</Alert> : null}
        </div>);
}

export default errorMessage;