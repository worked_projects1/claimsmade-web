
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  fieldColor: {
    width: '100%',
    fontSize: '10px',
    '& :after': {
      borderBottomColor: '#9dbcdc',
      fontSize: '10px',
    },
    '& :before': {
      borderBottomColor: '#9dbcdc',
      fontSize: '10px',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#9dbcdc',
      fontSize: '10px',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#9dbcdc',
      fontSize: '10px',
    },
  },
  textSize: {
    fontSize: '14px',
  }
}));


export default useStyles;