/**
 * 
 * Create Record Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { ImplementationFor } from './utils';
import { Grid, Button } from '@mui/material';
import Styles from './styles';
import Error from '../Error';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import { normalize } from 'utils/tools';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function createRecordForm(props) {
  const classes = Styles();
  const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, destroy, invalid, submitButtonView, name } = props;// eslint-disable-line


  useEffect(() => {
    return () => destroy();
  }, []);


  return (<div>
    <form onSubmit={handleSubmit} className={classes.form} >
      <Grid container spacing={3}>
        {(fields || []).map((field, index) => {
          const InputComponent = ImplementationFor[field.type];
          return <Grid key={index} item xs={12} style={field.style || null}>
            <Field
              name={field.value}
              label={field.label}
              type={field.type}
              metaData={metaData}
              component={InputComponent}
              normalize={normalize(field)}
              disabled={field.disableOptons && field.disableOptons.create}
              {...field} />
          </Grid>
        })}
        {error ? <Grid item xs={12} className={classes.error}>
          {error && <Error errorMessage={error} /> || ''}
        </Grid> : null}
      </Grid>
      <Grid container className={classes.footer}>
        {submitButtonView ?
          React.createElement(submitButtonView, props) :
          <Button
            type="submit"
            disabled={pristine || submitting || invalid}
            variant="contained"
            color="primary"
            className={classes.submitBtn}>
            {submitting ? <ButtonSpinner /> : name == "treatment" ? 'Upload' : 'Create'}
          </Button>}
        <Link to={{ pathname: path, state: { ...locationState } }}>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className={classes.cancelBtn}>
            Cancel
          </Button>
        </Link>
      </Grid>
    </form>
  </div>)

}


export default reduxForm({
  form: 'createRecord',
  validate,
  touchOnChange: true
})(createRecordForm);