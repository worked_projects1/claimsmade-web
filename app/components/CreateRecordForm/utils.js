import InputField from 'components/InputField';
import CheckboxField from 'components/CheckboxField';
import SelectField from 'components/SelectField';
import DatePicker from 'components/DatePicker';
import FileUpload from 'components/FileUpload';
import PasswordField from 'components/PasswordField';
import RadioBoxField from 'components/RadioBoxField';
import PhoneNumberField from 'components/PhoneNumberField';
import TextareaField from 'components/TextareaField';
import Signature from 'components/SignatureForm';
import CustomSignature from 'components/SelectSignatureForm';
import SwitchField from 'components/SwitchField';
import AutoCompleteField from 'components/AutoCompleteField';
import MultiCheckBox from 'components/MultiCheckBox';
import FieldArray from 'components/FieldArray';
import MultiUploadFile from 'components/MultiUploadFile';


export const ImplementationFor = {
    input: InputField,
    number: InputField,
    checkbox: CheckboxField,
    select: SelectField,
    date: DatePicker,
    upload: FileUpload,
    password: PasswordField,
    radio: RadioBoxField,
    phone: PhoneNumberField,
    textarea: TextareaField,
    signature: Signature,
    customSignature: CustomSignature,
    multicheck: MultiCheckBox,
    switch: SwitchField,
    autoComplete: AutoCompleteField,
    multiupload: MultiUploadFile,
    fieldArray: FieldArray
};