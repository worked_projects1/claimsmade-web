

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();
    return ({
        form: {
            width: '100%',
            marginTop: theme.spacing(4)
        },
        submitBtn: {
            height: '35px',
            lineHeight: '35px',
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            marginRight: '15px',
            paddingTop: '10px'
        },
        cancelBtn: {
            height: '35px',
            lineHeight: '35px',
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            backgroundColor: 'gray !important',
            paddingTop: '10px'
        },
        footer: {
            display: 'flex',
            marginTop: '20px',
        },
        error: {
            padding: '24px'
        }
    })
});


export default useStyles;