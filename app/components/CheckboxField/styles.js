

import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/material/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
    formControlLabel: {
        marginTop: useTheme().spacing(2),
        '& .MuiFormControlLabel-root.Mui-disabled': {
                opacity: 0.5
            }
    },
    textSize: {
        fontSize: '14px',
    },
    error: {
        fontSize: '14px',
        color: 'red'
    },
    signUpForm: {
        '& .MuiTypography-root': {
            paddingLeft: '5px',
        }
    },
    checkBox: {
        '&.Mui-disabled': {
            opacity: 0.5,
        }
    }
}));


export default useStyles;