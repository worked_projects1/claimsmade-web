
/**
 * 
 * Checkbox Field
 * 
 */

import React, { useEffect } from 'react';
import { FormControlLabel, Checkbox } from '@mui/material';
import Styles from './styles';

export default function ({ input, id, label, styles, note, warning, meta: { touched, error }, defaultValue, disabled, rfaFormRowLimit }) {// eslint-disable-line
    const { name } = input;
    const classes = Styles();
    const setStyles = label == 'Remember Me' ? classes.formControlLabel : null;
    const setFontSize = label != 'Remember Me' ? classes.textSize : null;

    useEffect(() => {
        if (input.value == "" && input.name == 'is_admin') {
            input.onChange(false)
        }
    }, [])

    return (<div>
        <FormControlLabel className={name == "privacy_policy" || "terms_of_use" ? classes.signUpForm : setStyles} // eslint-disable-line
            disabled={disabled}
            control={<Checkbox
                id={`${id}`}
                style={styles ? styles : { color: "#00ca1f" }}
                defaultChecked={defaultValue}
                checked={input.value || defaultValue || false}
                className={classes.checkBox}
                disabled={disabled}
                onChange={(e) => input.onChange(!rfaFormRowLimit && e.target.checked || false)} />}
            label={<span className={setFontSize} dangerouslySetInnerHTML={{ __html: label }} />} />
        {note && <div className={classes.textSize}>{note}</div> || null}
        <div className={classes.error}>
            {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
        </div>
    </div>)
}