
/***
 *
 * Input Field
 *
 */


import React from 'react';
import TextField from '@mui/material/TextField';
import Styles from './styles';

export default function ({ input, label, placeholder, autoFocus, type, disabled, style, variant, multiline, rows, className, errorStyle, defaultBlur, meta: { touched, error, warning } }) {// eslint-disable-line

  const classes = Styles();
  const { name, value, onChange } = input;

  const InputChange = defaultBlur ? Object.assign({}, {
    onBlur: (e) => onChange(e.target.value),
    defaultValue: value || ''
  }) : Object.assign({}, {
    onChange: (e) => onChange(e.target.value),
    value: value || ''
  });

  return (
    <div style={style || {}}>
      <TextField
        name={name}
        type={type}
        label={<span className={classes.textSize} >{label}</span>}
        placeholder={placeholder}
        disabled={disabled}
        className={className || classes.fieldColor}
        fullWidth
        variant={variant || "standard"}
        {...InputChange}
        InputProps={{ classes: { input: classes.input } }}
        autoFocus={autoFocus}
        multiline={multiline || false}
        rows={rows || 1} />
      <div style={errorStyle || {}} className={classes.error}>
        {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
      </div>
    </div>

  )
}
