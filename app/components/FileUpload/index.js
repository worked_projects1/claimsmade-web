/**
 * 
 *  File Upload
 * 
 */

import React, { useCallback, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import PublishIcon from '@mui/icons-material/Publish';
import DoneIcon from '@mui/icons-material/Done';
import styles from './styles';
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import ProgressProvider from 'components/ProgressProvider';
import 'react-circular-progressbar/dist/styles.css';
import { Grid, Button, Paper, Typography } from '@mui/material';
import Error from 'components/Error';
import { uploadDocument, getUploadUrl, uploadingAttachment } from 'blocks/poc2/remotes.js';
import { capitalizeFirstLetterOnly, capitalizeFirstWord } from 'utils/tools';
import { getContentType, FilesList } from './utils';
import Icons from 'components/Icons';
// import { cloneDeep } from '';

function FileUpload({ input, contentType = '*', label, upload, multiple, max, viewFiles, meta: { touched, error, warning }, errorType , uploadName, rfaFormId}) {// eslint-disable-line
    const [uploadFiles, setuploadFiles] = useState(false);
    const [uploadError, setuploadError] = useState(false);
    const [uploadPercentage, setPercentage] = useState(false);
    const [uploadedFile, setUploadedFile] = useState(1);
    const [totalFiles, setTotalFiles] = useState(false);

    const inputRef = useRef(false);
    const labelValue = label === 'MODIFIED TEMPLATE' || 'Custom Document Template' ? capitalizeFirstWord(label) : capitalizeFirstLetterOnly(label);// eslint-disable-line
    const accept = contentType;

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.readAsDataURL(file)
          fileReader.onload = () => {
            resolve(fileReader.result);
          }
          fileReader.onerror = (error) => {
            reject(error);
          }
        })
      }

    const onUpload = (files, index) => {
        if (!files || files.length <= index) {
            return false;
        }
        const fileName = files[index].name;
        const fileContentType = getContentType(fileName);
        const uploadingApi = input.name == 'logoFile' && getUploadUrl || uploadName && uploadingAttachment || uploadDocument
        var file_name = files[index].name;
        var treatment_name = files[index].name.substring(0, files[index].name.indexOf('.'));
        var reader = new FileReader();
        let base64 = '';
        (async () => {
            const file = files[index];
            base64 = await convertBase64(file)
        })();

        reader.readAsArrayBuffer(files[index]);

        const obj = (input.name == 'logoFile') && Object.assign({}, {
            "file_name": file_name,
            "content_type": (input.name == 'logoFile' ||  uploadName) && fileContentType || "application/json",
        }) ||  uploadName && Object.assign({}, {
            "file_name": file_name,
            "path_identifier": uploadName,
            "content_type": fileContentType,
            "rfa_form_id": rfaFormId
        }) || Object.assign({}, {
            "file_name": file_name,
            "treatment_name": treatment_name,
            "content_type": (input.name == 'logoFile' ||  uploadName) && fileContentType || "application/json",
            "body_system": "neck-upper-back"
        }) 

        uploadingApi(obj).then((result) => {
            const { uploadURL, s3_file_key, public_url } = result;
            if (uploadURL != undefined) {
                const ajax = new XMLHttpRequest();
                ajax.upload.addEventListener("progress", (e) => setPercentage(Math.round((e.loaded / e.total) * 100)), false);
                ajax.addEventListener("load", () => {
                    const inputVal = input.name == 'logoFile' && public_url || uploadName && Object.assign({}, { "s3_file_key": s3_file_key, base64: base64 }) || Object.assign({}, { "s3_file_key": s3_file_key });
                    if (index === 0) {
                        inputRef.current = [inputVal];
                    } else {
                        inputRef.current.push(inputVal);
                    }
                    if ((files.length - 1) === index) {
                        input.onChange(multiple ? inputRef.current : inputRef.current[0]);
                        inputRef.current = false;
                        setUploadedFile(1);
                        setPercentage(false);
                        setTotalFiles(false);
                    } else {
                        setUploadedFile(index + 2);
                        setPercentage(false);
                        onUpload(files, index + 1);
                    }
                }, false);
                ajax.addEventListener("error", () => setuploadError('Upload Failed'), false);
                ajax.addEventListener("abort", () => setuploadError('Upload Failed'), false);
                ajax.open("PUT", uploadURL, true);
                ajax.setRequestHeader("Content-Type", (input.name == 'logoFile' ||  uploadName) && fileContentType || "application/json");
                ajax.send(files[index]);
            } else {
                setuploadError('Upload Failed');
            }
        })

    }

    const onDrop = useCallback((uploadedFiles) => {
        const acceptedFiles = uploadedFiles.map(file => new File([file], file.name.replace(/[^a-zA-Z.0-9]/g, ""), { type: file.type, lastModified: file.lastModified }));

        setuploadFiles(false);
        setuploadError(false);
        setPercentage(false);
        setUploadedFile(0);
        setTotalFiles(false);
        input.onChange(false);
        inputRef.current = false;

        if (max && acceptedFiles && max < acceptedFiles.length) {
            errorType ? setuploadError(`You cannot upload more than ${max} summary files`) : setuploadError(`If you need to upload more than ${max} files, do it in batches of upto ${max}`);
        } else if (upload) {
            inputRef.current = [];
            setTotalFiles(acceptedFiles.length);
            onUpload(acceptedFiles, 0);
        } else {
            setuploadFiles(acceptedFiles);
            setTimeout(() => input.onChange(acceptedFiles), 1000);
        }
    }, [setuploadFiles, onUpload, setuploadError, upload, input, uploadFiles]);

    const onDropRejected = useCallback((error) => {
        setuploadError(error[0].errors[0].message);
    }, []);

    const handleFileDelete = (fileIndex) => {
        const Files = input.value && input.value.length > 0 && input.value.filter((e, i) => i !== fileIndex) || [];
        if (Files && Array.isArray(Files) && Files.length > 0) {
            setuploadFiles(Files);
            input.onChange(Files);
        } else {
            setuploadFiles(false);
            input.onChange(false);
        }
    }

    const { getRootProps, open, getInputProps } = useDropzone({ onDrop, accept, multiple, onDropRejected });
    const classes = styles();

    return <Grid container style={{ width: '100%' }}>
        {input && input.value && contentType.indexOf('image') > -1 ?
            <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                <input {...getInputProps()} />
                <img src={input.value} className={classes.image} />
            </Grid> :
            <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                <input {...getInputProps()} />
                <Paper className={classes.upload}>
                    <ProgressProvider values={input.value ? [100] : uploadFiles && !uploadError ? [0, 50, 100] : [0]}>
                        {percentage => (<CircularProgressbarWithChildren
                            value={uploadPercentage || percentage}
                            className={classes.progress}
                            styles={buildStyles({
                                pathColor: '#3c89c9',
                                pathTransition:
                                    percentage === 0
                                        ? "none"
                                        : "stroke-dashoffset 0.5s ease 0s"
                            })}>
                            {(uploadPercentage || percentage) && !input.value ? <div className={classes.percentage}>{uploadPercentage || percentage}%</div> : input.value && upload ? <DoneIcon className={classes.icon} /> : <PublishIcon className={classes.icon} />}
                        </CircularProgressbarWithChildren>)}
                    </ProgressProvider>
                </Paper>
                <Grid className={classes.animate}>
                    <div className={classes.circle} style={{ animationDelay: '0s' }} />
                    <div className={classes.circle} style={{ animationDelay: '1s' }} />
                    <div className={classes.circle} style={{ animationDelay: '2s' }} />
                </Grid>
            </Grid>}
        <Grid item xs={12}>
            <Grid container className={classes.container} justify="center" direction="column">
                {!input.value ? <Typography component="span">{uploadPercentage || inputRef.current ? `Uploading ${uploadedFile} of ${totalFiles} ${label}` : `Drag ${label === 'files' ? 'files or folders' : label} to upload, or`}</Typography> : null}
                {!inputRef.current ?
                    <Typography component="span">
                        <Button type="button" variant="contained" color="primary" onClick={open} className={classes.button}>
                            {input.value && `Change ${labelValue}` || `Choose ${labelValue}`}
                        </Button>
                    </Typography> : null}
                {input.value && !upload && viewFiles ? <FilesList
                    style={{ justifyContent: 'center', marginTop: '18px' }}
                    onDelete={handleFileDelete}
                    files={input.value}>
                    {(openModal) =>
                        <Typography onClick={openModal} component="span" variant="subtitle2" className={classes.template}>
                            {`View ${labelValue} To Upload:`} <Icons type="Description" className={classes.description} />
                        </Typography>}
                </FilesList> : null}
            </Grid>
        </Grid>
        {(uploadError || (touched && (error || warning))) && !uploadFiles && !uploadPercentage && uploadedFile ? <Grid container className={classes.container}>
            <Error errorMessage={uploadError || error || warning} />
        </Grid> : null}
    </Grid>
}

FileUpload.propTypes = {
    children: PropTypes.func,
    accept: PropTypes.string,
};

export default FileUpload;