/**
 * 
 * Signature Form
 * 
 */


import React, { useState, useEffect } from 'react';
import { Grid, Typography, Button } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import SignatureCanvas from 'react-signature-canvas';
import ClearIcon from '@mui/icons-material/Clear';
import Styles from './styles';

export default function Signature({ input, showSignature, signature }) {// eslint-disable-line

    const classes = Styles();
    const signPad = React.createRef();
    const [width, setWidth] = useState(window.innerWidth);// eslint-disable-line
    const [sign, setSign] = useState(showSignature || false);
    const theme = useTheme();// eslint-disable-line
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    useEffect(() => {
        let mounted = true;// eslint-disable-line
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);
    const handleClear = () => {
        if (signPad.current != null) {
            signPad.current.clear();
            input.onChange('');
        } else if (sign) {
            setSign(false);
        }
    }

    return (
        <Grid container id="signature_form">
            <Grid item xs={12} style={{ color: "#464444", marginBottom: '12px' }}>
                <Grid container direction="row" justifyContent={!sign ? "space-between" : "flex-end"}>
                    {!sign ? <Grid style={{ display: 'inline-grid', maxWidth: '570px', alignItems: 'center' }}>
                        <Typography variant="inherit">Draw your signature here</Typography>
                    </Grid> : null}
                    <Grid>
                        <Button className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>Clear</Button>
                    </Grid>
                </Grid>
            </Grid>
            {sign && signature ? <Grid className={classes.signatureDiv}>
                <img src={signature} className={classes.signatureImg} alt='No signature' />
            </Grid> :
                <Grid item xs={12} className={classes.signature}>
                    <SignatureCanvas penColor='black'
                        canvasProps={{ width: !sm ? width - 80 : 460, height: 200, className: 'signCanvas' }}
                        ref={signPad}
                        onStart={() => setSign(false)}
                        onEnd={(e) => {// eslint-disable-line
                            input.onChange(signPad.current.getTrimmedCanvas().toDataURL('image/png'))
                        }} />
                </Grid>}
        </Grid>
    )
}
