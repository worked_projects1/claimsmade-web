

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line

    btnClear: {
        opacity: "0.5",
        color: 'rgba(0, 0, 0, 0.87) !important',
        background: '#e0e0e0',
        borderRadius: '3px',
        fontFamily: 'MyriadPro-Regular',
        height: '35px',
        lineHeight: '35px'
    },
    error: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'red'
    },
    signatureImg: {
        width: '60%',
        height: '50%',
    }, 
    signature: {
        backgroundColor: "rgb(128 128 128 / 38%)", 
        // marginBottom: '10px'
    },
    signatureDiv: {
        marginLeft: '25px',
        marginTop: '10px'
    }
}));


export default useStyles;
