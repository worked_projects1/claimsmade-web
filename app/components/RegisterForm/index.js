
/**
 * 
 * Register Form
 * 
 */


 import React, { useEffect } from 'react';
 import { Field, reduxForm } from 'redux-form';
 import { Link } from 'react-router-dom';
 import { Grid, Button, Typography } from '@mui/material';
 import InputField from 'components/InputField';
 import PasswordField from 'components/PasswordField';
 import ButtonSpinner from 'components/ButtonSpinner';
 import CheckboxField from 'components/CheckboxField';
 import Error from '../Error';
 import useStyles from './styles';
 
 
 function RegisterForm({ handleSubmit, submitting, errorMessage, clearCache, destroy, locationState = {}, iframe }) {// eslint-disable-line
     const classes = useStyles();
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     return (
         <form onSubmit={handleSubmit} className={classes.form} noValidate >
             <Grid container direction="row" justifyContent="center" alignItems="center" style={{ marginBottom: '50px' }}>
                <img src={require('images/home/vettedclaims_color.png')} style={{ width: '75%' }} />
            </Grid>
             <Grid container spacing={3}>
                 <Grid item xs={12}>
                     <Field name="practice_name" label="Practice Name" component={InputField} type="text" required autoFocus />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="name" label="Physician Name" component={InputField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="contact_name" label="Contact Name" component={InputField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="email" label="Email" component={InputField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="password" label="Password" component={PasswordField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="confirm_password" label="Confirm Password" component={PasswordField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="specialty" label="Speciality" component={InputField} type="text" required />
                 </Grid>
                 <Grid item xs={12}>
                     <Field name="privacy_policy" label={"I have read and accepted <a href='https://vettedclaims-policy.s3-us-west-1.amazonaws.com/privacy.html' style='color:#0077c5' target='_blank'>VettedClaims Privacy Policy</a>"} styles={{ color: "#0077c5", padding: '2px', alignSelf: 'flex-start', marginLeft: "9px" }} component={CheckboxField} type="checkbox" required />
                 </Grid>
                 <Grid item xs={12} style={{ paddingTop: '10px', paddingBottom: '10px' }}>
                     <Field name="terms_of_use" label="I have read and accepted <a href='https://vettedclaims-policy.s3-us-west-1.amazonaws.com/terms.html' style='color:#0077c5' target='_blank'>VettedClaims Terms of Use</a>" styles={{ color: "#0077c5" , padding: '2px', alignSelf: 'flex-start', marginLeft: "9px"}} component={CheckboxField} type="checkbox" required />
                 </Grid>
             </Grid>
             <Button
                 type="submit"
                 fullWidth
                 variant="contained"
                 color="primary"
                 className={classes.submit} >
                 {submitting && <ButtonSpinner /> || 'Register'}
             </Button>
             {errorMessage && <Error errorMessage={errorMessage} onClose={clearCache} /> || null}
             <Grid className={classes.div}>
                 <Typography variant="body2" className={classes.marginTopMedium}>
                     Already have an account?
                 </Typography>
                 <Grid item xs className={classes.signIn}>
                     <Link
                         to={{
                             pathname: iframe ? 'signin' : '/',
                             state: Object.assign({}, { ...locationState }, { form: 'login' })
                         }}
                         className={classes.linkColor}
                         onClick={clearCache}>
                         Sign In
                     </Link>
                 </Grid>
             </Grid>
         </form>
     )
 }
 
 const validate = (values) => {
 
     const errors = {}
 
     const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig
 
     const identical = /^(?!.*(.)\1\1.*).*$/igm
 
     const requiredFields = ['practice_name', 'name', 'contact_name', 'email', 'password', 'confirm_password', 'state_bar_number', 'role', 'privacy_policy', 'terms_of_use'];
     const commonNames = ["123456", "password", "123456789", "12345678", "12345",
         "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
         "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
         "aa123456", "donald", "password1", "qwerty123"
     ]
 
     requiredFields.forEach(field => {
         if (!values[field]) {
             errors[field] = 'Required'
         }
     })
 
     if (
         values.email &&
         !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
     ) {
         errors.email = 'Invalid Email'
     }
 
     if (values.password && values.password.length < 8) {
         errors.password = 'Password must be at least 8 characters'
     }
 
     if (values.password && values.password.length >= 8 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(values.password)) {
         errors.password = 'Must contain at least one numeric or special character '
     }
 
     if (values.password && sequence.test(values.password) || !identical.test(values.password)) {
         errors.password = 'Avoid consecutive sequential and identical characters'
     }
 
     if (values.state_bar_number && values.state_bar_number.length < 5) {
         errors.state_bar_number = 'State bar number should be at least 5 characters.'
     }
 
     commonNames.forEach(field => {
         if (values.password == field) {
             errors.password = "Password is easily guessable"
         }
     })
 
     if (values.password && values.confirm_password && values.confirm_password != values.password) {
         errors.confirm_password = 'Password Mismatch'
     }
 
     return errors
 }
 
 export default reduxForm({
     form: 'register',
     validate,
     touchOnChange: true,
 })(RegisterForm);