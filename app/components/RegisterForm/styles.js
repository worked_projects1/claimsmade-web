

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    root: {
      height: '100vh',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: `${theme.spacing(4)}`,
      flex: 1
    },
    submit: {
      margin: theme.spacing(1, 0, 1),
      textTransform: 'none',
      fontFamily: 'MyriadPro-Regular',
      color: '#fff',
      paddingTop: '7px',
      height:'37px',
    },
    marginTopMedium: {
      marginTop: theme.spacing(2),
    },
    div: {
      textAlign: 'center',
      marginTop: theme.spacing(1)
    },
    linkColor: {
      color: '#009cd2',
      textDecoration: 'none'
    },
    signIn: {
    }
  })
});


export default useStyles;