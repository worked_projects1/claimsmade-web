

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    height:'37px',
    backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
    textTransform: 'none',
    color: '#fff !important'
  }
})
});


export default useStyles;