

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({ // eslint-disable-line
    root: {
        marginTop: '12px', 
        marginBottom: '12px'
    },
    Button: {
        // fontWeight: 'bold',
        textTransform: 'capitalize',
        fontFamily: 'MyriadPro-Bold',
        backgroundColor: '#fff',
        boxShadow: 'none',
        color: '#fff',
        fontSize: '18px',
        // marginLeft: '10px',
        "&:hover": {
            backgroundColor: '#fff',
            boxShadow: 'none',
        },
        padding: '2px 0px',
        borderRadius: '3px'
    },
    label: {
        fontWeight: 'bold',
        fontFamily: 'MyriadPro-Regular',
        textTransform: 'uppercase',
        marginRight: '2em'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none'
    },
    value: {
        marginTop: '0px'
    },
    clear: {
        color: '#1976d2',
        marginRight: '2px',
        marginTop: '-6px'
    }
}));


export default useStyles;