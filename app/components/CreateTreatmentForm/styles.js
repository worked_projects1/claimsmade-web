

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();
    return ({
        form: {
            width: '100%',
            marginTop: theme.spacing(4),
        },
        submitBtn: {
            height: '35px',
            lineHeight: '35px',
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            marginTop: '20px',
            marginRight: '15px',
            paddingTop: '10px'
        },
        cancelBtn: {
            height: '35px',
            lineHeight: '35px',
            fontWeight: 'bold',
            borderRadius: '3px',
            fontFamily: 'MyriadPro-Regular',
            paddingLeft: '25px',
            paddingRight: '25px',
            marginTop: '20px',
            backgroundColor: 'gray !important',
            paddingTop: '10px'
        },
        footer: {
            display: 'flex',
        },
        fieldStyle: {
            paddingTop: '0px !important',
        }
    })
});


export default useStyles;