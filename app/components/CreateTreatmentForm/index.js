/**
 * 
 * Create Treatment Form
 * 
 */

import React, { useEffect } from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Grid, Button } from '@mui/material';
import Styles from './styles';
import ButtonSpinner from 'components/ButtonSpinner';
import { normalize } from 'utils/tools';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import { renderPolicy } from './Components/index';
import Error from '../Error';


/**
 * 
 * @param {object} props 
 * @returns 
 */

function CreateTreatmentForm(props) {
  const classes = Styles();
  const { handleSubmit, submitting, fields, path, error, metaData, locationState, destroy, submitButtonView, formValues, initialValues, submitButtonName, user, updateField } = props;// eslint-disable-line

  useEffect(() => {
    return () => destroy();
  }, []);


  return (<div>
    <form onSubmit={handleSubmit} className={classes.form} >
      <Grid container spacing={3}>
        {user.email != "siva@gmail.com" ? <Grid item xs={12} style={{ paddingTop: "0px" }}>
          <h3>{`${submitButtonName == "Create" ? "Create" : "Update"} Policy Doc`}</h3>
        </Grid> : null}
        {(fields || []).map((field, index) => {
          const InputComponent = ImplementationFor[field.type];

          return <Grid key={index} item xs={12} className={index == 0 && classes.fieldStyle || field.style}>
            {field.node && fields[index - 1].nodeValue != field.nodeValue ? <h3>{field.nodeValue}</h3> : null}
            <Field
              name={field.value}
              label={field.label == "Text" ? (formValues && formValues.Policies[index]) && formValues.Policies[index].source : field.label}
              type="text"
              metaData={metaData}
              component={InputComponent}
              required={field.required}
              disabled={field.disabled}
              normalize={normalize(field)}
              {...field} />

          </Grid>
        })}

        {formValues.form.policy == "yes" ? <Grid item xs={12}>
          <FieldArray name="policies" metaData={metaData} formValues={formValues} component={renderPolicy} defaultValue={initialValues} submitButtonName={submitButtonName} updateField={updateField} />
        </Grid> : null}
        <Grid item xs={12}>
          {error && <Error errorMessage={error} /> || ''}
        </Grid>
      </Grid>
      <Grid container className={classes.footer}>
        {submitButtonView ?
          React.createElement(submitButtonView, props) :
          <Button
            type="submit"
            disabled={formValues.form.entry != undefined ? formValues.form.entry == "fileUpload" && !formValues.form.doc ? true : false : true}
            variant="contained"
            color="primary"
            className={classes.submitBtn}>
            {submitting && <ButtonSpinner /> || submitButtonName && submitButtonName && user.email != "siva@miotiv.com" && "Download" || submitButtonName && submitButtonName || 'Create'}
          </Button>}
        <Link to={{ pathname: path, state: { ...locationState } }}>
          <Button
            type="button"
            variant="contained"
            color="primary"
            className={classes.cancelBtn}>
            Cancel
          </Button>
        </Link>
      </Grid>
    </form>
  </div>)

}

const validate = (values) => {
  const errors = {}
  const requiredFields = ['body-system', 'treatment', 'policy', 'treatment-type'];

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })

  if (!values["policy"]) {
    errors["policy"] = 'Required'
  }

  const policyArrayErrors = [];

  if (values.policy == "yes") {
    if (values.policies) {
      values.policies.map((diagnosis, i) => {
        const policyErrors = {}
        if (!diagnosis || !diagnosis.source) {
          policyErrors.source = 'Required'
          policyArrayErrors[i] = policyErrors
        }
        if (!diagnosis || !diagnosis.diagnosis) {
          policyErrors.diagnosis = 'Required'
          policyArrayErrors[i] = policyErrors
        }
        if (!diagnosis || !diagnosis.recommendation) {
          policyErrors.recommendation = 'Required'
          policyArrayErrors[i] = policyErrors
        }
        if (diagnosis && diagnosis.source && !diagnosis[`${diagnosis.source}-text`]) {
          policyErrors[`${diagnosis.source}-text`] = 'Required'
          policyArrayErrors[i] = policyErrors
        }
        if (diagnosis && diagnosis.diagnosis) {
          values.policies.map((diag, ind) => {
            if (ind != i && ind < i) {
              if (diag.diagnosis == diagnosis.diagnosis) {
                policyErrors.diagnosis = 'This Diagnosis Already Exist'
                policyArrayErrors[i] = policyErrors
                errors["policies"] = 'This Diagnosis Already Exist'
              }
            }
          })
        }
        if (diagnosis && diagnosis.recommendation == "askaquestion") {
          if (diagnosis && diagnosis.questions && diagnosis.questions.length) {
            const questionArrayErrors = [];
            diagnosis.questions.forEach((question, questionIndex) => {
              const questionErrors = {};
              if (!question || !question.lengh) {
                questionArrayErrors[questionIndex] = 'Required'
              }
              if (!question || !question.questionsText) {
                questionErrors.questionsText = 'Required'
                questionArrayErrors[questionIndex] = questionErrors
              }
              if (!question || !question.yes) {
                questionErrors.yes = 'Required'
                questionArrayErrors[questionIndex] = questionErrors
              }
              if (!question || !question.no) {
                questionErrors.no = 'Required'
                questionArrayErrors[questionIndex] = questionErrors
              }

            })
            if (questionArrayErrors.length) {
              policyErrors.questions = questionArrayErrors
              policyArrayErrors[i] = policyErrors
            }
          }

        }
      }
      )
    }
  }

  if (policyArrayErrors.length) {
    errors.policies = policyArrayErrors
  }
  return errors
}

export default reduxForm({
  form: 'createTreatmentForm',
  validate,
  touchOnChange: true
})(CreateTreatmentForm);