/**
 * 
 * Auto Complete Field
 * 
 */


 import React, { useState, useEffect, useRef } from 'react';
 import Autocomplete from '@mui/material/Autocomplete';
 import Styles from './styles';
 import { FormControl, TextField } from '@mui/material';
 
 export default function ({ input, label, placeholder, metaData, options, disabled, style, meta: { touched, error, warning }, defaultValue }) {// eslint-disable-line
     const classes = Styles();
     const [open, setOpen] = useState(false);
     const [inputValue, setInputValue] = useState("");
     const componentMounted = useRef(true);

     useEffect(() => {
           input.onChange(defaultValue);
           setInputValue(defaultValue);
         return () => { // This code runs when component is unmounted
             componentMounted.current = false; // (4) set it to false when we leave the page
         }
       }, [defaultValue]);
 
     
     const { id, name, value, onChange } = input;// eslint-disable-line
     const isPreDefinedSet = Array.isArray(options);
     const Options = isPreDefinedSet ? options : metaData[options] || [];
 
     const onBlur = (e) => {
         onChange(e.target.value)
     }
 
     return (
         <div className={classes.selectField} style={style || {}}>
             <FormControl className={classes.formControl}>
                 <Autocomplete
                     id={id}
                     name={name}
                     open={open}
                     onOpen={() => {
                         // only open when in focus and inputValue is not empty
                         if (inputValue) {
                             setOpen(true);
                         }
                     }}
                     onClose={() => setOpen(false)}
                     options={Options}
                     getOptionLabel={(option) => option.label || ''}
                     autoComplete
                     freeSolo
                     clearOnBlur={false}
                     includeInputInList
                     disabled={disabled}
                     inputValue={value}
                     onBlur={onBlur}
                     onInputChange={(e, value) => {// eslint-disable-line
                         input.onChange(value);
                         setInputValue(value);
                         setOpen(true);
                         // only open when inputValue is not empty after the user typed something
                         if (!value) {
                             setOpen(false);
                         }
                         return value
                     }}
                     renderInput={(params) => (
                         <TextField
                             {...params}
                             variant="standard"
                             className={classes.fieldColor}
                             label={label}
                             placeholder={placeholder}
                         />
                     )}
                 />
             </FormControl>
             <div className={classes.error}>{touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}</div>
         </div>
     )
 }
 
 