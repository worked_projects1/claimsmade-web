

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => {
    return ({
        btn: {
            height: '35px',
            width: '150px',
            lineHeight: '35px',
            padding: '0px 25px',
            paddingTop: '3px',
            backgroundImage: 'linear-gradient(90deg,#009cd2,#00ca1f)',
            border: 'none',
            borderRadius: '3px',
            color: '#fff !important'
        },
        inputGrid: {
            paddingTop: '24px'
        },
        totalContainer: {
            padding: '0px !important'
        },
        mainContainer: {
            paddingLeft: '24px',
            marginTop: '10px'
        },
        subContainer: {
            boxShadow: '0px 0px 8px 1px lightgrey',
            marginTop: '15px',
            padding: '24px'
        },
        headerContainer:{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        attachDelete: {
            justifyContent: 'flex-end',
        },
        deleteBtn:{
            justifyContent: 'right',
            padding: '0px',
            minWidth: 'fit-content'
        }
    })
});


export default useStyles;