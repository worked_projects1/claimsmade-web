/** 
 * 
 * Components for createTreatmentForm
 * 
*/

import React, { useEffect } from 'react';
import SelectField from 'components/SelectField';
import AutoCompleteField from './AutoComplete';
import TextareaField from 'components/TextareaField';
import { Field, FieldArray } from 'redux-form';
import { Grid, Button } from '@mui/material';
import Styles from './styles';
import SVG from 'react-inlinesvg';
import AlertDialog from 'components/AlertDialog';

export const renderPolicy = ({ fields, defaultValue, meta: { touched, error, submitFailed }, metaData, formValues, updateField }) => {// eslint-disable-line
  const classes = Styles();
  useEffect(() => {
    fields.removeAll();
    if (defaultValue && defaultValue.policies.map((e, i) => e != undefined).includes(true) && defaultValue.policies.length != 0) {// eslint-disable-line
      defaultValue.policies.map((e, i) => {// eslint-disable-line

        const value = {
          "diagnosis": e.diagnosis,
          "mtus-text": e["mtus-text"],
          "odg-text": e["odg-text"],
          "source": e.source,
          "recommendation": e.recommendation,
          "question": e.question,
          "questions": e.questions,
        }
        fields.push(value);
      })
    } else {
      fields.push(Object.assign({formValues}, {
        Policies : [undefined]
      }));
    }

  }, []);

  return (<Grid container spacing={3} className={classes.mainContainer}>
    <Grid item xs={12} name='renderPolicy' className={classes.totalContainer}>

      {fields.map((policy, index) =>
        <Grid key={index} className={classes.subContainer}>
          <Grid className={classes.headerContainer}>
            <h4>Diagnosis #{index + 1}</h4>
            {fields.length != 1 ? <AlertDialog
              description={"Do you want to delete this Diagnosis?"}
              onConfirm={() => fields.remove(index)}
              onConfirmPopUpClose={true}
              btnLabel1='Delete'
              btnLabel2='Cancel' >
              {(open) => <Button className={classes.deleteBtn} onClick={open}><SVG src={require('images/icons/delete.svg')} className={classes.attachDelete} /></Button>}
            </AlertDialog> : null}
          </Grid>
          <Grid><Field
            name={`${policy}.diagnosis`}
            label={`Diagnosis`}
            type={"text"}
            component={AutoCompleteField}
            disabled={(formValues.form && formValues.form['treatment-type'] && formValues.form['treatment-type'] == "surgery-add-on")}
            defaultValue={(formValues.form && formValues.form['treatment-type'] && formValues.form['treatment-type'] == "surgery-add-on") ? "Surgery Add-on" : (formValues.form && formValues.form['treatment-type'] && formValues.form['treatment-type'] != "surgery-add-on") ? "" : defaultValue && defaultValue.policies.map((e) => e != undefined).includes(true) && defaultValue.policies[index] && defaultValue.policies[index].diagnosis || ""}

            options={(formValues.form && formValues.form['treatment-type'] && formValues.form['treatment-type'] == "surgery-add-on") ? [{ label: 'Surgery Add-on', value: 'surgery-add-on' }] : metaData.diagnosisOptions.filter((e) => formValues.Policies && !formValues.Policies.map((e1) => e1 && e1.diagnosis).includes(e.value)).filter((e) => e.value != "*").filter((e) => e.value != "NoDiagnosisFound").sort((a, b) => a.label.localeCompare(b.label))}
          /></Grid>
          {/* : null} */}

          <Grid className={classes.inputGrid}>
            <Field
              name={`${policy}.source`}
              label={"Source"}
              type={"select"}
              options={[{
                label: 'MTUS',
                value: 'mtus',
              },
              {
                label: 'ODG',
                value: 'odg',
              },]}
              component={SelectField}
            /></Grid>
          {formValues.Policies && formValues.Policies[index] && formValues.Policies[index].source ?
            <Grid className={classes.inputGrid}><Field
              name={formValues.Policies && formValues.Policies[index] && formValues.Policies[index].source ? `${policy}.${formValues.Policies[index].source}-text` : "text"}
              label={formValues.Policies && formValues.Policies[index] && formValues.Policies[index].source ? `${(formValues.Policies[index].source).toUpperCase()} Text` : "Text"}
              rows={4}
              type='textarea'
              component={TextareaField}
            /></Grid>
            : null}

          <Grid className={classes.inputGrid}><Field
            name={`${policy}.recommendation`}
            label={`Recommendation`}
            type={"select"}
            options={[{
              label: 'Approve',
              value: 'approve',
            },
            {
              label: 'Deny',
              value: 'deny',
            },
            {
              label: 'Ask a question',
              value: 'askaquestion',
            },
            {
              label: 'No recommendation',
              value: 'no_recommendation',
            }
            ]}
            component={SelectField}
          />
          </Grid>
          {(formValues.Policies && formValues.Policies[index] && formValues.Policies[index].recommendation && formValues.Policies[index].recommendation == "askaquestion") || (!(formValues.Policies && formValues.Policies[index] != undefined && formValues.Policies[index].recommendation && formValues.Policies[index].recommendation != "askaquestion") && (defaultValue && defaultValue.policies && defaultValue.policies[index] && defaultValue.policies[index].recommendation && defaultValue.policies[index].recommendation == "askaquestion")) ? <FieldArray name={`${policy}.questions`} metaData={metaData} formValues={formValues} index={index} component={renderQuestions} defaultValue={defaultValue} updateField={updateField} />
            : null}
        </Grid>
      )}
      {error && <Grid className="error">{error}</Grid>}

      {(formValues.form && formValues.form['treatment-type'] && formValues.form['treatment-type'] != "surgery-add-on") || (formValues.form && !formValues.form['treatment-type']) ? <Grid className={classes.inputGrid}>
        <Button
          className={classes.btn}
          type={((fields.getAll() || []).map((e, i) => e && e.source && e[`${e.source}-text`] && e.diagnosis && e.recommendation && ((e.recommendation == "askaquestion" && e.questions && !e.questions.map((question, ind) => question && question.questionsText && question.yes && question.no || false).includes(false)) || e.recommendation != "askaquestion") || false)).includes(false) ? "submit" : "button"}// eslint-disable-line
          // disabled={}
          onClick={((fields.getAll() || []).map((e, i) => e && e.source && e[`${e.source}-text`] && e.diagnosis && e.recommendation && ((e.recommendation == "askaquestion" && e.questions && !e.questions.map((question, ind) => question && question.questionsText && question.yes && question.no || false).includes(false)) || e.recommendation != "askaquestion") || false)).includes(false) || (((fields.getAll() || []).map((e, i) => e && e.diagnosis)).length !== (new Set((fields.getAll() || []).map((e, i) => e && e.diagnosis)).size)) ? (e) => e.stopPropagation() : () => fields.push({})}// eslint-disable-line
        >Add Diagnosis</Button>

      </Grid> : null}
      {(touched || submitFailed) && error && <span>{error}</span>}
    </Grid>
  </Grid>
  )
}

const renderQuestions = ({ fields, meta: { error }, metaData, formValues, index, defaultValue, updateField }) => {// eslint-disable-line
  const onChange = (e, i, value) => {

    if ((value == "no" && (formValues.Policies && formValues.Policies[index] && formValues.Policies[index].questions[i] && (formValues.Policies[index].questions[i].no != undefined && formValues.Policies[index].questions[i].no == "nextQuestion") || false
    )) || (value == "yes" && (formValues.Policies && formValues.Policies[index] && formValues.Policies[index].questions[i] && (formValues.Policies[index].questions[i].yes != undefined && formValues.Policies[index].questions[i].yes == "nextQuestion") || false
    ))) {
      fields.remove(i + 1);
    }

    if (e == "nextQuestion") {
      fields.push()
    }
  }

  const options = [{
    label: 'Approve',
    value: 'approve',
  },
  {
    label: 'Deny',
    value: 'deny',
  },
  {
    label: 'Ask another question',
    value: 'nextQuestion',
  },
  {
    label: 'No recommendation',
    value: 'no_recommendation',
  }
  ]

  useEffect(() => {
    fields.removeAll();
    if (defaultValue && defaultValue.policies.map((e, i) => e != undefined).includes(true) && defaultValue.policies.length != 0) {// eslint-disable-line
      const quest = defaultValue.policies[index] && defaultValue.policies[index].questions || false
      if (quest && quest.length != 0 && quest.map((e, i) => e != undefined).includes(true)) {// eslint-disable-line
        quest.map((e, i) => {// eslint-disable-line
          const value = {
            "questionsText": e.questionsText,
            "yes": e.yes ? e.yes : "nextQuestion",
            "no": e.no ? e.no : "nextQuestion"
          }
          fields.push(value);
        })
      } else {
        fields.push();
      }
    } else {
      fields.push();
    }
  }, []);

  const deleteQuestion = (ind) => {
    const policyValue = formValues.Policies;
    if ((formValues.Policies && formValues.Policies[index] && formValues.Policies[index].questions[ind - 1])) {
      if (formValues.Policies[index].questions[ind - 1].yes == "nextQuestion") {
        policyValue[index].questions[ind - 1].yes = "";
        updateField("createTreatmentForm", `Policies`, policyValue);
      } else if (formValues.Policies[index].questions[ind - 1].no == "nextQuestion") {
        policyValue[index].questions[ind - 1].no = "";
        updateField("createTreatmentForm", `Policies`, policyValue);
      } 
    }
    fields.remove(ind);
  }

  const classes = Styles();
  return (
    <Grid container spacing={3} className={classes.mainContainer}>
      <Grid item xs={12} name='renderPolicy' className={classes.totalContainer}>

        {fields.map((question, i) =>
          <Grid key={i} className={classes.subContainer}>
            <Grid className={classes.headerContainer}>
              <h4 style={{ margin: '0px' }}>Question #{i + 1}</h4>
              {fields.length != 1 ? <AlertDialog
                description={"Do you want to delete this Question?"}
                onConfirm={() => deleteQuestion(i)}
                onConfirmPopUpClose={true}
                btnLabel1='Delete'
                btnLabel2='Cancel' >
                {(open) => <Button className={classes.deleteBtn} onClick={open}><SVG src={require('images/icons/delete.svg')} className={classes.attachDelete} /></Button>}
              </AlertDialog> : null}
            </Grid>
            <Field
              name={`${question}.questionsText`}
              label={`Question Text`}
              rows={2}
              type='textarea'
              component={TextareaField}
            />

            <Grid className={classes.inputGrid}><Field
              name={`${question}.yes`}
              label={`Yes`}
              type={"select"}
              options={(formValues.Policies && formValues.Policies[index] && formValues.Policies[index].questions[i] && formValues.Policies[index].questions[i].no != undefined) ? options.filter((e, ind) => e.value != formValues.Policies[index].questions[i].no) : options}// eslint-disable-line
              component={SelectField}
              onChange={(e) => onChange(e, i, "yes")}
            />
            </Grid>
            <Grid className={classes.inputGrid}><Field
              name={`${question}.no`}
              label={`No`}
              type={"select"}
              options={(formValues.Policies && formValues.Policies[index] && formValues.Policies[index].questions[i] && formValues.Policies[index].questions[i].yes != undefined) ? options.filter((e, ind) => e.value != formValues.Policies[index].questions[i].yes) : options}// eslint-disable-line
              component={SelectField}
              onChange={(e) => onChange(e, i, "no")}
            />
            </Grid>
          </Grid>
        )}
        {error && <Grid className="error">{error}</Grid>}
      </Grid>
    </Grid>
  )
}