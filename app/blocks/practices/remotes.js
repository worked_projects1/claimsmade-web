/*
 *
 *  practice remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {// eslint-disable-line

    /**
     * @param {integer} id 
     */
    function loadPractice(id) {
        return api.get(`practices/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updatePractice(record) {
        return api.put(`practices/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {integer} id 
     */
    function loadPracticeSettings(id) {
        return api.get(`rest/practice-settings?practice_id=${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updatePracticeSettings(record) {
        return api.put(`rest/practice-settings`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        loadPractice,
        updatePractice,
        loadPracticeSettings,
        updatePracticeSettings
    }
}

