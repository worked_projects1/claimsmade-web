/*
 *
 * practice actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {

    const {
        LOAD_PRACTICE,
        LOAD_PRACTICE_ERROR,
        LOAD_PRACTICE_SUCCESS,
        UPDATE_PRACTICE,
        UPDATE_PRACTICE_ERROR,
        UPDATE_PRACTICE_SUCCESS,
        LOAD_PRACTICE_SETTINGS,
        UPDATE_PRACTICE_SETTINGS,
        UPDATE_PRACTICE_SETTINGS_ERROR,
        UPDATE_PRACTICE_CUSTOME_TEMPLATE,
        UPDATE_PRACTICE_CUSTOME_TEMPLATE_ERROR,
        UPDATE_PRACTICE_CUSTOME_TEMPLATE_SUCCESS
    } = constants;


    /**
     * @param {integer} id 
     */
    function loadPractice(id) {
        return {
            type: LOAD_PRACTICE,
            id
        };
    }

    /**
     * @param {string} error 
     */
    function loadPracticeError(error) {
        return {
            type: LOAD_PRACTICE_ERROR,
            error,
        };
    }

    /**
     * @param {object} record 
     */
    function loadPracticeSuccess(record) {
        return {
            type: LOAD_PRACTICE_SUCCESS,
            record
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
    function updatePractice(record, form, setShowForm, setFormLoader) {
        return {
            type: UPDATE_PRACTICE,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeError(error) {
        return {
            type: UPDATE_PRACTICE_ERROR,
            error,
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updatePracticeSuccess(record, success) {
        return {
            type: UPDATE_PRACTICE_SUCCESS,
            record,
            success
        };
    }



    /**
     * 
     * @param {integer} id 
     * @returns 
     */
    function loadPracticeSettings(id) {
        return {
            type: LOAD_PRACTICE_SETTINGS,
            id
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
    function updatePracticeSettings(record, form) {
        return {
            type: UPDATE_PRACTICE_SETTINGS,
            record,
            form
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeSettingsError(error) {
        return {
            type: UPDATE_PRACTICE_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
     function updatePracticeCustomTemplate(record, form, setShowForm, setFormLoader) {
        return {
            type: UPDATE_PRACTICE_CUSTOME_TEMPLATE,
            record,
            form,
            setShowForm,
            setFormLoader
        };
    }

    /**
     * @param {string} error 
     */
    function updatePracticeCustomTemplateError(error) {
        return {
            type: UPDATE_PRACTICE_CUSTOME_TEMPLATE_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updatePracticeCustomTemplateSuccess(record, success) {
        return {
            type: UPDATE_PRACTICE_CUSTOME_TEMPLATE_SUCCESS,
            record,
            success
        };
    }


    return {
        loadPractice,
        loadPracticeError,
        loadPracticeSuccess,
        updatePractice,
        updatePracticeError,
        updatePracticeSuccess,
        loadPracticeSettings,
        updatePracticeSettings,
        updatePracticeSettingsError,
        updatePracticeCustomTemplate,
        updatePracticeCustomTemplateError,
        updatePracticeCustomTemplateSuccess
    }




}
