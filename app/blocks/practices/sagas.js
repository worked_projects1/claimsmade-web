/*
 *
 *  practice sagas
 *
 */


import { call, take, put, race, all } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';

import {
    DEFAULT_LOAD_PRACTICE_ERROR,
    DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR,
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {string} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {// eslint-disable-line

    const {
        LOAD_PRACTICE,
        UPDATE_PRACTICE,
    } = constants;


    const {
        loadPracticeError,
        loadPracticeSuccess,
        updatePracticeSuccess,
        updatePracticeSettingsError
    } = actions;


    const {
        loadPractice,
        updatePractice,
    } = remotes;



    function* loadPracticeSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { load } = yield race({
                load: take(LOAD_PRACTICE),
            });
            const { id } = load || {};
            if (id) {
                try {
                    const result = yield call(loadPractice, id);
                    if (result) {
                        yield put(loadPracticeSuccess(result));
                    } else {
                        yield put(loadPracticeError(DEFAULT_LOAD_PRACTICE_ERROR));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_LOAD_PRACTICE_ERROR;
                    yield put(loadPracticeError(Err));
                }
            }
        }
    }



    function* updatePracticeSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { update } = yield race({
                update: take(UPDATE_PRACTICE),
            });
            const { record, form, setShowForm, setFormLoader } = update || {};// eslint-disable-line
            if (record) {
                yield put(startSubmit(form));
                try {
                    const result = yield call(updatePractice, record);
                    if (result) {
                        yield put(updatePracticeSuccess(result, 'Profile Updated'));
                        yield put(stopSubmit(form));
                    } else {
                        yield put(updatePracticeSettingsError(DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR));
                        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR }));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR;
                    yield put(updatePracticeSettingsError(Err));
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR }));
                }
            }
        }
    }


    return function* rootSaga() {
        yield all([
            loadPracticeSaga(),
            updatePracticeSaga()
        ]);
    }
}

