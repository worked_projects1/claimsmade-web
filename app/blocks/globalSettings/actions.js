/*
 *
 * global settings actions
 *
 */


/**
 * 
 * @param {object} constants 
 * @returns 
 */
export default function (constants) {
    const {
        LOAD_GLOBAL_SETTINGS,
        LOAD_GLOBAL_SETTINGS_ERROR,
        LOAD_GLOBAL_SETTINGS_SUCCESS,
        UPDATE_GLOBAL_SETTINGS,
        UPDATE_GLOBAL_SETTINGS_ERROR,
        UPDATE_GLOBAL_SETTINGS_SUCCESS
    } = constants;


    /**
 * @param {object} record 
 * @param {string} form 
 */
    function loadGlobalSettings(record, form) {
        return {
            type: LOAD_GLOBAL_SETTINGS,
            record,
            form
        };
    }

    /**
     * @param {string} error 
     */
    function loadGlobalSettingsError(error) {
        return {
            type: LOAD_GLOBAL_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     */
    function loadGlobalSettingsSuccess(record) {
        return {
            type: LOAD_GLOBAL_SETTINGS_SUCCESS,
            record
        };
    }

    /**
     * @param {object} record 
     * @param {string} form 
     */
    function updateGlobalSettings(record, form) {
        return {
            type: UPDATE_GLOBAL_SETTINGS,
            record,
            form
        };
    }

    /**
     * @param {string} error 
     */
    function updateGlobalSettingsError(error) {
        return {
            type: UPDATE_GLOBAL_SETTINGS_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     * @param {string} success 
     */
    function updateGlobalSettingsSuccess(record, success) {
        return {
            type: UPDATE_GLOBAL_SETTINGS_SUCCESS,
            record,
            success
        };
    }


    return {
        loadGlobalSettings,
        loadGlobalSettingsError,
        loadGlobalSettingsSuccess,
        updateGlobalSettings,
        updateGlobalSettingsError,
        updateGlobalSettingsSuccess
    }

}


