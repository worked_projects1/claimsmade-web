/*
 *
 *  global settings sagas
 *
 */


import { call, take, put, race, all } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import {
    DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR,
    DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR
} from 'utils/errors';


/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {object} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {// eslint-disable-line
    const {
        LOAD_GLOBAL_SETTINGS,
        UPDATE_GLOBAL_SETTINGS
    } = constants;


    const {
        loadGlobalSettingsError,
        loadGlobalSettingsSuccess,
        updateGlobalSettingsError,
        updateGlobalSettingsSuccess
    } = actions;


    const {
        loadGlobalSettings,
        updateGlobalSettings
    } = remotes;




    function* loadGlobalSettingsSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { load } = yield race({
                load: take(LOAD_GLOBAL_SETTINGS),
            });
            if (load) {
                try {
                    const result = yield call(loadGlobalSettings);
                    if (result) {
                        yield put(loadGlobalSettingsSuccess(result));
                    } else {
                        yield put(loadGlobalSettingsError(DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR;
                    yield put(loadGlobalSettingsError(Err));
                }
            }
        }
    }

    function* updateGlobalSettingsSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { update } = yield race({
                update: take(UPDATE_GLOBAL_SETTINGS),
            });
            const { record, form } = update || {};
            if (record) {
                yield put(startSubmit(form));
                try {
                    const result = yield call(updateGlobalSettings, record);
                    if (result) {
                        yield put(updateGlobalSettingsSuccess(result, form && form === 'SettingsForm_3' ? 'Medical History Pricing Updated' : form && form === 'SettingsForm_2' ? 'RFA Form Limits Updated' : 'HIPAA Compliance Updated'));
                        yield put(stopSubmit(form));
                    } else {
                        yield put(updateGlobalSettingsError(DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR));
                        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR }));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR;
                    yield put(updateGlobalSettingsError(Err));
                    yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR }));
                }
            }
        }
    }



    return function* rootSaga() {
        yield all([
            loadGlobalSettingsSaga(),
            updateGlobalSettingsSaga()
        ]);
    }
}

