/*
 *
 * global settings ants
 *
 */


/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {
    const url = `lg/${name}`;

    return {
        LOAD_GLOBAL_SETTINGS: `${url}/LOAD_GLOBAL_SETTINGS`,
        LOAD_GLOBAL_SETTINGS_ERROR: `${url}/LOAD_GLOBAL_SETTINGS_ERROR`,
        LOAD_GLOBAL_SETTINGS_SUCCESS: `${url}/LOAD_GLOBAL_SETTINGS_SUCCESS`,
        UPDATE_GLOBAL_SETTINGS: `${url}/UPDATE_GLOBAL_SETTINGS`,
        UPDATE_GLOBAL_SETTINGS_ERROR: `${url}/UPDATE_GLOBAL_SETTINGS_ERROR`,
        UPDATE_GLOBAL_SETTINGS_SUCCESS: `${url}/UPDATE_GLOBAL_SETTINGS_SUCCESS`,
    }
}
