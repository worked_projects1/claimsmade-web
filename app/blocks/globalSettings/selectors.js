/**
 * 
 * global settings selectors
 * 
 */

import { createSelector } from 'reselect';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {

    const selectDomain = () => (state) => state[name] || false;

    const selectGlobalSettings = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.record || {},
    );


    return {
        selectDomain,
        selectGlobalSettings
    }

}

