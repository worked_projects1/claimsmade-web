/*
 *
 *  global settings remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {// eslint-disable-line
    function loadGlobalSettings() {
        return api.get(`users/getSettings`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * @param {object} record 
     */
    function updateGlobalSettings(record) {
        return api.put(`users/setSettings`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    return {
        loadGlobalSettings,
        updateGlobalSettings
    }
}


