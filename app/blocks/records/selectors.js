/*
 *
 *  selectors
 *
 */

import { createSelector } from 'reselect';
import mapRecords from './utils';
import mapRecordsMetaData from './metaData';

/**
 * @param {string} name 
 */
export default function selectors(name) {
  const selectDomain = () => (state) => state[name] || false;

  const selectLoading = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.loading || false,
  );

  const selectProgress = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.progress || false,
  );

  const selectRecordsMetaData = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.recordsMetaData && mapRecordsMetaData(domain.recordsMetaData, name) || {},
  );

  const selectRecords = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.records && mapRecords(domain.records, name) || [],
  );

  const selectTreatments = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.treatments || [],
  );

  const selectRecord = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.record && mapRecords([domain.record], name)[0] || {}
  );

  const selectError = () => createSelector(
    selectDomain(),
    (domain) => domain && (domain.error || domain.pageError || false),
  );

  const selectSuccess = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.success || false,
  );

  const selectUpdateError = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.updateError || false,
  );

  const selectUpdateTimestamp = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.lastUpdate || false,
  );

  const selectKey = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.key || 0,
  );

  const selectSettings = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.settings || {},
  );
  const selectAdministrator = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.Administrator || {},
  );

  const selectTotalPageCount = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.totalPageCount || false,
  );

  const selectHeaders = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.headers || {},
  );

  const selectFilter = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.filter || {}
  );

  const selectPageLoader = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.pageLoader || false
  );

  const selectPatients = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.patients || [],
  );

  const selectbodySystemNames = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.bodySystemNames || [],
  );

  const selectTreatmentsNames = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.treatmentNames || [],
  );

  const selectAllTreatmentsAndDiagnosis = () => createSelector(
    selectDomain(),
    (domain) => domain && domain.treatmentDatas || []  
)

const selectPolicyDocUpdated = () => createSelector(
  selectDomain(),
  (domain) => domain && domain.policyDocDate || "" 
)

  return {
    selectDomain,
    selectProgress,
    selectLoading,
    selectRecords,
    selectRecordsMetaData,
    selectRecord,
    selectError,
    selectSuccess,
    selectUpdateError,
    selectUpdateTimestamp,
    selectKey,
    selectSettings,
    selectAdministrator,
    selectTotalPageCount,
    selectHeaders,
    selectFilter,
    selectPageLoader,
    selectPatients,
    selectTreatments,
    selectbodySystemNames,
    selectTreatmentsNames,
    selectAllTreatmentsAndDiagnosis,
    selectPolicyDocUpdated
  };
}
