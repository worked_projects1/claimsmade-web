
/*
 *
 *  utils
 *
 */

import moment from 'moment';
import { AsYouType } from 'libphonenumber-js';

/**
 * @param {object} records 
 */
export function mapClients(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.createdAt = record.createdAt && moment(record.createdAt).format('MM/DD/YYYY') || false;
            Record.dob = record.dob && moment(record.dob).format('MM/DD/YYYY') || false;
            Record.phone = Record.phone && new AsYouType('US').input(Record.phone) || Record.phone;
            return Record;
        });
    }
    return records;
}


/**
 * @param {object} records 
 */
 export function mapOtherParties(records) {
    if (records && records.length > 0) {
        return records.map((record) => {
            let Record = Object.assign({}, record);
            Record.createdAt = record.createdAt && moment(record.createdAt).format('MM/DD/YYYY') || false;
            Record.dob = record.dob && moment(record.dob).format('MM/DD/YYYY') || false;
            Record.phone = Record.phone && new AsYouType('US').input(Record.phone) || Record.phone;
            return Record;
        });
    }
    return records;
}


/**
 * @param {object} records 
 */
 export function mapCases(records) {
    if (records && records.length > 0) {

        return records.map((record) => {
            let Record = Object.assign({}, record);
            if(Record.date_of_birth){
                Record.date_of_birth = Record.date_of_birth && moment(Record.date_of_birth).format('MM/DD/YYYY') || '';
            }
            if(Record.date_of_injury){
                Record.date_of_injury = Record.date_of_injury && moment(Record.date_of_injury).format('MM/DD/YYYY') || '';
            }
            if (record['case_data'] != undefined) {
                Record['case_data'].date_of_birth = Record['case_data'].date_of_birth && moment(Record['case_data'].date_of_birth).format('MM/DD/YYYY') || '';
                Record['case_data'].date_of_injury = Record['case_data'].date_of_injury && moment(Record['case_data'].date_of_injury).format('MM/DD/YYYY') || '';
            }
            return Record;
        });
    }
    return records;

}

/**
 * @param {object} records 
 * @param {string} name 
 */
export default function mapRecords(records, name) {
    switch (name) {
        case 'clients':
            return mapClients(records);
        case 'rfaForms':
            return mapCases(records);
        case 'rest/other-parties':
            return mapOtherParties(records);    
        default:
            return records;
    }
}
