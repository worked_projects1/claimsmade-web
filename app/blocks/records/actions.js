/*
 *
 *  actions
 *
 */

export default function actions(constants) {
  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    SET_HEADERS_DATA,
    LOAD_RECORDS_META_DATA,
    LOAD_RECORDS_META_DATA_SUCCESS,
    LOAD_RECORDS_META_DATA_ERROR,
    CREATE_TREATMENT,
    CHANGE_STATUS,
    SET_HISTORY_RECORD_SUCCESS,
    SET_HISTORY_RECORD_ERROR,
    LOAD_CASE_SUCCESS,
    RESET_LOGIN_ATTEMPT,
    RESET_LOGIN_ATTEMPT_SUCCESS,
    RESET_LOGIN_ATTEMPT_ERROR,
    SET_AUTHORIZATION,
    GET_ALL_BODY_SYSTEMS,
    GET_ALL_BODY_SYSTEMS_SUCCESS,
    GET_TREATMENTS_NAME,
    GET_TREATMENTS_NAME_SUCCESS,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS,
    RFA_FORM_DETAIL_CLEAR,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS,
    LOAD_TREATMENT_DIAGNOSIS_ERROR,
    GET_ALL_BODY_SYSTEMS_ERROR,
    GET_ALL_TREATMENT_ERROR,
    GET_RFA_FORM_DETAIL_ERROR,
    SYNC,
    SYNCSUCCESS,
    SYNCERROR,
    SET_AUTHORIZATION_ERROR
  } = constants;


  function loadRecordsCacheHit() {
    return {
      type: LOAD_RECORDS_VALID_CACHE,
    };
  }

  /**
   * @param {integer} id 
   */
  function loadRecord(id) {
    return {
      type: LOAD_RECORD,
      id,
    };
  }

  /**
   * @param {object} record 
   * @param {object} recordsMetaData
   */
  function loadRecordSuccess(record, recordsMetaData) {
    return {
      type: LOAD_RECORD_SUCCESS,
      record,
      recordsMetaData
    };
  }

  /**
   * @param {object} record 
   * @param {object} recordsMetaData
   */
  function loadCasesSuccess(record) {
    return {
      type: LOAD_CASE_SUCCESS,
      record
    };
  }

  /**
   * @param {string} error 
   */
  function loadRecordError(error) {
    return {
      type: LOAD_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {boolean} invalidateCache 
   */
  function loadRecords(invalidateCache, queryParams) {
    return {
      type: LOAD_RECORDS,
      invalidateCache,
      queryParams
    };
  }

  /**
   * @param {array} records 
   * @param {number} totalPageCount 
   */
  function loadRecordsSuccess(records, totalPageCount) {
    return {
      type: LOAD_RECORDS_SUCCESS,
      records,
      totalPageCount
    };
  }

  /**
   * @param {string} error 
   */
  function loadRecordsError(error) {
    return {
      type: LOAD_RECORDS_ERROR,
      error,
    };
  }

 /**
   * @param {object} record 
   * @param {string} form 
   */
  function createRecord(record, form, fun) {
    return {
      type: CREATE_RECORD,
      record,
      form,
      fun
    };
  }

  /**
   * @param {object} record 
   */
  function createRecordSuccess(record) {
    return {
      type: CREATE_RECORD_SUCCESS,
      record,
    };
  }

  /**
   * @param {string} error 
   */
  function createRecordError(error) {
    return {
      type: CREATE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   * @param {string} form 
   * @param {function} posLoader
   */
  function updateRecord(record, form, posLoader) {
    return {
      type: UPDATE_RECORD,
      record,
      form,
      posLoader
    };
  }

  /**
   * @param {object} record 
   */
  function updateRecordSuccess(record, update) {
    return {
      type: UPDATE_RECORD_SUCCESS,
      record,
      update
    };
  }

  /**
   * @param {string} error 
   */
  function updateRecordError(error) {
    return {
      type: UPDATE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {integer} id 
   * @param {string} form 
   */
  function deleteRecord(id, form) {
    return {
      type: DELETE_RECORD,
      id,
      form,
    };
  }

  /**
   * @param {integer} id 
   */
  function deleteRecordSuccess(id) {
    return {
      type: DELETE_RECORD_SUCCESS,
      id,
    };
  }

  /**
   * @param {string} error 
   */
  function deleteRecordError(error) {
    return {
      type: DELETE_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} record 
   */
  function setHeadersData(record) {
    return {
      type: SET_HEADERS_DATA,
      record
    };
  }

  function loadRecordsMetaData() {
    return {
      type: LOAD_RECORDS_META_DATA
    }
  }

  /**
   * 
   * @param {object} recordsMetaData 
   * @returns 
   */
  function loadRecordsMetaDataSuccess(recordsMetaData) {
    return {
      type: LOAD_RECORDS_META_DATA_SUCCESS,
      recordsMetaData
    }
  }

  /**
   * 
   * @param {string} error 
   * @returns 
   */
  function loadRecordsMetaDataError(error) {
    return {
      type: LOAD_RECORDS_META_DATA_ERROR,
      error
    }
  }

  function getAllBodySystems() {
    return {
      type: GET_ALL_BODY_SYSTEMS
    }
  }

  /**
  * 
  * @param {array} bodySystemNames 
  */
  function getAllBodySystemSuccess(bodySystemNames) {
    return {
      type: GET_ALL_BODY_SYSTEMS_SUCCESS,
      bodySystemNames
    }
  }

  /**
  * 
  * @param {array} treatments 
  */
  function createTreatment(treatments) {
    return {
      type: CREATE_TREATMENT,
      treatments,
    };
  }

  /**
   * @param {array} treatments 
   */
  function setHistorySuccess(treatments) {
    return {
      type: SET_HISTORY_RECORD_SUCCESS,
      treatments,
    };
  }

  /**
   * @param {string} error 
   */
  function setHistoryError(error) {
    return {
      type: SET_HISTORY_RECORD_ERROR,
      error,
    };
  }

  /**
   * @param {object} status 
   * @param {integer} id 
   * @param {string} form 
   */
  function changeStatusAction(id, status, form) {
    return {
      type: CHANGE_STATUS,
      id,
      status,
      form
    }
  }

  /**
   * 
   * @param {object} record 
   */
  function resetLoginAttempt(record) {
    return {
      type: RESET_LOGIN_ATTEMPT,
      record
    };
  }

  /**
  * @param {string} success 
  */
  function resetLoginAttemptSuccess(success) {
    return {
      type: RESET_LOGIN_ATTEMPT_SUCCESS,
      success,
    };
  }

  /**
  * @param {string} error 
  */
  function resetLoginAttemptError(error) {
    return {
      type: RESET_LOGIN_ATTEMPT_ERROR,
      error,
    };
  }

  /**
  * @param {string} status
  * @param {integer} id 
  */
  function setAuthorizationStatus(id, status) {
    return {
      type: SET_AUTHORIZATION,
      id,
      status,
    };
  }

  /**
  * @param {integer} bodySystemId 
  */
  function getTreatmentsName(bodySystemId) {
    return {
      type: GET_TREATMENTS_NAME,
      bodySystemId
    }
  }

  /**
  * @param {array} treatmentNames
  */
  function getTreatmentsNameSuccess(treatmentNames) {
    return {
      type: GET_TREATMENTS_NAME_SUCCESS,
      treatmentNames
    }
  }

  /**
  * @param {array} treatmentDatas
  * @param {string} policyDocDate
  */
  function getAllTreatmentByBodySystemsSuccess(treatmentDatas, policyDocDate) {
    return {
      type: GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS,
      treatmentDatas,
      policyDocDate
    }
  }

  /**
  * @param {string} policyDocDate
  */
  function rfaFormDetailClear(policyDocDate) {
    return {
      type: RFA_FORM_DETAIL_CLEAR,
      policyDocDate
    }
  }

  /**
  * @param {integer} id
  */
  function getRfaFormDetail(id, loadRecord) {
    return {
      type: GET_ALL_TREATMENT_BY_BODYSYSTEMS,
      id,
      loadRecord
    }
  }

  /**
  * @param {string} error 
  */
  function loadAllTreatmentDiagnosisError(error) {
    return {
      type: LOAD_TREATMENT_DIAGNOSIS_ERROR,
      error
    }
  }

  /**
  * @param {string} error 
  */
  function getAllBodySystemError(error) {
    return {
      type: GET_ALL_BODY_SYSTEMS_ERROR,
      error
    }
  }

  /**
  * @param {string} error 
  */
  function getAllTreatmentError(error) {
    return {
      type: GET_ALL_TREATMENT_ERROR,
      error
    }
  }

  /**
  * @param {string} error 
  */
  function getRFAFormDetailError(error) {
    return {
      type: GET_RFA_FORM_DETAIL_ERROR,
      error
    }
  }

  /**
  * @param {function} func 
  */
  function sync(func) {
    return {
      type: SYNC,
      func
    }
  }

  /**
  * @param {object} success 
  */
  function syncSuccess(success) {
    return {
      type: SYNCSUCCESS,
      success
    }
  }

  /**
  * @param {object} error 
  */
  function syncError(error) {
    return {
      type: SYNCERROR,
      error
    }
  }

  /**
  * @param {object} error 
  */
   function setAuthorizeError(error) {
    return {
      type: SET_AUTHORIZATION_ERROR,
      error
    }
  }

  return {
    loadRecord,
    loadRecordSuccess,
    loadRecordError,
    loadRecords,
    loadRecordsSuccess,
    loadRecordsError,
    createRecord,
    createRecordSuccess,
    createRecordError,
    updateRecord,
    updateRecordSuccess,
    updateRecordError,
    deleteRecord,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    setHeadersData,
    loadRecordsMetaData,
    loadRecordsMetaDataSuccess,
    loadRecordsMetaDataError,
    createTreatment,
    changeStatusAction,
    setHistorySuccess,
    setHistoryError,
    loadCasesSuccess,
    resetLoginAttempt,
    resetLoginAttemptSuccess,
    resetLoginAttemptError,
    setAuthorizationStatus,
    getAllBodySystems,
    getAllBodySystemSuccess,
    getTreatmentsName,
    getTreatmentsNameSuccess,
    getAllTreatmentByBodySystemsSuccess,
    rfaFormDetailClear,
    getRfaFormDetail,
    loadAllTreatmentDiagnosisError,
    getAllBodySystemError,
    getAllTreatmentError,
    getRFAFormDetailError,
    sync,
    syncSuccess,
    syncError,
    setAuthorizeError
  };
}
