
/*
 *
 *   metaData
 *
 */

/**
 * 
 * @param {object} metaData 
 */
function mapDiagnosis(metaData) {
    if (metaData) {
        const { diagnosis = [], bodySystem = [] } = metaData;
        const diagnosisOptions = (diagnosis || []).map(c => Object.assign({}, { label: c.name, value: c.name }));
        const bodySystemList = (bodySystem || []).map(c => Object.assign({}, { label: c.name, value: c.name, id : c.id }));
        return Object.assign({}, { diagnosisOptions, bodySystemList })
    }
    return metaData;
}

/**
 * 
 * @param {object} metaData 
 */
function mapUsers(metaData) {
    if (metaData) {
        const { practices = [] } = metaData;
        const practicesOptions = (practices || []).map(c => Object.assign({}, { label: c.name, value: c.id }));
        return Object.assign({}, metaData, { practicesOptions })
    }
    return metaData;
}


/**
 * @param {object} metaData 
 * @param {string} name 
 */
export default function mapRecordsMetaData(metaData, name) {
    switch (name) {
        case 'users':
            return mapUsers(metaData);
        case 'practice/users':
            return mapUsers(metaData);
        case 'treatments':
            return mapDiagnosis(metaData);
        default:
            return metaData;
    }
}

