/*
 *
 *  constants
 *
 */

export default function constants(name) {
  const url = `lg/${name}`;

  return {
    LOAD_RECORD: `${url}/LOAD_RECORD`,
    LOAD_RECORD_SUCCESS: `${url}/LOAD_RECORD_SUCCESS`,
    LOAD_RECORD_ERROR: `${url}/LOAD_RECORD_ERROR`,
    LOAD_RECORDS: `${url}/LOAD_RECORDS`,
    LOAD_RECORDS_SUCCESS: `${url}/LOAD_RECORDS_SUCCESS`,
    LOAD_RECORDS_ERROR: `${url}/LOAD_RECORDS_ERROR`,
    CREATE_RECORD: `${url}/CREATE_RECORD`,
    CREATE_RECORD_SUCCESS: `${url}/CREATE_RECORD_SUCCESS`,
    CREATE_RECORD_ERROR: `${url}/CREATE_RECORD_ERROR`,
    UPDATE_RECORD: `${url}/UPDATE_RECORD`,
    UPDATE_RECORD_SUCCESS: `${url}/UPDATE_RECORD_SUCCESS`,
    UPDATE_RECORD_ERROR: `${url}/UPDATE_RECORD_ERROR`,
    DELETE_RECORD: `${url}/DELETE_RECORD`,
    DELETE_RECORD_SUCCESS: `${url}/DELETE_RECORD_SUCCESS`,
    DELETE_RECORD_ERROR: `${url}/DELETE_RECORD_ERROR`,
    LOAD_RECORDS_VALID_CACHE: `${url}/LOAD_RECORDS_VALID_CACHE`,
    SET_HEADERS_DATA: `${url}/SET_HEADERS_DATA`,
    LOAD_RECORDS_META_DATA: `${url}/LOAD_RECORDS_META_DATA`,
    LOAD_RECORDS_META_DATA_SUCCESS: `${url}/LOAD_RECORDS_META_DATA_SUCCESS`,
    LOAD_RECORDS_META_DATA_ERROR: `${url}/LOAD_RECORDS_META_DATA_ERROR`,
    UPDATE_STATE_FILTER: `${url}/UPDATE_STATE_FILTER`,
    CREATE_TREATMENT : `${url}/createTreatment`,
    CHANGE_STATUS: `${url}/changeStatus`,
    SET_HISTORY_RECORD_ERROR : `${url}/setHistoryError`,
    SET_HISTORY_RECORD : `${url}/setHistory`,
    SET_HISTORY_RECORD_SUCCESS : `${url}/setHistorySuccess`,
    LOAD_CASE_SUCCESS: `${url}/LOAD_CASE_SUCCESS`,
    RESET_LOGIN_ATTEMPT:`${url}/RESET_LOGIN_ATTEMPT`,
    RESET_LOGIN_ATTEMPT_SUCCESS:`${url}/RESET_LOGIN_ATTEMPT_SUCCESS`,
    RESET_LOGIN_ATTEMPT_ERROR: `${url}/RESET_LOGIN_ATTEMPT_ERROR`,
    SET_AUTHORIZATION : `${url}/SET_AUTHORIZATION`,
    GET_ALL_BODY_SYSTEMS: `${url}/GET_ALL_BODY_SYSTEMS`,
    GET_ALL_BODY_SYSTEMS_SUCCESS: `${url}/GET_ALL_BODY_SYSTEMS_SUCCESS`,
    GET_ALL_BODY_SYSTEMS_ERROR: `${url}/GET_ALL_BODY_SYSTEMS_ERROR`,
    GET_TREATMENTS_NAME : `${url}/GET_TREATMENTS_NAME`,
    GET_TREATMENTS_NAME_SUCCESS: `${url}/GET_TREATMENTS_NAME_SUCCESS`,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS: `${url}/GET_ALL_TREATMENT_BY_BODYSYSTEMS`,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS : `${url}/GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS`,
    RFA_FORM_DETAIL_CLEAR: `${url}/RFA_FORM_DETAIL_CLEAR`,
    UPDATE_POLICY_DOC_DATE: `${url}/UPDATE_POLICY_DOC_DATE`,
    LOAD_TREATMENT_DIAGNOSIS_ERROR: `${url}/LOAD_TREATMENT_DIAGNOSIS_ERROR`,
    GET_ALL_TREATMENT_ERROR: `${url}/GET_ALL_TREATMENT_ERROR`,
    GET_RFA_FORM_DETAIL_ERROR: `${url}/GET_RFA_FORM_DETAIL_ERROR`,
    SYNC: `${url}/SYNC`,
    SYNCSUCCESS: `${url}/SYNCSUCCESS`,
    SYNCERROR:`${url}/SYNCERROR`,
    SET_AUTHORIZATION_ERROR: `${url}/SET_AUTHORIZATION_ERROR`
  };
}
