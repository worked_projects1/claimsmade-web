/*
 *
 *   reducer
 *
 */

import produce from 'immer';

import merge from 'lodash/merge';

import {
  UPDATE_VERSION_ERROR
} from 'blocks/session/constants';


const initialState = {
  records: [],
  policies: {},
  addedPolicies: [],
  treatments: [],
  record: {},
  recordsMetaData: {},
  settings: {},
  administrator: {},
  loading: false,
  pageLoader: false,
  pageError: false,
  error: false,
  progress: false,
  update: false,
  lastUpdate: null,
  totalPageCount: false,
  alerting: {},
  label: "",
  value: "",
  alertIcon: [],
  headers: {
    offset: 0,
    limit: 25,
    search: false,
    filter: false,
    sort: false,
    page: 1
  },
  patients: [],
  filter: { state: 'CA' },
  bodySystemNames: [],
  treatmentRecord: [],
  treatmentNames: [],
  diagnosisList: [],
  treatmentDatas: [],
  policyDocDate: ""
};

/**
 * @param {object} constants 
 * @param {string} name 
 */
export default function reducer(constants, name, additionalConstants = {}) {

  const {
    LOAD_RECORD,
    LOAD_RECORD_SUCCESS,
    LOAD_RECORD_ERROR,
    LOAD_RECORDS,
    LOAD_RECORDS_SUCCESS,
    LOAD_RECORDS_ERROR,
    CREATE_RECORD_SUCCESS,
    CREATE_RECORD_ERROR,
    UPDATE_RECORD_SUCCESS,
    UPDATE_RECORD_ERROR,
    DELETE_RECORD,
    DELETE_RECORD_SUCCESS,
    DELETE_RECORD_ERROR,
    LOAD_RECORDS_VALID_CACHE,
    SET_HEADERS_DATA,
    LOAD_RECORDS_META_DATA_SUCCESS,
    LOAD_RECORDS_META_DATA_ERROR,
    CREATE_TREATMENT,
    SET_HISTORY_RECORD_ERROR,
    SET_HISTORY_RECORD_SUCCESS,
    LOAD_CASE_SUCCESS,
    RESET_LOGIN_ATTEMPT_ERROR,
    RESET_LOGIN_ATTEMPT_SUCCESS,
    GET_ALL_BODY_SYSTEMS_SUCCESS,
    GET_TREATMENTS_NAME_SUCCESS,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS,
    RFA_FORM_DETAIL_CLEAR,
    LOAD_TREATMENT_DIAGNOSIS_ERROR,
    GET_ALL_BODY_SYSTEMS_ERROR,
    GET_ALL_TREATMENT_ERROR,
    GET_RFA_FORM_DETAIL_ERROR,
    SYNCSUCCESS,
    SYNCERROR,
    SET_AUTHORIZATION_ERROR
  } = constants;

  const {
    LOAD_GLOBAL_SETTINGS_ERROR,
    LOAD_GLOBAL_SETTINGS_SUCCESS,
    UPDATE_GLOBAL_SETTINGS_ERROR,
    UPDATE_GLOBAL_SETTINGS_SUCCESS,
    LOAD_POC2_ERROR,
    LOAD_POC2_SUCCESS,
    LOAD_POC2_DOC_SUCCESS,
    ADD_POC2_DOC,
    LOAD_POC2_PROGRESS,
    LOAD_PRACTICE_ERROR,
    LOAD_PRACTICE_SUCCESS,
    LOAD_POC_META_SUCCESS,
    LOAD_POC_META_ERROR,
    LOAD_POC2_DOC,
    UPDATE_PRACTICE_SUCCESS,
    UPDATE_POC3_RECORD,
    UPDATE_PRACTICE_SETTINGS_ERROR,
    RWC_FORM_SUCCESS,
    RWC_FORM_ERROR,
    LOAD_PHYSICIAN_DETAIL_SUCCESS,
    LOAD_PHYSICIAN_DETAIL_ERROR,
    LOAD_ADMINISTRATOR_DETAIL_SUCCESS,
    LOAD_ADMINISTRATOR_DETAIL_ERROR,
    RWC_FORM_MERGE_SUCCESS,
    RWC_FORM_MERGE_ERROR,
    CLEAR_ADMINISTRATOR_DETAIL,
    GET_TREATMENTS_RECORD_SUCCESS,
    GET_DIAGNOSIS_BY_TREATMENT_SUCCESS,
    GET_ALL_RFARECORDS,
    CLEAR_RFAFORM_RECORD
  } = additionalConstants;


  return function recordsReducer(state = initialState, { type, id, record, records, recordsMetaData = {}, error, success, totalPageCount, policies, treatments, settings, update, headers = {
    offset: 0,
    limit: 25,
    search: false,
    filter: false,
    sort: false,
    page: 1
  }, treatmentRecord, bodySystemNames, treatmentNames, diagnosisList, treatmentDatas, policyDocDate }) {// eslint-disable-line
    return produce(state, draft => {
      switch (type) {
        case GET_ALL_RFARECORDS:
          draft.alerting = record;
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_VALID_CACHE:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS:
          draft.loading = true;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          draft.pageError = false;
          break;
        case LOAD_RECORD:
          draft.pageLoader = true;
          // draft.loading = true;
          draft.pageError = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_PHYSICIAN_DETAIL_SUCCESS:
          draft.loading = false;
          draft.pageError = false;
          draft.error = false;
          draft.settings = settings;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_ADMINISTRATOR_DETAIL_SUCCESS:
          draft.loading = false;
          draft.pageError = false;
          draft.error = false;
          draft.administrator = settings;
          draft.success = false;
          draft.progress = false;
          break;
        case CLEAR_ADMINISTRATOR_DETAIL:
          draft.loading = false;
          draft.pageError = false;
          draft.error = false;
          draft.administrator = settings;
          draft.success = false;
          draft.progress = false;
          break;

        case LOAD_PRACTICE_SUCCESS:
          draft.record = record;
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case GET_ALL_BODY_SYSTEMS_SUCCESS:
          draft.bodySystemNames = bodySystemNames;
          break;
        case LOAD_CASE_SUCCESS:
          const { result, limit } = record;// eslint-disable-line
          draft.record = Object.assign({}, draft.record, result);
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.pageError = false;
          draft.pageLoader = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_POC2_DOC:
        case LOAD_POC2_PROGRESS:
          draft.loading = true;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break
        case LOAD_RECORDS_SUCCESS:
          draft.records = records;
          draft.lastUpdate = Math.floor(Date.now() / 1000);
          draft.totalPageCount = parseInt(totalPageCount);
          draft.loading = false;
          draft.pageError = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_TREATMENT:
          draft.treatments = treatments;
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case SET_HISTORY_RECORD_SUCCESS:
          draft.treatments = treatments;
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_POC2_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_POC2_DOC_SUCCESS:
          draft.policies = policies;
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case ADD_POC2_DOC:
          draft.addedPolicies = [...draft.addedPolicies, policies]
          draft.loading = false;
          draft.error = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_META_DATA_SUCCESS:
        case LOAD_POC_META_SUCCESS:
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_RECORD_ERROR:
        case UPDATE_RECORD_ERROR:
        case DELETE_RECORD_ERROR:
        case UPDATE_GLOBAL_SETTINGS_ERROR:
        case LOAD_GLOBAL_SETTINGS_ERROR:
        case UPDATE_VERSION_ERROR:
        case RESET_LOGIN_ATTEMPT_ERROR:
        case LOAD_RECORDS_META_DATA_ERROR:
        case LOAD_PRACTICE_ERROR:
        case LOAD_POC_META_ERROR:
        case UPDATE_PRACTICE_SETTINGS_ERROR:
        case LOAD_PHYSICIAN_DETAIL_ERROR:
        case LOAD_ADMINISTRATOR_DETAIL_ERROR:
        case LOAD_TREATMENT_DIAGNOSIS_ERROR:
        case GET_ALL_BODY_SYSTEMS_ERROR:
        case GET_ALL_TREATMENT_ERROR:
        case GET_RFA_FORM_DETAIL_ERROR:
        case SET_AUTHORIZATION_ERROR:
          draft.loading = false;
          draft.error = false;
          draft.updateError = error;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_POC2_ERROR:
        case SET_HISTORY_RECORD_ERROR:
          draft.loading = false;
          draft.error = error;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORDS_ERROR:
          draft.loading = false;
          draft.error = true;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case LOAD_RECORD_ERROR:
          draft.loading = false;
          draft.pageLoader = false;
          draft.pageError = true;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case CREATE_RECORD_SUCCESS:
          draft.records = [record].concat(draft.records);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          draft.headers = headers;
          break;
        case LOAD_RECORD_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.recordsMetaData = Object.assign({}, draft.recordsMetaData, recordsMetaData);
          draft.pageError = false;
          draft.pageLoader = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case UPDATE_POC3_RECORD:
        case RWC_FORM_ERROR:
        case RWC_FORM_MERGE_SUCCESS:
        case RWC_FORM_MERGE_ERROR:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          draft.update = update;
          break;
        case RWC_FORM_SUCCESS:
        case UPDATE_RECORD_SUCCESS:
          draft.records = state.records.find(r => record.id === r.id) ? state.records.map((r) => record.id === r.id ? Object.assign({}, r, record) : Object.assign({}, r)) : state.records.concat([Object.assign({}, record)]);
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          draft.update = update;
          break;
        case DELETE_RECORD_SUCCESS:
          draft.records = draft.records.filter((r, i) => r.id !== id);// eslint-disable-line
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = false;
          break;
        case DELETE_RECORD:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = false;
          draft.progress = true;
          break;
        case RESET_LOGIN_ATTEMPT_SUCCESS:
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case LOAD_GLOBAL_SETTINGS_SUCCESS:
        case UPDATE_GLOBAL_SETTINGS_SUCCESS:
        case UPDATE_PRACTICE_SUCCESS:
          draft.record = Object.assign({}, draft.record, record);
          draft.loading = false;
          draft.error = false;
          draft.updateError = false;
          draft.success = success;
          draft.progress = false;
          break;
        case SET_HEADERS_DATA:
          draft.headers = merge({}, draft.headers, record);
          break;
        case GET_TREATMENTS_RECORD_SUCCESS:
          draft.treatmentRecord = [...draft.treatmentRecord, treatmentRecord]
          break;
        case GET_TREATMENTS_NAME_SUCCESS:
          draft.treatmentNames = treatmentNames;
          break;
        case GET_DIAGNOSIS_BY_TREATMENT_SUCCESS:
          draft.diagnosisList = [...draft.diagnosisList, diagnosisList];
          break;
        case GET_ALL_TREATMENT_BY_BODYSYSTEMS_SUCCESS:
          draft.treatmentDatas = [...draft.treatmentDatas, ...treatmentDatas];
          draft.policyDocDate = policyDocDate;
          break;
        case RFA_FORM_DETAIL_CLEAR:
          draft.treatmentDatas = [];
          draft.policyDocDate = policyDocDate;
          break;
        case SYNCSUCCESS:
          draft.success = success;
          draft.error = false;
          break;
        case SYNCERROR:
          draft.loading = false;
          draft.error = error;
          draft.success = false;
          draft.progress = false;
          break;
        case CLEAR_RFAFORM_RECORD:
          draft.record = {};
          break;
      }
    });

  };
}