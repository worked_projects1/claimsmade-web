/*
 *
 *  remotes
 *
 */

import api from 'utils/api.js';

/**
 * @param {string} name 
 * @param {object} remotesBlock 
 */
export default function (name, remotesBlock = {}) {

  let constantName = name;

  function loadRecords() {
    return api.get(`/${constantName}`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} headers 
   * @returns 
   */
  function loadRecordsWithHeader(headers, role) {
    const url = (constantName === 'users' || constantName === 'treatments' || constantName === 'bodySystem' || constantName === 'rfaForms' || constantName === 'claims_admin') ? `rest/${constantName}` : (constantName === 'vettedclaims/users' || constantName === 'practice/users') ? `rest/users?type=${role}` : (constantName === 'practices') ? `rest/practices` : (constantName === 'diagnosis') ? `rest/diagnoses` : `/${constantName}`
    return api.get((url), { headers }).then((response) => response).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} id 
   * @returns 
   */
  function loadRecord(id) {
    const url = (constantName === 'vettedclaims/users' || constantName === 'practice/users') ? `/users/${id}` : `/${constantName}/${id}`;
    return api.get(url).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} id 
   * @returns 
   */
  function loadHistory(id) {
    const url = (constantName === 'treatments') ? `getTreatmentHistories/${id}` : `/${constantName}/${id}`;
    return api.get(url).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function createRecord(record) {
    const url = (constantName === 'vettedclaims/users' || constantName === 'practice/users') ? `rest/users` : (constantName === 'users' || constantName === 'practices' || constantName === 'bodySystem' || constantName === 'claims_admin') ? `rest/${constantName}` : constantName === 'treatments' ? (!record.user ? `/${constantName}` : `/treatments/getDownloadUrl`) : `/${constantName}`;

    const result = (constantName == "treatments") ? record.doc : record
    return api.post(url, result).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {object} record 
   * @returns 
   */
  function updateRecord(record) {
    let recordData = record;
    if (constantName == "users" || constantName == "vettedclaims/users" || constantName === 'practice/users') {
      recordData = Object.assign({}, { name: record.name, password: record.password, role: record.role, is_admin: record.is_admin })
      recordData = Object.assign({}, { user: record })
    } else if (constantName == "treatments") {
      recordData = record.doc
    }
    const url = (constantName === 'vettedclaims/users' || constantName === 'practice/users') ? `/users` : constantName === 'treatments' ? `/${constantName}` : `/${constantName}`

    return api.put(`${url}/${record.id}`, recordData).then((response) => response.data).catch((error) => Promise.reject(error));
  }


  /**
   * 
   * @param {integer} id 
   * @returns 
   */
  function deleteRecord(id) {
    const url = (constantName === 'vettedclaims/users' || constantName === 'practice/users') ? `/users` : `/${constantName}`
    return api.delete(`${url}/${id}`).catch((error) => Promise.reject(error));
  }

  /**
   * 
   * @param {integer} id 
   * @returns 
   */
  function changeStatus(id, status) {
    const url = `/${constantName}`;
    if (constantName == 'treatments') {
      return api.post(`updateTreatmentStatus/${id}`, status).catch((error) => Promise.reject(error));
    } else {
      return api.delete(`${url}/${id}`).catch((error) => Promise.reject(error));
    }
  }
  /** 
  * 
  *@param {array} record
  */
  function resetLoginAttempt(record) {
    return api.put(`/rest/users/reset-login-attempt`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function loadAdministrator(record) {// eslint-disable-line
    return api.get(`claims_admin/getClaimsUsersNames`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function setAuthorize(headers) {
    return api.post(`users/updateAuthorization`, headers).catch((error) => Promise.reject(error));
  }

  function getAllBodySystems() {
    return api.get(`rest/getAllBodySystemNames`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function getTreatmentRecords(record) {
    return api.post(`rest/treatments/getAllTreatmentNames`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function getAllTreatmentByBodySystems(record) {
    return api.post(`/rest/treatments/getAllTreatmentByBodySystems`, record).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function updateAPIForBodySysSync() {
    return api.get(`rest/updateAPIForBodySysSync`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function updateAPIForTreatmentSync() {
    return api.get(`rest/updateAPIForTreatmentSync`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  function updateAPIForDiagnosisSync() {
    return api.get(`rest/updateAPIForDiagnosisSync`).then((response) => response.data).catch((error) => Promise.reject(error));
  }

  return {
    loadRecords,
    loadRecordsWithHeader,
    loadRecord,
    loadHistory,
    createRecord,
    updateRecord,
    deleteRecord,
    changeStatus,
    resetLoginAttempt,
    loadAdministrator,
    setAuthorize,
    getAllBodySystems,
    getTreatmentRecords,
    getAllTreatmentByBodySystems,
    updateAPIForBodySysSync,
    updateAPIForTreatmentSync,
    updateAPIForDiagnosisSync,
    ...remotesBlock
  }

}