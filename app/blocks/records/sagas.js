
/*
 *
 *  sagas
 *
 */

import { push } from 'react-router-redux';
import { call, take, put, race, select, all, delay } from 'redux-saga/effects';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import { selectUser } from 'blocks/session/selectors';
import appRemotes from './remotes';
import history from 'utils/history';
import {
  DEFAULT_CREATE_RECORD_ERROR,
  DEFAULT_UPDATE_RECORD_ERROR,
  DEFAULT_DELETE_RECORD_ERROR,
  DEFAULT_STATUS_CHANGE,
  DEFAULT_RESET_LOGIN_ATTEMPT_ERROR
} from 'utils/errors';
import validation from './validation'

export default function sagas(constants, actions, remotes, selectors, entityUrl, additionalSaga) {// eslint-disable-line

  const {
    LOAD_RECORD,
    LOAD_RECORDS,
    CREATE_RECORD,
    UPDATE_RECORD,
    DELETE_RECORD,
    LOAD_RECORDS_META_DATA,
    CREATE_TREATMENT,
    CHANGE_STATUS,
    RESET_LOGIN_ATTEMPT,
    SET_AUTHORIZATION,
    GET_ALL_BODY_SYSTEMS,
    GET_TREATMENTS_NAME,
    GET_ALL_TREATMENT_BY_BODYSYSTEMS,
    SYNC
  } = constants;

  const {
    loadRecordSuccess,
    loadRecordError,
    loadRecords: loadRecordsAction,
    loadRecord: loadRecordAction,// eslint-disable-line
    loadRecordsSuccess,
    loadRecordsError,
    createRecordSuccess,
    createRecordError,
    updateRecordSuccess,
    updateRecordError,
    deleteRecordSuccess,
    deleteRecordError,
    loadRecordsCacheHit,
    loadRecordsMetaData,
    loadRecordsMetaDataSuccess,
    loadRecordsMetaDataError,
    loadCasesSuccess,
    resetLoginAttemptError,
    resetLoginAttemptSuccess,
    getAllBodySystemSuccess,
    getTreatmentsNameSuccess,
    getAllTreatmentByBodySystemsSuccess, // eslint-disable-line
    rfaFormDetailClear,
    loadAllTreatmentDiagnosisError,
    getAllBodySystemError,
    getAllTreatmentError,
    getRFAFormDetailError,
    syncError,
    syncSuccess,
    setAuthorizeError
    // setHistoryError
  } = actions;

  const {
    loadRecord,
    // loadRecords,
    loadRecordsWithHeader,
    createRecord,
    updateRecord,
    deleteRecord,
    changeStatus,
    resetLoginAttempt,
    // loadAdministrator,
    setAuthorize,
    getAllBodySystems,
    getTreatmentRecords,
    getAllTreatmentByBodySystems, // eslint-disable-line
    updateAPIForBodySysSync,
    updateAPIForTreatmentSync,
    updateAPIForDiagnosisSync,
  } = remotes;

  const {
    // selectRecord,
    selectRecords,
    selectUpdateTimestamp,
    selectHeaders,
    selectAllTreatmentsAndDiagnosis,
    selectPolicyDocUpdated
  } = selectors;




  function* loadRecordsSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const load = yield race({
        explicitLoad: take(LOAD_RECORDS),
      });
      const { explicitLoad } = load;
      const { invalidateCache, queryParams } = explicitLoad || {};
      const lastLoad = yield select(selectUpdateTimestamp());
      const currentTimestamp = Math.floor(Date.now() / 1000);
      const VALID_CACHE_DIFF = entityUrl === 'cases1' ? -240 : -30;

      // if(entityUrl != 'rfaForms'){
      yield put(loadRecordsMetaData());// Calling Dropdown API's
      // }
      if (explicitLoad) {
        if (!invalidateCache && (lastLoad && (lastLoad - currentTimestamp) > VALID_CACHE_DIFF)) {
          yield put(loadRecordsCacheHit());
        } else {
          try {
            const headers = yield select(selectHeaders());
            const result = yield call(loadRecordsWithHeader, headers, queryParams)
            if (result) {
              // yield put(loadRecordsSuccess(result));
              if (entityUrl == "diagnosis") {
                yield put(loadRecordsSuccess(result.data.data, result.headers && result.headers.totalpagecount || false))
                yield put({ type: CREATE_TREATMENT, treatments: result.data.treatments });
              } else {
                yield put(loadRecordsSuccess(result.data, result.headers && result.headers.totalpagecount || false))
              }

            } else {
              yield put(loadRecordsError());
            }
          } catch (error) {
            yield put(loadRecordsError(error));
          }
        }
      }
    }
  }

  function* loadRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const loadRequest = yield race({
        request: take(LOAD_RECORD),
      });
      const { request } = loadRequest;
      if (request) {
        const { id } = request;
        try {

          const record = yield call(loadRecord, id);
          // var result;
          // if (entityUrl == "treatments") {
          //   result = yield call(loadHistory, id);
          //   if (Array.isArray(result)) {
          //     yield put(setHistorySuccess(result));
          //   } else {
          //     yield put(setHistoryError());
          //   }
          // }

          let recordsMetaData = {};
          if (record) {
            // Delays the dispatch of loadRecordSuccess untill the store is populated with an initial list of records.
            if (entityUrl != 'rfaForms') {
              while (true) { // eslint-disable-line no-constant-condition
                const recordsInStore = yield select(selectRecords());
                if (recordsInStore && recordsInStore.length > 0) {
                  break;
                }
                yield delay(500);
              }
              yield put(loadRecordSuccess(record, recordsMetaData));
            }
            else if (entityUrl == 'rfaForms') {
              const user = yield select(selectUser());
              const policyDocUpdated = yield select(selectPolicyDocUpdated());
              if (record && (record.policy_doc_updated_date == undefined || (record.policy_doc_updated_date && record.policy_doc_updated_date != policyDocUpdated))) {
                yield put(rfaFormDetailClear(record.policy_doc_updated_date));
              }
              if (record && record.form_doc) {
                const formDoc = record.form_doc; // eslint-disable-line
                const globalData = yield select(selectAllTreatmentsAndDiagnosis());
                const formData = (formDoc[`treatments`] || []).filter(formval => formval.bodySystemId);
                const uniqueIds = [];
                const filteredData = formData.filter(element => {
                  const isDuplicate = uniqueIds.includes(element.bodySystemId);
                  if (!isDuplicate) {
                    uniqueIds.push(element.bodySystemId);
                    return true;
                  }
                  return false;
                });
                let newArr = []
                const existedData = filteredData.map(val => val.bodySystemId);
                const Data = globalData.filter((ele, ind) => ind === globalData.findIndex(elem => elem.bodySystemId === ele.bodySystemId)).sort((a, b) => (a.bodySystemId).localeCompare(b.bodySystemId));

                if (Data.length > 0) {
                  const globalData1 = Data.map((val) => val.bodySystemId);
                  filteredData.map(val => {
                    if (!globalData1.includes(val.bodySystemId)) {
                      newArr.push(val.bodySystemId)
                    }
                  })
                } else {
                  newArr = existedData;
                }
                if (newArr.length != 0) {
                  try {
                    const resultData = yield call(getAllTreatmentByBodySystems, Object.assign({}, { "bodySystemIds": newArr }));
                    if (resultData) {
                      yield put(getAllTreatmentByBodySystemsSuccess(resultData, user.policy_doc_updated_date));
                    }
                  } catch (err) {
                    yield put(loadAllTreatmentDiagnosisError(err));
                  }
                }
              }
              yield put(loadCasesSuccess({ result: record, limit: user['rfa_form_row_limit'] }));
            }
          }
        } catch (error) {
          yield put(loadRecordError(error));
        }
      }
    }
  }

  function* createRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { create } = yield race({
        create: take(CREATE_RECORD),
      });
      const { record, form, fun } = create || {};
      if (create) {
        yield put(startSubmit(form));
        const sessionUser = yield select(selectUser());
        const redirectURL = entityUrl == "vettedclaims/users" || entityUrl == "practice/users" ? `/userCategories/rest/${entityUrl}` : entityUrl == "treatments" && sessionUser.email != "siva@miotiv.com" ? (record.id != undefined ? `/${entityUrl}/${record.id}/edit` : `/${entityUrl}/create`) : `/${entityUrl}`;
        try {
          yield call(validation, record, Object.assign({}, { user: sessionUser }));
          const result = yield call(createRecord, record);
          if (result) {
            if (entityUrl == "treatments" && record.user) {
              window.open(result.publicUrl, '_blank')
            }
            yield put(createRecordSuccess(result));
            yield put(stopSubmit(form));
            if (entityUrl == 'rfaForms' && history.location.pathname != "/claims") {
              yield put(push({ pathname: `/rfaForms/${result.id}/poc3`, state: Object.assign({}, { ...history.location.state }, { createPoc3: true }) }));
            }
            else if (history.location.pathname == "/claims") {
              if (fun) {
                fun(result)
              }
            }
            else {
              yield put(push({ pathname: redirectURL, state: history.location.state }));
            }
            if (entityUrl == "vettedclaims/users") {
              yield put(loadRecordsAction(true, "admin"));
            } else if (entityUrl == "practice/users") {
              yield put(loadRecordsAction(true, "practice"));
            } else if (entityUrl != 'rfaForms') {
              yield put(loadRecordsAction(true));
            }
          } else {
            yield put(createRecordError(DEFAULT_CREATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_CREATE_RECORD_ERROR }));
          }

        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CREATE_RECORD_ERROR;
          yield put(createRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }

  function* editRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { edit } = yield race({
        edit: take(UPDATE_RECORD),
      });
      const { record, form, posLoader } = edit || {};
      if (edit) {
        if (entityUrl != "rfaForms") yield put(startSubmit(form));
        const sessionUser = yield select(selectUser());
        const redirectURL = entityUrl == "vettedclaims/users" || entityUrl == "practice/users" ? `/userCategories/rest/${entityUrl}` : entityUrl == "treatments" && sessionUser.email != "siva@miotiv.com" ? `/${entityUrl}/${record.id}/edit` : entityUrl == "rfaForms" ? `/rfaForms/${record.id}/poc3` : `/${entityUrl}`;

        try {
          yield call(validation, record, Object.assign({}, { user: sessionUser }));
          const result = yield call(updateRecord, record);

          if (result) {
            if (entityUrl == "rfaForms") {
              yield put(updateRecordSuccess(result, { updateSuccess: form == 'saveForm' && 'Form Saved' || form == 'clearForm' && 'Form Cleared' || 'Patient Info Updated' }));
              if (posLoader) {
                posLoader();
              }

            } else {
              yield put(updateRecordSuccess(result));
              if (posLoader) {
                posLoader();
              }
            }
            if (entityUrl != "rfaForms") {

              yield put(push({ pathname: redirectURL, state: history.location.state }));
            }
            if (entityUrl == "vettedclaims/users") {
              yield put(loadRecordsAction(true, "admin"));
            } else if (entityUrl == "practice/users") {
              yield put(loadRecordsAction(true, "practice"));
            } else if (entityUrl != 'rfaForms') {
              yield put(loadRecordsAction(true));
            }
            if (entityUrl != "rfaForms") yield put(stopSubmit(form));
          } else {
            yield put(updateRecordError(DEFAULT_UPDATE_RECORD_ERROR));
            yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_RECORD_ERROR }));
          }

        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_RECORD_ERROR;
          yield put(updateRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }


  function* deleteRecordSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { del } = yield race({
        del: take(DELETE_RECORD),
      });
      const { id, form } = del || {};
      if (del) {
        yield put(startSubmit(form));
        const redirectURL = entityUrl == "vettedclaims/users" || entityUrl == "practice/users" ? `/userCategories/rest/${entityUrl}` : entityUrl == "rfaForms" ? `/rfaForms` : `/${entityUrl}`;
        try {
          yield call(deleteRecord, id);
          yield put(deleteRecordSuccess(id));
          yield put(stopSubmit(form));
          yield put(push({ pathname: redirectURL, state: history.location.state }));
          if (entityUrl == "vettedclaims/users") {
            yield put(loadRecordsAction(true, "admin"));
          } else if (entityUrl == "practice/users") {
            yield put(loadRecordsAction(true, "practice"));
          } else {
            yield put(loadRecordsAction(true));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_DELETE_RECORD_ERROR;
          yield put(deleteRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }

  function* changeStatusSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { del } = yield race({
        del: take(CHANGE_STATUS),
      });
      const { id, status, form } = del || {};
      if (del) {
        yield put(startSubmit(form));
        const redirectURL = `/${entityUrl}`;
        try {
          yield call(changeStatus, id, status);
          yield put(deleteRecordSuccess(id));
          yield put(stopSubmit(form));
          yield put(push({ pathname: redirectURL, state: history.location.state }));
          yield put(loadRecordsAction(true));
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_STATUS_CHANGE;
          yield put(deleteRecordError(Err));
          yield put(stopSubmit(form, { _error: Err }));
        }
      }
    }
  }

  function* loadRecordsMetaDataSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const loadMetaData = yield race({// eslint-disable-line
        metaData: take(LOAD_RECORDS_META_DATA),
      });
      try {

        let recordsMetaData = {};
        if (entityUrl === 'practice/users') {
          const practicesRemotes = yield call(appRemotes, 'rest/practicesNames');
          const practices = yield call(practicesRemotes.loadRecords);

          recordsMetaData = { practices };
        } else if (entityUrl === 'treatments') {
          const diagnosisRemotes = yield call(appRemotes, 'rest/diagnosisNames');
          const diagnosis = yield call(diagnosisRemotes.loadRecords);
          const bodySystemRemotes = yield call(appRemotes, 'rest/bodySystem');
          const bodySystem = yield call(bodySystemRemotes.loadRecords);
          recordsMetaData = { diagnosis, bodySystem };
        }
        yield put(loadRecordsMetaDataSuccess(recordsMetaData));
      } catch (error) {
        yield put(loadRecordsMetaDataError(error));
      }
    }
  }

  function* resetLoginAttemptSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { reset } = yield race({
        reset: take(RESET_LOGIN_ATTEMPT),
      });
      const { record } = reset || {};
      if (record) {
        try {
          const result = yield call(resetLoginAttempt, record);
          if (result) {
            yield put(resetLoginAttemptSuccess('Login Attempt cleared successfully'));
          } else {
            yield put(resetLoginAttemptError(DEFAULT_RESET_LOGIN_ATTEMPT_ERROR));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_RESET_LOGIN_ATTEMPT_ERROR;
          yield put(resetLoginAttemptError(Err));
        }
      }
    }
  }

  function* setAuthorizationStatus() {
    while (true) {
      const { record } = yield race({
        record: take(SET_AUTHORIZATION),
      });

      if (record) {
        try {
          const result = yield call(setAuthorize, { "staff_id": record.id, "authType": record.status }) // eslint-disable-line
          if (result) {
            yield put(loadRecordsAction(true));
          }
        } catch (error) {
          const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Authorize Physician error";
          yield put(setAuthorizeError(Err));
        }
      }
    }
  }

  function* getAllBodySystemSaga() {
    while (true) {
      const data = yield race({// eslint-disable-line
        data: take(GET_ALL_BODY_SYSTEMS),
      });
      try {
        const bodySystems = yield call(getAllBodySystems);
        if (bodySystems) {
          yield put(getAllBodySystemSuccess(bodySystems))
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Getting Body Systems error";
        yield put(getAllBodySystemError(Err));
      }
    }
  }

  function* getTreatmentsRecordsSaga() {
    while (true) {
      const { bodySystem } = yield race({
        bodySystem: take(GET_TREATMENTS_NAME),
      });
      const { bodySystemId } = bodySystem;
      try {
        const result = yield call(getTreatmentRecords, Object.assign({}, { "body_system_id": bodySystemId }));
        if (result) {
          yield put(getTreatmentsNameSuccess(result))
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Getting Treatments error";
        yield put(getAllTreatmentError(Err));
      }
    }
  }

  function* getRfaFormDetailSaga() {
    while (true) { // eslint-disable-line no-constant-condition
      const { bodySystem } = yield race({
        bodySystem: take(GET_ALL_TREATMENT_BY_BODYSYSTEMS),
      });
      const { id, loadRecord } = bodySystem;
      try {
        const result = yield call(getAllTreatmentByBodySystems, Object.assign({}, { "bodySystemIds": [id] }));
        if (result) {
          const user = yield select(selectUser());
          yield put(getAllTreatmentByBodySystemsSuccess(result, user.policy_doc_updated_date));
          if (loadRecord) {
            loadRecord();
          }
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Getting RFA Form Detail Error";
        yield put(getRFAFormDetailError(Err));
      }
    }
  }

  function* syncSaga() {
    while (true) {
      const { data } = yield race({ // eslint-disable-line
        data: take(SYNC),
      });
      try {
        const bodySystemSyncData = yield call(updateAPIForBodySysSync);
        if (bodySystemSyncData) {
          const treatmentSyncData = yield call(updateAPIForTreatmentSync);
          if (treatmentSyncData) {
            const diagnosisSyncData = yield call(updateAPIForDiagnosisSync);
            if (diagnosisSyncData) {
              const data1 = Object.assign({}, { "syncSuccess": diagnosisSyncData.Message });
              yield put(syncSuccess(data1));
              if (data.func) data.func();
              yield delay(1000);
              yield put(loadRecordsAction(true));
            }
          }
        }
        else {
          yield put(syncError({ syncError: "Sync Failed" }));
          if (data.func) data.func();
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error ? error.response.data.error : "Sync Failed";
        yield put(syncError({ syncError: Err }));
        if (data.func) data.func();
      }
    }
  }

  function* recordsSaga() {
    yield all([
      loadRecordSaga(),
      loadRecordsSaga(),
      createRecordSaga(),
      editRecordSaga(),
      deleteRecordSaga(),
      changeStatusSaga(),
      loadRecordsMetaDataSaga(),
      resetLoginAttemptSaga(),
      setAuthorizationStatus(),
      getAllBodySystemSaga(),
      getTreatmentsRecordsSaga(),
      getRfaFormDetailSaga(),
      syncSaga()
    ])
  }

  return function* rootSaga() {
    yield all([
      recordsSaga(),
      additionalSaga()
    ])
  }
}