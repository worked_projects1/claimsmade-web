/*
 *
 * Session reducer
 *
 */

import produce from 'immer';

import {
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  LOG_IN_SUCCESS,
  LOG_OUT_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT_ERROR,
  SIGN_UP_SUCCESS,
  SIGN_UP_ERROR,
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  CHANGE_PASSWORD_SUCCESS,
  REQUEST_DEMO_ERROR,
  REQUEST_DEMO_SUCCESS,
  UPDATE_VERSION_SUCCESS,
  SESSION_TIMEOUT_ERROR,
  SESSION_TIMEOUT_SUCCESS,
  SESSION_LOGIN_ERROR,
  SESSION_LOGIN_SUCCESS,
  VERIFY_OTP_ERROR,
  VERIFY_OTP_SUCCESS,
  RESEND_OTP_ERROR,
  RESEND_OTP_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_ERROR,
  CLEAR_CACHE,
  SESSION_EXPAND,
  SESSION_CLEAR_TIMEOUT_SUCCESS,
  SESSION_CLEAR_TIMEOUT_ERROR,
  SESSION_TOKEN_SUCCESS,
  SESSION_TOKEN_ERROR,
  UPDATE_SIGNATURE_SUCCESS,
  UPDATE_SIGNATURE_ERROR,
  LOAD_APP_VERSION_SUCCESS
} from './constants';


const initialState = { error: {}, success: {}, activeSession: 1, secret: false, version: '1.0', expand: true, timeout: false, loading: false, metaData: {}, appVersion: false, rfaOptions: [] };

/**
 * @param {object} state 
 * @param {object} action 
 */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case VERIFY_SESSION_SUCCESS:
        draft.loggedIn = true;
        draft.user = action.user;
        draft.loading = false;
        break;
      case LOG_IN_SUCCESS:
      case SIGN_UP_SUCCESS:
      case VERIFY_OTP_SUCCESS:
        draft.loggedIn = true;
        draft.user = action.user;
        draft.secret = action.token;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case SESSION_TOKEN_SUCCESS:
        draft.secret = action.token;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case VERIFY_SESSION_ERROR:
      case LOG_IN_ERROR:
      case SIGN_UP_ERROR:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = { login: action.error };
        draft.success = {};
        draft.loading = false;
        break;
      case VERIFY_OTP_ERROR:
      case RESEND_OTP_ERROR:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = { verifyOtp: action.error };
        draft.success = {};
        draft.loading = false;
        break;
      case LOG_OUT_ERROR:
      case LOG_OUT_SUCCESS:
        draft.loggedIn = false;
        draft.user = false;
        draft.secret = false;
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case FORGOT_PASSWORD_ERROR:
        draft.error = { forgot: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case FORGOT_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { forgot: action.success };
        draft.loading = false;
        break;
      case RESET_PASSWORD_ERROR:
        draft.error = { reset: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case RESET_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { reset: action.success };
        draft.loading = false;
        break;
      case RESEND_OTP_SUCCESS:
        draft.error = {}
        draft.success = { verifyOtp: action.success };
        draft.loading = false;
        break;
      case CHANGE_PASSWORD_ERROR:
        draft.error = { changePassword: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case CHANGE_PASSWORD_SUCCESS:
        draft.error = {}
        draft.success = { changePassword: action.success };
        draft.loading = false;
        break;
      case REQUEST_DEMO_ERROR:
      case SESSION_TIMEOUT_ERROR:
      case SESSION_LOGIN_ERROR:
      case SESSION_CLEAR_TIMEOUT_ERROR:
      case SESSION_TOKEN_ERROR:
        draft.error = { [action.form]: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case REQUEST_DEMO_SUCCESS:
        draft.error = {}
        draft.success = { [action.form]: action.success };
        draft.loading = false;
        break;
      case UPDATE_VERSION_SUCCESS:
        draft.error = {}
        draft.user.version = action.version;
        draft.loading = false;
        break;
      case UPDATE_SIGNATURE_SUCCESS:
        draft.error = {}
        draft.success = { signature: action.success };
        draft.loading = false;
        break;
      case UPDATE_SIGNATURE_ERROR:
        draft.error = {}
        draft.success = { signature: action.error };
        draft.loading = false;
        break;
      case CLEAR_CACHE:
        draft.error = {};
        draft.success = {};
        draft.loading = false;
        break;
      case SESSION_TIMEOUT_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.timeout = action.timeout;
        draft.loading = false;
        break;
      case SESSION_CLEAR_TIMEOUT_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.timeout = false;
        draft.loading = false;
        break;
      case SESSION_LOGIN_SUCCESS:
        draft.error = {};
        draft.success = {};
        draft.activeSession = action.duration;
        draft.loading = false;
        break;
      case SESSION_EXPAND:
        draft.expand = action.expand;
        draft.loading = false;
        break;
      case TWO_FACTOR_AUTHENTICATION_ERROR:
        draft.error = { twoFactorAuthentication: action.error }
        draft.success = {};
        draft.loading = false;
        break;
      case TWO_FACTOR_AUTHENTICATION_SUCCESS:
        draft.user = action.user;
        draft.error = {}
        draft.success = { twoFactorAuthentication: action.success };
        draft.loading = false;
        break;
      case LOAD_APP_VERSION_SUCCESS:
        draft.loading = false;
        draft.success = {};
        draft.error = {};
        draft.appVersion = action.appVersion;
        break;
    }
  });


export default appReducer;
