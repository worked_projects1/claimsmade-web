/*
 *
 * Session constants
 *
 */

export const VERIFY_SESSION = 'lg/session/VERIFY_SESSION';
export const VERIFY_SESSION_SUCCESS = 'lg/session/VERIFY_SESSION_SUCCESS';
export const VERIFY_SESSION_ERROR = 'lg/session/VERIFY_SESSION_ERROR';

export const LOG_IN = 'lg/session/LOG_IN';
export const LOG_IN_SUCCESS = 'lg/session/LOG_IN_SUCCESS';
export const LOG_IN_ERROR = 'lg/session/LOG_IN_ERROR';

export const LOG_OUT = 'lg/session/LOG_OUT';
export const LOG_OUT_SUCCESS = 'lg/session/LOG_OUT_SUCCESS';
export const LOG_OUT_ERROR = 'lg/session/LOG_OUT_ERROR';

export const SIGN_UP = 'lg/session/SIGN_UP';
export const SIGN_UP_SUCCESS = 'lg/session/SIGN_UP_SUCCESS';
export const SIGN_UP_ERROR = 'lg/session/SIGN_UP_ERROR';

export const FORGOT_PASSWORD = 'lg/session/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCESS = 'lg/session/FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'lg/session/FORGOT_PASSWORD_ERROR';

export const RESET_PASSWORD = 'lg/session/RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'lg/session/RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_ERROR = 'lg/session/RESET_PASSWORD_ERROR';

export const CHANGE_PASSWORD = 'lg/session/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'lg/session/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_ERROR = 'lg/session/CHANGE_PASSWORD_ERROR';

export const REQUEST_DEMO = 'lg/session/REQUEST_DEMO';
export const REQUEST_DEMO_SUCCESS = 'lg/session/REQUEST_DEMO_SUCCESS';
export const REQUEST_DEMO_ERROR = 'lg/session/REQUEST_DEMO_ERROR';

export const UPDATE_VERSION = 'lg/session/UPDATE_VERSION';
export const UPDATE_VERSION_SUCCESS = 'lg/session/UPDATE_VERSION_SUCCESS';
export const UPDATE_VERSION_ERROR = 'lg/session/UPDATE_VERSION_ERROR';

export const SESSION_TIMEOUT = 'lg/session/SESSION_TIMEOUT';
export const SESSION_TIMEOUT_SUCCESS = 'lg/session/SESSION_TIMEOUT_SUCCESS';
export const SESSION_TIMEOUT_ERROR = 'lg/session/SESSION_TIMEOUT_ERROR';

export const SESSION_LOGIN = 'lg/session/SESSION_LOGIN';
export const SESSION_LOGIN_SUCCESS = 'lg/session/SESSION_LOGIN_SUCCESS';
export const SESSION_LOGIN_ERROR = 'lg/session/SESSION_LOGIN_ERROR';

export const VERIFY_OTP = 'lg/session/VERIFY_OTP';
export const VERIFY_OTP_SUCCESS = 'lg/session/VERIFY_OTP_SUCCESS';
export const VERIFY_OTP_ERROR = 'lg/session/VERIFY_OTP_ERROR';

export const RESEND_OTP = 'lg/session/RESEND_OTP';
export const RESEND_OTP_SUCCESS = 'lg/session/RESEND_OTP_SUCCESS';
export const RESEND_OTP_ERROR = 'lg/session/RESEND_OTP_ERROR';

export const TWO_FACTOR_AUTHENTICATION = 'lg/session/TWO_FACTOR_AUTHENTICATION';
export const TWO_FACTOR_AUTHENTICATION_SUCCESS = 'lg/session/TWO_FACTOR_AUTHENTICATION_SUCCESS';
export const TWO_FACTOR_AUTHENTICATION_ERROR = 'lg/session/TWO_FACTOR_AUTHENTICATION_ERROR';

export const ROUTER = '@@router/LOCATION_CHANGE';
export const CLEAR_CACHE = '@@router/CLEAR_CACHE';
export const SESSION_EXPAND = '@@router/SESSION_EXPAND';

export const SESSION_CLEAR_TIMEOUT = 'lg/session/SESSION_CLEAR_TIMEOUT';
export const SESSION_CLEAR_TIMEOUT_SUCCESS = 'lg/session/SESSION_CLEAR_TIMEOUT_SUCCESS';
export const SESSION_CLEAR_TIMEOUT_ERROR = 'lg/session/SESSION_CLEAR_TIMEOUT_ERROR';

export const SESSION_TOKEN = 'lg/session/SESSION_TOKEN';
export const SESSION_TOKEN_SUCCESS = 'lg/session/SESSION_TOKEN_SUCCESS';
export const SESSION_TOKEN_ERROR = 'lg/session/SESSION_TOKEN_ERROR';

export const UPDATE_SIGNATURE = 'lg/session/UPDATE_SIGNATURE';
export const UPDATE_SIGNATURE_SUCCESS = 'lg/session/UPDATE_SIGNATURE_SUCCESS';
export const UPDATE_SIGNATURE_ERROR = 'lg/session/UPDATE_SIGNATURE_ERROR';

export const LOAD_APP_VERSION = 'lg/session/LOAD_APP_VERSION';
export const LOAD_APP_VERSION_SUCCESS = 'lg/session/LOAD_APP_VERSION_SUCCESS';
export const LOAD_APP_VERSION_ERROR = 'lg/session/LOAD_APP_VERSION_ERROR';
