/*
 *
 *  session
 *
 */

import actions from './actions';
import reducer from './reducer';
import saga from './sagas';
import remotes from './remotes';
import selectors from './selectors';


export default {
    name: 'session',
    actions,
    reducer,
    saga,
    remotes,
    selectors
}