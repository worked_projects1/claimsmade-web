/*
 *
 * Session actions
 *
 */

import {
  VERIFY_SESSION,
  VERIFY_SESSION_SUCCESS,
  VERIFY_SESSION_ERROR,
  LOG_IN,
  LOG_IN_SUCCESS,
  LOG_IN_ERROR,
  LOG_OUT,
  LOG_OUT_SUCCESS,
  LOG_OUT_ERROR,
  SIGN_UP,
  SIGN_UP_SUCCESS,
  SIGN_UP_ERROR,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  REQUEST_DEMO,
  REQUEST_DEMO_SUCCESS,
  REQUEST_DEMO_ERROR,
  UPDATE_VERSION,
  UPDATE_VERSION_SUCCESS,
  UPDATE_VERSION_ERROR,
  SESSION_TIMEOUT,
  SESSION_TIMEOUT_SUCCESS,
  SESSION_TIMEOUT_ERROR,
  SESSION_LOGIN,
  SESSION_LOGIN_SUCCESS,
  SESSION_LOGIN_ERROR,
  VERIFY_OTP,
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_ERROR,
  RESEND_OTP,
  RESEND_OTP_SUCCESS,
  RESEND_OTP_ERROR,
  CLEAR_CACHE,
  SESSION_EXPAND,
  TWO_FACTOR_AUTHENTICATION,
  TWO_FACTOR_AUTHENTICATION_SUCCESS,
  TWO_FACTOR_AUTHENTICATION_ERROR,
  SESSION_CLEAR_TIMEOUT,
  SESSION_CLEAR_TIMEOUT_SUCCESS,
  SESSION_CLEAR_TIMEOUT_ERROR,
  SESSION_TOKEN,
  SESSION_TOKEN_SUCCESS,
  SESSION_TOKEN_ERROR,
  UPDATE_SIGNATURE,
  UPDATE_SIGNATURE_SUCCESS,
  UPDATE_SIGNATURE_ERROR,
  LOAD_APP_VERSION,
  LOAD_APP_VERSION_SUCCESS,
  LOAD_APP_VERSION_ERROR
} from './constants';

/**
 * @param {string} secret 
 */
export function verifySession(secret) {
  return {
    type: VERIFY_SESSION,
    secret
  };
}

/**
 * @param {object} user 
 */
export function verifySessionSuccess(user) {
  return {
    type: VERIFY_SESSION_SUCCESS,
    user
  };
}

/**
 * @param {string} error 
 */
export function verifySessionError(error) {
  return {
    type: VERIFY_SESSION_ERROR,
    error,
  };
}

/**
 * @param {string} identifier 
 * @param {string} secret 
 * @param {object} queryParams 
 * @param {string} form 
 */
export function logIn(identifier, secret, queryParams, form) {
  return {
    type: LOG_IN,
    identifier,
    secret,
    queryParams,
    form,
  };
}

/**
 * @param {object} user 
 * @param {string} token 
 */
export function logInSuccess(user, token) {
  return {
    type: LOG_IN_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function logInError(error) {
  return {
    type: LOG_IN_ERROR,
    error,
  };
}

export function logOut() {
  return {
    type: LOG_OUT,
  };
}

export function logOutSuccess() {
  return {
    type: LOG_OUT_SUCCESS,
  };
}

/**
 * @param {string} error 
 */
export function logOutError(error) {
  return {
    type: LOG_OUT_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function signUp(record, form) {
  return {
    type: SIGN_UP,
    record,
    form,
  };
}

/**
 * @param {object} user 
 * @param {string} token 
 */
export function signUpSuccess(user, token) {
  return {
    type: SIGN_UP_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function signUpError(error) {
  return {
    type: SIGN_UP_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function forgotPassword(record, form) {
  return {
    type: FORGOT_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function forgotPasswordSuccess(success) {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function forgotPasswordError(error) {
  return {
    type: FORGOT_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function resetPassword(record, form) {
  return {
    type: RESET_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function resetPasswordSuccess(success) {
  return {
    type: RESET_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function resetPasswordError(error) {
  return {
    type: RESET_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function changePassword(record, form) {
  return {
    type: CHANGE_PASSWORD,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function changePasswordSuccess(success) {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function changePasswordError(error) {
  return {
    type: CHANGE_PASSWORD_ERROR,
    error,
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function requestDemo(record, form) {
  return {
    type: REQUEST_DEMO,
    record,
    form
  };
}

/**
 * @param {string} form 
 * @param {string} success 
 */
export function requestDemoSuccess(form, success) {
  return {
    type: REQUEST_DEMO_SUCCESS,
    form,
    success
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function requestDemoError(form, error) {
  return {
    type: REQUEST_DEMO_ERROR,
    form,
    error
  };
}

/**
 * @param {object} record 
 * @param {string} form 
 */
export function updateVersion(record, form) {
  return {
    type: UPDATE_VERSION,
    record,
    form
  };
}

/**
 * @param {integer} version 
 */
export function updateVersionSuccess(version) {
  return {
    type: UPDATE_VERSION_SUCCESS,
    version
  };
}

/**
 * @param {string} error 
 */
export function updateVersionError(error) {
  return {
    type: UPDATE_VERSION_ERROR,
    error
  };
}

/**
 * @param {function} dispatch 
 */
export function sessionTimeout(dispatch) {
  return {
    type: SESSION_TIMEOUT,
    dispatch
  };
}

/**
 * @param {function} timeout 
 */
export function sessionTimeoutSuccess(timeout) {
  return {
    type: SESSION_TIMEOUT_SUCCESS,
    timeout
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function sessionTimeoutError(form, error) {
  return {
    type: SESSION_TIMEOUT_ERROR,
    form,
    error
  };
}

/**
 * @param {string} identifier 
 * @param {string} secret 
 * @param {string} form 
 */
export function sessionLogin(identifier, secret, form) {
  return {
    type: SESSION_LOGIN,
    identifier,
    secret,
    form
  };
}

/**
 * @param {integer} duration 
 */
export function sessionLoginSuccess(duration) {
  return {
    type: SESSION_LOGIN_SUCCESS,
    duration
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 */
export function sessionLoginError(form, error) {
  return {
    type: SESSION_LOGIN_ERROR,
    form,
    error
  };
}

/**
 * 
 * @param {string} email 
 * @param {string} otp 
 * @param {string} form 
 * @returns 
 */
export function verifyOtp(email, otp, form) {
  return {
    type: VERIFY_OTP,
    email,
    otp,
    form
  };
}


/**
 * @param {object} user 
 * @param {string} token 
 */
export function verifyOtpSuccess(user, token) {
  return {
    type: VERIFY_OTP_SUCCESS,
    user,
    token
  };
}

/**
 * @param {string} error 
 */
export function verifyOtpError(error) {
  return {
    type: VERIFY_OTP_ERROR,
    error
  };
}


/**
 * 
 * @param {string} identifier 
 * @param {string} secret 
 * @param {function} setSpinner 
 * @returns 
 */
export function resendOtp(identifier, secret, setSpinner) {
  return {
    type: RESEND_OTP,
    identifier,
    secret,
    setSpinner
  };
}

/**
 * @param {string} success 
 */
export function resendOtpSuccess(success) {
  return {
    type: RESEND_OTP_SUCCESS,
    success
  };
}

/**
 * @param {string} error 
 */
export function resendOtpError(error) {
  return {
    type: RESEND_OTP_ERROR,
    error,
  };
}


/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @returns 
 */
export function twoFactorAuthentication(record, form) {
  return {
    type: TWO_FACTOR_AUTHENTICATION,
    record,
    form
  };
}

/**
 * @param {string} success 
 */
export function twoFactorAuthenticationSuccess(user, success) {
  return {
    type: TWO_FACTOR_AUTHENTICATION_SUCCESS,
    user,
    success
  };
}

/**
 * @param {string} error 
 */
export function twoFactorAuthenticationError(error) {
  return {
    type: TWO_FACTOR_AUTHENTICATION_ERROR,
    error,
  };
}


export function clearCache() {
  return {
    type: CLEAR_CACHE
  };
}

/**
 * 
 * @param {string} expand 
 * @returns 
 */
export function sessionExpand(expand) {
  return {
    type: SESSION_EXPAND,
    expand
  };
}


export function sessionClearTimeout() {
  return {
    type: SESSION_CLEAR_TIMEOUT
  };
}

export function sessionClearTimeoutSuccess() {
  return {
    type: SESSION_CLEAR_TIMEOUT_SUCCESS
  };
}

/**
 * @param {string} form
 * @param {string} error 
 */
export function sessionClearTimeoutError(form, error) {
  return {
    type: SESSION_CLEAR_TIMEOUT_ERROR,
    form,
    error
  };
}

export function sessionToken(record) {
  return {
    type: SESSION_TOKEN,
    record
  };
}

/**
 * 
 * @param {string} token 
 * @returns 
 */
export function sessionTokenSuccess(token) {
  return {
    type: SESSION_TOKEN_SUCCESS,
    token
  };
}

/**
 * @param {string} form 
 * @param {string} error 
 * @returns 
 */
export function sessionTokenError(form, error) {
  return {
    type: SESSION_TOKEN_ERROR,
    form,
    error
  };
}

/**
 * 
 * @param {object} record 
 * @param {string} form 
 * @returns 
 */
 export function updateSignature(record, form) {
  return {
    type: UPDATE_SIGNATURE,
    record,
    form
  }
}

/**
 * 
 * @param {string} success 
 * @returns 
 */
 export function updateSignatureSuccess(success) {
  return {
    type: UPDATE_SIGNATURE_SUCCESS,
    success
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function updateSignatureError(error) {
  return {
    type: UPDATE_SIGNATURE_ERROR,
    error
  }
}

export function loadAppVersion() {
  return {
    type: LOAD_APP_VERSION,
  }
}

/**
 * 
 * @param {integer} appVersion 
 * @returns 
 */
export function loadAppVersionSuccess(appVersion) {
  return {
    type: LOAD_APP_VERSION_SUCCESS,
    appVersion
  }
}

/**
 * 
 * @param {string} error 
 * @returns 
 */
export function loadAppVersionError(error) {
  return {
    type: LOAD_APP_VERSION_ERROR,
    error
  }
}


export default {
  verifySession,
  verifySessionSuccess,
  verifySessionError,
  logIn,
  logInSuccess,
  logInError,
  logOut,
  logOutSuccess,
  logOutError,
  forgotPassword,
  forgotPasswordSuccess,
  forgotPasswordError,
  resetPassword,
  resetPasswordSuccess,
  resetPasswordError,
  changePassword,
  changePasswordSuccess,
  changePasswordError,
  requestDemo,
  requestDemoSuccess,
  requestDemoError,
  updateVersion,
  updateVersionSuccess,
  updateVersionError,
  sessionTimeout,
  sessionTimeoutSuccess,
  sessionTimeoutError,
  sessionLogin,
  sessionLoginSuccess,
  sessionLoginError,
  verifyOtp,
  verifyOtpSuccess,
  resendOtp,
  resendOtpSuccess,
  resendOtpError,
  twoFactorAuthentication,
  twoFactorAuthenticationSuccess,
  twoFactorAuthenticationError,
  clearCache,
  sessionExpand,
  sessionClearTimeout,
  sessionClearTimeoutSuccess,
  sessionClearTimeoutError,
  sessionToken,
  sessionTokenSuccess,
  sessionTokenError,
  updateSignature,
  updateSignatureSuccess,
  updateSignatureError,
  loadAppVersion,
  loadAppVersionSuccess,
  loadAppVersionError
}