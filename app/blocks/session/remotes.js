/*
 *
 *  session remotes
 *
 */

import api from 'utils/api';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');

export function verifySession() {
  return api
    .get(`/users/me`)
    .then(response => {
      if (!response.data || (response.data && response.data.error)) {
        return Promise.reject('Session Timout');
      }
      const { data = {} } = response;
      const { is_admin, role } = data;
      let Record = Object.assign({}, { ...data });

      if(data && data.signature && data.signature != '') {
        const bytes = CryptoJS.AES.decrypt(data.signature, data.id);
        let signature = bytes.toString(CryptoJS.enc.Utf8);
        Record.signature = base64.decode(signature);
      }

      if (role != 'superAdmin' && is_admin) {
        return Object.assign({}, { ...Record }, { routes: 'physician' })
      } else {
        return Object.assign({}, { ...Record }, { routes: role })
      }
    })
    .catch(error => Promise.reject(error));
}

/**
 * @param {string} email 
 * @param {string} password 
 * @param {object} queryParams 
 * @param {boolean} sessionLogin 
 */
export function logIn(email, password, queryParams, sessionLogin) {
  return api
    .post(`/login`, { email, password, reactivation_token: queryParams && queryParams.reactivation || false, session: sessionLogin || false })
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

export function logOut() {
  return api
    .delete(`/session`)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function signUp(record) {
  return api
    .post(`/users/signup`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function forgotPassword(record) {
  return api
    .post(`/users/forget-password`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function resetPassword(record) {
  return api
    .post(`/users/reset-password`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function changePassword(record) {
  return api
    .post(`/users/changePassword`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function sessionToken(record) {
  return api
    .post(`/rest/users/session`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
export function updateSignature(record) {
  return api
    .put('/rest/user/update-signature', record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function loadAppVersion(record) {
  return api
    .get(`/rest/versionHistory`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function updateVersion(record) {
  return api
    .put(`/rest/versionHistory`, record)
    .then(response => response.data)
    .catch(error => Promise.reject(error));
}

/**
 * @param {object} record 
 */
 export function twoFactorAuthentication(record) {
  return api.put(`/users/updateTwoFactorStatus`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

/**
 * 
 * @param {string} email 
 * @param {string} otp 
 * @returns 
 */
 export function verifyOtp(email, otp){
  return api.post('/login/verifyOtp',{email, otp}).then(response => {return response.data}).catch(error => Promise.reject(error));
}
// /rest/treatments/getAllTreatmentDetails
// export function GlobalRfaFormDetails(){
//   return api.get('/rest/treatments/getAllTreatmentDetails').then(response => {return response.data}).catch(error => Promise.reject(error));
// }

export default {
  verifySession,
  logIn,
  logOut,
  signUp,
  forgotPassword,
  resetPassword,
  changePassword,
  sessionToken,
  updateSignature,
  loadAppVersion,
  updateVersion,
  twoFactorAuthentication,
  verifyOtp,
  // GlobalRfaFormDetails
};
