/*
 *
 *  session sagas
 *
 */

import { call, take, put, select, all, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { startSubmit, stopSubmit } from 'redux-form/immutable';
import history from 'utils/history';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');

import { DEFAULT_SESSION_TOKEN_ERROR, DEFAULT_CHANGE_PASSWORD_ERROR, DEFAULT_SESSION_TIMEOUT_ERROR,  DEFAULT_SESSION_CLEAR_TIMEOUT_ERROR, DEFAULT_FORGOT_PASSWORD_ERROR, DEFAULT_RESET_PASSWORD_ERROR, DEFAULT_UPDATE_VERSION_ERROR, DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR, DEFAULT_VERIFY_OTP_ERROR, DEFAULT_RESEND_OTP_ERROR } from 'utils/errors.js';
import { setAuthToken } from 'utils/api';
import { Timer, Localize } from 'utils/tools';

import store2 from 'store2';
import { LOG_IN, LOG_OUT, VERIFY_SESSION, SIGN_UP, ROUTER, CHANGE_PASSWORD, SESSION_TIMEOUT, SESSION_LOGIN, SESSION_CLEAR_TIMEOUT, SESSION_TOKEN, FORGOT_PASSWORD, RESET_PASSWORD, UPDATE_SIGNATURE, UPDATE_VERSION, LOAD_APP_VERSION, TWO_FACTOR_AUTHENTICATION, VERIFY_OTP, RESEND_OTP
  // , GLOBALRFADETAIL
} from './constants';// eslint-disable-line

import {
  verifySessionSuccess,
  verifySessionError,
  logInSuccess,
  logInError,
  // logOut as logOutAction,
  logOutSuccess,
  logOutError,
  signUpSuccess,
  signUpError,
  changePasswordError,
  changePasswordSuccess,
  sessionTimeoutError,
  sessionTimeoutSuccess,
  sessionLoginError,
  sessionLoginSuccess,
  // createSubscriptionPlan as createSubscriptionPlanAction,
  verifySession as verifySessionAction,
  sessionClearTimeoutSuccess,
  sessionClearTimeoutError,
  sessionToken as sessionTokenAction,
  sessionTokenSuccess,
  sessionTokenError,
  forgotPasswordError,
  forgotPasswordSuccess,
  resetPasswordSuccess,
  resetPasswordError,
  updateSignatureSuccess,
  updateSignatureError,
  updateVersionSuccess,
  updateVersionError,
  loadAppVersionSuccess,
  loadAppVersionError,
  twoFactorAuthenticationSuccess,
  twoFactorAuthenticationError,
  verifyOtpSuccess,
  verifyOtpError,
  resendOtpSuccess,
  resendOtpError,
  // globalRfaDetail,
  // globalRfaDetailSuccess,
  // globalRfaDetailError
} from './actions';

import { logIn, verifySession, signUp, changePassword, sessionToken, forgotPassword, resetPassword, updateSignature, updateVersion, loadAppVersion, twoFactorAuthentication, verifyOtp, 
  // GlobalRfaFormDetails
} from './remotes';// eslint-disable-line

import { selectToken, selectLoggedIn, selectUser, selectActiveSession, selectTimeout } from './selectors';

export function* verifyInitialSessionSaga() {
  const secret = store2.get('secret');
  const activeSession = store2.get('activeSession');
  const location = window.location && window.location.pathname || '/';

  if (secret && !['/reset-password/'].includes(location)) {
    try {
      setAuthToken(secret);
      store2.set('secret', secret);
      const user = yield call(verifySession);
      yield put(verifySessionSuccess(user));
      yield put(sessionTokenAction(user));
      if (activeSession != null) {
        yield put(sessionLoginSuccess(activeSession));
      }
    } catch (error) {
      store2.remove('secret');
      store2.remove('activeSession');
      yield put(verifySessionError(DEFAULT_SESSION_TOKEN_ERROR));
      yield put(push(process.env.PUBLIC_PATH || '/'));
    }
  } else {
    try {
      const loggedIn = yield select(selectLoggedIn());
      if (!activeSession && !loggedIn && !['/login', '/signin', '/signup', '/forgot', '/reset-password/'].includes(location)) {
        yield put(push(process.env.PUBLIC_PATH || '/'));
      }
      yield put(logOutSuccess());
    } catch (error) {
      yield put(logOutError(error));
    } finally {
      store2.remove('activeSession');
      store2.remove('secret');
    }
  }
}

export function* verifySessionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { secret } = yield take(VERIFY_SESSION);
    const loggedIn = yield select(selectLoggedIn());
    if (secret) {
      try {
        setAuthToken(secret);
        const user = yield call(verifySession);
        yield put(verifySessionSuccess(user));
      } catch (error) {
        const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        if (loggedIn && session_error === "invalid auth token") {
          const record = yield select(selectUser());
          yield put(sessionTokenAction(record));
          yield put(sessionLoginSuccess(0));
          store2.set('activeSession', 0);
        }
      }
    }
  }
}

export function* loginSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, queryParams, form } = yield take(LOG_IN);
    yield put(startSubmit(form));

    try {
      const result = yield call(logIn, identifier, secret, queryParams);
      if (result && result.user && result.user['two_factor']) {
        yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: { form: 'verifyOtp', identifier, secret } }));
      } else {
        store2.set('secret', result.authToken);
        setAuthToken(result.authToken);
        yield put(logInSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
        const user = yield call(verifySession);
        yield put(verifySessionSuccess(user));
        yield put(verifySessionAction(result.authToken));
        // if(result.user && (result.user.role == "physician" || result.user.role == "staff")){
        // yield put(globalRfaDetail());
        // }
        yield put(push({ pathname: process.env.PUBLIC_PATH || '/' }));

      }
    } catch (error) {
      const login_attempts = error.response && error.response.data && error.response.data.login_attempts || false;
      if (login_attempts && login_attempts >= 3 && form != 'loginFailureForm') {
        yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: { form: 'loginFailure', identifier, secret } }));
      } else {
        store2.remove('secret');
        store2.remove('activeSession');
        yield put(logInError(error));
      }
    } finally {
      yield put(stopSubmit(form));
    }

    // try {
    //   if (identifier == "admin@vettedclaims.com" && secret == "ClaimsMade22!") {
    //     const user = {
    //       "name": "nathan",
    //       "role": "physician",
    //       "is_admin": true
    //     }
    //     const result = {
    //       "id": "a808e4aa-8e40-4e79-b6eb-8650edb21475",
    //       "name": "nathan",
    //       "email": "admin@vettedclaims.com",
    //       "password": "$2a$10$mHFsXjupR2g5ByrvHqUseOxvv1ws2H7L7qX2xF.snnk0WzVcSmBau",
    //       "practice_id": "993e253c-404d-4d08-9798-67b712bfebcc",
    //       "role": "physician",
    //       "is_admin": true,
    //       "is_deleted": false,
    //       "temporary_password": null,
    //       "createdAt": "2022-04-29T10:44:42.000Z",
    //       "updatedAt": "2022-04-29T10:44:42.000Z",
    //       "practiceDetails": {
    //         "id": "993e253c-404d-4d08-9798-67b712bfebcc",
    //         "name": "nathan",
    //         "address": null,
    //         "street": "Vinayagar Koil st",
    //         "city": "jhkjkgjl",
    //         "state": "6071-01",
    //         "zip_code": "1235",
    //         "logoFile": null,
    //         "phone": null,
    //         "is_deleted": null,
    //         "createdAt": "2022-04-29T10:44:42.000Z",
    //         "updatedAt": "2022-04-29T10:52:13.000Z"
    //       },
    //       "lastPasswordChangeDate": "2022-04-29T09:33:14.000Z"
    //     }
    //     yield put(logInSuccess(Object.assign({}, result, { routes: user && user.role || false })));
    //     // yield put(verifySessionSuccess(result));
    //     yield put(push({ pathname: process.env.PUBLIC_PATH || '/' }));
    //   } else {
    //     yield put(logInError("Invalid email and password"));
    //   }
    // } finally {
    //   yield put(stopSubmit(form));
    // }
  }
}

export function* logOutSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const watcher = yield take(LOG_OUT);

    if (watcher) {
      try {
        if (['production', 'staging'].includes(process.env.ENV)) {
          yield put(logOutSuccess());
          window.location.href = (process.env.ENV === 'production') ? `https://app.vettedclaims.com/` : `https://stagingapp.vettedclaims.com/`;
        } else {
          yield put(logOutSuccess());
          yield put(push(process.env.PUBLIC_PATH || '/'));
        }
      } catch (error) {
        yield put(logOutError(error));
      } finally {
        store2.remove('secret');
        store2.remove('activeSession');
      }
    }
  }
}

export function* signUpSaga() {
  while (true) { // eslint-disable-line no-constant-condition
    const { record, form } = yield take(SIGN_UP);
    yield put(startSubmit(form));

    try {
      const result = yield call(signUp, record);
      if (result) {
        yield put(signUpSuccess(user, result.authToken));
        setAuthToken(result.authToken);
        store2.set('secret', result.authToken);
        const user = yield call(verifySession);
        yield put(verifySessionSuccess(user));
        yield put(verifySessionAction(result.authToken));

        yield put(push(history && history.location && history.location.pathname || process.env.PUBLIC_PATH || '/'));

      }
    } catch (error) {
      yield put(signUpError(error));
    } finally {
      yield put(stopSubmit(form));
    }
  }
}

export function* forgotPasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record, form } = yield take(FORGOT_PASSWORD);
    yield put(startSubmit(form));

    try {
      const result = yield call(forgotPassword, record);
      if (result) {
        yield put(forgotPasswordSuccess(result.message));
        yield put(stopSubmit(form));
      } else {
        yield put(forgotPasswordError(DEFAULT_FORGOT_PASSWORD_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_FORGOT_PASSWORD_ERROR }));
      }
    } catch (error) {
      const forgot_password_error = error.response && error.response.data && error.response.data.error || DEFAULT_FORGOT_PASSWORD_ERROR;
      yield put(forgotPasswordError(forgot_password_error));
      yield put(stopSubmit(form, { _error: forgot_password_error }));
    }
  }
}

export function* resetPasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record, form } = yield take(RESET_PASSWORD);
    yield put(startSubmit(form));

    try {
      const result = yield call(resetPassword, record);
      if (result) {
        yield put(resetPasswordSuccess(result.message))
        yield put(stopSubmit(form));
      } else {
        yield put(resetPasswordError(DEFAULT_RESET_PASSWORD_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_RESET_PASSWORD_ERROR }));
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_RESET_PASSWORD_ERROR;
      yield put(resetPasswordError(Error && Error === 'invalid auth token' && 'Reset password link is expired. Please try resetting the password again.' || Error));
      yield put(stopSubmit(form, { _error: Error && Error === 'invalid auth token' && 'Reset password link is expired. Please try resetting the password again.' || Error }));
    }
  }
}

export function* sessionLoginSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, form } = yield take(SESSION_LOGIN);
    yield put(startSubmit(form));

    try {
      const result = yield call(logIn, identifier, secret, false, true);
      if (result) {
        setAuthToken(result.authToken);
        yield put(verifySessionAction(result.authToken));
        store2.set('secret', result.authToken);
        store2.set('activeSession', 1);
        yield put(sessionLoginSuccess(1));
      }
    } catch (error) {
      const login_attempts = error.response && error.response.data && error.response.data.login_attempts || false;
      if (login_attempts && login_attempts >= 3 && form != 'loginFailureForm') {
        yield put(logOutSuccess());
        yield put(push(process.env.PUBLIC_PATH || '/'));
        store2.remove('secret');
        store2.remove('activeSession');
      } else {
        const Err = login_attempts === 1 ? `Wrong Password. You have 2 left attempts` : login_attempts === 2 ? `Wrong Password. You have 1 left attempt` : 'Something Went Wrong!';
        yield put(sessionLoginError(form, Err));
        yield put(stopSubmit(form, { _error: Err }));
      }
    }
  }
}


export function* routerSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    yield take(ROUTER);
    const secret = store2.get('secret');
    const token = yield select(selectToken());

    const loggedIn = yield select(selectLoggedIn());
    if (secret || token) {
      try {
        setAuthToken(secret || token);
        yield call(verifySession);
      } catch (error) {
        const session_error = error.response && error.response.data && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        if (loggedIn && session_error === "invalid auth token") {
          const record = yield select(selectUser());
          yield put(sessionTokenAction(record));
        }
      }
    }
  }
}

export function* sessionTimeoutSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { dispatch } = yield take(SESSION_TIMEOUT);

    try {
      const user = yield select(selectUser());
      const activeSession = yield select(selectActiveSession());
      if (user && user.session_timeout && activeSession && user.role && user.role !== "superAdmin") {
        const timeout = yield call(Timer, () => Localize(() => dispatch(sessionLoginSuccess()), 'activeSession', 0), (parseInt(user.session_timeout) * 1000));
        yield put(sessionTimeoutSuccess(timeout));
      }
    } catch (error) {
      yield put(sessionTimeoutError('sessionTimeout', DEFAULT_SESSION_TIMEOUT_ERROR));
    }
  }
}

export function* sessionClearTimeoutSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    yield take(SESSION_CLEAR_TIMEOUT);

    try {
      const sessionTimeout = yield select(selectTimeout());
      if (sessionTimeout) {
        clearTimeout(sessionTimeout);
        yield put(sessionClearTimeoutSuccess());
      }
    } catch (error) {
      yield put(sessionClearTimeoutError('sessionTimeout', DEFAULT_SESSION_CLEAR_TIMEOUT_ERROR));
    }
  }
}

export function* changePasswordSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(CHANGE_PASSWORD);
    yield put(startSubmit(form));

    try {
      const { password, new_password } = record;
      const secret = store2.get('secret');
      if (password && password.length < 8) {
        yield put(changePasswordError('Password must be at least 8 characters'));
        yield put(stopSubmit(form, { _error: 'Password must be at least 8 characters' }));
      } else if (password && new_password && password != new_password) {
        yield put(changePasswordError('Password Mismatch'));
        yield put(stopSubmit(form, { _error: 'Password Mismatch' }));
      } else {
        const result = yield call(changePassword, record);
        if (result) {
          yield put(changePasswordSuccess(result.message));
          yield put(verifySessionAction(secret));
          yield put(stopSubmit(form));
        } else {
          yield put(changePasswordError(DEFAULT_CHANGE_PASSWORD_ERROR));
          yield put(stopSubmit(form, { _error: DEFAULT_CHANGE_PASSWORD_ERROR }));
        }
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_CHANGE_PASSWORD_ERROR;
      yield put(changePasswordError(Error));
      yield put(stopSubmit(form, { _error: Error }));
    }
  }
}

export function* sessionTokenSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {} } = yield take(SESSION_TOKEN);

    if (record) {
      try {
        const result = yield call(sessionToken, record);
        if (result) {
          // Decrypt
          var bytes = CryptoJS.AES.decrypt(result.authToken, record.id);
          var secret = bytes.toString(CryptoJS.enc.Utf8);

          setAuthToken(secret);
          store2.set('secret', secret);
          yield put(sessionTokenSuccess(secret));
          // if(result.user && result.user.role && (result.user.role == "physician" || result.user.role == "staff")){
          // yield put(globalRfaDetail());
          // }
          yield put(push(history && history.location && history.location.pathname || process.env.PUBLIC_PATH || '/'));
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error || DEFAULT_SESSION_TOKEN_ERROR;
        yield put(sessionTokenError('sessionTimeout', Err));
      }
    }
  }
}

export function* updateSignatureSaga() {
  yield takeLatest(UPDATE_SIGNATURE, function* updater({ record, form }) {
    if (record) {
      yield put(startSubmit(form));
      try {
        const user = yield select(selectUser());
        const userId = user && user.id;
        const encodeBase64 = base64.encode(record.signature);
        const encodedSignature = CryptoJS.AES.encrypt(encodeBase64, userId).toString();

        if (encodedSignature) {
          let res = yield call(updateSignature, Object.assign({}, { signature: encodedSignature }));
          if (res) yield put(updateSignatureSuccess('Signature updated successfully'));
          yield put(stopSubmit(form));
          const user = yield call(verifySession);
          if (user) {
            yield put(verifySessionSuccess(user));
          }
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to update Signature';
        yield put(stopSubmit(form, { _error: Err }));
        yield put(updateSignatureError('Failed to update Signature'));
      }
    }
  })
}

export function* updateVersionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { record = {}, form } = yield take(UPDATE_VERSION);
    yield put(startSubmit(form));

    try {
      const result = yield call(updateVersion, record);
      if (result) {
        yield put(updateVersionSuccess(record && record.version))
        yield put(stopSubmit(form));
      } else {
        yield put(updateVersionError(DEFAULT_UPDATE_VERSION_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_VERSION_ERROR }));
      }
    } catch (error) {
      const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_UPDATE_VERSION_ERROR;
      yield put(updateVersionError(Err));
      yield put(stopSubmit(form, { _error: DEFAULT_UPDATE_VERSION_ERROR }));
    }
  }
}

export function* loadAppVersionSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const version = yield take(LOAD_APP_VERSION);

    if (version) {
      try {
        let result = yield call(loadAppVersion);
        if (result) {
          yield put(loadAppVersionSuccess(result.version));
        }
      } catch (error) {
        const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Failed to load app version';
        yield put(loadAppVersionError(Err));
      }
    }
  }
}

export function* twoFactorAuthenticationSaga() {

  while (true) {
    const { record = {}, form } = yield take(TWO_FACTOR_AUTHENTICATION);
    yield put(startSubmit(form));

    try {
      const result = yield call(twoFactorAuthentication, record);
      if (result) {
        const user = yield select(selectUser());
        yield put(twoFactorAuthenticationSuccess(Object.assign({}, user, { two_factor_status: record.two_factor_status }), result.message));
        yield put(stopSubmit(form));
      } else {
        yield put(twoFactorAuthenticationError(DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR));
        yield put(stopSubmit(form, { _error: DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR }));
      }
    } catch (error) {
      const Error = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR;
      yield put(twoFactorAuthenticationError(Error));
      yield put(stopSubmit(form, { _error: Error }));
    }
  }
}

export function* verifyOtpSaga() {
  while (true) {
    const { email, otp, form } = yield take(VERIFY_OTP);
    yield put(startSubmit(form));

    try {
      const result = yield call(verifyOtp, email, otp);
      store2.set('secret', result.authToken);
      setAuthToken(result.authToken);

      yield put(verifyOtpSuccess(Object.assign({}, result.user, { routes: result.user && result.user.role || false }), result.authToken));
      yield put(verifySessionSuccess(result.user));
      yield put(verifySessionAction(result.authToken));
      yield put(push({ pathname: process.env.PUBLIC_PATH || '/', state: Object.assign({}, { ...history.location.state }) }));
    } catch (error) {
      const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_VERIFY_OTP_ERROR;
      yield put(verifyOtpError(Err));
    } finally {
      yield put(stopSubmit(form));
    }
  }
}

export function* resendOtpSaga() {
  while (true) {
    // eslint-disable-line no-constant-condition
    const { identifier, secret, setSpinner } = yield take(RESEND_OTP);
    try {
      const result = yield call(logIn, identifier, secret);
      if (result) {
        yield put(resendOtpSuccess("Email Sent."));
        yield call(setSpinner, false);
      } else {
        yield put(resendOtpError(DEFAULT_RESEND_OTP_ERROR));
      }
    } catch (error) {
      const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : DEFAULT_RESEND_OTP_ERROR;
      yield put(resendOtpError(Err));
    }
  }
}

export default function* rootSaga() {
  yield all([
    verifyInitialSessionSaga(),
    verifySessionSaga(),
    loginSaga(),
    logOutSaga(),
    signUpSaga(),
    routerSaga(),
    sessionLoginSaga(),
    sessionTimeoutSaga(),
    sessionClearTimeoutSaga(),
    changePasswordSaga(),
    sessionTokenSaga(),
    forgotPasswordSaga(),
    resetPasswordSaga(),
    updateSignatureSaga(),
    updateVersionSaga(),
    loadAppVersionSaga(),
    twoFactorAuthenticationSaga(),
    verifyOtpSaga(),
    resendOtpSaga()
  ]);
}