/*
 *
 *  global settings sagas
 *
 */


import { call, take, put, race, all, select } from 'redux-saga/effects';
import { selectUser } from 'blocks/session/selectors';
/**
 * 
 * @param {object} constants 
 * @param {object} actions 
 * @param {object} remotes 
 * @param {object} selectors 
 * @param {object} entityUrl 
 * @returns 
 */
export default function sagas(constants, actions, remotes, selectors, entityUrl) {// eslint-disable-line
    const {
        LOAD_POC2,
        LOAD_POC2_DOC,
        LOAD_POC_META_DATA,
        RWC_FORM_DOWNLOAD,
        LOAD_PHYSICIAN_DETAIL,
        LOAD_ADMINISTRATOR_DETAIL,
        GET_DIAGNOSIS_BY_TREATMENT
    } = constants;



    const {
        loadPoc2Progress,
        loadPoc2Error,
        loadPoc2Success,
        loadPoc2DocSuccess,
        loadPoc2DocError,
        addPoc2Doc,
        loadRecordsMetaDataSuccess,
        loadRecordsMetaDataError,
        rwcFormDownloadSuccess,
        rwcFormDownloadError,
        loadPhysicianSuccess,
        loadPhysicianError,
        loadAdministratorSuccess,
        loadAdministratorError,
        rwcFormMergeError,
        rwcFormMergeSuccess,
        getDiagnosisByTreatmentSuccess
    } = actions;


    const {
        loadPoc2BodySystem,
        loadPoliciesDoc,
        getPatientDetails,
        uploadingPdf,
        loadRecordsWithHeader,
        uploadMultipleFiles,
        loadAdministrator,
        selectClaimAdmin,
        getDiagnosisByTreatments
    } = remotes;

    const {
        selectPoc2AddedDoc, selectHeaders, selectDiagnosisByTreatmentRecords } = selectors;// eslint-disable-line


    function* loadPoc2BodySystemSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { load } = yield race({
                load: take(LOAD_POC2),
            });
            if (load) {
                yield put(loadPoc2Progress());
                try {
                    const result = yield call(loadPoc2BodySystem, Object.assign({}, { "body_system": load.records }));
                    if (result) {
                        yield put(loadPoc2Success(result));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Policies Getting Error";
                    yield put(loadPoc2DocError(Err));
                }
            }
        }
    }

    function* getPoliciesDocSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const { update } = yield race({
                update: take(LOAD_POC2_DOC),
            });
            if (update) {

                yield put(loadPoc2Progress());
                try {
                    const result = yield call(loadPoliciesDoc, update.record);
                    if (result) {
                        const selectDoc = yield select(selectPoc2AddedDoc());

                        const selectedDoc = selectDoc.map(e => e.treatment)
                        const schema = result['policy_doc'];
                        if (!selectedDoc.includes(update.record.treatment_id) && entityUrl != "diagnosis") {
                            yield put(addPoc2Doc(Object.assign({}, { treatment: update.record.treatment_id, schema: schema })));
                        }
                        yield put(loadPoc2DocSuccess(result));
                        if (update.fun) {
                            update.fun();
                        }
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : "Policies Json Getting Error";
                    yield put(loadPoc2Error(Err));
                }
            }
        }
    }

    function* loadPocMetaDataSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const loadMetaData = yield race({// eslint-disable-line
                metaData: take(LOAD_POC_META_DATA),
            });
            try {
                if (entityUrl === 'poc3' || entityUrl === 'rfaForms') {
                    const user = yield select(selectUser());
                    let PhysicianDetails = []
                    if (user.role == "staff") {
                        PhysicianDetails = yield call(loadRecordsWithHeader, Object.assign({}, { "role": "physician" }));
                    }
                    let ClaimsAdministratorDetails = [];
                    ClaimsAdministratorDetails = yield call(loadAdministrator);

                    const { data } = PhysicianDetails;
                    let patientDetails = [];
                    if (user.id) {
                        patientDetails = yield call(getPatientDetails, user.id);
                    }

                    const PhysicianOptions = (data || []).map(c => Object.assign({}, { label: c.name, value: c.id }))

                    const AdministratorOptions = (ClaimsAdministratorDetails || []).map(c => {
                        let label = c.company_name;
                        if (c.address) {
                            label += `, ${c.address}`
                        }
                        if (c.city) {
                            label += `, ${c.city}`
                        }
                        if (c.state) {
                            label += `, ${c.state}`
                        }
                        if (c.zip_code) {
                            if (c.state) {
                                label += ` - ${c.zip_code}`
                            } else {
                                label += `, ${c.zip_code}`
                            }
                        }
                        return Object.assign({}, { label: label, value: c.id })
                    }).sort((a, b) => a.label.localeCompare(b.label))
                    AdministratorOptions.push({ label: "Others", value: "others" });
                    yield put(loadRecordsMetaDataSuccess(Object.assign({}, { patientDetails, PhysicianOptions, AdministratorOptions })));

                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Physician or Claims Administrator Failed';
                yield put(loadRecordsMetaDataError(Err));
            }
        }
    }

    function* rwcDownloadSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const rwcDownload = yield race({// eslint-disable-line
                record: take(RWC_FORM_DOWNLOAD),
            });
            const { record } = rwcDownload || {};
            if (record) {
                try {
                    const result = yield call(uploadingPdf, record.record.formPayLoad);
                    if (result) {
                        if (typeof result.attachments == 'string') {
                            result.attachments = JSON.parse(result.attachments);
                        }
                        if (record.record.mergePayLoad) {
                            try {
                                const result1 = yield call(uploadMultipleFiles, record.record.mergePayLoad);
                                if (result1) {
                                    window.open(result1.publicUrl, '_blank');
                                    record.stopUpload();
                                    record.closeForm();
                                    record.setAdminAssignedValue();
                                    yield put(rwcFormDownloadSuccess(result));
                                    yield put(rwcFormMergeSuccess({ successUpload: 'RFA Form generated' }));
                                }
                            } catch (error) {
                                record.stopUpload();
                                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Upload Failed';
                                yield put(rwcFormMergeError({ errorUpload: Err }));
                            }
                        }

                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Form Saving Failed';
                    record.stopUpload();
                    yield put(rwcFormDownloadError({ errorUpload: Err }));
                }
            }
        }
    }

    function* loadPhysicianSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const physician = yield race({// eslint-disable-line
                record: take(LOAD_PHYSICIAN_DETAIL),
            });
            const { record } = physician || {};
            if (record) {
                try {
                    const result = yield call(getPatientDetails, record.record);
                    if (result && result.length != 0) {
                        const resData = Object.assign({}, result[0], { 'id': record.record })
                        yield put(loadPhysicianSuccess(resData));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Loading Physician Error';
                    yield put(loadPhysicianError(Err));
                }
            }
        }
    }

    function* loadAdministratorSaga() {
        while (true) { // eslint-disable-line no-constant-condition
            const administrator = yield race({// eslint-disable-line
                record: take(LOAD_ADMINISTRATOR_DETAIL),
            });

            const { record } = administrator || {};
            if (record) {
                try {
                    const result = yield call(selectClaimAdmin, record.record);
                    if (result && result.length != 0) {
                        yield put(loadAdministratorSuccess(result));
                    }
                } catch (error) {
                    const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Loading Administrator Error';
                    yield put(loadAdministratorError(Err));
                }
            }
        }
    }

    function* getDiagnosisByTreatmentSagas() {
        while (true) {
            const { data } = yield race({// eslint-disable-line
                data: take(GET_DIAGNOSIS_BY_TREATMENT),
            });
            const { body_system_id, treatment_id } = data;
            try {
                const diagnosis = yield call(getDiagnosisByTreatments, { "body_system_id": body_system_id, "treatment_id": treatment_id });
                const diagnoList = diagnosis.diagnosisList;
                const otherList = diagnosis.othersList;
                const diagnosisList = (diagnoList || []).map((c) => Object.assign({}, { id: c.id, label: c.name, value: c.id }));
                const othersList = (otherList || []).map((c) => Object.assign({}, { id: c.id, label: c.name, value: c.id }));
                const data = Object.assign({}, { 'treatmentID': treatment_id }, { diagnosisList, othersList });
                const selectDiagnosis = yield select(selectDiagnosisByTreatmentRecords());
                const treatmentID = selectDiagnosis.map(val => val.treatmentID);
                if (!treatmentID.includes(treatment_id)) {
                    yield put(getDiagnosisByTreatmentSuccess(data));
                }
            } catch (error) {
                const Err = error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error : 'Getting Treatment and diagnosis list Error';
                yield put(loadAdministratorError(Err));
            }
        }
    }

    return function* rootSaga() {
        yield all([
            loadPoc2BodySystemSaga(),
            getPoliciesDocSaga(),
            loadPocMetaDataSaga(),
            rwcDownloadSaga(),
            loadPhysicianSaga(),
            loadAdministratorSaga(),
            getDiagnosisByTreatmentSagas(),
        ]);
    }
}
