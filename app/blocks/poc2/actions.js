/*
 *
 * poc2 actions
 *
 */

/**
 * 
 * @param {object} constants 
 * @returns 
 */
 export default function (constants) {
    const {
        LOAD_POC2,
        LOAD_POC2_ERROR,
        LOAD_POC2_SUCCESS,
        LOAD_POC2_DOC,
        LOAD_POC2_DOC_SUCCESS,
        ADD_POC2_DOC,
        LOAD_POC2_PROGRESS,
        LOAD_POC_META_DATA,
        LOAD_POC_META_SUCCESS,
        LOAD_POC_META_ERROR,
        UPDATE_POC3_RECORD,
        RWC_FORM_DOWNLOAD,
        RWC_FORM_SUCCESS,
        RWC_FORM_ERROR,
        LOAD_PHYSICIAN_DETAIL,
        LOAD_PHYSICIAN_DETAIL_SUCCESS,
        LOAD_PHYSICIAN_DETAIL_ERROR,
        LOAD_ADMINISTRATOR_DETAIL,
        LOAD_ADMINISTRATOR_DETAIL_SUCCESS,
        LOAD_ADMINISTRATOR_DETAIL_ERROR,
        RWC_FORM_MERGE,
        RWC_FORM_MERGE_SUCCESS,
        RWC_FORM_MERGE_ERROR,
        CLEAR_ADMINISTRATOR_DETAIL,
        GET_ALL_BODY_SYSTEMS,
        GET_ALL_BODY_SYSTEMS_SUCCESS,
        GET_DIAGNOSIS_BY_TREATMENT,
        GET_DIAGNOSIS_BY_TREATMENT_SUCCESS,
        GET_ALL_RFARECORDS,
        CLEAR_RFAFORM_RECORD
    } = constants;


    /**
 * @param {object} record 
 */
    function loadPoc2(records) {
        return {
            type: LOAD_POC2,
            records
        };
    }

    /**
     * @param {string} error 
     */
    function loadPoc2Error(error) {
        return {
            type: LOAD_POC2_ERROR,
            error
        };
    }

    /**
     * @param {object} record 
     */
    function loadPoc2Success(record) {
        return {
            type: LOAD_POC2_SUCCESS,
            record
        };
    }

    /**
    * @param {object} record
    * @param {function} fun
    */
    function loadPoc2Doc(record, fun) {
        return {
            type: LOAD_POC2_DOC,
            record,
            fun
        };
    }

    /**
     * @param {object} policies 
     */
    function loadPoc2DocSuccess(policies) {
        return {
            type: LOAD_POC2_DOC_SUCCESS,
            policies
        };
    }

    /**
     * @param {object} policies
     */
    function addPoc2Doc(policies) {
        return {
            type: ADD_POC2_DOC,
            policies
        };
    }

    function loadPoc2Progress() {
        return {
            type: LOAD_POC2_PROGRESS,

        };
    }

    function loadPocMetaData(bodySystem) {
        return {
            type: LOAD_POC_META_DATA,
            bodySystem
        }
    }


    function getDiagnosisByTreatment(body_system_id, treatment_id) {
        return {
            type: GET_DIAGNOSIS_BY_TREATMENT,
            body_system_id,
            treatment_id
        }
    }

    function getDiagnosisByTreatmentSuccess(diagnosisList) {
        return {
            type: GET_DIAGNOSIS_BY_TREATMENT_SUCCESS,
            diagnosisList
        }
    }

    function loadRecordsMetaDataError(error) {
        return {
            type: LOAD_POC_META_ERROR,
            error
        }
    }

    /**
   * @param {object} record 
   * @param {object} recordsMetaData
   */
    function loadRecordsMetaDataSuccess(recordsMetaData) {
        return {
            type: LOAD_POC_META_SUCCESS,
            recordsMetaData
        }
    }

    function updatePoc3(update) {
        return {
            type: UPDATE_POC3_RECORD,
            update
        }
    }

    function rwcFormDownload(record, stopUpload, closeForm, setAdminAssignedValue) {
        return {
            type: RWC_FORM_DOWNLOAD,
            record,
            stopUpload,
            closeForm,
            setAdminAssignedValue
        }
    }

    function rwcFormDownloadSuccess(record) {
        return {
            type: RWC_FORM_SUCCESS,
            record
        }
    }

    function rwcFormDownloadError(update) {
        return {
            type: RWC_FORM_ERROR,
            update
        }
    }

    function loadPhysician(record) {
        return {
            type: LOAD_PHYSICIAN_DETAIL,
            record,
        }
    }

    function loadPhysicianSuccess(settings) {
        return {
            type: LOAD_PHYSICIAN_DETAIL_SUCCESS,
            settings
        }
    }

    function loadPhysicianError(error) {
        return {
            type: LOAD_PHYSICIAN_DETAIL_ERROR,
            error
        }
    }
    function loadAdministrator(record) {
        return {
            type: LOAD_ADMINISTRATOR_DETAIL,
            record
        }
    }

    function clearAdministrator(settings) {
        return {
            type: CLEAR_ADMINISTRATOR_DETAIL,
            settings
        }
    }

    function loadAdministratorSuccess(settings) {
        return {
            type: LOAD_ADMINISTRATOR_DETAIL_SUCCESS,
            settings
        }
    }

    function loadAdministratorError(error) {
        return {
            type: LOAD_ADMINISTRATOR_DETAIL_ERROR,
            error
        }
    }

    function rwcFormMerge(record, stopUpload, closeForm) {
        return {
            type: RWC_FORM_MERGE,
            record,
            stopUpload,
            closeForm
        }
    }

    function rwcFormMergeSuccess(update) {
        return {
            type: RWC_FORM_MERGE_SUCCESS,
            update
        }
    }

    function rwcFormMergeError(update) {
        return {
            type: RWC_FORM_MERGE_ERROR,
            update
        }
    }

    function getAllBodySystems() {
        return {
            type: GET_ALL_BODY_SYSTEMS
        }
    }

    function getAllBodySystemSuccess(data) {
        return {
            type: GET_ALL_BODY_SYSTEMS_SUCCESS,
            data
        }
    }

    function getAllRFARecords(record) {
        return {
            type: GET_ALL_RFARECORDS,
            record
        }
    }
    function clearRecord() {
        return {
            type: CLEAR_RFAFORM_RECORD
        }
    }

    return {
        loadPoc2,
        loadPoc2Error,
        loadPoc2Success,
        loadPoc2Doc,
        loadPoc2DocSuccess,
        addPoc2Doc,
        loadPoc2Progress,
        loadPocMetaData,
        loadRecordsMetaDataSuccess,
        loadRecordsMetaDataError,
        updatePoc3,
        rwcFormDownload,
        rwcFormDownloadSuccess,
        rwcFormDownloadError,
        loadPhysician,
        loadPhysicianSuccess,
        loadPhysicianError,
        loadAdministrator,
        loadAdministratorSuccess,
        loadAdministratorError,
        rwcFormMerge,
        rwcFormMergeSuccess,
        rwcFormMergeError,
        clearAdministrator,
        getAllBodySystems,
        getAllBodySystemSuccess,
        getDiagnosisByTreatment,
        getDiagnosisByTreatmentSuccess,
        getAllRFARecords,
        clearRecord
    }
}


