/*
 *
 *  poc2 remotes
 *
 */

import api from 'utils/api';

/**
 * 
 * @param {string} name 
 * @returns 
 */
export default function (name) {// eslint-disable-line
    function loadPoc2BodySystem(record) {
        return api.post(`/getPolicies`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
    * @param {object} record 
    */
    function loadPoliciesDoc(record) {
        return api.post(`/getPolicyDoc`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function loadPoc3MetadataRecord(record) {
        return api.post(`rest/getTreatmentDiagnosisByType`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function getPatientDetails(id) {
        return api.get(`/getUserDetails/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function uploadingPdf(record) {
        return api.put(`/rest/rfaForm/updateRfaFormDetails`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function selectClaimAdmin(id) {
        return api.get(`/claims_admin/${id}`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function getAllBodySystems() {
        return api.get(`rest/getAllBodySystemNames`).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    function getDiagnosisByTreatments(record) {
        return api.post(`rest/treatments/getAllDiagnosisByBodysystemId`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }

    /**
     * 
     * @param {object} headers 
     * @returns 
     */
    function loadRecordsWithHeader(headers, role) {// eslint-disable-line
        return api.get(('rest/users/getAllUsersByRole'), { headers }).then((response) => response).catch((error) => Promise.reject(error));
    }

    function uploadMultipleFiles(record) {
        return api.post(`rfaForms/uploadMultipleFiles`, record).then((response) => response.data).catch((error) => Promise.reject(error));
    }
    function loadAdministrator() {// eslint-disable-line
        return api.get((`rest/claimsAdmin/getAllClaimsAdmins`)).then((response) => response.data).catch((error) => Promise.reject(error));
    }


    return {
        loadPoc2BodySystem,
        loadPoliciesDoc,
        loadPoc3MetadataRecord,
        getPatientDetails,
        loadRecordsWithHeader,
        uploadingPdf,
        uploadMultipleFiles,
        loadAdministrator,
        selectClaimAdmin,
        getAllBodySystems,
        getDiagnosisByTreatments
    }
}

export function uploadDocument(record) {
    return api.post(`/treatments/getUploadUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getPublicUrl(record) {
    return api.post(`/treatments/getSecuredPublicUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getPatientDetails(record) {
    return api.get(`/getUserDetails/${record.id}`).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getUploadUrl(record) {
    return api.post(`/practices/getUploadUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function saveCases(record) {
    return api.put(`/rfaForms/${record.id}`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getUploadPdfUrl(record) {
    return api.post(`/rfaForms/getUploadUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function uploadingPdf(record) {
    return api.put(`/rest/rfaForm/updateRfaFormDetails`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function uploadingAttachment(record) {
    return api.post(`/rfaForms/getUploadUrlForAttachment`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getPublicUrlForRfaForm(record) {
    return api.post(`/rfaForms/getSecuredPublicUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function uploadMultipleFiles(record) {
    return api.post(`rfaForms/uploadMultipleFiles`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function getFileContent(record) {
    return api.post(`rfaForms/readFileContent`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function openUrl(record) {
    return api.post(`rfaForms/getPublicUrl`, record).then((response) => response.data).catch((error) => Promise.reject(error));
}

export function searchResult(url, headers) {
    return api.get((`${url}`), { headers }).then((response) => response.data).catch((error) => Promise.reject(error));

}