/**
 * 
 * poc2 selectors
 * 
 */

 import { createSelector } from 'reselect';

 /**
  * 
  * @param {string} name 
  * @returns 
  */
 export default function (name) {
     const selectDomain = () => (state) => state && state[name] || false;
  
     const selectPoc2BodySystems = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.record || {} },
     );
 
     const selectPoc2PoliciesDoc = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.policies || {} },
     );
 
     const selectPoc2AddedDoc = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.addedPolicies || [] },
     );
 
     const selectPocMetaData = () => createSelector(
         selectDomain(),
         (domain) => domain && domain.recordsMetaData || {},
       );
 
       const selectPocAlert = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.alerting || {} },
     );  
     const selectAdministrator =() => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.administrator || {} },
     );
 
     const selectPoc3Record = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.treatments || [] },
     ); 
     
     const selectPoc3Questions = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.patients || [] },
     ); 
 
     const selectUpdate = () => createSelector(
         selectDomain(),
         (domain) => { return domain && domain.update || {} },
     )
 
     const selectHeaders = () => createSelector(
         selectDomain(),
         (domain) => domain && domain.headers || {},
       );

    const selectDiagnosisByTreatmentRecords = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.diagnosisList || []  
    )

    const selectPhysicians = () => createSelector(
        selectDomain(),
        (domain) => domain && domain.settings || []  
    )

     return {
         selectDomain,
         selectPoc2BodySystems,
         selectPoc2PoliciesDoc,
         selectPoc2AddedDoc,
         selectPocMetaData,
         selectPocAlert,
         selectAdministrator,
         selectPoc3Record,
         selectPoc3Questions,
         selectUpdate,
         selectHeaders,
         selectDiagnosisByTreatmentRecords,
         selectPhysicians
     }
 
 }
 
 