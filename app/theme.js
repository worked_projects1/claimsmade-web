

import { createTheme } from '@mui/material/styles';




const theme = createTheme({
    button: {
        textTransform: 'none'
    },
    typography: {
        "fontFamily": "MyriadPro-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        h5: {
            "fontFamily": "MyriadPro-Bold",
            "textTransform": "Capitalize"
        }
    },
});



export default theme;