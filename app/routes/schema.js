import moment from 'moment';
import { capitalizeFirstLetter } from 'utils/tools';
import React from 'react';
import SummarizeOutlinedIcon from '@mui/icons-material/SummarizeOutlined';
import { Button, Box } from '@mui/material';
import { getPublicUrl, getPublicUrlForRfaForm } from 'blocks/poc2/remotes';
import { AsYouType } from 'libphonenumber-js';
import { GrDocumentPdf } from "react-icons/gr";

const GrIconWrapper = ({ icon: Icon, sx, ...props }) => (// eslint-disable-line
    <Box
        {...props}
        as={Icon}
        sx={{
            ...sx,
            padding: '3px', fontSize: '24px',
            path: {
                stroke: '#3c89c9',
            },
        }}
    />
);

export default function () {

    function changePassword() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'password',
                    label: 'Password',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'new_password',
                    label: 'Confirm Password',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: true
                }
            ]
        }
    }

    function login() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'identifier',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    disabled: true,
                    type: 'input',
                },
                {
                    id: 2,
                    value: 'secret',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'password'
                }
            ]
        }
    }

    function users(record = {}) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    sortColumn: 'Users.name',
                    mandatory: true
                },
                {
                    id: 2,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    sortColumn: 'Users.email',
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: {
                        create: true
                    }
                },
                {
                    id: 4,
                    value: 'role',
                    label: 'ROLE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    sortColumn: 'Users.role',
                    html: (row) => row['role'] === 'lawyer' ? 'Attorney' : row['role'] === 'paralegal' ? 'Non-Attorney' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'],
                    options: [
                        {
                            label: 'Physician',
                            value: 'physician',
                        },
                        {
                            label: 'Staff',
                            value: 'staff',
                        }
                    ],
                    type: 'select',
                    mandatory: true
                },
                {
                    id: 6,
                    value: 'specialty',
                    label: 'SPECIALTY',
                    editRecord: record.role ? record.role != 'staff' ? true : false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    sortColumn: 'Users.specialty',
                },
                {
                    id: 7,
                    value: 'is_admin',
                    label: 'ADMIN',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    sortColumn: 'Users.is_admin',
                    html: (row) => row['is_admin'] ? 'Yes' : 'No',
                    type: 'checkbox'
                },
                {
                    id: 8,
                    value: 'is_admin',
                    label: 'Make a user an Admin',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'checkbox'
                },
                {
                    id: 9,
                    value: 'Authorize',
                    label: 'SIGNATURE AUTHORIZATION STATUS',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: record.role == "staff" ? false : true,
                    type: 'button',
                },
            ]
        }
    }

    function userSettings() {
        return {
            section: [
                {
                    schemaId: 1,
                    name: 'Change Password',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'changePassword',
                    message: 'Password must be changed every 90 days',
                    btnLabel: "Update",
                    onSubmitClose: false,
                    confirmButton: false,
                    enableSubmitBtn: true,
                    disableCancelBtn: (record) => (
                        (moment().diff(record.lastPasswordChangeDate, 'days') >= 90 || record.temporary_password)
                            ? true
                            : false
                    ),
                    show: (record) => (
                        (moment().diff(record.lastPasswordChangeDate, 'days') >= 90 || record.temporary_password)
                            ? true
                            : false
                    ),
                    confirmMessage: '',
                    disableMessage: '',
                    columns: [
                        {
                            id: 1,
                            value: 'password',
                            label: 'Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        },
                        {
                            id: 2,
                            value: 'new_password',
                            label: 'Confirm Password',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'password',
                            mandatory: true
                        }
                    ]
                },
                {
                    schemaId: 2,
                    name: 'Two Factor Authentication',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'enabledTwoFactorAuthentication',
                    message: 'Two Factor Authentication provides additional security. While logging in, a code will be sent to your email to complete login.',
                    btnLabel: "Update",
                    onSubmitClose: true,
                    enableSubmitBtn: true,
                    confirmButton: true,
                    disableCancelBtn: true,
                    show: (record) => (
                        (moment().diff(record.lastPasswordChangeDate, 'days') >= 90 || record.temporary_password)
                            ? true
                            : false
                    ),
                    confirmMessage: (user) => `This will require you to access your email (${user.email}) in order to login in future. Do you want to make this change?`,
                    disableMessage: 'Are you sure you want to disable two factor authentication?',
                    columns: [
                        {
                            id: 1,
                            value: 'two_factor_status',
                            label: 'Enable/Disable Two Factor Authentication',
                            editRecord: true,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'switch',
                            mandatory: true
                        },
                    ]
                }
            ]
        }
    }


    function patients() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'NAME',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'email',
                    label: 'EMAIL',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'disease',
                    label: 'DISEASE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
            ]
        }
    }

    function insurances(records = {}) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'guidelines',
                    label: 'Guidelines',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    options: [
                        {
                            label: 'Ankle and Foot Disorders',
                            value: 'ankleAndFootDisorders'
                        },
                        {
                            label: 'Antiemetics',
                            value: 'antiemetics'
                        },
                        {
                            label: 'Cervical and Thoracic Spine',
                            value: 'cervicalAndThoracicSpine'
                        },
                        {
                            label: 'Chronic Pain',
                            value: 'chronicPain'
                        },
                        {
                            label: 'COVID-19 (Coronavirus)',
                            value: 'covid-19(coronavirus)'
                        },
                        {
                            label: 'Elbow Disorders',
                            value: 'elbowDisorders'
                        },
                        {
                            label: 'Eye Disorders',
                            value: 'eyeDisorders'
                        },
                        {
                            label: 'Hand, Wrist, and Forearm Disorders',
                            value: 'handWristAndForearmDisorders'
                        },
                        {
                            label: 'Hip and Groin Disorders',
                            value: 'hipAndGroinDisorders'
                        },
                        {
                            label: 'Interstitial Lung Disease',
                            value: 'interstitialLungDisease'
                        },
                        {
                            label: 'Knee Disorders',
                            value: 'kneeDisorders'
                        },
                        {
                            label: 'Low Back Disorders',
                            value: 'lowBackDisorders'
                        },
                        {
                            label: 'Occupational/Work-Related Asthma',
                            value: 'occupationalWorkRelatedAsthma'
                        },
                        {
                            label: 'Opioids',
                            value: 'opioids'
                        },
                        {
                            label: 'Shoulder Disorders',
                            value: 'shoulderDisorders'
                        },
                        {
                            label: 'Traumatic Brain Injury',
                            value: 'traumaticBrainInjury'
                        },
                        {
                            label: 'Posttraumatic Stress Disorder',
                            value: 'posttraumaticStressDisorder'
                        },
                        {
                            label: 'Depressive Disorders',
                            value: 'depressiveDisorders'
                        },
                        {
                            label: 'Anxiety Disorders',
                            value: 'anxietyDisorders'
                        },
                    ],
                },
                {
                    id: 2,
                    value: 'chapters',
                    label: 'Chapters',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    disabled: records.guidelines != 'cervicalAndThoracicSpine' ? true : false,
                    options: [
                        {
                            label: 'Cervicothoracic Pain',
                            value: 'cervicothoracicPain',
                        },
                        {
                            label: 'Compression Fractures',
                            value: 'compressionFractures',
                        },
                        {
                            label: 'Headache Pain',
                            value: 'headachePain',
                        },
                        {
                            label: 'Herniated Discs',
                            value: 'herniatedDiscs',
                        },
                        {
                            label: 'Post-Surgical Spine Pain',
                            value: 'postSurgicalSpinePain',
                        },
                        {
                            label: 'Radicular Pain',
                            value: 'radicularPain',
                        },
                        {
                            label: 'Spondylolisthesis',
                            value: 'spondylolisthesis',
                        },
                        {
                            label: 'Spinal Stenosis',
                            value: 'spinalStenosis',
                        },
                        {
                            label: 'Whiplash',
                            value: 'whiplash',
                        },
                    ],
                },
                {
                    id: 3,
                    value: 'diagnosis',
                    label: 'Diagnosis',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    disabled: records.chapters != 'cervicothoracicPain' ? true : false,
                    options: [
                        {
                            label: 'Cervicothoracic Pain - Acute',
                            value: 'cervicothoracicPainAcute'
                        },
                        {
                            label: 'Cervicothoracic Pain - Subacute',
                            value: 'cervicothoracicPainSubacute'
                        },
                        {
                            label: 'Cervicothoracic Pain - Chronic',
                            value: 'cervicothoracicPainChronic'
                        },
                        {
                            label: 'Cervicothoracic Pain - Chronic Episodic',
                            value: 'cervicothoracicPainChronicEpisodic'
                        },
                        {
                            label: 'Cervical Pain - Acute',
                            value: 'cervicalPainAcute'
                        },
                        {
                            label: 'Cervical Pain - Acute Postoperative',
                            value: 'cervicalPainAcutePostoperative'
                        },
                        {
                            label: 'Cervical Pain - Subacute',
                            value: 'cervicalPainSubacute'
                        },
                        {
                            label: 'Cervical Pain - Chronic',
                            value: 'cervicalPainChronic'
                        },
                        {
                            label: 'Cervical Pain - Chronic Disabling',
                            value: 'cervicalPainChronicDisabling'
                        },
                        {
                            label: 'Cervical Pain - Post-Surgical',
                            value: 'cervicalPainPostsurgical'
                        },
                        {
                            label: 'Thoracic Pain - Acute',
                            value: 'thoracicPainAcute'
                        },
                        {
                            label: 'Thoracic Pain - Subacute',
                            value: 'thoracicPainSubacute'
                        },
                        {
                            label: 'Thoracic Pain - Chronic',
                            value: 'thoracicPainChronic'
                        },
                        {
                            label: 'Thoracic Pain - Chronic Disabling',
                            value: 'thoracicPainChronicDisabling'
                        },
                        {
                            label: 'Thoracic Pain - Post-Surgical',
                            value: 'thoracicPainPostsurgical'
                        },
                        {
                            label: 'Radicular Pain - Acute',
                            value: 'radicularPainAcute'
                        },
                        {
                            label: 'Radicular Pain - Subacute',
                            value: 'radicularPainSubacute'
                        },
                        {
                            label: 'Radicular Pain - Chronic',
                            value: 'radicularPainChronic'
                        },
                        {
                            label: 'Radicular Pain - Chronic Episodic',
                            value: 'radicularPainChronicEpisodic'
                        },
                        {
                            label: 'Radicular Syndromes - Acute',
                            value: 'radicularSyndromesAcute'
                        },
                        {
                            label: 'Radicular Syndromes - Subacute',
                            value: 'radicularSyndromesSubacute'
                        },
                        {
                            label: 'Radicular Syndromes - Chronic',
                            value: 'radicularSyndromesChronic'
                        },
                        {
                            label: 'Radiculopathy - Acute',
                            value: 'radiculopathyAcute'
                        },
                        {
                            label: 'Radiculopathy - Subacute',
                            value: 'radiculopathySubacute'
                        },
                        {
                            label: 'Radiculopathy - Chronic',
                            value: 'radiculopathyChronic'
                        },
                        {
                            label: 'Radiculopathy - Post-Operative',
                            value: 'radiculopathyPostOperative'
                        },
                        {
                            label: 'Spinal Stenosis',
                            value: 'spinalStenosis'
                        },
                        {
                            label: 'Spine Pain - Acute',
                            value: 'spinePainAcute'
                        },
                        {
                            label: 'Spine Pain - Subacute',
                            value: 'spinePainSubacute'
                        },
                        {
                            label: 'Spine Pain - Chronic',
                            value: 'spinePainChronic'
                        },
                        {
                            label: 'Spine Pain - Post-Surgical',
                            value: 'spinePainPostSurgical'
                        },
                        {
                            label: 'Neuropathic Pain',
                            value: 'neuropathicPain'
                        },
                        {
                            label: 'Neuropathic Pain - Chronic',
                            value: 'neuropathicPainChronic'
                        },
                        {
                            label: 'Peri-Operative Pain',
                            value: 'periOperativePain'
                        },
                        {
                            label: 'Cervical Spine Conditions',
                            value: 'cervicalSpineConditions'
                        },
                        {
                            label: 'Tention Headaches',
                            value: 'tentionHeadaches'
                        },
                        {
                            label: 'Cervical Myofascial Pain',
                            value: 'cervicalMyofascialPain'
                        },
                        {
                            label: 'Cervicogenic Headaches',
                            value: 'cervicogenicHeadaches'
                        },
                    ],
                },
                {
                    id: 4,
                    value: 'services',
                    label: 'Services',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    disabled: records.diagnosis != undefined ? false : true,
                    options: [
                        {
                            label: 'X-ray',
                            value: 'xray'
                        },
                        {
                            label: 'EMG',
                            value: 'emg'
                        },
                        {
                            label: 'Surface EMG (sEMG)',
                            value: 'surfaceEmg(sEMG)'
                        },
                        {
                            label: 'MRI',
                            value: 'mri'
                        },
                        {
                            label: 'Discography',
                            value: 'discography'
                        },
                        {
                            label: 'SPECT',
                            value: 'spect'
                        },
                        {
                            label: 'FCE',
                            value: 'fce'
                        },
                        {
                            label: 'Myeloscopy',
                            value: 'myeloscopy'
                        },
                        {
                            label: 'Ultrasound',
                            value: 'ultrasound'
                        },
                        {
                            label: 'Thermography',
                            value: 'thermography'
                        },
                        {
                            label: 'Fluoroscopy',
                            value: 'fluoroscopy'
                        },
                        {
                            label: 'Videofluoroscopy',
                            value: 'videofluoroscopy'
                        },
                        {
                            label: 'Educational Programs',
                            value: 'educationalPrograms'
                        },
                        {
                            label: 'Rest and Immobilization',
                            value: 'restAndImmobilization'
                        },
                        {
                            label: 'Sleep Posture',
                            value: 'sleepPosture'
                        },
                        {
                            label: 'Mattresses',
                            value: 'mattresses'
                        },
                        {
                            label: 'Optimal Sleeping Surfaces',
                            value: 'optimalSleepingSurfaces'
                        },
                        {
                            label: 'General Exercise Approach',
                            value: 'generalExerciseApproach'
                        },
                        {
                            label: 'Aerobic Exercises',
                            value: 'aerobicExercises'
                        },
                        {
                            label: 'Directional Exercises',
                            value: 'directionalExercises'
                        },
                        {
                            label: 'Stretching Exercises',
                            value: 'stretchingExercises'
                        },
                        {
                            label: 'Strengthening and Stabilization Exercises',
                            value: 'strengtheningAndStabilizationExercises'
                        },
                        {
                            label: 'Fear Avoidance Belief Training',
                            value: 'fearAvoidanceBeliefTraining'
                        },
                        {
                            label: 'Aquatic Therapy (Includes Swimming)',
                            value: 'aquaticTherapy(includesSwimming)'
                        },
                        {
                            label: 'Yoga',
                            value: 'yoga'
                        },
                        {
                            label: 'NSAIDs',
                            value: 'nsaids'
                        },
                        {
                            label: 'Acetaminophen',
                            value: 'acetaminophen'
                        },
                        {
                            label: 'TCAs',
                            value: 'tcas'
                        },
                        {
                            label: 'SNRIs',
                            value: 'snris'
                        },
                        {
                            label: 'Anti-Depressants',
                            value: 'antiDepressants'
                        },
                        {
                            label: 'Topiramate',
                            value: 'topiramate'
                        },
                        {
                            label: 'Carbamazepine',
                            value: 'carbamazepine'
                        },
                        {
                            label: 'Gabapentin',
                            value: 'gabapentin'
                        },
                        {
                            label: 'Capsaicin',
                            value: 'capsaicin'
                        },
                        {
                            label: 'Spiroflor',
                            value: 'spiroflor'
                        },
                        {
                            label: 'Topical NSAIDs',
                            value: 'topicalNSAIDs'
                        },
                        {
                            label: 'DMSO',
                            value: 'dmso'
                        },
                        {
                            label: 'N-Acetylcysteine',
                            value: 'NAcetylcysteine'
                        },
                        {
                            label: 'EMLA Creme',
                            value: 'EMLACreme'
                        },
                        {
                            label: 'Wheatgrass Cream',
                            value: 'wheatgrassCream'
                        },
                        {
                            label: 'Other Creams and Ointments',
                            value: 'otherCreamsAndOintments'
                        },
                        {
                            label: 'Lidocaine Patches',
                            value: 'lidocainePatches'
                        },
                        {
                            label: 'Oral Colchicine',
                            value: 'oralColchicine'
                        },
                        {
                            label: 'IV Colchicine',
                            value: 'IVColchicine'
                        },
                        {
                            label: 'Thiocolchicoside',
                            value: 'thiocolchicoside'
                        },
                        {
                            label: 'Glucocorticosteroids',
                            value: 'glucocorticosteroids'
                        },
                        {
                            label: 'Physician',
                            value: 'physician'
                        },
                        {
                            label: 'Muscle Relaxants',
                            value: 'muscleRelaxants'
                        },
                        {
                            label: 'Opioids',
                            value: 'opioids'
                        },
                        {
                            label: 'Vitamins',
                            value: 'vitamins'
                        },
                        {
                            label: 'Physical Therapy',
                            value: 'physicalTherapy'
                        },
                        {
                            label: 'Occupational Therapy',
                            value: 'occupationalTherapy'
                        },
                        {
                            label: 'Magnets and Magnetic Stimulation',
                            value: 'magnetsAndMagneticStimulation'
                        },
                        {
                            label: 'Iontophoresis',
                            value: 'iontophoresis'
                        },
                        {
                            label: 'Acupuncture',
                            value: 'acupuncture'
                        },
                        {
                            label: 'Ultrasound (Theraputical)',
                            value: 'ultrasound(theraputical)'
                        },
                        {
                            label: 'Low Level Laser Therapy',
                            value: 'lowLevelLaserTherapy'
                        },
                        {
                            label: 'Manipulation/Mobilization',
                            value: 'manipulationMobilization'
                        },
                        {
                            label: 'Manipulation',
                            value: 'manipulation'
                        },
                        {
                            label: 'Cervical Manipulation',
                            value: 'cervicalManipulation'
                        },
                        {
                            label: 'Manipulation Under Anesthesia (MUA)',
                            value: 'manipulationUnderAnesthesia(MUA)'
                        },
                        {
                            label: 'Medication-Assisted Spinal Manipulation (MASM)',
                            value: 'medicationAssistedSpinalManipulation(MASM)'
                        },
                    ],
                }
            ]
        }
    }

    function globalSettings() {
        return {
            section:
                [
                    {
                        schemaId: 1,
                        name: 'HIPAA Complaince',
                        label: 'OTHER SETTINGS',
                        path: '',
                        create: true,
                        edit: true,
                        view: true,
                        value: 'pricing',
                        filter: (record) => record,
                        confirmMessage: "Do you want to save this change?",
                        noText: true,
                        columns: [
                            {
                                id: 1,
                                value: 'session_timeout',
                                label: 'Session Expiration Timeout (in seconds)',
                                editRecord: true,
                                viewRecord: true,
                                viewMode: false,
                                visible: false,
                                mandatory: true,
                                type: 'number'
                            }
                        ]
                    },
                    {
                        schemaId: 2,
                        name: 'RFA Form Limits',
                        label: 'OTHER SETTINGS',
                        path: '',
                        create: true,
                        edit: true,
                        view: true,
                        value: 'pricing',
                        filter: (record) => record,
                        confirmMessage: "Do you want to save this change?",
                        columns: [
                            {
                                id: 1,
                                value: 'rfa_form_row_limit',
                                label: 'No. of maximum treatments allowed',
                                editRecord: true,
                                viewRecord: true,
                                viewMode: false,
                                visible: false,
                                mandatory: true,
                                type: 'number'
                            }
                        ]
                    },
                ]
        }
    }

    function practices() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'contact_name',
                    label: 'CONTACT NAME',
                    sortColumn: 'contact_name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'created_at',
                    label: 'CREATED DATE',
                    sortColumn: 'created_at',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['created_at'] && moment(row['created_at']).format('MM/DD/YY'),
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'address',
                    label: 'ADDRESS',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'city',
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'state',
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'phone_number',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'phone'
                },
                {
                    id: 10,
                    value: 'fax_number',
                    label: 'FAX NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'phone'
                },
                {
                    id: 11,
                    value: 'npi_number',
                    label: 'NPI NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 12,
                    value: 'logoFile',
                    label: 'LOGO',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    upload: true,
                    contentType: 'image/*',
                    type: 'upload'
                }
            ],
        }
    }
    function ClaimsAdministrator() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'company_name',
                    label: 'COMPANY NAME',
                    sortColumn: 'company_name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'contact_name',
                    label: 'CONTACT NAME',
                    sortColumn: 'contact_name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: false,
                    mandatory: false,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'email',
                    label: 'EMAIL',
                    sortColumn: 'email',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 5,
                    value: 'address',
                    label: 'ADDRESS',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'city',
                    label: 'CITY',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    sortColumn: 'city',
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'state',
                    label: 'STATE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    sortColumn: 'state',
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'zip_code',
                    label: 'ZIP CODE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 9,
                    value: 'phone_number',
                    label: 'PHONE',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row['phone_number'] && (row['phone_number'].replace(/[()\ \s-]+/g, '').length == 10) && new AsYouType('US').input(row.phone_number) || row['phone_number'],// eslint-disable-line
                    // max: 14,
                    type: 'phone'
                },
                {
                    id: 10,
                    value: 'fax_number',
                    label: 'FAX NUMBER',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    html: (row) => row['fax_number'] && (row['fax_number'].replace(/[()\ \s-]+/g, '').length == 10) && new AsYouType('US').input(row.fax_number) || row['fax_number'],// eslint-disable-line
                    // max: 14,
                    type: 'phone'
                },
                {
                    id: 11,
                    value: 'created_at',
                    label: 'CREATED DATE',
                    sortColumn: 'created_at',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => row['created_at'] && moment(row['created_at']).format('MM/DD/YY'),
                    type: 'input'
                },
            ],
        }
    }


    function adminclaimConnectsColumns() {
        return {
            columns: [
                {
                    id: 1,
                    value: 'id',
                    label: 'ID',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'NAME',
                    sortColumn: 'name',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 3,
                    value: 'email',
                    label: 'EMAIL',
                    sortColumn: 'email',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 4,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    type: 'password',
                    mandatory: {
                        create: true
                    }
                },
                {
                    id: 5,
                    value: 'role',
                    label: 'ROLE',
                    sortColumn: 'role',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    html: (row) => {
                        return row['role'] === 'superAdmin' ? 'Super Admin' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'];
                    },
                    options: [
                        {
                            label: 'Super Admin',
                            value: 'superAdmin',
                        },
                        {
                            label: 'Manager',
                            value: 'manager',
                        },
                        {
                            label: 'Operator',
                            value: 'operator',
                        },
                    ],
                    type: 'select',
                    mandatory: true
                }
            ]
        }
    }

    function adminPracticeUsers(record = {}) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'practice_name',
                    label: 'PRACTICE NAME',
                    sortColumn: 'Practices.name',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'practice_id',
                    label: 'PRACTICE NAME',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    options: 'practicesOptions',
                    disableOptons: {
                        edit: true
                    },
                    mandatory: true,
                    type: 'autoComplete'
                },
                {
                    id: 3,
                    value: 'name',
                    label: 'NAME',
                    sortColumn: 'Users.name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 4,
                    value: 'email',
                    label: 'EMAIL',
                    sortColumn: 'Users.email',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    disableOptons: {
                        edit: true
                    },
                    sort: true,
                    type: 'input',
                    mandatory: true
                },
                {
                    id: 5,
                    value: 'password',
                    label: 'PASSWORD',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'password',
                    mandatory: {
                        create: true
                    },
                },
                {
                    id: 6,
                    value: 'role',
                    label: 'ROLE',
                    sortColumn: 'Users.role',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['role'] === 'physician' ? 'Physician' : row['role'] === 'staff' ? 'Staff' : capitalizeFirstLetter && capitalizeFirstLetter(row['role']) || row['role'],
                    options: [
                        {
                            label: 'Physician',
                            value: 'physician',
                        },
                        {
                            label: 'Staff',
                            value: 'staff',
                        }
                    ],
                    type: 'select',
                    mandatory: true
                },
                {
                    id: 7,
                    value: 'specialty',
                    label: 'SPECIALTY',
                    editRecord: record.role ? record.role != 'staff' ? true : false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'Users.specialty',
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'is_admin',
                    label: 'ADMIN',
                    sortColumn: 'Users.is_admin',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    sort: true,
                    html: (row) => row['is_admin'] ? 'Yes' : 'No',
                    type: 'checkbox'
                },
                {
                    id: 9,
                    value: 'is_admin',
                    label: 'Make a user an Admin',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    sort: true,
                    type: 'checkbox'
                },

            ]
        }
    }

    function treatments(record = {}) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'body_system',
                    label: 'BODY SYSTEM',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 2,
                    value: 'name',
                    label: 'POLICY DOC NAME (TREATMENT)',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'name',
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'type',
                    label: 'TREATMENT TYPE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    html: (row) => row['type'] && (row['type'] == "surgery-add-on" && "Surgery Add-On" || capitalizeFirstLetter(row['type'])) || row['type'],
                    sort: true,
                    sortColumn: 'type',
                    type: 'input',

                },
                {
                    id: 4,
                    value: 'policyDoc',
                    label: 'POLICY LINK',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea',
                    html: (row, policyStyle) => {// eslint-disable-line
                        return <SummarizeOutlinedIcon />
                    }
                },
                {
                    id: 5,
                    value: 'created_at',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'created_at',
                    html: (row) => row && row['created_at'] && moment(row['created_at']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 6,
                    value: 'created_by',
                    label: 'CREATED BY',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'created_by',
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'doc',
                    label: 'TREATMENT FILE',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    contentType: '.json',
                    upload: true,
                    type: 'upload',
                },
                {
                    id: 8,
                    value: 's3_file_key',
                    label: 'S3 File Key',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'textarea'
                },
                {
                    id: 9,
                    value: 'status',
                    label: 'STATUS',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'status',
                    type: 'input'
                },
                {
                    id: 10,
                    value: 'showText',
                    label: 'This page shows all policy docs currently available in the system. Each policy doc is for one treatment (eg: Acupuncture) in one body system (eg: Neck and Upper Back). A policy doc can have one or more policies, one for each diagnosis.',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
            ]
        }
    }

    function diagnosis(record = {}) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'treatments',
                    label: 'Treatments JSON',
                    placeholder: 'Paste a JSON here.',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    type: 'textarea'
                },
                {
                    id: 2,
                    value: 'body_system',
                    label: 'BODY SYSTEM',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'treatment_name',
                    label: 'POLICY DOC NAME (TREATMENT)',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'treatment_name',
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'name',
                    label: 'DIAGNOSIS',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'name',
                    html: (row) => row && row['name'] == "NoDiagnosisFound" ? '-' : row['name'],
                    type: 'input'
                },
                {
                    id: 5,
                    value: 'source',
                    label: 'SOURCE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'source',
                    html: (row) => row && row['source'] && row['source'].toUpperCase() || '-',
                    type: 'input'
                },
                {
                    id: 6,
                    value: 'created_at',
                    label: 'CREATED DATE',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'created_at',
                    html: (row) => row && row['created_at'] && moment(row['created_at']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 7,
                    value: 'created_by',
                    label: 'CREATED BY',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'created_by',
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'showText',
                    label: 'This page shows all policies currently available in the system. Each policy is for one treatment (eg: Acupuncture) in one body system (eg: Neck and Upper Back) for one diagnosis (eg: Cervical Pain - Chronic). All these policies are picked up from policy docs (to update/edit go to policy doc menu).',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
            ]
        }
    }

    function treatmentsCreate(record = {}) {// eslint-disable-line
        const policyValue = record && record.policy || "";
        return {
            columns: [
                {
                    id: 1,
                    value: 'entry',
                    label: 'Entry',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    defaultValue: 'manual',
                    onChange: () => typeof record.updateField === 'function' && record.updateField("createTreatmentForm", "policy", "") || false,
                    options: [{
                        label: `${record.process} Policy Doc`,
                        value: 'manual',
                    },
                    {
                        label: 'Treatment JSON',
                        value: 'fileUpload',
                    }],
                    type: 'radio'
                },
                {
                    id: 2,
                    value: 'body-system',
                    label: 'Body System',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    options: "bodySystemList",
                    type: 'select'
                },
                {
                    id: 3,
                    value: 'treatment',
                    label: 'Treatment',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: 'treatment-type',
                    label: 'Treatment Type',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: false,
                    mandatory: true,
                    type: 'select',
                    options: [
                        {
                            label: 'Surgery',
                            value: 'surgery',
                        },
                        {
                            label: 'Surgery Add-on',
                            value: 'surgery-add-on',
                        },
                        {
                            label: 'Other',
                            value: 'other',
                        }
                    ]
                },
                {
                    id: 5,
                    value: 'mtus',
                    label: 'MTUS',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    placeholder: 'mm/dd/yy',
                    type: 'input',
                    node: true,
                    nodeValue: 'Version'
                },
                {
                    id: 6,
                    value: 'odg',
                    label: 'ODG',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    placeholder: 'mm/dd/yy',
                    type: 'input',
                    node: true,
                    nodeValue: 'Version'
                },
                {
                    id: 7,
                    value: 'comments',
                    label: 'Comments',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input',
                    node: true,
                    nodeValue: 'Version'
                },
                {
                    id: 8,
                    value: 'mtus-link',
                    label: 'MTUS Link',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    rows: 2,
                    type: 'textarea',
                    node: true,
                    nodeValue: 'References'
                },
                {
                    id: 9,
                    value: 'odg-link',
                    label: 'ODG Link',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    rows: 2,
                    type: 'textarea',
                    node: true,
                    nodeValue: 'References'
                },
                {
                    id: 10,
                    value: 'policy',
                    label: 'Policy',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: true,
                    type: 'radio',
                    node: true,
                    required: true,
                    defaultValue: policyValue,
                    nodeValue: 'Policies',
                    options: [{
                        label: 'Yes',
                        value: 'yes',
                    },
                    {
                        label: 'No',
                        value: 'no',
                    },]
                },
                {
                    id: 11,
                    value: 'doc',
                    label: 'Treatment JSON',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    mandatory: true,
                    type: 'textarea',
                },
            ]
        }
    }

    function bodySystem(record = {}) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'name',
                    label: 'Name',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: "input"
                },
                {
                    id: 2,
                    value: 'created_at',
                    label: 'Created Date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row && row['created_at'] && moment(row['created_at']).format('MM/DD/YY HH:mm:ss'),
                    type: "input"
                },
                {
                    id: 3,
                    value: 'created_by',
                    label: 'Created By',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
            ]
        }
    }

    function viewHistory(record = {}) {// eslint-disable-line
        return {
            columns: [

                {
                    id: 1,
                    value: 'updated_at',
                    label: 'Updated Date',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    html: (row) => row && row['updated_at'] && moment(row['updated_at']).format('MM/DD/YY HH:mm:ss'),
                    type: 'date'
                },
                {
                    id: 2,
                    value: 'updated_by',
                    label: 'Updated By',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 3,
                    value: 'policyDoc',
                    label: 'Policy Doc',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'textarea',
                    html: (row, policyStyle) => {// eslint-disable-line
                        return <Button name="btnTocheck" className={policyStyle} onClick={(e) => {
                            e.stopPropagation();
                            getPublicUrl({ "s3_file_key": row['s3_file_key'] }).then(res => {
                                window.open(res.publicUrl, '_blank')
                            }).catch(error => Promise.reject(error))
                        }}><SummarizeOutlinedIcon /></Button>
                    }
                },
                {
                    id: 4,
                    value: 'jsonSchema',
                    label: 'Difference',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    type: 'jsonData'
                },
            ]
        }
    }

    function settings(user = {}) {// eslint-disable-line
        return {

            section: [
                {
                    schemaId: 1,
                    name: '',
                    create: true,
                    edit: true,
                    view: true,
                    value: 'practice',
                    filter: (record) => record,
                    confirmMessage: "Changes you make to practice settings will apply for all users in your practice. Do you want to continue?",
                    columns: [
                        {
                            id: 1,
                            value: 'id',
                            label: 'ID',
                            editRecord: false,
                            viewRecord: false,
                            viewMode: false,
                            visible: false,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'name',
                            label: 'Practice Name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 3,
                            value: 'contact_name',
                            label: 'Contact Name',
                            sortColumn: 'contact_name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: false,
                            mandatory: false,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 4,
                            value: 'address',
                            label: 'Address',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input',
                            placeholder: 'eg: 101 Main Street'
                        },
                        {
                            id: 5,
                            value: 'city',
                            label: 'City',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 6,
                            value: 'state',
                            label: 'State',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 7,
                            value: 'zip_code',
                            label: 'Zip Code',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 8,
                            value: 'phone_number',
                            label: 'Phone',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'phone'
                        },
                        {
                            id: 9,
                            value: 'fax_number',
                            label: 'Fax',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'phone'
                        },
                        {
                            id: 10,
                            value: 'npi_number',
                            label: 'NPI Number',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 11,
                            value: 'logoFile',
                            label: 'Logo',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: true,
                            upload: true,
                            contentType: 'image/*',
                            type: 'upload'
                        }

                    ]
                },
                {
                    schemaId: 2,
                    name: 'App Version',
                    create: true,
                    view: user && (user.email === 'siva@ospitek.com' || user.email === 'senthilei27@gmail.com') ? true : false,
                    edit: true,
                    value: 'version',
                    confirmMessage: "Changing the version will reset the cache data for all Users.Do you want to continue?",
                    filter: (record) => record || {},
                    columns: [
                        {
                            id: 1,
                            value: 'version',
                            label: 'Version',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: false,
                            visible: false,
                            mandatory: false,
                            type: 'number'
                        }
                    ]
                }

            ]
        }
    }

    function poc3(records = []) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: `approval`,
                    label: 'Confidence of Approval',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 2,
                    value: `bodySystemId`,
                    label: 'Body System',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'select',
                    options: 'bodySystems'
                },
                {
                    id: 5,
                    value: `icdCode`,
                    label: 'ICD-Code (Required)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input'
                },
                {
                    id: 6,
                    value: `diagnosis`,
                    label: 'Diagnosis (Required)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'selectAccordion',
                    options: 'diagnosisOptions'
                },
                {
                    id: 3,
                    value: `cptCode`,
                    label: 'CPT/HCPCS Code (If known)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 4,
                    value: `treatments`,
                    label: 'Service/Good Requested (Required)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'selectAccordion',
                    options: 'treatmentOptions'
                },
                {
                    id: 7,
                    value: `otherInformation`,
                    label: 'Other Information: (Frequency, Duration Quantity, etc.)',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 8,
                    value: `delete`,
                    label: '',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'button',
                }
            ]
        }
    }

    function cases(user) {
        return {
            columns: [
                // {
                //     id: 1,
                //     value: 'entry',
                //     label: 'Entry',
                //     editRecord: true,
                //     viewRecord: false,
                //     viewMode: true,
                //     visible: false,
                //     mandatory: true,
                //     defaultValue: 'new',
                //     options: [{
                //         label: `New Patient`,
                //         value: 'new',
                //     },
                //     {
                //         label: 'Existing Patient',
                //         value: 'exist',
                //     }],
                //     type: 'radio'
                // },
                // {
                //     id: 11,
                //     value: `existingPatients`,
                //     label: 'Existing Patients',
                //     editRecord: user && user.entry && user.entry === 'exist' ? true : false,
                //     mandatory: true,
                //     viewRecord: true,
                //     viewMode: true,
                //     visible: false,
                //     type: 'select',
                //     options: 'existingPatients'
                // },
                {
                    id: 1,
                    value: `name`,
                    label: 'NAME',
                    editRecord: user && user.entry && user.entry === "exist" && !user.existingPatients ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'name',
                    type: 'input',
                },
                {
                    id: 2,
                    value: `date_of_injury`,
                    label: 'DATE OF INJURY',
                    placeholder: 'mm/dd/yyyy',
                    editRecord: user && user.entry && user.entry === "exist" && !user.existingPatients ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'date_of_injury',
                    html: (row) => row['date_of_injury'] && moment(row['date_of_injury']).format('MM/DD/YYYY') || row['date_of_injury'],
                    mandatory: true,
                    type: 'input',
                },
                {
                    id: 3,
                    value: `date_of_birth`,
                    label: 'DATE OF BIRTH',
                    placeholder: 'mm/dd/yyyy',
                    editRecord: user && user.entry && user.entry === "exist" && !user.existingPatients ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    html: (row) => row['date_of_birth'] && moment(row['date_of_birth']).format('MM/DD/YYYY') || row['date_of_birth'],
                    mandatory: true,
                    type: 'input'
                },
                {
                    id: 4,
                    value: `claim_number`,
                    label: 'CLAIM NUMBER',
                    editRecord: user && user.entry && user.entry === "exist" && !user.existingPatients ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    mandatory: true,
                    sort: true,
                    sortColumn: 'claim_number',
                    type: 'input',
                },
                {
                    id: 5,
                    value: `employer`,
                    label: 'EMPLOYER',
                    editRecord: user && user.entry && user.entry === "exist" && !user.existingPatients ? false : true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    sort: true,
                    sortColumn: 'employer',
                    type: 'input',
                },
                {
                    id: 6,
                    value: 'showText',
                    label: 'Patient Information entered here is used to fill DWC Form RFA.',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: false,
                    type: 'input'
                },
                {
                    id: 7,
                    value: 'assingee_name',
                    label: 'PHYSICIAN ASSIGNED',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: user.role == 'staff' ? true : false,
                    type: 'input'
                },
                {
                    id: 8,
                    value: 'claims_company_name',
                    label: 'CLAIMS ADMINISTRATOR',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 9,
                    value: 'generated_by',
                    label: 'GENERATED BY',
                    editRecord: false,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                },
                {
                    id: 10,
                    value: 'generatedDoc',
                    label: 'GENERATED FORM',
                    editRecord: false,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'input',
                    html: (row, policyStyle) => {// eslint-disable-line
                        return row['s3_file_key'] && <Button name="btnTocheck" style={{ display: 'flex', flexDirection: 'column', width: '100%' }} onClick={(e) => {
                            e.stopPropagation();

                            getPublicUrlForRfaForm({ "s3_file_key": row['s3_file_key'] }).then(res => {
                                window.open(res.publicUrl, '_blank')
                            }).catch(error => Promise.reject(error))
                        }}>
                            <GrIconWrapper icon={GrDocumentPdf} />
                            <span style={{ fontSize: '11px' }}>{row['form_generate_date'] && moment(row['form_generate_date']).format('MM/DD/YY') || ''}</span>
                        </Button> || ''
                    }
                },
            ]
        }
    }

    function signature(user) {
        return {
            columns: [
                {
                    id: 1,
                    value: 'signature',
                    label: 'Signature',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'signature',
                    mandatory: true,
                    showSignature: user && user.signature ? true : false,
                    signature: user && user.signature,
                }
            ]
        }
    }

    function generateRfa(user, record, adminAssignedValue, path, entry) {// eslint-disable-line
        return {
            columns: [
                {
                    id: 1,
                    value: 'request_type',
                    label: 'Request Type',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: true,
                    type: 'multicheck',
                    options:
                        [
                            {
                                label: 'New Request',
                                value: 'newRequest',
                            },
                            {
                                label: 'Resubmission – Change in Material Facts',
                                value: 'resubmission',
                            },
                            {
                                label: 'Expedited Review: Check box if employee faces an imminent and serious threat to his or her health',
                                value: 'expeditedReview',
                            },
                            {
                                label: 'Check box if request is a written confirmation of a prior oral request.',
                                value: 'checkboxConfirmation',
                            }
                        ],
                    nodeValue: 'Request Type',
                },
                
                // {
                //     id: 2,
                //     value: 'entry',
                //     // label: 'Patient Info',
                //     nodeValue: 'Patient Info',
                //     editRecord: true,
                //     viewRecord: false,
                //     viewMode: true,
                //     visible: path && path === '/claims' ? true : false,
                //     defaultValue: 'new',
                //     options: [{
                //         label: `New Patient`,
                //         value: 'new',
                //     },
                //     {
                //         label: 'Existing Patient',
                //         value: 'exist',
                //     }],
                //     type: 'radio'
                // },
                // {
                //     id: 3,
                //     value: `existingPatients`,
                //     label: 'Existing Patients',
                //     editRecord: true,
                //     viewRecord: true,
                //     viewMode: true,
                //     mandatory: true,
                //     visible: entry && entry === 'exist' ? true : false,
                //     type: 'select',
                //     options: 'existingPatients'
                // },

                {
                    id: 4,
                    value: 'patientInfo',
                    label: 'Patient Info',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: path && path == '/claims' ? true : false,
                    nodeValue: 'Patient Info', //For without existing Patiet
                    // visible: entry && entry === 'new' ? true : entry && entry == 'exist' && (adminAssignedValue.existingPatients && adminAssignedValue.existingPatients != '') ? true : false,  //For existing Patiet
                    type: 'fieldArray',
                    max: 5,
                    defaultField: true,
                    fieldArray: [
                        {
                            id: 1,
                            value: `name`,
                            label: 'NAME',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            sort: true,
                            sortColumn: 'name',
                            type: 'input',
                        },
                        {
                            id: 2,
                            value: `date_of_injury`,
                            label: 'DATE OF INJURY',
                            placeholder: 'mm/dd/yyyy',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            sortColumn: 'date_of_injury',
                            mandatory: true,
                            type: 'input',
                        },
                        {
                            id: 3,
                            value: `date_of_birth`,
                            label: 'DATE OF BIRTH',
                            placeholder: 'mm/dd/yyyy',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            type: 'input'
                        },
                        {
                            id: 4,
                            value: `claim_number`,
                            label: 'CLAIM NUMBER',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            sort: true,
                            sortColumn: 'claim_number',
                            type: 'input',
                        },
                        {
                            id: 5,
                            value: `employer`,
                            label: 'EMPLOYER',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            sortColumn: 'employer',
                            type: 'input',
                        }
                    ]
                },


                {
                    id: 5,
                    value: `assignee_id`,
                    label: `Physician`,
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: user.role === "staff" ? true : false,
                    type: 'select',
                    options: 'PhysicianOptions',
                    nodeValue: 'Physician Assigned',
                },
                {
                    id: 6,
                    value: `claims_admin_id`,
                    label: `Claims Administrator`,
                    editRecord: true,
                    viewRecord: true,
                    viewMode: true,
                    visible: true,
                    type: 'select',
                    options: 'AdministratorOptions',
                    nodeValue: 'Claims Administrator Assigned',

                },
                {
                    id: 7,
                    value: 'claim_admin_details',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: true,
                    visible: adminAssignedValue && adminAssignedValue.claims_admin_id == 'others' ? true : false,
                    type: 'fieldArray',
                    max: 5,
                    defaultField: true,
                    fieldArray: [
                        {
                            id: 1,
                            value: 'company_name',
                            label: 'COMPANY NAME',
                            sortColumn: 'company_name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 2,
                            value: 'contact_name',
                            label: 'CONTACT NAME',
                            sortColumn: 'contact_name',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: false,
                            mandatory: false,
                            sort: true,
                            type: 'input'
                        },
                        {
                            id: 3,
                            value: 'email',
                            label: 'EMAIL',
                            sortColumn: 'email',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            type: 'input',
                            mandatory: true
                        },
                        {
                            id: 4,
                            value: 'address',
                            label: 'ADDRESS',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            mandatory: true,
                            type: 'input'
                        },
                        {
                            id: 5,
                            value: 'city',
                            label: 'CITY',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            sortColumn: 'city',
                            type: 'input'
                        },
                        {
                            id: 6,
                            value: 'state',
                            label: 'STATE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            sort: true,
                            sortColumn: 'state',
                            type: 'input'
                        },
                        {
                            id: 7,
                            value: 'zip_code',
                            label: 'ZIP CODE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            type: 'input'
                        },
                        {
                            id: 8,
                            value: 'phone_number',
                            label: 'PHONE',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            html: (row) => row['phone_number'] && (row['phone_number'].replace(/[()\ \s-]+/g, '').length == 10) && new AsYouType('US').input(row.phone_number) || row['phone_number'],// eslint-disable-line
                            // max: 14,
                            type: 'phone'
                        },
                        {
                            id: 9,
                            value: 'fax_number',
                            label: 'FAX NUMBER',
                            editRecord: true,
                            viewRecord: true,
                            viewMode: true,
                            visible: true,
                            html: (row) => row['fax_number'] && (row['fax_number'].replace(/[()\ \s-]+/g, '').length == 10) && new AsYouType('US').input(row.fax_number) || row['fax_number'],// eslint-disable-line
                            // max: 14,
                            type: 'phone'
                        }
                    ]
                },
                {
                    id: 8,
                    value: 'cover_page',
                    label: 'Cover Page',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    viewFiles: true,
                    contentType: 'application/pdf',
                    multiple: true,
                    max: 1,
                    uploadName: 'cover_page',
                    rfaFormId: record && record['case_data'] && record['case_data'].id || '',
                    type: 'multiupload',
                    upload: true,
                    nodeValue: 'Cover Page',
                    errorType: true
                },
                {
                    id: 9,
                    value: 'clinical_notes',
                    label: 'Clinical Notes',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    nodeValue: 'Clinical Notes',
                    uploadName: 'clinical_notes',
                    contentType: 'application/pdf',
                    multiple: true,
                    max: 5,
                    type: 'multiupload',
                    rfaFormId: record && record['case_data'] && record['case_data'].id || '',
                    upload: true,
                    errorType: true
                },
                {
                    id: 10,
                    value: 'imaging_reports',
                    label: 'Imaging Reports',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    nodeValue: 'Imaging Reports',
                    uploadName: 'imaging_reports',
                    contentType: 'application/pdf',
                    rfaFormId: record && record['case_data'] && record['case_data'].id || '',
                    multiple: true,
                    max: 10,
                    type: 'multiupload',
                    upload: true,
                    errorType: true
                },
                {
                    id: 11,
                    value: 'supporting_docs',
                    label: 'Other supporting docs',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    nodeValue: 'Other supporting docs',
                    uploadName: 'supporting_docs',
                    contentType: 'application/pdf',
                    rfaFormId: record && record['case_data'] && record['case_data'].id || '',
                    multiple: true,
                    max: 10,
                    type: 'multiupload',
                    upload: true,
                    errorType: true
                },
                {
                    id: 12,
                    value: 'non_confirming_claims',
                    label: 'claims confirming to guidelines',
                    editRecord: true,
                    viewRecord: false,
                    viewMode: false,
                    visible: true,
                    nodeValue: 'Explanation of claims confirming to guidelines',
                    uploadName: 'non_confirming_claims',
                    contentType: 'application/pdf',
                    rfaFormId: record && record['case_data'] && record['case_data'].id || '',
                    multiple: true,
                    max: 1,
                    type: 'multiupload',
                    upload: true,
                    errorType: true
                },
                {
                    id: 13,
                    value: 'signature',
                    label: 'Signature',
                    editRecord: true,
                    viewRecord: true,
                    viewMode: false,
                    visible: user.role === "staff" ? false : true,
                    type: 'customSignature',
                    nodeValue: 'Signature',
                    signature: user && user.signature,
                },

            ]
        }
    }

    return {
        users,
        changePassword,
        login,
        userSettings,
        patients,
        insurances,
        globalSettings,
        practices,
        ClaimsAdministrator,
        adminclaimConnectsColumns,
        adminPracticeUsers,
        treatments,
        diagnosis,
        treatmentsCreate,
        bodySystem,
        viewHistory,
        settings,
        poc3,
        cases,
        signature,
        generateRfa
    }
}
