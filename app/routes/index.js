
/**
 * 
 * Routes
 * 
 */

import React from 'react';
import { Switch } from 'react-router-dom';
import App from 'containers/App';
import Auth from 'utils/routing/auth';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import simpleLazyLoadedRouteProvider from 'utils/routing/simpleLazyLoadedRoute';
import routeSwitcher from 'utils/routing/routeSwitcher';
import { selectLoggedIn, selectUser, selectAppVersion } from 'blocks/session/selectors';
import reducerInjectors from 'utils/reducerInjectors';
import sagaInjectors from 'utils/sagaInjectors';
import injectBlocks from 'utils/injectBlocks';
import applyPromise from 'utils/promises';
import sessionSagas from 'blocks/session/sagas';


function Routes(routeProps) {

    const { store, user, loggedIn, appVersion } = routeProps;
    const { injectReducer } = reducerInjectors(store);
    const { injectSaga } = sagaInjectors(store);
    /**
     * External Sagas will import here
     */
    injectSaga('session', { saga: sessionSagas });
    const simpleLazyLoadedRoute = simpleLazyLoadedRouteProvider(injectReducer, injectSaga, injectBlocks, applyPromise);
    const routesProvider = routeSwitcher(user, loggedIn, simpleLazyLoadedRoute);

    return (<App pages={routesProvider.filter(_ => _.data && !_.data.route)}
    >
        <Switch>
            {routesProvider.map((route, i) => {
                const { path, childRoutes = [] } = route;
                return <Auth
                    user={user}
                    loggedIn={loggedIn}
                    key={i} {...route}
                    appVersion={appVersion}
                    path={`${process.env.PUBLIC_PATH || ''}/${path}`} >
                    <div>
                        <Switch>
                            {childRoutes.map((child, c) => {
                                const { childRoutes: additionalRoutes = [] } = child;
                                return <Auth
                                    user={user}
                                    loggedIn={loggedIn}
                                    key={c} {...child}
                                    path={`${process.env.PUBLIC_PATH || ''}/${path}/${child.path}`}>
                                    <div>
                                        <Switch>
                                            {additionalRoutes.map((child2, d) => {
                                                const { childRoutes: additionalRoutes2 = [] } = child2;
                                                return <Auth
                                                    user={user}
                                                    loggedIn={loggedIn}
                                                    key={d} {...child2}
                                                    path={`${process.env.PUBLIC_PATH || ''}/${path}/${child.path}/${child2.path}`}>
                                                    <div>
                                                        <Switch>
                                                            {additionalRoutes2.map((child3, e) => {
                                                                return <Auth
                                                                    user={user}
                                                                    loggedIn={loggedIn}
                                                                    key={e} {...child3}
                                                                    path={`${process.env.PUBLIC_PATH || ''}/${path}/${child.path}/${child2.path}/${child3.path}`} />
                                                            })}
                                                        </Switch>
                                                    </div>
                                                </Auth>
                                            })}
                                        </Switch>
                                    </div>
                                </Auth>
                            })}
                        </Switch>
                    </div>
                </Auth>
            })}
        </Switch>
    </App>)

}


const mapStateToProps = createStructuredSelector({
    loggedIn: selectLoggedIn(),
    user: selectUser(),
    appVersion: selectAppVersion()
});


export default connect(mapStateToProps)(Routes);
