
/**
 * 
 * Administrator
 * 
 */

 import schema from './schema';
 import ResetLoginAttampets from '../components/ResetLoginAttampets';
 const treatmentsColumns = schema().treatments().columns;
 const diagnosisColumns = schema().diagnosis().columns;
 const bodySystemsColumns = schema().bodySystem().columns;
 const globalColumns = schema().globalSettings;
 const practicesEditColumns = schema().practices;
 const practicesColumns = schema().practices().columns;
 const ClaimsAdministratorEditColumns = schema().ClaimsAdministrator;
 const ClaimsAdministratorColumns = schema().ClaimsAdministrator().columns;
 const adminclaimConnectsColumns = schema().adminclaimConnectsColumns().columns;
 const adminPracticeUsersColumns = schema().adminPracticeUsers().columns;
 
 const ResourceCategories = (name, path, title, icon, users, simpleLazyLoadedRoute, childRoutes) => {
     return simpleLazyLoadedRoute({
         path,
         name,
         require: ['CategoriesPage', name],
         data: {
             path: `/${path}`,
             title,
             users,
             icon
         },
         container: function CategoriesPage() {
             return this(name, `${process.env.PUBLIC_PATH || ''}/${path}`, childRoutes)
         },
         childRoutes
     })
 }
 const devUsers = ['siva@miotiv.com'];
 
 export default function (simpleLazyLoadedRoute) {
 
     return [
         simpleLazyLoadedRoute({
             path: '',
             pageName: '',
             exact: true,
             data: {
                 title: 'Home',
                 path: '/',
                 route: true
             },
             container: 'HomePage1'
         }),
         simpleLazyLoadedRoute({
             path: 'signin',
             pageName: 'session',
             exact: true,
             data: {
                 title: 'Login',
                 path: '/signin',
                 route: true
             },
             container: 'NotFoundPage'
         }),
         simpleLazyLoadedRoute({
             path: 'signup',
             pageName: 'session',
             exact: true,
             data: {
                 title: 'Register',
                 path: '/signup',
                 route: true
             },
             container: 'NotFoundPage'
         }),
         simpleLazyLoadedRoute({
             path: 'forgot',
             pageName: 'session',
             exact: true,
             data: {
                 title: 'Forgot Password',
                 path: '/forgot',
                 route: true
             },
             container: 'NotFoundPage'
         }),
         simpleLazyLoadedRoute({
             path: `practices`,
             name: 'practices',
             pageName: 'practices',
             require: ['RecordsPage', 'practices'],
             chunk: ['practices'],
             data: {
                 path: '/practices',
                 title: 'Practices',
                 icon: 'Practice'
             },
             container: function RecordsPage(practices) {
                 return this('practice', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors, true, true)
             },
             childRoutes: [
                 simpleLazyLoadedRoute({
                     path: `create`,
                     name: 'practices.create',
                     pageName: 'practices',
                     require: ['CreateRecordPage', 'practices'],
                     container: function CreateRecordPage(practices) {
                         return this('practice', `${process.env.PUBLIC_PATH || ''}/practices`, practicesEditColumns, practices.actions, practices.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id/edit`,
                     name: 'practices.edit',
                     pageName: 'practices',
                     require: ['EditRecordPage', 'practices'],
                     container: function EditRecordPage(practices) {
                         return this('practice', `${process.env.PUBLIC_PATH || ''}/practices`, practicesEditColumns, practices.actions, practices.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id`,
                     name: 'practices.view',
                     pageName: 'practices',
                     require: ['ViewRecordPage', 'practices'],
                     container: function ViewRecordPage(practices) {
                         return this('practice', `${process.env.PUBLIC_PATH || ''}/practices`, practicesColumns, practices.actions, practices.selectors)
                     }
                 })
             ]
         }),
         ResourceCategories('userCategories', 'userCategories', 'Users', 'Users', false, simpleLazyLoadedRoute, [
             simpleLazyLoadedRoute({
                 path: `rest/vettedclaims/users`,
                 name: 'vettedclaimsUsers',
                 pageName: 'users',
                 queryParams: 'admin',
                 require: ['RecordsPage', 'vettedclaims/users'],
                 data: {
                     path: '/rest/vettedclaims/users',
                     title: 'VettedClaims Users',
                     icon: 'Users'
                 },
                 container: function RecordsPage(vettedclaimsUsers) {
                     return this('vettedclaimsUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/vettedclaims/users`, adminclaimConnectsColumns, vettedclaimsUsers.actions, vettedclaimsUsers.selectors, true, true, true)
                 },
                 childRoutes: [
                     simpleLazyLoadedRoute({
                         path: `create`,
                         name: 'vettedclaims Users',
                         pageName: 'users',
                         queryParams: 'admin',
                         require: ['CreateRecordPage', 'vettedclaims/users'],
                         container: function CreateRecordPage(vettedclaimsUsers) {
                             return this('vettedclaimsUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/vettedclaims/users`, schema().adminclaimConnectsColumns, vettedclaimsUsers.actions, vettedclaimsUsers.selectors)
                         }
                     }),
                     simpleLazyLoadedRoute({
                         path: `:id/edit`,
                         name: 'vettedclaims Users',
                         pageName: 'users',
                         queryParams: 'admin',
                         require: ['EditRecordPage', 'vettedclaims/users'],
                         container: function EditRecordPage(vettedclaimsUsers) {
                             return this('vettedclaimsUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/vettedclaims/users`, schema().adminclaimConnectsColumns, vettedclaimsUsers.actions, vettedclaimsUsers.selectors)
                         }
                     }),
                     simpleLazyLoadedRoute({
                         path: `:id`,
                         name: 'vettedclaims Users',
                         pageName: 'users',
                         queryParams: 'admin',
                         require: ['ViewRecordPage', 'vettedclaims/users'],
                         container: function ViewRecordPage(vettedclaimsUsers) {
                             return this('vettedclaimsUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/vettedclaims/users`, adminclaimConnectsColumns, vettedclaimsUsers.actions, vettedclaimsUsers.selectors, ResetLoginAttampets)
                         }
                     })
                 ]
             }),
             simpleLazyLoadedRoute({
                 path: `rest/practice/users`,
                 name: 'practiceUsers',
                 pageName: 'users',
                 queryParams: 'practice',
                 require: ['RecordsPage', 'practice/users'],
                 data: {
                     path: '/rest/practice/users',
                     title: 'Practice Users',
                     icon: 'Users'
                 },
                 container: function RecordsPage(practiceUsers) {
                     return this('practiceUsers', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, true, true, true)
                 },
                 childRoutes: [
                     simpleLazyLoadedRoute({
                         path: `create`,
                         name: 'Practice Users',
                         pageName: 'users',
                         queryParams: 'practice',
                         require: ['CreateRecordPage', 'practice/users'],
                         container: function CreateRecordPage(practiceUsers) {
                             return this('practiceUsers.create', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors)
                         }
                     }),
                     simpleLazyLoadedRoute({
                         path: `:id/edit`,
                         name: 'Practice Users',
                         pageName: 'users',
                         queryParams: 'practice',
                         require: ['EditRecordPage', 'practice/users'],
                         container: function EditRecordPage(practiceUsers) {
                             return this('practiceUsers.edit', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, schema().adminPracticeUsers, practiceUsers.actions, practiceUsers.selectors)
                         }
                     }),
                     simpleLazyLoadedRoute({
                         path: `:id`,
                         name: 'Practice Users',
                         pageName: 'users',
                         queryParams: 'practice',
                         require: ['ViewRecordPage', 'practice/users'],
                         container: function ViewRecordPage(practiceUsers) {
                             return this('practiceUsers.view', `${process.env.PUBLIC_PATH || ''}/userCategories/rest/practice/users`, adminPracticeUsersColumns, practiceUsers.actions, practiceUsers.selectors, ResetLoginAttampets)
                         }
                     })
                 ]
            })
        ]),

         simpleLazyLoadedRoute({
             path: `claims_admin`,
             name: 'ClaimsAdministrator',
             pageName: 'ClaimsAdministrator',
             require: ['RecordsPage', 'claims_admin'],
             data: {
                 path: '/claims_admin',
                 title: 'Claim Administrators',
                 icon: 'AccountCircleIcon'
             },
             container: function RecordsPage(ClaimsAdministrator) {
                 return this('Claim Administrator', `${process.env.PUBLIC_PATH || ''}/claims_admin`, ClaimsAdministratorColumns, ClaimsAdministrator.actions, ClaimsAdministrator.selectors, true, true)
             },
             childRoutes: [
                 simpleLazyLoadedRoute({
                     path: `create`,
                     name: 'ClaimsAdministrator.create',
                     pageName: 'ClaimsAdministrator',
                     require: ['CreateRecordPage', 'claims_admin'],
                     container: function CreateRecordPage(ClaimsAdministrator) {
                         return this('Claim Administrator', `${process.env.PUBLIC_PATH || ''}/claims_admin`, ClaimsAdministratorEditColumns, ClaimsAdministrator.actions, ClaimsAdministrator.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id/edit`,
                     name: 'ClaimsAdministrator.edit',
                     pageName: 'ClaimsAdministrator',
                     require: ['EditRecordPage', 'claims_admin'],
                     container: function EditRecordPage(ClaimsAdministrator) {
                         return this('Claim Administrator', `${process.env.PUBLIC_PATH || ''}/claims_admin`, ClaimsAdministratorEditColumns, ClaimsAdministrator.actions, ClaimsAdministrator.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id`,
                     name: 'ClaimsAdministrator.view',
                     pageName: 'ClaimsAdministrator',
                     require: ['ViewRecordPage', 'claims_admin'],
                     container: function ViewRecordPage(ClaimsAdministrator) {
                         return this('Claim Administrator', `${process.env.PUBLIC_PATH || ''}/claims_admin`, ClaimsAdministratorColumns, ClaimsAdministrator.actions, ClaimsAdministrator.selectors)
                     }
                 })
             ]
         }),
         simpleLazyLoadedRoute({
             path: `treatments`,
             name: 'treatments',
             pageName: 'treatments',
             require: ['RecordsPage', 'treatments'],
             data: {
                 path: '/treatments',
                 title: 'Policy Docs',
                 icon: 'Treatment'
             },
             container: function RecordsPage(practices) {
                 return this('treatment', `${process.env.PUBLIC_PATH || ''}/treatments`, treatmentsColumns, practices.actions, practices.selectors, true, true)
             },
             childRoutes: [
                 simpleLazyLoadedRoute({
                     path: `create`,
                     name: 'treatments.create',
                     pageName: 'treatments',
                     require: ['CreateTreatmentPage', 'treatments'],
                     container: function CreateRecordPage(practices) {
                         return this('treatment', `${process.env.PUBLIC_PATH || ''}/treatments`, schema().treatmentsCreate, practices.actions, practices.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id/edit`,
                     name: 'treatments.edit',
                     pageName: 'treatments',
                     require: ['EditTreatmentPage', 'treatments'],
                     container: function EditRecordPage(practices) {
                         return this('treatment', `${process.env.PUBLIC_PATH || ''}/treatments`, schema().treatmentsCreate, practices.actions, practices.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id`,
                     name: 'treatments.view',
                     pageName: 'treatments',
                     require: ['ViewRecordPage', 'treatments'],
                     container: function ViewRecordPage(practices) {
                         return this('treatment', `${process.env.PUBLIC_PATH || ''}/treatments`, treatmentsColumns, practices.actions, practices.selectors)
                     }
                 })
             ]
         }),
         simpleLazyLoadedRoute({
             path: `diagnosis`,
             name: 'diagnosis',
             pageName: 'diagnosis',
             require: ['RecordsPage', 'diagnosis'],
             data: {
                 path: '/diagnosis',
                 title: 'Policies',
                 icon: 'Diagnosis'
             },
             container: function RecordsPage(practices) {
                 return this('diagnosis', `${process.env.PUBLIC_PATH || ''}/diagnosis`, diagnosisColumns, practices.actions, practices.selectors, false, true)
             },
 
         }),
         simpleLazyLoadedRoute({
             path: `bodySystem`,
             name: 'bodySystem',
             pageName: 'bodySystem',
             require: ['RecordsPage', 'bodySystem'],
             data: {
                 path: '/bodySystem',
                 title: 'Body Systems',
                 users: devUsers,
                 icon: 'Practice'
             },
             container: function RecordsPage(bodySystem) {
                 return this('Body System', `${process.env.PUBLIC_PATH || ''}/bodySystem`, bodySystemsColumns, bodySystem.actions, bodySystem.selectors, true, true)
             },
             childRoutes: [
                 simpleLazyLoadedRoute({
                     path: `create`,
                     name: 'bodySystem.create',
                     pageName: 'bodySystem',
                     require: ['CreateRecordPage', 'bodySystem'],
                     container: function CreateRecordPage(bodySystem) {
                         return this('Body System', `${process.env.PUBLIC_PATH || ''}/bodySystem`, bodySystemsColumns, bodySystem.actions, bodySystem.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id/edit`,
                     name: 'bodySystem.edit',
                     pageName: 'bodySystem',
                     require: ['EditRecordPage', 'bodySystem'],
                     container: function EditRecordPage(bodySystem) {
                         return this('Body System', `${process.env.PUBLIC_PATH || ''}/bodySystem`, bodySystemsColumns, bodySystem.actions, bodySystem.selectors)
                     }
                 }),
                 simpleLazyLoadedRoute({
                     path: `:id`,
                     name: 'bodySystem.view',
                     pageName: 'bodySystem',
                     require: ['ViewRecordPage', 'bodySystem'],
                     container: function ViewRecordPage(bodySystem) {
                         return this('Body System', `${process.env.PUBLIC_PATH || ''}/bodySystem`, bodySystemsColumns, bodySystem.actions, bodySystem.selectors)
                     }
                 })
             ]
         }),
         simpleLazyLoadedRoute({
             path: `globalSettings`,
             name: 'globalSettings',
             pageName: 'globalSettings',
             require: ['ViewSettingsPage', 'globalSettings'],
             chunk: ['globalSettings'],
             data: {
                 path: '/globalSettings',
                 title: 'Settings',
                 icon: 'SettingsIcon'
             },
             container: function GlobalSettingsPage(settings) {
                 return this('globalSettings', `${process.env.PUBLIC_PATH || ''}/globalSettings`, globalColumns, settings.actions, settings.selectors, true)
             },
         }),
 
         simpleLazyLoadedRoute({
             path: '*',
             exact: true,
             container: 'NotFoundPage'
         })
     ]
 }
 