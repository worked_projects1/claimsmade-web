
/**
 * 
 * Physician
 * 
 */

import schema from './schema';
const userEditColumns = schema().users;
const usersColumns = schema().users().columns;
const settingsColumns = schema().settings;
const poc3Columns = schema().poc3().columns;
const casesColumns = schema().cases;

// const devUsers = ['siva@ospitek.com', 'senthilei27@gmail.com'];
export default function (simpleLazyLoadedRoute) {
    
    return [
        simpleLazyLoadedRoute({
            path: '',
            pageName: '',
            exact: true,
            data: {
                title: '',
                path: '/',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signin',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Login',
                path: '/signin',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'signup',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Register',
                path: '/signup',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: 'forgot',
            pageName: 'session',
            exact: true,
            data: {
                title: 'Forgot Password',
                path: '/forgot',
                route: true
            },
            container: 'NotFoundPage'
        }),
        simpleLazyLoadedRoute({
            path: `claims`,
            name: 'rfaForms',
            pageName: 'rfaForms',
            require: ['Poc3Page', 'rfaForms'],
            chunk: ['poc2'],
            data: {
                path: '/claims',
                title: 'Claim Checker',
                // users: devUsers,
                icon: 'claims'
            },
            container: function Poc3Page(rfaForms) {
                return this('rfaForm', `${process.env.PUBLIC_PATH || ''}/claims`, poc3Columns, rfaForms.actions, rfaForms.selectors, true, true)
            }
        }),
        simpleLazyLoadedRoute({
            path: `rfaForms`,
            name: 'rfaForms',
            pageName: 'rfaForms',
            require: ['RecordsPage', 'rfaForms'],
            chunk: ['poc2'],
            data: {
                path: '/rfaForms',
                title: 'Forms',
                icon: 'rfaForms'
            },
            container: function RecordsPage(rfaForms) {
                return this('rfaForm', `${process.env.PUBLIC_PATH || ''}/rfaForms`, casesColumns, rfaForms.actions, rfaForms.selectors, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'rfaForms.create',
                    pageName: 'rfaForms',
                    require: ['CreateRecordPage', 'rfaForms'],
                    container: function CreateRecordPage(rfaForms) {
                        return this('rfaForm', `${process.env.PUBLIC_PATH || ''}/rfaForms`, casesColumns, rfaForms.actions, rfaForms.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/poc3`,
                    name: 'rfaForms.view',
                    pageName: 'rfaForms',
                    require: ['Poc3Page', 'rfaForms'],
                    chunk: ['poc2'],
                    container: function ViewRecordPage(rfaForms) {
                        return this('rfaForm', `${process.env.PUBLIC_PATH || ''}/rfaForms`, poc3Columns, rfaForms.actions, rfaForms.selectors, true, true)
                    }
                })
            ]
        }),
        simpleLazyLoadedRoute({
            path: `users`,
            name: 'users',
            pageName: 'users',
            require: ['RecordsPage', 'users'],
            data: {
                path: '/users',
                title: 'Users',
                icon: 'PeopleAlt',

            },
            container: function RecordsPage(users) {
                return this('users', `${process.env.PUBLIC_PATH || ''}/users`, userEditColumns, users.actions, users.selectors, true, true)
            },
            childRoutes: [
                simpleLazyLoadedRoute({
                    path: `create`,
                    name: 'users.create',
                    pageName: 'users',
                    require: ['CreateRecordPage', 'users'],
                    container: function CreateRecordPage(users) {
                        return this('users.create', `${process.env.PUBLIC_PATH || ''}/users`, userEditColumns, users.actions, users.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id/edit`,
                    name: 'users.edit',
                    pageName: 'users',
                    require: ['EditRecordPage', 'users'],
                    container: function EditRecordPage(users) {
                        return this('users.edit', `${process.env.PUBLIC_PATH || ''}/users`, userEditColumns, users.actions, users.selectors)
                    }
                }),
                simpleLazyLoadedRoute({
                    path: `:id`,
                    name: 'users.view',
                    pageName: 'users',
                    require: ['ViewRecordPage', 'users'],
                    container: function ViewRecordPage(users) {
                        return this('users.view', `${process.env.PUBLIC_PATH || ''}/users`, usersColumns, users.actions, users.selectors)
                    }
                })
            ]
        }),
        simpleLazyLoadedRoute({

            path: `settings`,
            name: 'settings',
            pageName: 'settings',
            require: ['ViewSettingsPage', 'settings'],
            chunk: ['practices'],
            data: {
                path: '/settings',
                title: 'Practice Profile',
                icon: 'SettingsIcon'
            },
            container: function ViewSettingsPage(practices) {
                return this('settings', `${process.env.PUBLIC_PATH || ''}/settings`, settingsColumns, practices.actions, practices.selectors, true)
            },
        }),

        simpleLazyLoadedRoute({
            path: '*',
            exact: true,
            container: 'NotFoundPage'
        })
    ]
}