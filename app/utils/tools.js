import store2 from 'store2';
import moment from 'moment';
import { useEffect, useRef } from 'react';
export function getUTCTimestamp(date) {
    if (!date) {
        return false;
    }
    const utcTimestamp = moment.utc(date).format('YYYY-MM-DD HH:mm:ss');
    return moment(utcTimestamp).format("X");
}

/**
 * 
 * @param {function} fn 
 * @param {string} name 
 * @param {boolean} val 
 */
 export function Localize(callback, name, val) {
    store2.set(name, val);
    callback();
}


/**
 * callback Set Timeout Function
 * @param {function} fn 
 * @param {number} duration 
 */
 export function Timer(fn, duration) {
    return setTimeout(fn, duration);
}

/**
 * 
 * @param {object} field 
 */

 export function normalize(field) {
    return (val) => {
        
      if(field.type === 'phone' && field.value != 'phone_number' && field.value != 'fax_number') {
        let value = val.replace(/[^\d\(\)\-]/g,'');// eslint-disable-line
        return field.max && val.length > field.max ? val.substring(0, field.max) : value;
      } else if(field.value == 'phone_number' || field.value == 'fax_number'){
        let value = val.replace(/[^\d\(\)\-\ ]/g,'');// eslint-disable-line 
        return field.max && val.length > field.max ? val.substring(0, field.max) : value;
    } else {
        return field.dollar && val.match(/^[-+]?[0-9]+\.[0-9]+[0-9]$/) ? parseFloat(val).toFixed(2) : field.max && val.length > field.max ? val.substring(0, field.max): val;// eslint-disable-line
      } 
    }  
  }

  export function getDefaultHeaders() {
    return {
        offset: 0,
        limit: 25,
        search: false,
        filter: false,
        sort: false,
        page: 1
    }
}

export const RecordsData = Array.from(Array(10), (x, index) => Object.assign({}, { id: index + 1 }));

/**
 * capitalize only the first letter of the string. 
 * @param {string} string 
 */
 export function capitalizeFirstLetter(string) {
  return string && string.charAt(0).toUpperCase() + string.slice(1) || string;
}

/**
 * Visibility Listener Function
 * @param {object} param0 
 */
 export function Visibility({ callback, clearSessionTimeout, ...props }) {// eslint-disable-line

  // Set the name of the hidden property and the change event for visibility
  var hidden, visibilityChange;
  if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
      hidden = "hidden";
      visibilityChange = "visibilitychange";
  } else if (typeof document.msHidden !== "undefined") {
      hidden = "msHidden";
      visibilityChange = "msvisibilitychange";
  } else if (typeof document.webkitHidden !== "undefined") {
      hidden = "webkitHidden";
      visibilityChange = "webkitvisibilitychange";
  }

  document.addEventListener(visibilityChange, () => {
      if (document[hidden]) {
          callback();
      } else {
          clearSessionTimeout();
      }
  }, false);

}

/**
 * capitalize all words of a string. 
 * @param {string} string 
 */
 export function capitalizeWords(string) {
    return string && string.replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); }) || string
}

/**
 *capitalize only the first letter and others are lower case of the string. 
 * @param {string} string 
 */
export function capitalizeFirstLetterOnly(string) {
    return string && string.charAt(0).toUpperCase() + string.slice(1).toLowerCase() || string;
}

/**
 * 
 * @param {string} string 
 */
export function capitalizeFirstWord(string) {  
    return string.toLowerCase().split(' ').map(s => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');  
}

/**
 * 
 * @param {string} id 
 */
 export function getOffset(id) {
    if (!id) {
        return false;
    }
    const main = document.getElementById(id);
    return main && main.offsetHeight || 0;
}
/**
 * 
 * @param {function} callback 
 * @param {array} deps 
 */
 export function useDidUpdate(callback, deps) {
    const hasMount = useRef(false)

    useEffect(() => {
        if (hasMount.current) {
            callback()
        } else {
            hasMount.current = true
        }
    }, deps)
}
