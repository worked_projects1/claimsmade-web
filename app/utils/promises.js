/**
 * 
 * Inject Promise
 * 
 */


import records from 'blocks/records';

export default function useApplyPromise({ injectReducer, injectSaga, container, require, synthesizeData }) {
  
    const containerProvider = container;
    const [containerName, ...blockNames] = require || {};
    const blocksPromise = blockNames.map((blockName) => Promise.resolve(records(blockName, synthesizeData || {})));
    const containerPromise = import(`containers/${containerName}/index`);
    const promises = [containerPromise, ...blocksPromise];

    return Promise.all(promises).then(([container, ...blocks]) => { // eslint-disable-line no-shadow
      blocks.forEach((block) => {
        const { name, reducer, saga } = block.default || block;

        if (reducer) {
          /**
           * Injecting Reducer as Async Reducers
           */
          injectReducer(name, reducer);
        }

        if (saga) {
          /**
           * Injecting Saga
           */
          injectSaga(name, { saga });
        }
      });


      try {
        const finalContainer = containerProvider.apply(container.default, blocks.map((block) => block.default || block));
        return finalContainer
      } catch (error) {
        return error;
      }


    }).catch(error => {
      return error;
    });
}