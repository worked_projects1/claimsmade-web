export const DEFAULT_UPDATE_ERROR = 'There was an error saving your changes. Please try again.';
export const DEFAULT_CREATE_ERROR = 'There was an error creating this record. Have you supplied all the required fields? Please try again.';
export const DEFAULT_DELETE_ERROR = 'There was an error deleting this record. Please try again.';
export const CATEGORY_DELETE_ERROR = 'There was an error deleting this record. Maybe there are still products in this category.';

export const DEFAULT_LOGIN_ERROR = 'There was an error logging in. Check your credentials and try again. Contact the administrator if the error persists.';
export const DEFAULT_SESSION_TOKEN_ERROR = 'Your session has expired. Please log in again.';
export const DEFAULT_UNEXPECTED_SESSION_ERROR = 'An unexpected error has occured. Please log in again.';
export const WRONG_CREDENTIALS = 'Your credentials were not accepted. Check your email and password and try again.';
export const NOT_AUTHORIZED = 'You are not authorized to access this platform. Please contact our support for help.';
export const ACCOUNT_EXPIRED = 'Your account expired, please contact VettedClaims team to extend your licence.';
export const EXPIRED_TOKEN = 'Your reset token is not valid.';
export const NO_SUCH_USER = 'No user with this email exists.';

export const DEFAULT_NETWORK_ERROR = 'There was a network error. Try again in a few moments. Contact the administrator if the error persists.';
export const DEFAULT_ANALYTICS_ERROR = 'There was an error loading the Dashboard. Contact the administrator if the error persists.';


export const DEFAULT_DOCUMENT_ERROR = 'We were unable to extract individual questions from this document. Please make sure you have uploaded the highest quality PDF possible. If you are still facing this error, please use the button below to get support from VettedClaims. Our support team will extract questions from this document and update the system promptly.';
export const DEFAULT_UPLOAD_FORM_ERROR = 'Document Upload Failed';
export const DEFAULT_GENERATE_FORM_ERROR = 'Document Generation Failed';
export const DEFAULT_CANCEL_FORM_ERROR = 'Updating Document Progress Status Failed';
export const DEFAULT_SAVE_FORM_ERROR = 'Unable to Save Document';
export const DEFAULT_LOAD_FORM_ERROR = 'Failed to Load Document';
export const DEFAULT_UPDATE_FORM_ERROR = 'Failed to Update Document';
export const DEFAULT_FETCH_FORM_ERROR = 'Failed to Fetch Document';
export const DEFAULT_GENERATE_DOCUMENT_ERROR = 'Document Generation Failed';
export const DEFAULT_SAVE_DOCUMENT_ERROR = 'Failed to Save Template';
export const DEFAULT_SEND_QUESTIONS_ERROR = 'Failed to Send Questions';

export const DEFAULT_LOAD_PRACTICE_ERROR = 'Failed to Read Practice';
export const DEFAULT_UPDATE_PRACTICE_ERROR = 'Failed to Update Practice';


export const DEFAULT_LOAD_GLOBAL_SETTINGS_ERROR = 'Failed to Load Global Settings';
export const DEFAULT_UPDATE_GLOBAL_SETTINGS_ERROR = 'Failed to Update Global Settings';


export const DEFAULT_LOAD_BILLING_DETAILS_ERROR = 'Failed to Load Billing Details';
export const DEFAULT_UPDATE_BILLING_DETAILS_ERROR = 'Failed to Update Billing Details';

export const DEFAULT_RESET_PASSWORD_ERROR = 'Failed to Reset Password';
export const DEFAULT_FORGOT_PASSWORD_ERROR = 'Failed to Send Password Reset Email';

export const DEFAULT_CREATE_RECORD_ERROR = 'Failed to Create Record';
export const DEFAULT_UPDATE_RECORD_ERROR = 'Failed to Update Record';
export const DEFAULT_DELETE_RECORD_ERROR = 'Failed to Delete Record';

export const DEFAULT_DELETE_FORM_ERROR = 'Failed to Delete Form';
export const DEFAULT_SAVE_STANDARD_FORM_ERROR = 'Failed to Save Form';
export const DEFAULT_SAVE_QUESTIONS_FORM_ERROR = 'Failed to Save Questions';
export const DEFAULT_SAVE_QUESTIONS_FORM_VALIDATE_ERROR = 'Please provide Question Number or Question Text';

export const DEFAULT_CHANGE_PASSWORD_ERROR = 'Failed to Change Password';

export const DEFAULT_CREATE_MEDICAL_HISTORY_ERROR = 'Failed to Create Medical Document';
export const DEFAULT_UPDATE_MEDICAL_HISTORY_ERROR = 'Failed to Update Medical Document';
export const DEFAULT_DELETE_MEDICAL_HISTORY_ERROR = 'Failed to Delete Medical Document';
export const DEFAULT_VIEW_MEDICAL_HISTORY_ERROR = 'Failed to View Medical Document';
export const DEFAULT_GENERATE_MEDICAL_SUMMARY_ERROR = 'Failed to Generate Medical Summary';

export const DEFAULT_REQUEST_SUPPORT_ERROR = 'Failed to Send Message';
export const DEFAULT_REQUEST_DEMO_ERROR = 'Failed to Send Message';

export const DEFAULT_REQUEST_HELP_ERROR = 'Failed sent Request';

export const DEFAULT_REQUEST_TRANSLATION_ERROR = 'Failed to Translate text';

export const DEFAULT_MERGE_TRANSLATION_ERROR = 'Failed to Copy Translated Text';

export const DEFAULT_BULK_TRANSLATION_ERROR = 'Failed to Upload Bulk Questions Translations JSON Data';

export const DEFAULT_UPDATE_VERSION_ERROR = 'Failed to Update Version';

export const DEFAULT_SESSION_TIMEOUT_ERROR = 'Failed to update Session Timeout';

export const DEFAULT_LOAD_BUSINESS_METRICS_ERROR = 'Failed to load Business Metrics data.';

export const DEFAULT_LOAD_HISTOGRAM_DATA_ERROR = 'Failed to load Histogram data.';

export const DEFAULT_SEND_VERIFICATION_ERROR = 'Failed to Send Verification.';

export const DEFAULT_LOAD_PRACTICE_SETTINGS_ERROR = 'Failed to Load Practice Settings.';
export const DEFAULT_UPDATE_PRACTICE_SETTINGS_ERROR = 'Failed to Update Practice Settings.';

export const DEFAULT_RESET_LOGIN_ATTEMPT_ERROR = 'Failed to reset login attempts.';

export const DEFAULT_CUSTOMER_OBJECTIONS_ERROR = 'Failed to update customer objections.';

export const DEFAULT_AUTO_SAVE_FORM_ERROR = 'Failed to update draft.';

export const DEFAULT_COPY_TO_LAWYER_RESPONSE_ERROR = 'Failed to copy client response.';

export const DEFAULT_LOAD_SUBSCRIPTIONS_ERROR = 'Failed to load subscriptions.';
export const DEFAULT_CREATE_SUBSCRIPTIONS_ERROR = 'Failed to create subscriptions.';
export const DEFAULT_UPDATE_SUBSCRIPTIONS_ERROR = 'Failed to update subscriptions.';
export const DEFAULT_CANCEL_SUBSCRIPTIONS_ERROR = 'Failed to cancel subscriptions.';

export const DEFAULT_GET_STRIPE_INDENT_ERROR = 'Failed to get stripe indent.';

export const DEFAULT_SETUP_STRIPE_PAYMENT_ERROR = 'Failed to setup stripe payment.';

export const DEFAULT_CREATE_SUBSCRIPTION_PLAN_ERROR = 'Failed to create subscription plan.';

export const DEFAULT_LOAD_SETTINGS_PLAN_ERROR = 'Failed to load settings plan.';

export const DEFAULT_UPDATE_PRACTICE_OBJECTIONS_ERROR = 'Failed to update practice objections.';

export const DEFAULT_VERIFY_OTP_ERROR = 'Invalid OTP';

export const DEFAULT_RESEND_OTP_ERROR = 'Failed to resend OTP';

export const DEFAULT_TWO_FACTOR_AUTHENTICATION_ERROR = 'Failed to Enable Two Factor Authentication';

export const DEFAULT_SHRED_ERROR = 'Failed to shred document';

export const DEFAULT_EDIT_FORM_ERROR = 'Failed to change file name';

export const DEFAULT_SESSION_CLEAR_TIMEOUT_ERROR = 'Failed to clear Session Timeout';

export const DEFAULT_ATTACH_CASE_ERROR = 'Failed to Clone Record';

export const DEFAULT_CREATE_CLIENT_RECORD_ERROR = 'Failed to Clone Record';

export const DEFAULT_SET_RESPONSE_DATE_ERROR = 'Failed to set response date.';

export const DEFAULT_RESEND_QUESTIONS_ERROR = 'Failed to Resend Questions';

export const DEFAULT_SAVE_ALL_FORM_ERROR = 'Failed to save all as final form';

export const DEFAULT_UPDATE_PRACTICE_CUSTOM_TEMPLATE_ERROR = 'Failed to Update Practice Custom Document Template';

export const DEFAULT_UPDATE_PRACTICE_MODIFIED_TEMPLATE_ERROR = 'Failed to Update Modified Template';

export const DEFAULT_SAVE_SUB_GROUP_FORM_ERROR = 'Failed to save sub group form';

export const DEFAULT_GENERATE_POS_DOCUMENT_ERROR = 'Failed to generate proof of service document';

export const DEFAULT_COPY_ALL_TO_LAWYER_RESPONSE_ERROR = 'Failed to copy all client response.';

export const DEFAULT_MEDICAL_TEXT_EXTRACT_ERROR = 'Failed to Extract Text';

export const DEFAULT_MEDICAL_HISTORY_KEY_VALUES_ERROR = 'Failed to Save Medical History Key Values';

export const DEFAULT_TEXTRACT_CANCEL_FORM_ERROR = 'Updating Document Progress Status Failed';

export const DEFAULT_MEDICAL_LOAD_FORM_ERROR = 'Failed to Load Document';

export const DEFAULT_STATUS_CHANGE = 'Failed to Change Status';
