
import React, { useState, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import * as Loads from 'react-loads';
import Spinner from 'components/Spinner';
import store2 from 'store2';
import { useClearCacheCtx } from 'react-clear-cache';

export default function ({ Component, path, exact = false, children, data, loggedIn, location, user = {}, queryParams, appVersion }) {// eslint-disable-line

    const [ticking, setTicking] = useState(true);
    const [count, setCount] = useState(0);

    const { role, version, email } = user;// eslint-disable-line
    // const devUsers = ['siva@ospitek.com', 'senthilei27@gmail.com'];
    const {
        response: Page,
        error,
        isPending,
        isResolved,
        isRejected
    } = Loads.useLoads(path, Component);

    const { emptyCacheStorage } = useClearCacheCtx();
    const storeVersion = store2.get('version');

    /* Reset Cache */
    if ((!storeVersion && version) || (version && storeVersion && version !== storeVersion)) {
        store2.set('version', version);
        emptyCacheStorage();
    }

    if ((!version && appVersion && storeVersion && appVersion !== storeVersion) || (appVersion && !version && !storeVersion)) {
        store2.set('version', appVersion);
        emptyCacheStorage();
    }

    useEffect(() => {
        const timer = setTimeout(() => ticking && isPending && setCount(count + 1), 3000);
        if (count == 1 && isPending) {
            emptyCacheStorage();
            setTicking(false);
        }
        return () => clearTimeout(timer)
    }, [count, ticking, isPending])

    return (
        <Route
            exact={exact}
            path={path}
            render={props => loggedIn && data && data.route ?
                <Redirect
                    to={{
                        pathname: `/${role && ['manager', 'superAdmin', 'operator'].includes(role) ? 'practices' : 'claims' }`,
                        state: { title: role && ['manager', 'superAdmin', 'operator'].includes(role) ? 'Practices' : 'Claims' }
                    }} /> :
                <div>
                    {isPending && !isResolved && !loggedIn && <Spinner loading={true} showHeight /> || isPending && !isResolved && 'Loading...'}
                    {(isRejected || error) && 'Something Went Wrong. Please try again.'}
                    {Page && <Page {...props} pathData={data} queryParams={queryParams || false}>{children}</Page>}
                </div>
            }
        />
    )

}
