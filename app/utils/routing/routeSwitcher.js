/**
 * 
 * Route Switcher
 * 
 */

import anonymousRoutesProvider from 'routes/anonymous';
import physicianRoutesProvider from 'routes/physician';
import adminRoutesProvider from 'routes/admin';

const routeSwitcher = (user, loggedIn, callback) => {
  if (!loggedIn) {
    return anonymousRoutesProvider(callback);
  }
  switch (user.routes) {
    case 'physician':
    case 'staff':
      return physicianRoutesProvider(callback);
    case 'superAdmin':
    case 'manager':
    case 'operator':
      return adminRoutesProvider(callback);
    default:
      return anonymousRoutesProvider(callback);
  }
};

export default routeSwitcher;