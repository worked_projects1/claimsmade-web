import moment from 'moment';

const validate = (values, props) => {
    const requiredFields = props.fields.reduce((a, el) => {
      if ((el.mandatory && typeof el.mandatory === 'boolean') || (el.mandatory && typeof el.mandatory === 'object' && ((el.mandatory.create && props.form.indexOf('create') > -1) || (el.mandatory.edit && props.form.indexOf('edit') > -1)))) {
        a.push(el.value)
      }
      if(el.columns && el.columns.length != 0){
        el.columns.map((e) => {
          if(e.mandatory && typeof e.mandatory === 'boolean' && e.value != "two_factor_status") {a.push(e.value)}
        })
      }
      return a;
    }, []);
    
    const errors = {}
  if (values.request_type && Object.keys(values.request_type).length <= 0) {
      errors.request_type = 'Required'
    }
  
    const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig
  
    const identical = /^(?!.*(.)\1\1.*).*$/igm
  
    const commonNames = ["123456", "password", "123456789", "12345678", "12345",
        "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
        "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
        "aa123456", "donald", "password1", "qwerty123"
    ]
  
  
    requiredFields.forEach(field => {
      if ((!values[field] && field != 'dob' && typeof values[field] === 'boolean') || (!values[field] && field != 'dob' && values[field] != '0')) {
        errors[field] = 'Required'
      } else if (field === 'date_of_loss') {
        errors[field] = dateValidation(values[field]);
      }
    })
    
  if (values.name && values.name.length < 4) {
      errors.name = 'Name must be at least 4 characters.'
    }
  
  if (values.case_title && values.case_title.length < 4) {
      errors.case_title = 'Case title must be at least 4 characters.'
    }
    
    if (values.objection_title && values.objection_title.trim().length < 2) {
      errors.objection_title = 'Objection title must be at least 2 characters.'
    }
  
    if (
      values.email &&
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(values.email)
    ) {
      errors.email = 'Invalid Email'
    }

  if (values.date_of_injury && !moment(values.date_of_injury, 'MM/DD/YYYY', true).isValid()) {
      errors.date_of_injury = 'Invalid date.';
    }
  if (values.date_of_birth && (!moment(values.date_of_birth, 'MM/DD/YYYY', true).isValid())) {
      errors.date_of_birth = 'Invalid date.';
    }

    if (values.password && values.password.length < 8) {
      errors.password = 'Password must be at least 8 characters'
    }
  
    if (values.password && values.password.length >= 8 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(values.password)) {
      errors.password = 'Must contain at least one numeric or special character '
    }
  
    if (values.password && sequence.test(values.password) || !identical.test(values.password)) {
      errors.password = 'Avoid consecutive sequential and identical characters'
    }
  
  if (values.state_bar_number && values.state_bar_number.length < 5) {
      errors.state_bar_number = 'State bar number should be at least 5 characters.' 
    }
  
    commonNames.forEach(field => {
      if (values.password == field) {
        errors.password = "Password is easily guessable"
      }
    })
  
    if (values.password && values.new_password && values.new_password != values.password) {
      errors.new_password = 'Password Mismatch'
    }
  
  if (values.address && values.address.length < 4) {
      errors.address = 'Address must be at least 4 characters.';
    }
  
    if (values.dob) {
      errors.dob = dateValidation(values.dob);
    }
  
  if (values.matter_id && values.matter_id.length > 65) {
      errors.matter_id = 'Only 64 characters allowed.';
    }
    
  if (values.claim_number && values.claim_number.length > 65) {
      errors.claim_number = 'Only 64 characters allowed.';
    }
  
  if (values.numberOfDays && values.numberOfDays < 0) {
      errors.numberOfDays = 'Negative values are not allowed.';
    }
  
  if (values.default_objections) {
      errors.default_objections = jsonValidation(values.default_objections)
    }
  
    if(values.no_of_free_tier && (values.no_of_free_tier).includes('.')){
      errors.no_of_free_tier = "Invalid number"
        }
    
    if(values.rfa_form_row_limit && (values.rfa_form_row_limit).includes('.')){
      errors.rfa_form_row_limit = "Invalid number"
    }

  const claimsAdminErrors = [];
  if (values.claims_admin_id == "others" && values.claim_admin_details) {
    values.claim_admin_details.map((value, i) => {
      const adminErrors = {}
      if (!value || !value.company_name) {
        adminErrors.company_name = 'Required'
        claimsAdminErrors[i] = adminErrors
      }
      if (!value || !value.email) {
        adminErrors.email = 'Required'
        claimsAdminErrors[i] = adminErrors
      }
      if (value && value.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(value.email)) {
        adminErrors.email = 'Invalid Email'
        claimsAdminErrors[i] = adminErrors
      }
      if (!value || !value.address) {
        adminErrors.address = 'Required'
        claimsAdminErrors[i] = adminErrors
      }
      if(value.address && value.address.length < 4){
        adminErrors.address = 'Address must be at least 4 characters.'
        claimsAdminErrors[i] = adminErrors
      }
    })
  }
  if (claimsAdminErrors.length) {
    errors.claim_admin_details = claimsAdminErrors
  }

  const patientInfoErrors = [];
  if (values.patientInfo) {
    values.patientInfo.map((value, i) => {
      const patientErrors = {}
      if(value.name && value.name.length < 4){
        patientErrors.name = 'Name must be at least 4 characters.'
        patientInfoErrors[i] = patientErrors
      }
      if (value.date_of_injury && !moment(value.date_of_injury, 'MM/DD/YYYY', true).isValid()) {
        patientErrors.date_of_injury = 'Invalid date.';
        patientInfoErrors[i] = patientErrors;
      }
    if (value.date_of_birth && (!moment(value.date_of_birth, 'MM/DD/YYYY', true).isValid())) {
      patientErrors.date_of_birth = 'Invalid date.';
      patientInfoErrors[i] = patientErrors;
      }
      if (!value || !value.name) {
        patientErrors.name = 'Required'
        patientInfoErrors[i] = patientErrors
      }
      if (!value || !value.date_of_injury) {
        patientErrors.date_of_injury = 'Required'
        patientInfoErrors[i] = patientErrors
      }
      if (!value || !value.date_of_birth) {
        patientErrors.date_of_birth = 'Required'
        patientInfoErrors[i] = patientErrors
      }
      if (!value || !value.claim_number) {
        patientErrors.claim_number = 'Required'
        patientInfoErrors[i] = patientErrors
      }
    })
  }
  if (patientInfoErrors.length) {
    errors.patientInfo = patientInfoErrors
  }

    return errors
  }
  
  const dateValidation = (val) => {
    var date = val;
    var result = '';
    if (!isNaN(Date.parse(date))) {
      result = ''
    } else {
      result = 'Invalid date'
    }
    return result
  }
  
  const jsonValidation = (val) => {
    var result = '';
    try {
      JSON.parse(val);
    } catch (error) {
      result = 'Invalid json format';
    }
    return result;
  }
  
  export default validate;
  