import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
    color: #1d1e1c;
  }

  body {
    font-family: MyriadPro-Regular;
  }

  body.fontLoaded {
    font-family:  MyriadPro-Regular;
  }

  #app {
    background-color: #fff !important;
    min-height: 100%;
    min-width: 100%;
    color: #1d1e1c;
  }

  p,
  label,
  span {
    font-family: MyriadPro-Regular;
    line-height: 1.5em;
    color: #1d1e1c;
  }

  .MuiButton-containedPrimary{
    // background-image:linear-gradient(to top right,#6d4aea,#09def2);
    background-image: linear-gradient(90deg, #009cd2, #00ca1f);
    box-shadow: none;
  }

a {
  text-decoration: none;
}  

.MuiFilledInput-multiline {
  padding: 12px 12px 10px;
}

.searchBtn {
  margin: 10px;
  width: 75px;
  padding: 7px;
  // padding-left: 10px;
  // padding-right: 10px;
  border-radius: 3px;
  cursor: pointer;
  margin-left: 0px;
  border: none;
  background: #e3ebf1;
  @media (max-width: 480px) {
    width: 55px;
    padding: 5px;
    font-size: 14px;
    margin-right: 7px;
    }
}

.clearBtn {
  margin: 10px 5px;
  width: 75px;
  padding: 7px;
  padding-left: 10px;
  padding-right: 10px;
  border-radius: 3px;
  cursor: pointer;
  margin-left: 0px;
  border: none;
  background: #cccdd3;
}

.MuiSelect-root:after{
  border-color: '#9dbcdc',
}

.my-grommet-icon path {
  stroke: #3c89c9,
}
`;

export default GlobalStyle;
