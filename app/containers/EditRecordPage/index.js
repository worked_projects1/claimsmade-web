
/**
 * 
 * Edit Record Page
 * 
 */


import React, { useState, useEffect, memo, useRef } from 'react';
import { connect } from 'react-redux';
import EditRecordForm from 'components/EditRecordForm';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { getFormValues } from 'redux-form';
import { Grid } from '@mui/material';
import {selectUser} from 'blocks/session/selectors';
const userEditName = ['users.edit', 'vettedclaimsUsers.edit', 'practiceUsers.edit'];

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView) {// eslint-disable-line

    const { selectRecord, selectRecordsMetaData } = selectors;

    /**
     * 
     * @param {object} props 
     */
    function EditRecordPage(props) {
        const { location = {}, record, metaData, dispatch, formRecord = {}, match, queryParams, user} = props;// eslint-disable-line
        const [loading, setLoading] = useState(true);
        const fields = (typeof columns === 'function' ? columns(formRecord).columns : columns).filter(_ => _.editRecord && !_.edit);
        const componentMounted = useRef(true);
        const [formLoader, setFormLoader] = useState(false);
        useEffect(() => {
            // let mounted = true;
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [match.params.id]);

        function handleEdit(record, dispatch, { form }) {
            setFormLoader(true);
         var submitRecord ="";
            if (userEditName.includes(name)) {
                if (record.role == "staff") {
                    record.specialty = ""
                }
            }
            if (match.path === '/bodySystem/:id/edit') {
                submitRecord = Object.assign(
                    {},
                    {
                        "created_at": record.created_at,
                        "id": record.id,
                        "index": record.index,
                        "is_deleted": record.is_deleted,
                        "name": record.name,
                        "updated_at": record.updated_at
                    }
                )
            }
            else {
               submitRecord = Object.assign({}, { ...record });
            }
            dispatch(actions.updateRecord(submitRecord, form))
            setTimeout(() => setFormLoader(false), 200);
        }
        return (<div>
            {name == "treatment" ? <Grid style={{ fontWeight: 'bold' }}>{record.name}</Grid> : null}
            {decoratorView && React.createElement(decoratorView, props)}
            {!loading && record && Object.keys(record).length > 0 ?
                <EditRecordForm
                    initialValues={userEditName.includes(name) ? Object.assign({}, record, { password: '' }) : name == "treatment" ? Object.assign({}, { id: record.id }) : record || {}}
                    form={`editRecord_${record.id}`}
                    name={name}
                    path={path}
                    metaData={metaData}
                    fields={fields}
                    onSubmit={handleEdit}
                    locationState={location.state}
                    spinner={formLoader}
                /> : 'Loading...'}
        </div>)

    }

    EditRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        record: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        user: selectUser(),
        formRecord: (state, props) => {// eslint-disable-line
            const record = selectRecord()(state);
            return getFormValues(`editRecord_${record.id}`)(state)
        },
        metaData: selectRecordsMetaData()
    });

    const withConnect = connect(
        mapStateToProps,
    );

    return compose(
        withConnect,
        memo
    )(EditRecordPage);

}