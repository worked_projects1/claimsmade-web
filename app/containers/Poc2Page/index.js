/**
 * 
 * Poc2 Page
 * 
 */


import React, { memo, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useStyles from './styles';
import { Grid } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { Helmet } from 'react-helmet';
import PocForm from './Components/PocForm'
import { formValueSelector, change } from 'redux-form';
import { createStructuredSelector } from 'reselect';

export default function (name, path, actions, selectors, create, view) {// eslint-disable-line
    const {
        selectPoc2BodySystems,
        selectPoc2PoliciesDoc,
        selectPoc2AddedDoc,
        selectLoading
    } = selectors;


    function PocPage(props) {
        const { location = {}, record, metaData, dispatch, formRecord = {}, match, queryParams, form, formValues, updateField, headers, policies, addedPolicies, loading } = props;// eslint-disable-line
        const classes = useStyles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const componentMounted = useRef(true);

        useEffect(() => {
            // let mounted = true;
            dispatch(actions.loadPoc2("neck-and-upper-back"));
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [])

        function getPolicies(record) {
            dispatch(actions.loadPoc2Doc(record));
        }

        function addPolicies(records) {
            dispatch(actions.addPoc2Doc(records));
        }

        return (<Grid container>
            <Helmet
                title="VettedClaims"
                meta={[
                    { name: 'description', content: 'Insurance Page' },
                ]}
            />
            <Grid item xs={12} className={classes.FormPage}>
                <div className={classes.component} style={{ width: md ? `620px` : `100%` }}>
                    <PocForm formValues={formValues} updateField={updateField} headers={headers} getPolicies={getPolicies} policies={policies} addPolicies={addPolicies} addedPolicies={addedPolicies} loading={loading}
                    />
                </div>
            </Grid>
        </Grid>);
    }

    const mapStateToProps = (state) => {// eslint-disable-line
        return createStructuredSelector({
            headers: selectPoc2BodySystems(),
            policies: selectPoc2PoliciesDoc(),
            addedPolicies: selectPoc2AddedDoc(),
            loading: selectLoading(),
            formValues: (state, props) => {// eslint-disable-line
                return {
                    treatment: formValueSelector('pocForm')(state, 'treatment'),
                    diagnosis: formValueSelector('pocForm')(state, 'diagnosis'),
                    form: (state.form.pocForm && state.form.pocForm.values && Object.keys(state.form.pocForm.values).filter(key => !["treatment", "diagnosis"].includes(key))) || false,
                }
            },
        })
    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch,
            updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(PocPage);
}