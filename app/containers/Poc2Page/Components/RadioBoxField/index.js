/**
 * 
 * Radio Box
 * 
 */

import React from 'react';
import Styles from './styles';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import { FormControlLabel, FormLabel } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import { Grid } from '@mui/material';


export default function ({ input, label, options, disabled, defaultValue }) {// eslint-disable-line
  const classes = Styles();
  const text=label.replace(/\n/g, '<br />');
  const { name, value, } = input;
  const isPreDefinedSet = Array.isArray(options);
  return (
    <Grid className={classes.checkboxField}>
      <FormControl component="fieldset">
        <FormLabel key="demo-customized-radios"> <span dangerouslySetInnerHTML={{ __html: text }} /></FormLabel>
        <RadioGroup
          row
          name={name}
           value={value.value || defaultValue || ""}
           defaultValue={defaultValue}
          onChange={(e) => input.onChange(Object.assign({}, { name: name, value: e.target.value }))}>
          {isPreDefinedSet ? (options || []).map((opt, index) =>
            <FormControlLabel key={index} value={opt.action} name={opt.action} label={(opt.option).replace(/-/g, ' ').replace(/(?: |\b)(\w)/g, function (key) { return key.toUpperCase() })} control={<Radio
              classes={{ root: classes.radio, checked: classes.checked }}
            />} disabled={disabled ? true : false} labelPlacement="end" className={classes.radioLabel} />
          ) : null}
        </RadioGroup>
      </FormControl>
    </Grid>
  );
}
