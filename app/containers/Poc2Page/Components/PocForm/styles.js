import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
    root: {
        height: '100vh',
        flexGrow: 1
    },
    submitBtn: {
        fontWeight: 'bold',
        borderRadius: '3px',
        fontFamily: 'MyriadPro-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        height: '35px',
        marginRight: '15px',
        paddingTop: '8px'
    },

    dialog: {
        border: 'none',
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '75%',
    },
    dialogContent: {
        fontSize: "20px",
        padding: '25px',
        ['@media (max-width: 480px)']: {
            fontSize: '16px !important',

        },
        textAlign: 'center',
    },
    dialogAction: {
        justifyContent: 'center'
    },
    warning: {
        width: '55px',
        height: '60px'
    },
    warningDiv: {
        textAlign: 'center'
    },
    alertDiv: {
        width: '100%',
        marginTop: '20px'
    },
    alert: {
        // alignItems: 'center',
        '& .MuiAlert-message': {
            display: 'flex',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
        }
    },
    info: {
        cursor: 'pointer'
    },
    diagnosis: {
        display: 'flex',
        alignItems: 'center'
    }
}));


export default useStyles;
