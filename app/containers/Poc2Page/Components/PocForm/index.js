
/**
 * 
 * POC Form
 * 
 */

import React, { useEffect, useState } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Grid, Dialog, DialogActions, DialogContent, DialogContentText, Button, Alert } from '@mui/material';
import Warning from '@mui/icons-material/Warning';
import useStyles from './styles';
import { createTheme } from '@mui/material/styles';
import SelectField from '../SelectField';
import RadioBoxField from '../RadioBoxField';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
const CryptoJS = require("crypto-js");

/**
 * 
 * @param {object} props 
 * @returns 
 */

function PocForm(props) {

    const { handleSubmit, pristine, submitting, fields, path, error, metaData, locationState, destroy, formValues, dispatch, change, updateField, headers, getPolicies, policies, addPolicies, addedPolicies, loading } = props;// eslint-disable-line
    const classes = useStyles();
    const [showModal, setModalOpen] = useState(false);
    const [content, setContent] = useState([]);
    const [treatment, setTreatment] = useState([]);
    const [dialogText, setDialogText] = useState("");
    const [radio, setRadio] = useState([]);
    const [alertContent, setAlertContent] = useState("");
    const [alert, setAlertOpen] = useState(false);
    const [colorContent, setColor] = useState("#fff");
    const [icon, setIcon] = useState(false);
    const [diagnosisRecord, setDiagnosis] = useState([]);
    const theme = createTheme();

    useEffect(() => {
        const treatmentOptions = [];
        const diagnosisOptions = [];
        {
            (headers["Treatments"] || []).map((field, index) => {// eslint-disable-line
                treatmentOptions.push({ value: field.id, label: field.name });
                setTreatment(treatmentOptions);
                {
                    (field["Diagnoses"] || []).map((diagnosis, ind) => {// eslint-disable-line
                        diagnosisOptions.push({ value: diagnosis.name, label: diagnosis.name });
                        setDiagnosis(diagnosisOptions);
                    })
                }

            })
        }
        return () => destroy();
    }, [headers]);


    const handleChange = (val) => {
        const selectedTreatment = (headers["Treatments"] || []).filter((field, index) => {// eslint-disable-line
            return field.id == val
        })
        const existPolicy = addedPolicies.filter(e => e.treatment == val);
        let diagnosis = diagnosisRecord.filter((ele, ind) => ind === diagnosisRecord.findIndex(elem => elem.value === ele.value));
        diagnosis.sort((a, b) => a.label.localeCompare(b.label));
        setContent(diagnosis.filter((e) => e.value != "*").filter((e) => e.value != "NoDiagnosisFound"));
        setRadio([])
        setAlertOpen(false)
        setDialogText("")
        if (existPolicy.length == 0) {
            getPolicies(Object.assign({}, { "body_system": "neck-and-upper-back", "treatment_id": val, "name": selectedTreatment[0].name }))
        }
        updateField("pocForm", "diagnosis", "")

    }


    const handleDiagnosis = (val) => {
        let policies1 = []
        const existPolicy = addedPolicies.filter(e => e.treatment == formValues.treatment);

        if (existPolicy.length == 0) {
            policies1.push(policies["policy_doc"])
        } else {
            policies1 = existPolicy.map(e => e["schema"])
        }
        const bytes = CryptoJS.AES.decrypt(policies1[0], 'K6*^)&b=087&H%K!s2A0');
        const secret = bytes.toString(CryptoJS.enc.Utf8);
        const schema = JSON.parse(secret);
        const matchedArray1 = schema.policies.filter(r => r.diagnosis['label-name'] == val)
        const starDiagnosis = schema.policies.filter(r => r.diagnosis['label-name'] == '*')

        if ((matchedArray1.length != 0) || (starDiagnosis.length != 0)) {
            let diagnosis = {}
            if (matchedArray1.length != 0) {
                diagnosis = matchedArray1.find(e => e.questions)
            } else if (starDiagnosis.length != 0) {
                diagnosis = starDiagnosis.find(e => e.questions)
            }
            const questions = diagnosis.questions
            if (questions.length != 0) {
                if (formValues.form && formValues.form.length != 0) {
                    formValues.form.map(e => updateField("pocForm", e, ""))
                }
                setAlertOpen(false)
                setRadio([])
                setRadio(questions.map((e, i) => i == 0 ? Object.assign({}, e, { editRecord: true }) : Object.assign({}, e, { editRecord: false })))
            } else {
                setRadio([])
                if (diagnosis.recommendation == "deny") {

                    setAlertOpen(true)
                    setIcon(false)
                    setColor("error")
                    if (diagnosis.source == "mtus") {
                        setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br /> Relevant excerpt from MTUS Guidelines: <i>${diagnosis["mtus-text"]}</i>`)
                    } else if (diagnosis.source == "odg") {
                        setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${diagnosis["odg-text"]}</i>`)
                    }
                } else if (diagnosis.recommendation == "approve") {
                    // setAlertContent(`Approved by ${diagnosis.source.toUpperCase()}`)
                    setAlertOpen(true)
                    setIcon(true)
                    setColor("success")
                    if (diagnosis.source == "mtus") {
                        setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from MTUS Guidelines: <i>${diagnosis["mtus-text"]}</i>`)
                    } else if (diagnosis.source == "odg") {
                        setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${diagnosis["odg-text"]}</i>`)
                    }
                }
            }
        }
        else {
            setRadio([])
            setAlertContent(`Guidelines do not cover this scenario <br> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided.</i>
              `)
            setAlertOpen(true)
            setIcon(false)
            setColor("warning")
        }
    }

    const handleRadio = (val) => {
        
        let policies1 = []
        const existPolicy = addedPolicies.filter(e => e.treatment == formValues.treatment);

        if (existPolicy.length == 0) {
            policies1.push(policies["policy_doc"])
        } else {
            policies1 = existPolicy.map(e => e["schema"])
        }
        const bytes = CryptoJS.AES.decrypt(policies1[0], 'K6*^)&b=087&H%K!s2A0');
        const secret = bytes.toString(CryptoJS.enc.Utf8);
        const schema = JSON.parse(secret);
        const selected1 = (schema.policies || []).filter(r => r.diagnosis['label-name'] == formValues.diagnosis)
        const starDiagnosis = (schema.policies || []).filter(r => r.diagnosis['label-name'] == '*')
        if (val.value == "deny") {
            setAlertOpen(true)
            setIcon(false)
            setColor("error")
            let obj = {}
            if (selected1.length != 0) {

                obj = selected1.find(e3 => true)// eslint-disable-line
            } else if (starDiagnosis.length != 0) {
                obj = starDiagnosis.find(e3 => true)// eslint-disable-line
            }
            if (obj.source == "mtus") {
                setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from MTUS Guidelines: <i>${obj["mtus-text"]}</i>`)
            } else if (obj.source == "odg") {
                setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${obj["odg-text"]}</i>`)
            }
            setRadio(radio.map((e, i) => (val.name.slice(-1) < i) ? Object.assign({}, e, { editRecord: false }) : Object.assign({}, e, { editRecord: true })))
        } else if (val.value == "approve") {
            setAlertOpen(true)
            setIcon(true)
            setColor("success")
            let obj = {}
            if (selected1.length != 0) {
                obj = selected1.find(e3 => true)// eslint-disable-line
            } else if (starDiagnosis.length != 0) {
                obj = starDiagnosis.find(e3 => true)// eslint-disable-line
            }
            if (obj.source == "mtus") {
                setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from MTUS Guidelines: <i>${obj["mtus-text"]}</i>`)
            } else if (obj.source == "odg") {
                setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from ODG Guidelines: <i>${obj["odg-text"]}</i>`)
            }
            setRadio(radio.map((e, i) => (val.name.slice(-1) < i) ? Object.assign({}, e, { editRecord: false }) : Object.assign({}, e, { editRecord: true })))
        } else {
            radio.map((e, i) => (val.name.slice(-1) < i) ? updateField("pocForm", `questions${i}`, "") : e)
            setRadio(radio.map((e, i) => e["#"] == parseInt(val.value) || e.editRecord ? Object.assign({}, e, { editRecord: true }) : Object.assign({}, e, { editRecord: false })))// eslint-disable-line
            setAlertOpen(false)
        }
    }

    return (<div>
        {showModal ?
            <Grid className={classes.container}>
                <Dialog
                    open={showModal}
                    //   maxWidth={"md"}
                    sx={{ '& .MuiDialog-paper': { width: 420, maxHeight: 435, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                    maxWidth="xs"
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <DialogContent dividers>
                        <DialogContentText id="alert-dialog-description" className={classes.dialogContent}>
                            <span dangerouslySetInnerHTML={{ __html: dialogText }} />
                        </DialogContentText>
                    </DialogContent >
                    <DialogActions className={classes.dialogAction}>
                        <Button onClick={() => setModalOpen(false)} color="primary" >
                            OK
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid> : null}

        <form className={classes.form} noValidate >
            <Grid container spacing={3} name="insurancesForm">
                <Grid item xs={12}>
                    <Field name={"treatment"}
                        label={"Treatment"}
                        type="text"
                        value={"treatment"}
                        component={SelectField}
                        loading={formValues.treatment ? false : loading}
                        // required={field.required}
                        options={treatment}
                        onChange={handleChange}
                    />
                </Grid>

                <Grid item xs={12} className={classes.diagnosis}>
                    <Grid item style={{ width: '100%' }}>
                        <Field name={"diagnosis"}
                            label={"Diagnosis"}
                            type="text"
                            component={SelectField}
                            // required={field}
                            loading={loading}
                            disabled={content.length == 0 || formValues.treatment == ""}
                            options={content}
                            onChange={handleDiagnosis}
                        />
                    </Grid>
                </Grid>

                {radio.length != 0 ? (radio || []).map((e, i) => e.editRecord ?
                    <Grid item xs={12} key={i}>
                        <Field name={`questions${i}`}
                            label={e.question}
                            type="check"
                            component={RadioBoxField}
                            // required={field}
                            options={e["option-list"] || []}
                            onChange={handleRadio}
                        />
                    </Grid> : null) : null}

            </Grid>
            {alert ? <div className={classes.alertDiv}>
                <Alert
                    iconMapping={{
                        success: icon ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                    }}
                    color={colorContent}
                    style={colorContent == 'error' ? { background: '#f5b9b9' } : null}
                    className={classes.alert}
                >
                    <div>
                        <span dangerouslySetInnerHTML={{ __html: alertContent }} />
                    </div>
                </Alert>
            </div> : null}
        </form>
    </div>);
}



export default reduxForm({
    form: 'pocForm',
    enableReinitialize: true,
    touchOnChange: true
})(PocForm);

