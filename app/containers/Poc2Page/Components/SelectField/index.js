/**
 * 
 * Select Field
 * 
 */

import React from 'react';
import Styles from './styles';
import { FormControl, InputLabel, Select, MenuItem, Box } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';

export default function ({ input, label, required, metaData, options, variant, disabled, children, style, meta: { touched, error, warning }, loading }) {// eslint-disable-line
    const classes = Styles();
    const { name, value, onChange } = input;
    const isPreDefinedSet = Array.isArray(options);

    const changeValue = (e) => {
        onChange(e.target.value)
    }

    return (
        <div className={classes.selectField} style={style || {}}>
            {children || ''}
            <FormControl variant="standard" sx={{ m: 0, minWidth: 140 }} className={classes.formControl}>
                <InputLabel className={classes.fieldColor} id={`${name}-id`}>{label}</InputLabel>
                {loading ? <Box mt={2} pt={2.5}><Skeleton animation="wave" /></Box> : <Select
                    name={name}
                    fullWidth
                    disabled={disabled}
                    required={required}
                    labelId={`${name}-id`}
                    value={value}
                    classes={{
                        select: classes.select
                    }}
                    onChange={(e) => changeValue(e)}>
                    {(typeof options === 'string' && metaData[options] === undefined) ? <MenuItem value="">No options</MenuItem> : isPreDefinedSet ? (options || []).map((opt, index) => <MenuItem
                        key={index} disabled={opt.disabled || false} value={opt && opt.value || opt}>{opt && opt.label || opt}</MenuItem>) : (typeof options === 'string' && Object.keys(metaData[options]).length === 0) ? <MenuItem value="">No options</MenuItem> : (metaData[options] || []).map((opt, index) => <MenuItem key={index} disabled={opt.disabled || false} value={opt && opt.value != null
                            && opt.value || opt}>{opt && opt.label != null && opt.label || opt}</MenuItem>)
                    }
                </Select>}
            </FormControl>

            <div className={classes.error}>
                {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
            </div>
        </div>
    )
}

