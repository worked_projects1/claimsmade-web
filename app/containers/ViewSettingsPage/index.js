/**
 *
 * View Global Settings Page
 *
 */



import React, { useEffect, memo, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@mui/material';
import Styles from './styles';
import EditRecordForm from 'components/EditRecordForm';
import Snackbar from 'components/Snackbar';
import Spinner from 'components/Spinner';
import { Table, TableBody, TableCell, TableContainer, TableRow } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import Icons from 'components/Icons';
import {
    selectUser, selectVersion
} from 'blocks/session/selectors';
import store2 from 'store2';

import { updateVersion, loadAppVersion, verifySession } from 'blocks/session/actions';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, view) {// eslint-disable-line

    const {
        selectLoading,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectPractice,
        selectGlobalSettings
    } = selectors;
    /**
     * @param {object} props 
     */
    function ViewGlobalSettings(props) {
        const classes = Styles();
        const { dispatch, location = {}, loading, error, success, progress, children, match, user, version } = props;// eslint-disable-line
        const [showForm, setShowForm] = useState(false);
        const [showMedicalValidation, setShowMedicalValidation] = useState(false);// eslint-disable-line
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const componentMounted = useRef(true);

        async function handleEdit(record, dispatch, { form }) {
            if (showForm === 1) {
                if (name == "settings") {
                    dispatch(actions.updatePractice(record, form));
                } else {
                    dispatch(actions.updateGlobalSettings(record, form));
                }
                setShowForm(false);
            } else if (showForm === 2) {
                if (name == "settings") {
                    dispatch(updateVersion(record, form));
                }
                else {
                    dispatch(actions.updateGlobalSettings(record, form));
                }
                setShowForm(false);
            }
        }

        useEffect(() => {
            // let mounted = true;
            if (name == "globalSettings") {
                dispatch(actions.loadGlobalSettings());
            } else if (name == "settings") {
                dispatch(actions.loadPractice(user && user.practice_id));
                dispatch(loadAppVersion());
            }
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, []);

        const columValue = (column, value) => {
            switch (column.type) {
                case 'upload':
                    return (
                        <img
                            src={value || 'https://claimconnects-public-assets.s3.amazonaws.com/placeholder.jpeg.67764748-94f6-4abc-a906-42f08946338d..jpeg'}
                            role="presentation"
                            className={classes.img}
                        />
                    );
                case 'textarea':
                    return <p className={classes.terms}>{value != null && value != '' ? lineBreak(value) : '-'}</p>;
                case 'component':
                    return value && Array.isArray(value) && value.length > 0 ? <TableContainer>
                        <Table aria-label="spanning table" className={"medicalPrice"} style={{ maxWidth: '450px' }} >
                            <TableBody >
                                {value.map((r, i) => <TableRow key={i}>
                                    <TableCell style={{ paddingLeft: '0px' }}><b>{`Tier ${i + 1}: Pages`}</b><span style={{ marginLeft: '40px' }}>{i === (value.length - 1) ? `more than ${r.from} ` : r.from}</span>
                                        {i === (value.length - 1) ? null : <span> - </span>}
                                        {i === (value.length - 1) ? null : <span>{r.to}  </span>}
                                    </TableCell>

                                    <TableCell colSpan={i === (value.length - 1) ? 3 : 1}>
                                        {i === 0 ? 'Free' : i === (value.length - 1) ? 'Call for quote and order' : <div>${r.price}</div>}</TableCell>
                                </TableRow>)}
                            </TableBody>
                        </Table>
                    </TableContainer> : <p className={classes.value}>{value != null && value != '' ? value : '-'}</p>
                default:
                    return <p className={classes.value}>{value != null && value != '' ? column.dollar ? <span>${value}</span> : value : name == "globalSettings" ? '0' : <span>&nbsp;</span>}</p>;
            }
        };

        const lineBreak = (val) => {
            return val && val.toString().split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)
        }

        const section = (columns && typeof columns === 'function' && columns(user).section) || (columns && typeof columns === 'object' && columns.section) || false;

        if (progress) {
            return <Spinner className={classes.spinner} />;
        }

        const cancelBtn = (<Button
            onClick={() => setShowForm(false)}
            type="button"
            variant="contained"
            color="primary"
            className={classes.cancelBtn}>
            Cancel
        </Button>)

        return <Grid>
            {name == 'settings' && <Grid>
                <p style={{ margin: '0px' }}>Profile Information entered here is used to fill DWC Form RFA.</p>
            </Grid> || null}
            {section && section.map((tab, index) => <Grid container key={index}>
                <Grid item xs={12}>
                    {tab && tab.view && (<Grid className={classes.section} container style={tab.noText ? { marginTop: "0px" } : {}}>
                        <Grid item sm={6} xs={8} className={classes.sectionTitle}>
                            <Grid container justify="space-between">
                                <Typography component="h3" variant="h5" className={classes.name}>
                                    {tab.name}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item sm={6} xs={4}>
                            <Grid container justifyContent={"flex-end"}>
                                {tab.edit && view ? sm ? <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setShowForm(tab.schemaId)}
                                    className={classes.create} >
                                    {`Edit ${tab.name}`}
                                </Button> : <Icons type='Edit' className={classes.icons} onClick={() => setShowForm(tab.schemaId)} /> : null}
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container className={classes.sectionDiv}>
                                <Grid item xs={12} md={showForm ? 6 : 12} className={name == 'settings' ? classes.columns : ''}>
                                    {(typeof tab.columns === 'function' && tab.columns(props[tab.value]) || tab.columns).filter(_ => _.viewRecord).map((col, index) => {
                                        const columnVal = (col.value.indexOf('.') > -1 && col.value.split('.')) || col.value;
                                        const fieldVal = (Array.isArray(columnVal) && props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[columnVal[0]] && tab.filter(props[tab.value])[columnVal[0]][columnVal[1]])
                                            || (props[tab.value] && tab.filter(props[tab.value]) && tab.filter(props[tab.value])[col.value]);
                                        return (
                                            <Grid key={index} >
                                                {/* style={{marginTop: showForm ? md ? '55px' : '0px' : '0px'}} */}
                                                <Grid className={classes.settings}>
                                                    <Grid container className={name == "settings" && col.value != "session_timeout" ? classes.row2 : classes.row} alignItems={"center"}>
                                                        {col.label ?
                                                            <Grid item xs={6} md={showForm ? name == 'settings' ? 4 : 6 : 4} className={classes.label}
                                                            // style={name == 'settings' ? { fontWeight: 'bold'} : {}}
                                                            > {col.label}:
                                                            </Grid> : null}
                                                        <Grid item xs={col.label ? 6 : 12} md={!col.label ? 12 : showForm ? name == 'settings' ? 8 : 6 : 8}
                                                            className={classes.value}
                                                        // style={!col.label ? { paddingLeft: '0px' } : name == 'settings' ? { fontWeight: 'bold'} : null}
                                                        >
                                                            {col.docType ? <div className={classes.dollarDiv}><span className={classes.dollar}>$</span>{columValue(col, fieldVal, props[tab.value])}</div> : columValue(col, fieldVal, props[tab.value])}
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                {col.type !== 'component' && col.label !== 'Minimum Pricing' && <Grid item xs={12} md={showForm ? 10 : 8} ><hr className={classes.hr} />
                                                    {/* {name === "globalSettings" ? <div style={{ padding: '10px' }}></div> : null} */}
                                                </Grid> || null}
                                            </Grid>
                                        )
                                    })}
                                </Grid>

                                {!loading && showForm === tab.schemaId ?
                                    <Grid item xs={12} md={showForm ? 6 : 12} className={classes.table}>
                                        <Grid className={classes.edit}>
                                            <EditRecordForm
                                        initialValues={tab.filter(props[tab.value]) || tab.initialValues || {}}
                                        form={`SettingsForm_${tab.schemaId}`}
                                                name={name}
                                                path={path}
                                        fields={tab.columns.filter(_ => _.editRecord)}
                                                confirmButton={true}
                                        confirmMessage={tab.confirmMessage}
                                                btnLabel="Update"
                                                onSubmit={handleEdit.bind(this)}
                                                locationState={location.state}
                                                cancelBtn={cancelBtn}
                                            />
                                        </Grid>
                                    </Grid> : null}
                            </Grid>
                        </Grid>
                        <Snackbar show={error || success || showMedicalValidation ? true : false} text={error || success || showMedicalValidation} severity={showMedicalValidation && 'error' || error && 'error' || 'success'} handleClose={() => {
                            dispatch(actions.loadRecordsCacheHit());
                            if (success == "Profile Updated") {
                                const secret = store2.get('secret');
                                dispatch(verifySession(secret));
                            }
                        }}
                            autoHideDuration={4000}/>
                    </Grid>) || null}
                </Grid>
            </Grid>)}
        </Grid>


    }

    ViewGlobalSettings.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object,
        pricing: PropTypes.object,
        progress: PropTypes.bool,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = (state) => {// eslint-disable-line
        return createStructuredSelector({
            loading: selectLoading(),
            pricing: (name === "globalSettings") && selectGlobalSettings() || selectUser(),
            error: selectUpdateError(),
            success: selectSuccess(),
            progress: selectProgress(),
            practice: (name === "settings") && selectPractice() || selectUser(),
            version: selectVersion(),
            user: selectUser()
        });
    }

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewGlobalSettings);

}
