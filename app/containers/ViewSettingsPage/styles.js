import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    create: {
        fontWeight: 'bold',
        borderRadius: '3px',
        textTransform: 'capitalize',
        fontFamily: 'MyriadPro-Regular',
        padding: '6px 30px',
        height: '35px',
        lineHeight: '35px',
        // marginRight: theme.spacing(0.5),
        ['@media (max-width: 480px)']: {
            marginLeft: theme.spacing(2),
        },
        paddingTop: '8px'
    },
    name: {
        fontSize: "23px"
    },
    settings: {
        paddingLeft: '25px',
        paddingRight: '25px'
    },
    row: {
        // paddingTop: '15px'
    },
    row1:{
        // paddingTop: '15px'
    },
    hr: {
        borderTop: '1px solid lightgray',
        borderBottom: 'none',
        marginTop: '0px'
    },
    table: {
        boxShadow: '0px 0px 8px 1px lightgrey',
        borderRadius: '8px',
        paddingTop: '8px',
        paddingBottom: '8px',
        // marginTop: '40px',
        overflow: 'auto',
        [theme.breakpoints.down('sm')]: {
            marginTop: '35px',
        }
    },
    edit: {
        padding: '20px',
        paddingTop: '0px'
    },
    section: {
        marginTop: '24px'
    },
    label: {
        fontSize: '16px',
        paddingRight: '25px',
    },
    value: {
        fontSize: '15px',
        //fontFamily: 'MyriadPro-Semibold',
        paddingLeft: '25px'
    },
    dollar: {
        paddingBottom: '2px',
        paddingRight: '2px'
    },
    dollarDiv: {
        display: 'flex',
        alignItems: 'center'
    },
    icons: {
        marginLeft: '14px',
        height: '32px',
        color: '#3c89c9',
        cursor: 'pointer'
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '3px',
        fontFamily: 'MyriadPro-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        height: '35px',
        lineHeight: '35px',
        backgroundColor: 'gray !important',
        paddingTop: '8px'
    },
    img:{
        height: '50px',
        paddingLeft: '25px',
        marginBottom: '8px'
    },
    columns: {
        // paddingTop: '25px'
    },
    sectionDiv: {
        marginTop: '24px'
    },
    spinner: {
        marginTop: '75px'
    },
    sectionTitle: {
        alignSelf: 'flex-end'
    }
}));

export default useStyles;