/*
 * ViewRecordPage Messages
 *
 * This contains all the text for the ViewRecordPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ViewRecordPage';

export default defineMessages({
    edit: {
        id: `${scope}.edit`,
        defaultMessage: 'Edit',
    },
    delete: {
        id: `${scope}.delete`,
        defaultMessage: 'Delete',
    },
    close: {
        id: `${scope}.close`,
        defaultMessage: 'Close',
    },
    inActive: {
        id: `${scope}.inactive`,
        defaultMessage: 'inactive',
    },
    active: {
        id: `${scope}.active`,
        defaultMessage: 'active',
    },
    error: {
        id: `${scope}.error`,
        defaultMessage: 'There was an error loading the resource. Please try again.',
    }
});
