

/**
 * 
 * Vew Record Page
 * 
 */

import React, { memo, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Button, Dialog, DialogContent, DialogTitle } from '@mui/material';
import Styles from './styles';
import AlertDialog from '../../components/AlertDialog';
import Snackbar from 'components/Snackbar';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Skeleton from '@mui/material/Skeleton';
import ModalForm from 'components/ModalForm';
import schema from 'routes/schema.js';
import {
    selectUser,
} from 'blocks/session/selectors';
import remotes from 'blocks/poc2/remotes';
import CloseIcon from '@mui/icons-material/Close';
import { useTheme } from '@mui/material/styles';
import JSONPretty from 'react-json-pretty';
const CryptoJS = require("crypto-js");

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {boolean} decoratorView 
 */
export default function (name, path, columns, actions, selectors, decoratorView) {

    const {
        selectRecord,
        selectRecordsMetaData,
        selectUpdateError,
        selectSuccess,
        selectProgress,
        selectPageLoader,
        selectTreatments
    } = selectors;

    /**
     * @param {object} props 
     */
    function ViewRecordPage(props) {
        const classes = Styles();
        const { record, error, success, loading, progress, dispatch, location = {}, match = {}, metaData = {}, queryParams, treatments, user } = props;// eslint-disable-line
        const schemaId = location && location.state && location.state.schemaId || false;
        const section = (typeof columns === 'function') ? columns(record).columns : columns && typeof columns === 'object' && schemaId && columns.section || false;
        const sectionColumns = section && schemaId && section.find(_ => _.schemaId === schemaId) || false;
        const componentMounted = useRef(true);
        const theme = useTheme();

        const deleteRecord = () => {
            dispatch(actions.deleteRecord(record.id));
        };

        const changeStatus = (status) => {
            dispatch(actions.changeStatusAction(record.id, Object.assign({}, { status: status })));
        }
        const [showModal, setModalOpen] = useState(false);
        const [dialogText, setDialogText] = useState("");
        const [dialogOpen, setDialogOpen] = useState(false);
        const [dialogTitle, setDialogTitle] = useState("");
        const viewHistoryColumns = schema().viewHistory().columns;

        useEffect(() => {
            // let mounted = true;
            dispatch(actions.loadRecordsCacheHit());
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords(false, queryParams));
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [match.params.id]);

        let displayName;
        if (name == 'users.view') {
            displayName = "user";
        } else if (name == 'vettedclaimsUsers.view') {
            displayName = "vettedclaims user";
        }
        else if (name == 'plans.view') {
            displayName = "plan";
        } else if (name == 'practiceUsers.view') {
            displayName = "practice user";
        }else if (name == 'Claim Administrator') {
            displayName = "claim administrator";
        }else if (name == 'Body System') {
            displayName = "body system";
        }
         else {
            displayName = name;
        }

        const closeDialog = () => {
            setDialogOpen(false);
        }

        let deleteConfirmation = `Are you sure you want to delete this ${displayName || ""}?`;
        let inActiveConfirmation = `Do you want to make this treatment ${record.status == "Inactive" ? "active" : "inactive"}?`;
        if (name == 'clients.view' && record && record.totalCases > 0) {
            deleteConfirmation = `This client has case records. Deleting this client will delete all case records and documents attached to this client. Do you want to continue?`;
        }

        const openFile = (e) => {
            remotes(name).loadPoliciesDoc({ "treatment_id": e['id'], body_system: e['body_system_index'] }).then(res => {
                if (res["policy_doc"]) {
                    const bytes = CryptoJS.AES.decrypt(res["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
                    const secret = bytes.toString(CryptoJS.enc.Utf8);
                    const schema = JSON.parse(secret);
                    setDialogOpen(true);
                    setDialogText(schema);
                    setDialogTitle(`<Typography component="span" style='font-size: 1rem'> <span style='font-family:"MyriadPro-Bold"'>Treatment :</span> ${e.name}</Typography>`);
                }
                // window.open(res.publicUrl, '_blank')
            }).catch(error => Promise.reject(error))
        }

        const policyStyle = classes.policyStyle

        const columValue = (column, row) => {
            switch (column.type) {
                case 'upload':
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) :
                            (<img
                                src={`${row[column.value] || ''}`}
                                role="presentation" style={{ height: 64, padding: 2 }}
                            />);

                default:
                    return loading ? <Skeleton animation="wave" width={80} /> :
                        column.html ? column.html(row, metaData) == undefined ? '-' : column.value == 'policyDoc' ? <Button name="btnTocheck" className={policyStyle} onClick={(e) => {
                            e.stopPropagation();
                            openFile(row);
                        }}>{column.html(row, policyStyle)}</Button> : column.html(row, metaData) :
                            column.value == 'history' ? treatments.length > 1 ? <Button className={classes.policyStyle} name="btnTocheck" onClick={() => setModalOpen(!showModal)}>View History</Button> : '-' :
                                (<p
                                    className={classes.value}
                                    dangerouslySetInnerHTML={{ __html: !row[column.value] && row[column.value] != '0' ? '-' : row[column.value] || '-' }}
                                />);
            }
        };


        return (
            <Grid container name="viewRecordPage">
                <Grid item xs={12} className={classes.viewPage}>
                    <Grid container className={classes.header}>
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Link to={{ pathname: path, state: { ...location.state } }} className={classes.link}>
                                <FormattedMessage {...messages.close} />
                            </Link>}
                        {loading ? <Skeleton animation="wave" width={80} /> :
                            <Grid className={classes.section}>
                                <Link to={{ pathname: `${path}/${record.id}/edit`, state: { ...location.state } }} className={classes.link}>
                                    <FormattedMessage {...messages.edit} />
                                </Link>
                                {name != 'customTemplate.view' && name != 'state.view' && name != 'treatment' ? <AlertDialog
                                    description={deleteConfirmation}
                                    onConfirm={deleteRecord}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Delete'
                                    btnLabel2='Cancel' >
                                    {(open) => <span className={classes.delete} onClick={open}><FormattedMessage {...messages.delete} /></span>}
                                </AlertDialog> : name == 'treatment' && user.email == "siva@miotiv.com" ? <AlertDialog
                                    description={inActiveConfirmation}
                                    onConfirm={() => changeStatus(record.status == "Inactive" ? "Active" : "Inactive")}
                                    onConfirmPopUpClose={true}
                                    btnLabel1={record.status == "Inactive" ? "Active" : "InActive"}
                                    btnLabel2='Cancel' >
                                    {(open) => <span className={classes.delete} onClick={open}><FormattedMessage {...record.status == "Inactive" ? messages.active : messages.inActive} /></span>}
                                </AlertDialog> : null}
                            </Grid>}
                    </Grid>
                    <Grid item xs={12}>
                        {decoratorView && record && React.createElement(decoratorView, Object.assign({}, props, { actions }))}
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {(typeof columns === 'object' ? (section && sectionColumns && sectionColumns.columns || columns || section || []) : section).map((column) =>
                            (column.viewRecord ?
                                <Grid item xs={12} key={column.id}>
                                    <div>
                                        <div className={classes.label}>{column.label}:</div>
                                        {columValue(column, record)}
                                        <hr className={classes.hr} />
                                    </div>
                                </Grid> : null)
                            )}
                        </Grid>
                    </Grid>
                    {dialogOpen ? <Grid item xs={12} md={6}>
                        <Dialog
                            open={dialogOpen}
                            //   maxWidth={"md"}
                            sx={{ '& .MuiDialog-paper': { width: 850, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                            maxWidth="md"
                            scroll={"body"}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description">
                            <DialogTitle>
                                <Grid container className={classes.header1} justifyContent="space-between">
                                    <Grid item xs={10}>
                                        <span dangerouslySetInnerHTML={{ __html: dialogTitle }} />
                                    </Grid>
                                    <Grid item xs={2} style={{ textAlign: 'end' }}>
                                        {<CloseIcon onClick={closeDialog} className={classes.closeIcon} />}
                                    </Grid>
                                </Grid>
                            </DialogTitle>
                            <DialogContent dividers>
                                <JSONPretty id="json-pretty" data={dialogText} />
                            </DialogContent >
                        </Dialog>
                    </Grid> : null}
                    {showModal && <ModalForm
                        title="History"
                        fields={viewHistoryColumns}
                        onSubmitClose={true}
                        enableSubmitBtn
                        onClose={() => setModalOpen(false)}
                        show={showModal}
                        records={treatments.sort((a, b) => a['updated_at'].localeCompare(b['updated_at'])).reverse()}
                        loading={loading}
                        currentJson={record["policy_doc"]}
                    /> || null}
                </Grid>
                <Snackbar show={error || success ? true : false} text={error || success} severity={error ? 'error' : 'success'} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
            </Grid>);
    }

    ViewRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string]),
        history: PropTypes.object,
        loading: PropTypes.bool,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object,
        progress: PropTypes.bool,
        record: PropTypes.object,
        success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        metaData: selectRecordsMetaData(),
        error: selectUpdateError(),
        success: selectSuccess(),
        loading: selectPageLoader(),
        progress: selectProgress(),
        user: selectUser(),
        treatments: selectTreatments()
    });

    const withConnect = connect(
        mapStateToProps,
    );


    return compose(
        withConnect,
        memo
    )(ViewRecordPage);

}
