import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();
    return ({
        Button: {
            fontWeight: 'bold',
            textTransform: 'capitalize',
            fontFamily: 'MyriadPro-Bold',
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#fff',
            fontSize: '18px',
            marginLeft: '10px',
            "&:hover": {
                backgroundColor: '#fff',
                boxShadow: 'none',
            },
            padding: '2px 18px',
            borderRadius: '3px'
        },
        link: {
            fontWeight: 'bold',
            textTransform: 'capitalize',
            fontFamily: 'MyriadPro-Bold',
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#ccebf6 !important',
            fontSize: '18px',
            paddingTop: '10px',
            paddingBottom: '10px',
            '& span': {
                color: '#3c89c9 !important',
            }
        },
        delete: {
            marginLeft: theme.spacing(2),
            cursor: 'pointer',
            fontWeight: 'bold',
            textTransform: 'capitalize',
            fontFamily: 'MyriadPro-Bold',
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#ccebf6 !important',
            fontSize: '18px',
            paddingTop: '10px',
            paddingBottom: '10px',
            '& span': {
                color: '#3c89c9 !important',
            }
        },
        label: {
            fontWeight: 'bold',
            fontFamily: 'MyriadPro-Bold',
            textTransform: 'uppercase',
            marginRight: '2em'
        },
        hr: {
            borderTop: '1px solid lightgray',
            borderBottom: 'none'
        },
        value: {
            marginTop: '0px'
        },
        img: {
            height: 64
        },
        header: {
            justifyContent: 'space-between',
            marginBottom: '12px'
        },
        section: {
            display: 'flex',
            alignItems: 'center'
        },
        viewPage: {
            fontSize: "0.875em"
        },
        policyStyle:{
            minWidth: 'fit-content',
            justifyContent: 'left',
            padding: '6px 0px'
        },
        dialog: {
            border: 'none',
            padding: '25px',
            minWidth: '75%',
            outline: 'none',
            width: '75%',
        },
        closeIcon: {
            cursor: 'pointer'
        },
    })
});

export default useStyles;