
/**
 *
 * Create Record Page
 *
 */


import React, { useState, useEffect, memo, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import CreateRecordForm from 'components/CreateRecordForm';
import { set } from './utils';
import { getFormValues } from 'redux-form';
import lodash from 'lodash';
import {selectUser} from 'blocks/session/selectors';
/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView, submitButtonView) {// eslint-disable-line

    const { selectRecordsMetaData } = selectors;

    /**
     * @param {object} record 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleCreate(record, dispatch, { form }) {
        let submitRecord = Object.assign({}, { ...record });
        if (name === 'users.create' || name === 'vettedclaimsUsers.create' || name === 'practiceUsers.create') {
            let userCreatedIn = submitRecord && submitRecord.role === 'physician' || submitRecord.role === 'staff' || name === 'practiceUsers.create' ? 'customer_admin' : name === 'vettedclaimsUsers.create' ? 'super_admin' : '';
            submitRecord = Object.assign({}, submitRecord, { userCreatedIn, notification: true });
        }
        
        dispatch(actions.createRecord(submitRecord, form))
    }

    /**
     * @param {object} props 
     */
    function CreateRecordPage(props) {
        const [loading, setLoading] = useState(true);
        const { location = {}, user = {}, dispatch, metaData = {}, formRecord = {}, queryParams } = props;// eslint-disable-line
        const record = (Object.keys(formRecord).length > 0 && formRecord) || (location && location.state && location.state.formRecord) || {};
        const fieldsMetData = location && location.state && location.state.metaData && lodash.merge(metaData, location.state.metaData) || metaData;
        const fields = (typeof columns === 'function' ? columns(record).columns : columns).filter(_ => _.editRecord && !_.edit)
        const initialValues = set(fields);
        const initialRecords = initialValues
        const reInitialize = false
        const componentMounted = useRef(true);

        useEffect(() => {
            // let mounted = true;
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, []);

        return (<div>
            {decoratorView && React.createElement(decoratorView, props)}
            {!loading ? <CreateRecordForm
                initialValues={initialRecords}
                name={name}
                path={path}
                form={`Add_${name}`}
                metaData={fieldsMetData}
                fields={fields}
                enableReinitialize={reInitialize}
                keepDirtyOnReinitialize={reInitialize}
                onSubmit={handleCreate.bind(this)}
                locationState={location.state}
                submitButtonView={submitButtonView}
            /> : 'Loading...'}
        </div>
        )
    }

    CreateRecordPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        metaData: selectRecordsMetaData(),
        user:selectUser(),
        formRecord: getFormValues(`Add_${name}`)
    });

    const withConnect = connect(
        mapStateToProps
    );


    return compose(
        withConnect,
        memo
    )(CreateRecordPage);
}
