/**
 *
 * Categories Page
 *
 */



import React, { useEffect, memo, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Grid, Tabs, Tab, Paper } from '@mui/material';
import Styles from './styles';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

/**
 * 
 * @param {String} name 
 * @param {String} path 
 * @param {Array} categories 
 * @returns 
 */
export default function (name, path, categories) {// eslint-disable-line

    /**
     * @param {object} props 
     */
    function CategoriesPage(props) {

        const classes = Styles();
        const { location = {}, children, history } = props;// eslint-disable-line
        const selectedTab = location && location.pathname && (categories || []).findIndex(_ => location.pathname.indexOf(`${path}/${_.path}`) > -1) || 0;
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const componentMounted = useRef(true);

        useEffect(() => {
            // let mounted = true;
            if (selectedTab === -1 && categories && categories.length > 0 && categories[0].path) {
                history.push({ pathname: `${path}/${categories[0].path}`, state: location.state });
            }
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [selectedTab]);

        const handleTabs = (e, tabIndex) => {
            const category = categories.find((s, i) => i === tabIndex);
            if (category)
                history.push({ pathname: `${path}/${category.path}`, state: location.state });
        }

        return <Grid container id={name}>
            <Grid item xs={12}>
                <Paper square className={classes.paper}>
                    <Tabs
                        value={selectedTab}
                        onChange={handleTabs}
                        variant={md ? "fullWidth" : "standard"}
                        indicatorColor="primary"
                        textColor="primary">
                        {(categories || []).map((category, index) => <Tab key={index} label={category && category.data && category.data.title} />)}
                    </Tabs>
                </Paper>
            </Grid>
            <Grid item xs={12} className={classes.categoryChildren}>
                {children}
            </Grid>
        </Grid>


    }

    CategoriesPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        pathData: PropTypes.object
    };

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(CategoriesPage);

}
