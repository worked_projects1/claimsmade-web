import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
    paper: {
        flexGrow: 1,
        maxWidth: '75%',
        ['@media (max-width: 480px)']: {
            maxWidth: '100%',
        },
        maxHeight: '58px',
        boxShadow: 'none',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
                color: '#3c89c9',
                fontFamily: 'MyriadPro-Semibold'
            },
            '.MuiButtonBase-root': {
                width: '200px',
                ['@media (max-width: 480px)']: {
                    width: '175px'
                }
            },
            '.MuiButtonBase-root > span': {
                fontFamily: 'MyriadPro-Bold'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#3c89c9'
            }
        }
    },
    categoryChildren: {
        // marginTop: '25px'
    }
}));

export default useStyles;