import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    root: {
      height: '100vh',
      flexGrow: 1
    },
    component: {
      padding: '35px',
      background: 'white',
      display: 'flex',
      flexDirection: 'column',
      boxShadow: '0px 0px 20px 10px #00000026',
      marginTop: `${theme.spacing(7)}`
    },
    FormPage: {
      display: 'flex',
      justifyContent: 'center',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    dialog: {
      border: 'none',
      padding: '25px',
      minWidth: '40%',
      outline: 'none',
      width: '75%',
    },
    dialogContent: {
      padding: '8px',
    },
    dialogAction: {
      justifyContent: 'center'
    },
    warning: {
      width: '55px',
      height: '60px',
      // color: '#ea6225',
    },
    warningDiv: {
      textAlign: 'center'
    },
    alertDiv: {
      width: '100%',
      marginTop: '20px'
    },
    alert: {
      alignItems: 'center'
    },
    btn: {
      color: 'red !important'
    }
  })
});


export default useStyles;
