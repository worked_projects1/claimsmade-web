/**
 * 
 * Insurances Page
 * 
 */

import React, { memo, useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useStyles from './styles';
import { Grid, Dialog, DialogActions, DialogContent, DialogContentText, Button, Alert } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { Helmet } from 'react-helmet';
import InsurancesForm from 'components/InsurancesForm';
import { formValueSelector, change } from 'redux-form';
import Warning from '@mui/icons-material/Warning';
import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
// import Error from 'components/Error';
import Snackbar from '@mui/material/Snackbar';

export default function (name, path, columns, create, view) {// eslint-disable-line


    function InsurancesPage(props) {
        const { location = {}, record, metaData, dispatch, formRecord = {}, match, queryParams, form, formValues } = props;// eslint-disable-line
        const classes = useStyles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const [showModal, setModalOpen] = useState(false);
        const [content, setContent] = useState("#fff");
        const [icon, setIcon] = useState(false);
        const [alertContent, setAlertContent] = useState(false);
        const [alert, setAlertOpen] = useState(false);
        const componentMounted = useRef(true);
        const fields = columns && typeof columns === 'function' ? columns(formValues || {}).columns : [];

        const closeAlert = (clearContent) => {
            const values = ["guidelines", "chapters", "diagnosis", "services"]
            const formContent = values.splice(values.indexOf(clearContent) + 1, values.length - 1)
            formContent.map((clear) =>
                dispatch(change("insurancesForm", clear, ""))
            )
        }

        useEffect(() => {
            // let mounted = true;
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [])

        const closeModal = () => {
            setContent("#fff");
            setModalOpen(false);
        }

        const handleSubmit = (data, dispatch, { form }) => {// eslint-disable-line
            if (data.diagnosis == "cervicothoracicPainChronic" && data.services == "tcas") {
                setAlertContent('Service unconditionally approved by MTUS')
                setAlertOpen(true)
                setContent("success")
                setIcon(true)
            } else if (data.diagnosis == "cervicothoracicPainAcute" && data.services == "xray") {
                setAlertContent('Approved under following conditions. <br />X-Ray for Acute Cervicothoracic Pain is approved only for patients with red flags such as dangerous mechanism of injury, over 65 years of age, or parasthesias in extremities. Also, repeat films are not approved unless there is significant change in clinical status')
                setAlertOpen(true)
                setContent("warning")
                setIcon(true)
            } else if (data.diagnosis == "cervicalPainAcute" && data.services == "topicalNSAIDs") {
                setAlertContent('No recommendation from MTUS')
                setAlertOpen(true)
                setContent('info')
                setIcon(false)
            } else if (data.diagnosis == "cervicalPainAcute" && data.services == "spiroflor") {
                setAlertContent('Service NOT recommended by MTUS')
                setAlertOpen(true)
                setContent("error")
                setIcon(false)
            } else {
                setAlertOpen(false)
                setAlertContent('Below four combinations only available for this proof of concept release.<br /><ol><li>Diagnosis: Cervicothoracic Pain - Chronic; Service: TCAs</li><li>Diagnosis: Cervicothoracic Pain - Acute, Service: X-ray</li><li>Diagnosis: Cervical Pain - Acute, Service: Topical NSAIDs</li><li>Diagnosis Cervical Pain - Acute, Service: Spiroflor</li></ol>')
                setModalOpen(true)
                setContent("red")
                setIcon(false)
            }
        }

        function alerting(text) {
            if (text == 'chapters') {
                closeAlert('chapters');
                return <div className={classes.alertDiv}><Alert className={classes.alert} severity="error" >&#34;Cervicothoracic Pain&#34; is the only selection available for this Proof of Concept release</Alert></div>
            } else if (text == 'guidelines') {
                closeAlert('guidelines');
                return <div className={classes.alertDiv}><Alert className={classes.alert} severity="error" >&#34;Cervical and Thoracic Spine&#34; is the only selection available for this Proof of Concept release</Alert></div>
            } else {
                return <div className={classes.alertDiv}></div>
            }
        }

        return (<Grid container>
            <Helmet
                title="VettedClaims"
                meta={[
                    { name: 'description', content: 'Insurance Page' },
                ]}
            />
            <Grid item xs={12} className={classes.FormPage}>
                <div className={classes.component} style={{ width: md ? `420px` : `100%` }}>
                    {showModal ?
                        <Grid className={classes.container}>
                            <Dialog
                                open={showModal}
                                //   maxWidth={"md"}
                                sx={{ '& .MuiDialog-paper': { width: 420, maxHeight: 435, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                                maxWidth="xs"
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description">
                                <DialogContent dividers>
                                    <DialogContentText id="alert-dialog-description" className={classes.dialogContent}>
                                        <Grid className={classes.warningDiv}>
                                            {icon ? <CheckCircleOutlinedIcon fontSize="small" className={classes.warning} sx={{ color: content, fontSize: 12 }} /> : <Warning className={classes.warning} sx={{ color: content }} />}
                                        </Grid>
                                        <span dangerouslySetInnerHTML={{ __html: alertContent }} />
                                    </DialogContentText>
                                </DialogContent >
                                <DialogActions className={classes.dialogAction}>
                                    <Button onClick={closeModal} className={classes.btn}>
                                        Dismiss
                                    </Button>

                                </DialogActions>
                            </Dialog>
                        </Grid> : null}
                    <InsurancesForm
                        form={`insurancesForm`}
                        name={name}
                        path={path}
                        fields={fields}
                        locationState={location.state}
                        onSubmit={(data, dispatch, { form }) => handleSubmit(data, dispatch, { form })}
                        btnDisabled={((formValues.guidelines != undefined && formValues.guidelines == 'cervicalAndThoracicSpine') && (formValues.chapters != undefined && formValues.chapters == 'cervicothoracicPain') && (formValues.diagnosis != undefined) && (formValues.services != undefined))}
                    />
                    {(formValues.guidelines != undefined && formValues.guidelines != 'cervicalAndThoracicSpine') ? alerting('guidelines') : null}

                    {(formValues.chapters != undefined && formValues.chapters != 'cervicothoracicPain') ? alerting('chapters') : null}

                    <Snackbar open={alert} onClose={() => setAlertOpen(false)} className={classes.snackbar} sx={{ position: 'static' }}>
                        {alert && (formValues.guidelines == 'cervicalAndThoracicSpine' && formValues.chapters == 'cervicothoracicPain') ? <div className={classes.alertDiv}>
                            <Alert
                                iconMapping={{
                                    success: icon ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                                }}
                                //   fill="green"
                                // variant="filled"
                                color={content}
                                className={classes.alert}>
                                <span dangerouslySetInnerHTML={{ __html: alertContent }} />
                            </Alert>
                        </div> : null}
                    </Snackbar>
                </div>
            </Grid>
        </Grid>);
    }

    const mapStateToProps = (state) => ({
        formValues: {
            guidelines: formValueSelector('insurancesForm')(state, 'guidelines'),
            chapters: formValueSelector('insurancesForm')(state, 'chapters'),
            diagnosis: formValueSelector('insurancesForm')(state, 'diagnosis'),
            services: formValueSelector('insurancesForm')(state, 'services'),
        }
    })

    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(InsurancesPage);
}