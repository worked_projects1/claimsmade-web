/**
 * 
 * Records Page
 * 
 */



import React, { useEffect, memo, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';
import { Grid, Button, Dialog, DialogContent, DialogTitle, Typography } from '@mui/material';
import Styles from './styles';
import Error from 'components/Error';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { RecordsData, getDefaultHeaders } from 'utils/tools';// eslint-disable-line
import TableWrapper from 'components/ReactTableWrapper';
import remotes from 'blocks/poc2/remotes';
import { FormControl, MenuItem, TextField } from '@mui/material';
import {
    selectUser,
} from 'blocks/session/selectors';
const CryptoJS = require("crypto-js");
import CloseIcon from '@mui/icons-material/Close';
import JSONPretty from 'react-json-pretty';
import ButtonSpinner from 'components/ButtonSpinner';
import Snackbar from 'components/Snackbar';
import AlertDialog from '../../components/AlertDialog';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, create, view, extraSpace) {// eslint-disable-line
    const {
        selectLoading,
        selectRecords,
        selectRecordsMetaData,
        selectError,
        selectTotalPageCount,
        selectHeaders,
        selectFilter,
        selectTreatments,
        selectbodySystemNames,
        selectTreatmentsNames,
        selectSuccess,
    } = selectors;

    /**
     * @param {object} submitData 
     * @param {function} dispatch 
     * @param {object} param2 
     */

    /**
     * @param {object} props 
     */
    function RecordsPage(props) {

        const classes = Styles();
        const { dispatch, records, children, location = {}, history, error, loading, metaData = {}, totalPageCount, headers = {}, filterObjection = {}, queryParams, treatments, user, bodySystemNames, treatmentNames, success } = props;// eslint-disable-line
        const { pathname } = location;
        const activeChildren = path !== pathname;
        const fullView = activeChildren && pathname.indexOf('view') > -1 ? true : false;
        const section = columns && typeof columns === 'object' && columns.section || false;
        const theme = useTheme();
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const sm1 = useMediaQuery(theme.breakpoints.down('sm'));
        const showText = typeof columns === 'object' && columns.find(e => e.value == "showText") || typeof columns === 'function' && columns(user).columns.find(e => e.value == "showText") || '';
        const componentMounted = useRef(true);
        const [treatmentRecord, setTreatmentRecord] = useState("all");
        const [showModal, setModalOpen] = useState(false);
        const [dialogText, setDialogText] = useState("");
        const [dialogTitle, setDialogTitle] = useState("");
        const [bodySystemValue, setBodySystemValue] = useState("all");
        const [syncloading, setsyncloading] = useState(false);
        const errorMessage = error && (error.syncError);
        const successMessage = success && (success.syncSuccess);
        const syncConfirmation = `Do you want to sync the policy docs from staging site?`;

        function getRecordsData() {
            if (name == "treatment" || name == "diagnosis") {
                dispatch(actions.getAllBodySystems());
            }
            dispatch(actions.setHeadersData(getDefaultHeaders()));
            dispatch(actions.loadRecords(true, queryParams));
        }

        useEffect(() => {
            window.onpopstate = () => {
                getRecordsData();
            };
        }, []);

        useEffect(() => {
            // let mounted = true;
            getRecordsData();
            //  return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, []);

        useEffect(() => {
            successMessage ? setTimeout(() => {
                dispatch(actions.loadRecords(true, queryParams));
            }, 1000) : null
        }, [successMessage]);

        const closeDialog = () => {
            setModalOpen(false);
        }

        let displayName;
        if (name == 'users') {
            displayName = "user";
        } else if (name == 'vettedclaimsUsers') {
            displayName = "VettedClaims User";
        } else if (name == 'practiceUsers') {
            displayName = "Practice User";
        } else {
            displayName = name;
        }

        if (error && !error.syncError) {
            return <Grid container>
                <Grid item xs={12} className={classes.error}>
                    <Error errorMessage={<FormattedMessage {...messages.error} />} />
                </Grid>
            </Grid>
        }

        if (fullView) {
            return children;
        }

        const handlePageData = (data) => {
            dispatch(actions.setHeadersData(data));
            dispatch(actions.loadRecords(true, queryParams));
        }

        const openFile = (e) => {
            remotes(name).loadPoliciesDoc({ "treatment_id": e['treatment_id'] && e['treatment_id'] || e['id'], body_system: e['body_system_index'] }).then(res => {
                if (res["policy_doc"]) {
                    const bytes = CryptoJS.AES.decrypt(res["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
                    const secret = bytes.toString(CryptoJS.enc.Utf8);
                    const schema = JSON.parse(secret);
                    if (name == "diagnosis") {
                        const policies = schema.policies.filter((diag) => diag.diagnosis && diag.diagnosis["label-name"] == e["name"])
                        if (Object.keys(policies).length != 0) {
                            setModalOpen(true);
                        } else {
                            setModalOpen(false);
                        }
                        setDialogText(policies);
                        setDialogTitle(`<Typography component="span" style='font-size: 1rem'> <span style='font-family:"MyriadPro-Bold"'>Treatment :</span> ${e.treatment_name}</Typography><br>
                                        <Typography component="span" style='font-size: 1rem'> <span style='font-family:"MyriadPro-Bold"'>Diagnosis :</span> ${e.name} </Typography>`);

                    } else {
                        setModalOpen(true);
                        setDialogText(schema);
                        setDialogTitle(`<Typography component="span" style='font-size: 1rem'> <span style='font-family:"MyriadPro-Bold"'>Treatment :</span> ${e.name}</Typography>`);
                    }
                }
                // window.open(res.publicUrl, '_blank')
            }).catch(error => Promise.reject(error))
        }

        const handleFilter = (e) => {
            setTreatmentRecord(e.target.value)
            if (bodySystemValue == "all" && e.target.value == "all") {
                handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: false, search: false, sort: false, page: 1 }));
            }
            else {
                handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify({ body_system: bodySystemValue, treatment: e.target.value }), search: false, sort: false, page: 1 }));
            }
        }

        const handleFilterBodySystem = (event) => {
            dispatch(actions.getTreatmentsName(event.target.value));
            setTreatmentRecord("all");
            setBodySystemValue(event.target.value);
            if (event.target.value == "all") {
                handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: false, search: false, sort: false, page: 1 }));
            } else {
                handlePageData(Object.assign({}, { ...headers }, { offset: 0, limit: 25, filter: JSON.stringify({ body_system: event.target.value, treatment: 'all' }), search: false, sort: false, page: 1 }));
            }

        }

        const authorize = (row) => {
            const status = row.staff_id ? "revoke" : "authorize"
            dispatch(actions.setAuthorizationStatus(row.id, status));
        }

        const bodySystemContainer = <div className={classes.selectField} >
            <FormControl variant="standard" sx={{ m: 0, width: 200 }} className={classes.formControl}>
                <TextField
                    select
                    label=" "
                    SelectProps={{
                        MenuProps: {
                            classes: { paper: classes.menuPaper }
                        }
                    }}
                    size="small"
                    id="filled-hidden-label-small"
                    variant="outlined"
                    value={bodySystemValue}
                    margin="normal"
                    inputProps={{ className: classes.fieldColor }}
                    InputLabelProps={{ shrink: false }}
                    name={"name"}
                    onChange={handleFilterBodySystem}
                    classes={{
                        select: classes.select
                    }}
                    className={classes.fieldColor}
                >
                    {[{ name: 'Body System: All', id: "all" }, ...bodySystemNames].map((val, index) => (
                        <MenuItem
                            key={index}
                            label={val.name}
                            value={val.id}
                            classes={classes.menuOption}
                            style={{ whiteSpace: 'normal' }}
                        >
                            {val.name}
                        </MenuItem>
                    ))}
                </TextField>
            </FormControl>
        </div>

        const TreatmentDropDownContainer = <div className={classes.selectField} >
            <FormControl variant="standard" sx={{ m: 0, width: 200 }} className={classes.formControl}>
                <TextField
                    select
                    label=" "
                    SelectProps={{
                        MenuProps: {
                            classes: { paper: classes.menuPaper }
                        }
                    }}
                    size="small"
                    id="filled-hidden-label-small"
                    variant="outlined"
                    value={treatmentRecord}
                    margin="normal"
                    inputProps={{ className: classes.fieldColor }}
                    InputLabelProps={{ shrink: false }}
                    name={"name"}
                    onChange={handleFilter}
                    classes={{
                        select: classes.select
                    }}
                    className={classes.fieldColor}
                >
                    {(([{ name: 'Treatment: All', id: 'all' }, ...treatmentNames && treatmentNames.length != 0 && treatmentNames.filter((ele, ind) => ind === treatmentNames.findIndex(elem => elem['name'] === ele['name'])).sort((a, b) => a['name'].localeCompare(b['name'])) || treatments] || []).map((field, index) => (
                        <MenuItem
                            style={{ whiteSpace: 'normal' }}
                            key={index}
                            label={field['name']}
                            value={field['id']}
                        >
                            {field['name']}
                        </MenuItem>
                    )))}
                </TextField>
            </FormControl>
        </div>

        return <Grid container name={"recordPage"}>
            {showText && !(pathname.indexOf('poc3') > -1) && <Grid>
                <p style={{ margin: '0px' }}>{showText.label}</p>
            </Grid> || extraSpace && <p style={{ margin: '0px' }}>&nbsp;</p> || <Grid className={classes.noText}><p style={{ margin: '0px' }}>&nbsp;</p></Grid>}
            {name == "diagnosis" ? <Grid item xs={12}>
                <Grid container >
                    <Grid item sm={8} xs={12} className={classes.addBtn}>
                        <Grid container>
                            {bodySystemContainer}
                            {TreatmentDropDownContainer}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid> : null}
            {name == 'rfaForm' && pathname.indexOf('poc3') > -1 ?
                <Grid item xs={12}>
                    {children}
                </Grid> :
                <Grid item xs={12}>
                    {name == 'treatment' || create ? <Grid container >
                        <Grid item xs={12} className={classes.addBtn}>
                            {name == 'treatment' &&
                                <Grid container >
                                    {bodySystemContainer}
                                </Grid>}
                            <Grid container direction="row" justifyContent={sm ? "flex-end" : "flex-start"} alignSelf='flex-end'
                                className={classes.Btn}>
                                {process.env.ENV === 'production' && name == 'treatment' && user.email == 'siva@miotiv.com' ?
                                    <Grid className={classes.btn_container} style={sm1 && !showText ? { marginTop: '0px', } : sm1 ? { marginTop: '24px' } : { alignSelf: 'center', textAlign: "end", marginRight: '10px' }} >
                                        <AlertDialog
                                            description={syncConfirmation}
                                            onConfirm={() => {
                                                setsyncloading(true);
                                                dispatch(actions.sync(() => setsyncloading(false)))
                                            }}
                                            onConfirmPopUpClose={true}
                                            btnLabel1='SYNC'
                                            btnLabel2='CANCEL' >
                                            {(open) =>
                                                <Button
                                                    type="button"
                                                    variant="contained"
                                                    color="primary"
                                                    style={syncloading ? { width: '128px' } : null}
                                                    className={classes.create}
                                                    onClick={open}>
                                                    {syncloading ? <ButtonSpinner /> : `Sync Policy Docs`}
                                                </Button>}
                                        </AlertDialog>
                                    </Grid> : null}
                                {create && section ?
                                    <Grid className={classes.createGrid}>{null}
                                    </Grid> :
                                    create ? <Grid
                                        style={sm1 && !showText ? { marginTop: '0px', } : sm1 ? { marginTop: '24px', } : {}} className={classes.createDiv}>

                                        <Link to={{ pathname: `${path}/create`, state: { ...location.state } }} className={classes.link}>
                                            <Button
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className={classes.create} >
                                                {displayName == 'rfaForm' ? `Create New` : `Add New ${name == "treatment" ? "Policy Doc" : displayName}`}
                                            </Button>
                                        </Link>
                                    </Grid> : null}
                            </Grid>
                        </Grid>
                    </Grid> : null}

                    <Grid container name={"recordPageTable"} direction={!sm ? "column" : "row"} wrap={!sm ? "nowrap" : "wrap"}>
                        <Grid item xs={12} md={activeChildren ? 6 : 12} className={classes.tableData}>
                            {section ? section.map((tab, index) =>
                                <div key={index} className={classes.section}>
                                    <Typography component="h1" variant="h5">{tab.name}</Typography>
                                    <Grid className={classes.tableSection}>
                                        <TableWrapper
                                            schemaId={tab.schemaId}
                                            records={loading ? records : records}
                                            columns={tab.columns()}
                                            children={activeChildren}// eslint-disable-line
                                            path={path}
                                            name={name}
                                            history={history}
                                            locationState={location.state}
                                            view={view}
                                            sm={sm}
                                            metaData={metaData}
                                            pageLimit={[10, 25, 100]}
                                            totalPageCount={totalPageCount}
                                            onChangeData={handlePageData}
                                            headersData={headers}
                                            loading={loading}
                                            filterNeed={true}
                                        />
                                    </Grid>
                                </div>) :
                                <div className={classes.table}>
                                    <TableWrapper
                                        columns={name == "treatment" ? user.email == "siva@miotiv.com" ? columns : columns.filter((e) => e.value != "status") : (typeof columns === 'function') ? columns(user).columns : columns}
                                        records={loading ? records.length == 0 && RecordsData || records : records}
                                        children={activeChildren}// eslint-disable-line
                                        path={path}
                                        name={name}
                                        history={history}
                                        locationState={location.state}
                                        view={view}
                                        metaData={metaData}
                                        sm={sm}
                                        totalPageCount={totalPageCount}
                                        onChangeData={handlePageData}
                                        headersData={headers}
                                        loading={loading}
                                        openFile={openFile}
                                        filterNeed={true}
                                        action={actions}
                                        authorize={authorize}
                                    />
                                </div>}
                        </Grid>
                        {showModal ? <Grid item xs={12} md={6}>
                            <Dialog
                                open={showModal}
                                //   maxWidth={"md"}
                                sx={{ '& .MuiDialog-paper': { width: 850, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
                                maxWidth="md"
                                scroll={"body"}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description">
                                <DialogTitle>
                                    <Grid container className={classes.header} justify="space-between">
                                        <Grid item xs={10}>
                                            <span dangerouslySetInnerHTML={{ __html: dialogTitle }} />
                                        </Grid>
                                        <Grid item xs={2} style={{ textAlign: 'end' }}>
                                            {<CloseIcon onClick={closeDialog} className={classes.closeIcon} />}
                                        </Grid>
                                    </Grid>
                                </DialogTitle>
                                <DialogContent dividers>
                                    <JSONPretty id="json-pretty" data={dialogText} />
                                </DialogContent >
                            </Dialog>
                        </Grid> : null}
                        {/* </Box> */}
                        {activeChildren ?
                            <Grid item xs={12} md={6} style={section && { marginTop: '32px' } || {}}>
                                <div className="children">
                                    {children}
                                </div>
                            </Grid> : null}
                        <Snackbar show={errorMessage || successMessage ? true : false} text={errorMessage || successMessage} severity={errorMessage ? 'error' : successMessage ? 'success' : ''} handleClose={() => dispatch(actions.loadRecordsCacheHit())} />
                    </Grid>
                </Grid>}
        </Grid>
    }


    const mapStateToProps = createStructuredSelector({
        headers: selectHeaders(),
        loading: selectLoading(),
        records: selectRecords(),
        metaData: selectRecordsMetaData(),
        error: selectError(),
        totalPageCount: selectTotalPageCount(),
        filterObjection: selectFilter(),
        treatments: selectTreatments(),
        user: selectUser(),
        success: selectSuccess(),
        bodySystemNames: selectbodySystemNames(),
        treatmentNames: selectTreatmentsNames()
    });


    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(RecordsPage);

}