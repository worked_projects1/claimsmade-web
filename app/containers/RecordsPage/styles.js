import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
    const theme = createTheme();
    return ({
        create: {
            fontWeight: 'bold',
            borderRadius: '3px',
            textTransform: 'capitalize',
            fontFamily: 'MyriadPro-Regular',
            padding: '6px 20px',
            height: '35px',
            lineHeight: '35px',
            // marginRight: theme.spacing(0.5),
            ['@media (max-width: 480px)']: {
                width: '100% !important',
            },
            [theme.breakpoints.between('sm', 'md')]: {
                width: '175px',
                lineHeight: '17px'
            },
            paddingTop: '8px'
        },
        btn_container: {
            ['@media (max-width: 480px)']: {
                width: '100%'
            },
        },
        menuPaper: {
            width: '400px',
            ['@media (max-width: 480px)']: {
                width: '100%',
                marginLeft: '0px',
            },
            maxHeight: '300px',
            marginLeft: '99px',
            whiteSpace: 'nowrap'
        },
        table: {
            ['@media (max-width: 480px)']: {
                width: '100%',
                // marginLeft: '0px',
            },
            boxShadow: '0px 0px 8px 1px lightgrey',
            borderRadius: '8px',
            paddingTop: '8px',
            paddingBottom: '8px',
            marginTop: '24px',
            overflowX: 'auto',
            overflowY: 'auto'
        },
        tableSection: {
            boxShadow: '0px 0px 8px 1px lightgrey',
            borderRadius: '8px',
            paddingTop: '8px',
            marginTop: '20px',
            marginBottom: '40px',
            overflow: 'auto'
        },
        filter: {
            padding: '10px',
            border: '2px solid  #cccdd3',
            borderRadius: '28px',
            WebkitAppearance: 'none',
            paddingLeft: '20px',
            paddingRight: '40px',
            fontFamily: 'MyriadPro-Bold',
            outline: 'none',
            width: '239px',
            fontSize: '0.875rem',
            position: 'relative',
            backgroundImage: `url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUwcHgiDQoJIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MCA1MCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8ZyBpZD0iTGF5ZXJfMSIgZGlzcGxheT0ibm9uZSI+DQoJDQoJCTxwYXRoIGRpc3BsYXk9ImlubGluZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJTTQ1LjM0MywyNy43MjJ2LTUuNDQzbC00LjExMy0wLjg4NWMtMC40MjQtMS45MTktMS4xNzktMy43MTItMi4yMDMtNS4zMThsMi4yODMtMy41MzVsLTMuODUtMy44NDlsLTMuNTMzLDIuMjgyDQoJCWMtMS42MDctMS4wMjUtMy40LTEuNzgxLTUuMzItMi4yMDZsLTAuODg1LTQuMTA5aC01LjQ0M2wtMC44ODUsNC4xMDljLTEuOTE5LDAuNDI1LTMuNzEzLDEuMTgxLTUuMzIsMi4yMDZMMTIuNTQsOC42OTENCgkJTDguNjkxLDEyLjU0bDIuMjgyLDMuNTM1Yy0xLjAyMywxLjYwNi0xLjc3OSwzLjM5OS0yLjIwMyw1LjMxOGwtNC4xMTMsMC44ODV2NS40NDNsNC4xMTYsMC44ODYNCgkJYzAuNDI1LDEuOTE3LDEuMTgsMy43MDgsMi4yMDMsNS4zMTNMOC42OTEsMzcuNDZsMy44NDksMy44NWwzLjU0MS0yLjI4N2MxLjYwNCwxLjAyMiwzLjM5NSwxLjc3Nyw1LjMxLDIuMjAxbDAuODg3LDQuMTE5aDUuNDQzDQoJCWwwLjg4Ny00LjExOWMxLjkxNi0wLjQyNCwzLjcwNi0xLjE3OSw1LjMxMS0yLjIwMWwzLjU0MSwyLjI4N2wzLjg1LTMuODVsLTIuMjg2LTMuNTM5YzEuMDIzLTEuNjA1LDEuNzc5LTMuMzk2LDIuMjA0LTUuMzEzDQoJCUw0NS4zNDMsMjcuNzIyeiIvPg0KPC9nPg0KPGcgaWQ9IkxheWVyXzIiIGRpc3BsYXk9Im5vbmUiPg0KCQ0KCQk8Y2lyY2xlIGRpc3BsYXk9ImlubGluZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBjeD0iMjUiIGN5PSIyNSIgcj0iMTkuNSIvPg0KCTx0ZXh0IHRyYW5zZm9ybT0ibWF0cml4KDEgMCAwIDEgMTkuMTkxOSAzMy4zOTc1KSIgZGlzcGxheT0iaW5saW5lIiBmb250LWZhbWlseT0iJ015cmlhZFByby1SZWd1bGFyJyIgZm9udC1zaXplPSIyOC42MTA3Ij4/PC90ZXh0Pg0KPC9nPg0KPGcgaWQ9IkxheWVyXzMiIGRpc3BsYXk9Im5vbmUiPg0KCTxwb2x5Z29uIGRpc3BsYXk9ImlubGluZSIgcG9pbnRzPSIyNSwzNiAyNSwzNiAzLDE0IDQ3LDE0IAkiLz4NCjwvZz4NCjxnIGlkPSJMYXllcl80Ij4NCgk8Zz4NCgkJPHBvbHlsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzZCNkM3MiIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50cz0iNDcuOTgxLDEzLjE1NiAyNC4yOTQsMzYuODQ0IDI1LDM2LjEzOCANCgkJCTIuMDE5LDEzLjE1NiAJCSIvPg0KCTwvZz4NCgkNCgkJPHBhdGggZGlzcGxheT0ibm9uZSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNkI2QzcyIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJTTQxLjkxOSw3LjU1bC0zMy45NCwzNC45TDQxLjkxOSw3LjU1eiIvPg0KCQ0KCQk8cGF0aCBkaXNwbGF5PSJub25lIiBmaWxsPSJub25lIiBzdHJva2U9IiM2QjZDNzIiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIGQ9Ig0KCQlNNy40NTYsNy41NWwzNC40NjMsMzQuOUw3LjQ1Niw3LjU1eiIvPg0KPC9nPg0KPGcgaWQ9IkxheWVyXzUiIGRpc3BsYXk9Im5vbmUiPg0KCTxnIGRpc3BsYXk9ImlubGluZSI+DQoJCTxwYXRoIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzFCNzVCMSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgZD0iDQoJCQlNMjYuODg0LDEyLjgyOWw4LjEzMS04LjEzYzIuOTMtMi45Myw3LjY4LTIuOTMsMTAuNjA5LDBjMS40NiwxLjQ2LDIuMiwzLjM4LDIuMiw1LjNzLTAuNzQsMy44NC0yLjIsNS4zMWwtMTIuOTg5LDEyLjk5DQoJCQljLTIuODgxLDIuODgtNy41MDEsMi45MzEtMTAuNDQxLDAuMTUiLz4NCgkJPHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMUI3NUIxIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSINCgkJCU0yMy4xMjQsMzcuMjA3bC04LjE1Miw4LjEwOGMtMi45MzgsMi45MjItNy42ODgsMi45MDktMTAuNjA5LTAuMDI4Yy0xLjQ1Ni0xLjQ2NC0yLjE5MS0zLjM4Ni0yLjE4Ni01LjMwNg0KCQkJYzAuMDA1LTEuOTIsMC43NS0zLjgzOCwyLjIxNS01LjMwNWwxMy4wMjQtMTIuOTU1YzIuODg5LTIuODcyLDcuNTA4LTIuOTExLDEwLjQ0MS0wLjEyMyIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJMYXllcl82IiBkaXNwbGF5PSJub25lIj4NCgk8ZyBkaXNwbGF5PSJpbmxpbmUiPg0KCQk8cGF0aCBmaWxsPSJub25lIiBzdHJva2U9IiNGRkZGRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBkPSJNMzksNDEuNzkyYzAsMi4yMDktMS43OTEsNC00LDRIMTUNCgkJCWMtMi4yMDksMC00LTEuNzkxLTQtNHYtMThjMC0yLjIwOSwxLjc5MS00LDQtNGgyMGMyLjIwOSwwLDQsMS43OTEsNCw0VjQxLjc5MnoiLz4NCgkJPHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjRkZGRkZGIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgZD0iTTE1LjM3NSwxOS44NzZ2LTYuNzE1DQoJCQljMC00Ljk0NCw0LjAwOC04Ljk1Myw4Ljk1NC04Ljk1M2gxLjM0M2M0Ljk0NCwwLDguOTUzLDQuMDA5LDguOTUzLDguOTUzdjYuNzE1Ii8+DQoJCTxjaXJjbGUgZmlsbD0iI0ZGRkZGRiIgY3g9IjI1IiBjeT0iMzAuNzI5IiByPSIzLjA2MyIvPg0KCQkNCgkJCTxsaW5lIGZpbGw9Im5vbmUiIHN0cm9rZT0iI0ZGRkZGRiIgc3Ryb2tlLXdpZHRoPSIzIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgeDE9IjI1IiB5MT0iMzAuNzkyIiB4Mj0iMjUiIHkyPSIzOC4xNjciLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==) !important`,
            backgroundRepeat: 'no-repeat !important',
            backgroundSize: '16px !important',
            backgroundPosition: '95% !important',
            backgroundColor: 'transparent !important'
        },
        section: {
            marginTop: '20px'
        },
        error: {
            marginTop: '12px'
        },
        createGrid: {
            display: 'flex'
        },
        createDiv: {
            textAlign: 'end',
            // display:'flex'
            // [theme.breakpoints.down('sm')]: {
            // marginTop: '24px',
            // },  
            ['@media (max-width: 480px)']: {
                width: '100% !important',
            },
        },
        dropdownGrid: {
            marginRight: '14px'
        },
        spinner: {
            position: 'fixed',
            zIndex: '1400',
            margin: 0,
            height: '50vh',
            top: '50%',
            left: '8%'
        },
        addBtn: {
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            // marginTop: '35px',
            ['@media (min-width: 400px) and (max-width:479px)']: {
                width: '100%',
                // paddingLeft:'20px'
            },
            ['@media (min-width: 480px) and (max-width:600px)']: {
                flexDirection: "row !important",
            },
            [theme.breakpoints.down('sm')]: {
                // marginTop: '12px',
                flexDirection: 'column',
            }
        },
        button: {
            fontWeight: 'bold',
            borderRadius: '28px',
            textTransform: 'capitalize',
            fontFamily: 'MyriadPro-Regular'
        },
        fieldColor: {
            margin: '0px',
            '& .MuiOutlinedInput-root': {
                borderColor: '#1d1e1c !important',
                borderRadius: '50px',
                ['@media (min-width: 300px) and (max-width:500px)']: {
                    width: '100%',
                },
            },
            '& .MuiSelect-select': {
                padding: '8px 14px',
                ['@media (max-width: 480px)']: {
                    textAlign: 'center'
                }
            }

        },
        tableData: {
            ['@media (min-width: 400px) and (max-width:500px)']: {
                // paddingLeft:'20px'
            },
        },
        Btn: {
            ['@media (min-width: 480px) and (max-width:600px)']: {
                justifyContent: "flex-end !important",
            },
            //  ['@media (max-width: 480px)']: {
            //     width: '100%',
            //  },   
        },
        selectField: {
            marginRight: '15px',
            marginTop: '24px',
            ['@media (max-width: 480px)']: {
                width: '100%',
                marginRight: '0px',

            },
            '@global': {
                '.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
                    border: '1px solid #1d1e1c',
                },
                '.MuiOutlinedInput-notchedOutline': {
                    border: '1px solid #1d1e1c',
                }
            }
        },
        dialog: {
            border: 'none',
            padding: '25px',
            minWidth: '75%',
            outline: 'none',
            width: '75%',
        },
        closeIcon: {
            cursor: 'pointer'
        },
        selectFilterField: {
            flexDirection: 'row',
            ['@media (max-width: 480px)']: {
                flexDirection: 'column',
            },
        },
        formControl: {
            margin: '0px',
            ['@media (max-width: 480px)']: {
                width: '100%',
            },
        },
        link: {
            width: '100%'
        },
        noText: {
            display: 'none',
            ['@media (max-width: 480px)']: {
                display: 'none',
            },
        },
        search: {
            width: '33px',
            height: '31px',
        }
    })
});

export default useStyles;