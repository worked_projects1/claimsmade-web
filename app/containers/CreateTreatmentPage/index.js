
/**
 *
 * Create Treatment Page
 *
 */


import React, { useState, useEffect, useRef, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import CreateTreatmentForm from 'components/CreateTreatmentForm';
import moment from 'moment';
import { formValueSelector, change } from 'redux-form';
import lodash from 'lodash';
import {
    selectUser,
} from 'blocks/session/selectors';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView, submitButtonView) {// eslint-disable-line

    const { selectRecordsMetaData, selectRecords } = selectors;// eslint-disable-line

    /**
     * @param {object} record 
     * @param {function} dispatch 
     * @param {object} param2 
     */
    function handleCreate(user, entry, record, dispatch, { form }) {
        if (entry == "manual") {
            var policies = record.policy == "yes" && record.policies.map((e) => e != undefined).includes(true) ? (record.policies || []).map((policy) => {
                return Object.assign({}, {
                    "source": policy.source,
                    "diagnosis": {
                        "index-name": policy.diagnosis.trim().replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                        "label-name": policy.diagnosis.trim()
                    },
                    "recommendation": policy.recommendation != "askaquestion" ? policy.recommendation : "1",
                    "questions": policy.recommendation == "askaquestion" && policy.questions.length != 0 && policy.questions.find(e => true) != undefined ? // eslint-disable-line
                        policy.questions.map((question, i) =>
                            Object.assign({}, { "#": (i + 1).toString(), "question": question.questionsText, "option-list": [{ "option": "yes", "action": question.yes == "nextQuestion" ? (i + 1 + 1).toString() : question.yes }, { "option": "no", "action": question.no == "nextQuestion" ? (i + 1 + 1).toString() : question.no }] }))
                        : [],
                    ...(policy["mtus-text"] != undefined) && { "mtus-text": policy["mtus-text"] },
                    ...(policy["odg-text"] != undefined) && { "odg-text": policy["odg-text"] },
                }) || ""
            }) : [];

            const jsonObj = {
                "treatment": {
                    "index-name": record['treatment'].trim().replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                    "label-name": record['treatment'].trim(),
                    "treatment-type": record['treatment-type']
                },
                "body-system": {
                    "index-name": record['body-system'].replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                    "label-name": record['body-system']
                },
                "version": {
                    "mtus": record['mtus'] ? (!isNaN(Date.parse(record['mtus']))) && moment(record['mtus']).format('MM/DD/YY') || record['mtus'] : '',
                    "odg": record['odg'] ? (!isNaN(Date.parse(record['odg']))) && moment(record['odg']).format('MM/DD/YY') || record['odg'] : '',
                    "policy-doc": moment().format('MM/DD/YY'),
                    "created-by": user.name,
                    "comments": record['comments'] ? record['comments'] : ''
                },
                "policies": policies.filter(e => e != ""),
                "references": {
                    "mtus-link": record["mtus-link"] ? record["mtus-link"] : "",
                    "odg-link": record["odg-link"] ? record["odg-link"] : ""
                }
            }
            if (user.email == "siva@miotiv.com") {
                dispatch(actions.createRecord(Object.assign({}, { doc: jsonObj, entry: entry, user: false }), form))
            } else {
                dispatch(actions.createRecord(Object.assign({}, { doc: jsonObj, entry: entry, user: true }), form))
            }
        } else {
            if (user.email == "siva@miotiv.com") {
                dispatch(actions.createRecord({ doc: record.doc, entry: entry, user: false }, form))
            } else {
                dispatch(actions.createRecord({ doc: record.doc, entry: entry, user: true }, form))
            }
        }
    }

    /**
     * @param {object} props 
     */
    function CreateTreatmentPage(props) {
        const [loading, setLoading] = useState(true);
        const { location = {}, dispatch, records, metaData = {}, formRecord = {}, queryParams, formValues, updateField, user = {} } = props;// eslint-disable-line

        const fieldsMetData = location && location.state && location.state.metaData && lodash.merge(metaData, location.state.metaData) || metaData;

        const fields = (typeof columns === 'function' ? columns(Object.assign({}, { process: "Create", updateField: updateField })).columns : columns).filter(_ => formValues.form.entry == undefined ? _.value == "entry" : user.email == "siva@miotiv.com" ? (formValues.form.entry == "manual" ? _.value == "entry" || _.value != "doc" : formValues.form.entry == "fileUpload" ? _.value == "entry" || _.value == "doc" : _.editRecord && !_.edit) : (_.value != "entry" && _.value != "doc"));
        const reInitialize = false;
        const componentMounted = useRef(true);

        useEffect(() => {
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, []);

        return (<div>
            {decoratorView && React.createElement(decoratorView, props)}
            {!loading ? <CreateTreatmentForm
                name={name}
                path={path}
                form={`createTreatmentForm`}
                metaData={fieldsMetData}
                fields={fields}
                enableReinitialize={reInitialize}
                keepDirtyOnReinitialize={reInitialize}
                onSubmit={handleCreate.bind(this, user, formValues.form.entry)}
                locationState={location.state}
                submitButtonView={submitButtonView}
                submitButtonName={"Create"}
                formValues={formValues}
                user={user}
                updateField={updateField}
            /> : 'Loading...'}
        </div>
        )
    }

    CreateTreatmentPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        pathData: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        metaData: selectRecordsMetaData(),
        user: selectUser(),
        formValues: (state, props) => {// eslint-disable-line
            return {
                Policies: formValueSelector('createTreatmentForm')(state, 'policies'),
                form: (state.form.createTreatmentForm && state.form.createTreatmentForm.values) || false,
            }
        },
        records: selectRecords()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch,
            updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(CreateTreatmentPage);
}
