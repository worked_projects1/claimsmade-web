/**
 *
 * App Page
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import {
  selectLoggedIn,
  selectUser,
  selectError,
  selectSuccess,
  selectLocation,
  selectActiveSession,
  selectSessionExpand
} from 'blocks/session/selectors';

import { sessionLogin, logOut, clearCache, changePassword, sessionTimeout, sessionExpand, sessionClearTimeout, updateSignature, twoFactorAuthentication } from 'blocks/session/actions';

import {
  Grid,
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItemText,
  Toolbar,
  Typography
} from '@mui/material';

import MenuIcon from '@mui/icons-material/Menu';
import SVG from 'react-inlinesvg';
import GlobalStyle from '../../global-styles';
import { StyledListItem, useStyles } from './styles';
import '../../../src/main.css';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import ModalForm from 'components/ModalRecordForm';
import { Visibility } from 'utils/tools';
import Icons from 'components/Icons';
import schema from 'routes/schema';
import moment from 'moment';
import Snackbar from 'components/Snackbar';
import { getOffset } from 'utils/tools';
import SettingsForm from './components/SettingsForm';
const userSettingsColumns = schema().userSettings().section;

const loginFormColumns = schema().login().columns;
const changePasswordColumns = schema().changePassword().columns;
const signatureCol = schema

/**
 * @param {object} props 
 */
function App(props) {

  const { loggedIn, user = {}, container, pages, dispatch, error, success, location = {}, schema, activeSession, expand } = props;// eslint-disable-line
  // const devUsers = ['siva@ospitek.com', 'senthilei27@gmail.com'];
  const pathData = location && location.pathname && pages && pages.length > 0 && pages.find(_ => _.data && _.data.path && location.pathname.includes(_.data.path)) || false;
  const { email, lastPasswordChangeDate = moment() } = user;
  const { state = {} } = location;
  const activePath = location.pathname;
  const getNavWidth = () => {
    if (expand) {
      if (user.role == 'physician' || user.role == 'staff') {
        return 158;
      }
      else {
        return 205;
      }
    }
    else {
      return 60;
    }
  };

  const mobileIconStyle = () => {
    if (user.role == 'physician' || user.role == 'staff') {
      return 'translate(143px, 83px)'
    }
    else {
      return 'translate(190px, 83px)'
    }
  }
  //const classes = useStyles(expand ? 205 : 60)();
  const classes = useStyles(getNavWidth(), mobileIconStyle())();
  const theme = useTheme();
  const md = useMediaQuery(theme.breakpoints.up('md'));
  const [mobileOpen, setMobileOpen] = useState(false);
  const role = user && user.role || false;
  const componentMounted = useRef(true);
  const errorMessage = error && (error.practice || error.changePassword || error.twoFactorAuthentication || error.sessionTimeout || error.signature);
  const successMessage = success && (success.practice || success.changePassword || success.twoFactorAuthentication || success.signature);
  const [modalRecord, setModalRecord] = useState(false);
  const [settingsFormOpen, setSettingsForm] = useState(false);
  const [tabValue, setTabValue] = useState(0);
  const [position, setPosition] = useState(getOffset('VettedClaims-header-toolbar1'));// eslint-disable-line
  // const navbarWidth = expand ? 140 : 60;// eslint-disable-line
  // const [width, setWidth] = useState(window.innerWidth);

  const handleChangeTab = (value) => {
    setTabValue(value);
  };

  const signatureColumns = signatureCol().signature(user).columns;

  const handleExpandToggle = () => {
    dispatch(sessionExpand(!expand));
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleLogout = () => {
    dispatch(logOut())
  };

  const handleSessionLogin = (loginData, dispatch, { form }) => {
    dispatch(sessionLogin(loginData.identifier, loginData.secret, form));
  };

  const handleChangePassword = (data, dispatch, { form }) => {
    dispatch(changePassword(data, form));
    setModalRecord(false);
  };

  const handleSignature = (data, dispatch, { form }) => {
    dispatch(updateSignature(data, form))
  }

  const handleChangeSettings = (data, dispatch, { form }) => {
    if (tabValue === 0) {
      dispatch(changePassword(data, form));
    } else if (tabValue === 1) {
      dispatch(twoFactorAuthentication(data, form));
    }
    setSettingsForm(false);
  };

  useEffect(() => {
    // let mounted = true;
    Visibility({ callback: () => dispatch(sessionTimeout(dispatch)), clearSessionTimeout: () => dispatch(sessionClearTimeout()) });
    window.addEventListener('resize', () => {
      setPosition(getOffset('VettedClaims-header-toolbar1'));
    });
    //  return () => mounted = false;
    return () => { // This code runs when component is unmounted
      componentMounted.current = false; // (4) set it to false when we leave the page
    }
  }, []);

  const drawer = (
    <div>
      {!mobileOpen ? <IconButton
        onClick={handleExpandToggle}
        className={expand ? classes.mobileMenuIcon : classes.menuIcon}
      >
        <Icons type={expand ? "ChevronLeftIcon" : "ChevronRightIcon"} />
      </IconButton> : null}
      {(user && Object.keys(user).length > 0 && user.practiceDetails && user.practiceDetails.logoFile) ? <div className={classes.toolbar}>
        <img src={user.practiceDetails.logoFile || ''} alt="Logo" className={(expand || mobileOpen) ? classes.logo : classes.mobileLogo} />
      </div> : null}
      {!(expand || mobileOpen) ? <Grid>
        <Typography variant="h5" className={classes.title}>
          {/* {name || ''}  */}
        </Typography>
      </Grid> : null}
      <div className={classes.toolbar1}></div>
      <List>
        {pages.filter(p => (p.data && p.data.users && p.data.users.includes(user.email)) || (p.data && !p.data.users)).filter((e) => (e.data && e.data.admin && user["is_admin"]) || (e.data && !e.data.admin)).map((page, index) => {
          return <Link
            key={index}
            to={{ pathname: page.data && page.data.path || '/', state: { title: page.data.title } }}
            className={classes.link}>
            {page.data.separator ? <div className={classes.separator} /> : null}
            <StyledListItem
              button
              key={(page.data && page.data.title) || ''}
              selected={(activePath === page.data.path) || (page.data.path != '/' && activePath.indexOf(page.data.path) > -1)}
              onClick={() => setMobileOpen(false)}>
              <Icons type={(page.data && page.data.icon) || ''} />
              {(expand || mobileOpen) && <ListItemText primary={(page.data && page.data.title && (page.data.title == 'Practice Profile' && 'Practice' || page.data.title)) || ''} /> || null}
            </StyledListItem>
          </Link>
        })}
      </List>
    </div>
  );

  return (
    <div className={classes.root} name='App Page'>
      <Helmet
        title={"VettedClaims"}
        meta={[
          { name: 'description', content: "APP Page" },
        ]}
      />
      {loggedIn && (!['/signin', '/signup', '/forgot'].includes(activePath) || process.env.ENV === 'production') ? (
        <div>
          <CssBaseline />
          <AppBar id="VettedClaims-header" position="fixed" className={classes.appBar}>
            <Toolbar className={classes.toolbar1} id="VettedClaims-header-toolbar1">
              <Grid container direction="row" alignItems="center" >
                <Link
                  to={{
                    pathname: `/${role && ['manager', 'superAdmin'].includes(role) ? 'practices' : 'claims' }`,
                    state: { title: role && ['manager', 'superAdmin'].includes(role) ? 'Practices' : 'Claims' }
                  }}
                >
                  <img src={require('images/home/Claims1.svg')} style={{ width: '200px', paddingTop: '5%', paddingBottom: '5%' }} id="appLogo" />
                </Link>
              </Grid>
              <Grid container justifyContent="flex-end">
                <Grid className={classes.settings}>
                  {user.practice_id != null && user.role != "staff" ? <ModalForm
                    initialValues={Object.assign({}, { signature: user.signature })}
                    title="Signature"
                    fields={signatureColumns}
                    form={`userSignatureForm`}
                    btnLabel="Update"
                    onSubmitClose
                    footerStyle={{ borderTop: '1px solid lightgrey', paddingTop: '25px' }}
                    className={classes.settingForm}
                    onSubmit={handleSignature}>
                    {(open) =>
                      <Typography noWrap style={{ marginRight: '10px' }} onClick={open}>
                        <SVG src={require('images/icons/Signature.svg')} className={classes.svg} style={{ width: '34px', marginTop: '-3px', height: 'auto' }} /> {md && <FormattedMessage {...messages.signature} />}
                      </Typography>}
                  </ModalForm> : null}
                  {['superAdmin', 'manager', 'operator'].includes(user.role) ? <Typography noWrap onClick={() => setModalRecord(true)} className={classes.settingIcon}>
                    <SVG src={require('images/icons/change.svg')} className={classes.svg} style={{ width: '34px', marginTop: '-3px', height: 'auto' }} /> {md && email}
                  </Typography> : null}
                  {['superAdmin', 'manager', 'operator'].includes(user.role) || modalRecord ? <ModalForm
                    title="Change Password"
                    fields={changePasswordColumns}
                    message={'Password must be changed every 90 days'}
                    form={`changePasswordForm`}
                    btnLabel="Update"
                    // onSubmitClose
                    onClose={() => setModalRecord(false)}
                    enableSubmitBtn
                    disableCancelBtn={moment().diff(lastPasswordChangeDate, 'days') >= 90 ? true : false}
                    show={moment().diff(lastPasswordChangeDate, 'days') >= 90 || modalRecord ? true : false}
                    className={classes.changePasswordForm}
                    onSubmit={handleChangePassword.bind(this)} /> : null}

                  {!['superAdmin', 'manager', 'operator'].includes(user.role) && settingsFormOpen ? <SettingsForm
                    initialValues={{ "two_factor_status": user.two_factor_status == null || !user.two_factor_status ? false : true }}
                    fields={userSettingsColumns}
                    form={`settingForm`}
                    record={user}
                    className={classes.settingForm}
                    show={settingsFormOpen}
                    enableSubmitBtn
                    onClose={() => setSettingsForm(false)}
                    onSubmit={handleChangeSettings.bind(this)}
                    tabValue={tabValue}
                    click={(value) => handleChangeTab(value)}
                  />

                    : null}
                  {!['superAdmin', 'manager', 'operator'].includes(user.role) ? <Typography style={{ marginRight: '10px' }} onClick={() => setSettingsForm(true)} className={classes.settingIcon}>
                    <SVG
                      src={require('images/icons/change.svg')}
                      className={classes.svg}
                      style={{
                        width: '34px',
                        marginTop: '-3px',
                        height: 'auto',
                      }}
                    />{' '}
                    {md && email}
                  </Typography> : null}


                  <Typography noWrap className={classes.logout} onClick={handleLogout}>
                    <SVG src={require('images/icons/logout.svg')} className={classes.svg} style={{ marginTop: '-1px' }} />
                    {md && <FormattedMessage {...messages.logout} />}
                  </Typography>
                </Grid>
              </Grid>
            </Toolbar>
            <Toolbar className={classes.toolbar2} id="VettedClaims-header-toolbar2">
              <IconButton
                aria-label="open drawer"
                edge="start"
                onClick={handleDrawerToggle}
                className={classes.menuButton}>
                <MenuIcon />
              </IconButton>
              <Grid container justifyContent="flex-start">
              <Typography component="h1" variant="h5" style={{ color: 'black', fontSize: "23px" }}>
                {pathData && pathData.data && pathData.data.title && (pathData.data.title == 'Forms' && 'Request for Authorization(RFA) Forms' || pathData.data.title == 'Claim Checker' && 'Check If Your Claim Is Likely To Get Approved' || pathData.data.title) || ''}
                </Typography>
                {location && state.id && state.caseRecord && state.title && state.caseRecord.state && !['CA', 'TN'].includes(state.caseRecord.state) && state.title === 'Cases' && (location.pathname.indexOf('form') > -1 || location.pathname.indexOf('signature') > -1) ? <Grid className={classes.beta}>
                  <Typography variant="subtitle1">
                    BETA
                  </Typography>
                </Grid> : null}
              </Grid>
            </Toolbar>
          </AppBar>
          <nav className={classes.drawer} aria-label="mailbox folders">
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Hidden mdUp implementation="css">
              <Drawer
                container={container}
                variant="temporary"
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={mobileOpen}
                onClose={handleDrawerToggle}
                classes={{
                  paper: classes.drawerPaperFixed,
                }}
                ModalProps={{
                  keepMounted: true, // Better open performance on mobile.
                }}>
                {drawer}
              </Drawer>
            </Hidden>
            <Hidden smDown implementation="css">
              <Drawer
                classes={{
                  paper: classes.drawerPaper,
                }}
                variant="permanent"
                open>
                {drawer}
              </Drawer>
            </Hidden>
          </nav>
        </div>
      ) : null}
      <main className={loggedIn ? classes.content : classes.login} id="children">
        {/* {loggedIn ? <div className={classes.toolbar} /> : null} */}
        {props.children}
      </main>
      <Snackbar show={errorMessage || successMessage ? true : false} text={errorMessage || successMessage} severity={errorMessage ? 'error' : successMessage ? 'success' : ''} handleClose={() => dispatch(clearCache())} />
      <GlobalStyle />
      {!activeSession && loggedIn && role && <ModalForm
        initialValues={{ identifier: user.email }}
        title="Login"
        fields={loginFormColumns}
        message={'Your session has expired. Please login to continue.'}
        form={`SessionLoginForm`}
        btnLabel="Login"
        onSubmitClose={true}
        enableSubmitBtn
        show
        disableCancelBtn
        className={classes.changePasswordForm}
        onSubmit={handleSessionLogin} /> || null}
    </div>
  );
}

App.propTypes = {
  activeSession: PropTypes.number,
  children: PropTypes.object,
  dispatch: PropTypes.func,
  error: PropTypes.object,
  expand: PropTypes.bool,
  location: PropTypes.object,
  loggedIn: PropTypes.bool,
  pages: PropTypes.array,
  success: PropTypes.object,
  user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  loggedIn: selectLoggedIn(),
  error: selectError(),
  success: selectSuccess(),
  user: selectUser(),
  location: selectLocation(),
  activeSession: selectActiveSession(),
  expand: selectSessionExpand()
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(App);