
import { makeStyles, withStyles } from '@mui/styles';
import ListItem from '@mui/material/ListItem';
import { createTheme } from '@mui/material/styles';

export const StyledListItem = withStyles({
  root: {
    "&$selected": {
      backgroundColor: "#f3ecec54"
    },
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: '#06060633',
    },
    "&.MuiListItem-root.Mui-selected": {
      // backgroundColor: "#dbd3d375"
      backgroundColor: "#f3ecec54"
    },
    "&.MuiListItem-button": {
      minHeight: '48px'
    },
    '@global': {
      ".MuiListItemText-root": {
        marginLeft: '5px',
        marginTop: '8px',
        "& span": {
          color: "#fff"
        }
      },

    }
  },
  selected: {}
})(ListItem);

export const useStyles = (drawerWidth, mobileIconStyle) => makeStyles(() => {// eslint-disable-line
  const theme = createTheme();
  return ({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
      [theme.breakpoints.down('md')]: {
        display: 'none ',
      },
      background: '#393a3cd',
      width: '100px'
    },
    appBar: {
      [theme.breakpoints.up('md')]: {
        width: `calc(100% - ${drawerWidth}px) `,
        marginLeft: `${drawerWidth}`,
      },
      boxShadow: 'none',
      background: '#fff',
      zIndex: '999',
      position: 'fixed',
      overflow: 'hidden'
    },
    menuButton: {
      marginRight: `${theme.spacing(2)}`,
      [theme.breakpoints.up('md')]: {
        display: 'none ',
      },
      color: '#3c89c9',
    },
    menuIcon: {
      position: "fixed",
      transform: "translate(45px, 18px)",
      backgroundColor: "#e8e8e8",
      width: "25px",
      height: "25px",
      '&:hover': {
        backgroundColor: '#e8e8e8',
      }
    },
    mobileMenuIcon: {
      position: "fixed",
      // transform: "translate(225px, 18px)",
      transform: mobileIconStyle,
      zIndex: 1,
      backgroundColor: "#e8e8e8",
      width: "25px",
      height: "25px",
      '&:hover': {
        backgroundColor: '#e8e8e8',
      }
    },
    // necessary for content to be below app bar
    toolbar: {
      ...theme.mixins.toolbar,
      textAlign: 'center'
    },
    drawerPaper: {
      width: drawerWidth,
      // background: '#6358eb',
      backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
      boxShadow: 'none',
      color: '#F1F1F1',
      overflow: 'hidden'
    },
    drawerPaperFixed: {
      width: 230,
      // background: '#6358eb',
      backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
      boxShadow: 'none',
      color: '#F1F1F1 ',
      overflow: 'hidden'
    },
    commondiv: {
      height: '45px'
    },
    content: {
      flexGrow: 1,
      padding: '152px 24px 24px 24px',
      ['@media (max-width: 480px)']: {
        padding: '143px 24px 24px 24px',
    },
    },
    login: {
      flexGrow: 1,
    },
    title: {
      color: '#F1F1F1',
      textAlign: 'center',
      marginTop: '40px',
    },
    link: {
      textDecoration: 'none',
      color: '#F1F1F1',
    },
    settings: {
      position: 'relative',
      color: '#A6A6A9',
      cursor: 'pointer',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    logout: {
      // marginLeft: '12px',
      // display: 'flex',
      // alignItems: 'center',
      overflow: 'inherit',
      cursor: 'pointer',
      marginRight: '4px'
    },
    separator: {
      height: '1px',
      background: 'gray',
      margin: '10px 5%',
      opacity: '0.6'
    },
    logo: {
      width: '89px',
      height: '89px',
      borderRadius: '100%',
      margin: '15px 0px',
      // marginTop: '10px',
      padding: '10px'
    },
    mobileLogo: {
      width: '40px',
      height: '40px',
      borderRadius: '100%',
      margin: '80px 0px 20px',
    },
    svg: {
      width: '25px',
      height: '25px'
    },
    changePasswordForm: {
      // display: 'contents'
    },
    claimsmadeLogo: {
      width: '80px',
      height: '78px',
      borderRadius: '100%',
      margin: '30px 0px',
      marginTop: '10px'
    },
    settingForm: {
      display: 'contents'
    },
    beta: {
      background: '#2ca01c',
      borderRadius: '16px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      paddingLeft: '12px',
      paddingRight: '12px',
      paddingTop: '3px',
      marginLeft: '7px',
      height: '20px',
      '& > h6': {
        fontSize: '12px',
        color: 'white',
        fontFamily: 'MyriadPro-Regular'
      }
    },
    logo1:
    {
      width: '180px',
      margin: '30px 0px',
      marginTop: '50px',

    },
    title1: {
      color: '#f1f1f1',
      textAlign: 'center'
    },
    toolbar1: {
      // ...theme.mixins.toolbar,
      textAlign: 'center',
    },
    toolbar2: {
      backgroundImage: `url(${require('images/icons/repeat.png')})`,
      backgroundRepeat: 'repeat-x'

    },
    signture: {
      marginRight: '12px',
      display: 'flex',
      alignItems: 'center',
      cursor: 'pointer',
    },
    settingIcon: {
      overflow: 'inherit',
      marginRight: '12px'
    }
  })
});

