import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        paddingLeft: '25px',
        paddingRight: '25px',
        paddingBottom: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '25%',
        '@global': {
            '.MuiTab-textColorPrimary.Mui-selected': {
              color: '#3c89c9'
            },
            '.MuiTabs-indicator': {
                backgroundColor: '#3c89c9'
              },
              '.MuiSwitch-colorPrimary.Mui-checked': {
                color: '#3c89c9'
              },
              '.MuiSwitch-colorPrimary.Mui-checked + .MuiSwitch-track':{
                backgroundColor: '#3c89c9'
              }
        }
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    footer: {
        // paddingTop: '15px',
        // borderTop: '1px solid lightgray',
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '4px',
        boxShadow: 'none',
        height: '35px',
        fontFamily: 'MyriadPro-Regular',
        // marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        //marginRight: '12px',
        color: '#fff',
        paddingTop: '8px'
    },
    title: {
        fontFamily: 'MyriadPro-Bold',
        fontSize: '22px',
        marginTop: '10px'
    },
    message: {
        fontFamily: 'MyriadPro-SemiBold',
        fontSize: '16px',
        paddingTop: '18px',
    },
    messageRegular: {
        fontSize: '16px',
        paddingTop: '18px',
    },
    closeIcon: {
        cursor: 'pointer',
        marginTop: '12px',
        marginLeft: '475px'
    },
    messageGrid: {
        paddingTop: '10px'
    },
    error: {
        marginTop: '10px'
    },
    note: {
        fontSize: '16px',
    },
   
}));


export default useStyles;
