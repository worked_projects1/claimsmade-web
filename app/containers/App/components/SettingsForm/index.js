/**
 *
 *   Setting Form
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from 'components/CreateRecordForm/utils';
import {
    Modal,
    Paper,
    Fade,
    Grid,
    Typography,
    Button,
} from '@mui/material';
import validate from 'utils/validation';
import ButtonSpinner from 'components/ButtonSpinner';
import Error from 'components/Error';
import AlertDialog from 'components/AlertDialog';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Styles from './styles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

/**
 * 
 * @param {object} props 
 * @returns 
 */
function SettingsForm(props) {
    const { handleSubmit, submitting, children, fields, metaData, className, style, onOpen, onClose, error, pristine, invalid, footerStyle, destroy, notes, disableContainer, record, tabValue, click, show } = props;// eslint-disable-line

    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const closeModal = () => {
        setModalOpen(false);
        if (onClose) onClose();
    };

    useEffect(() => {
        return () => destroy();
    }, []);

    const handleConfirmClose = () => {
        handleSubmit();
        if (!invalid && tab.onSubmitClose) closeModal();
    };

    const handleChangeTab = (event, newValue) => {// eslint-disable-line
        click(newValue);
    };

    const tab = fields[tabValue];

    const description = record.twofactor_status === true ? tab.disableMessage : tab.confirmMessage && typeof tab.confirmMessage === 'function' && tab.confirmMessage(record) || tab.confirmMessage;
    return (
        <Grid container={disableContainer ? false : true} className={className} style={style}>
            {children && children(() => setModalOpen(!showModal))}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={showModal ||show ||  tab.show(record) || false}
                className={classes.modal}
                onClose={closeModal}
                closeAfterTransition
                //BackdropComponent={Backdrop}
                // onRendered={onOpen}
                BackdropProps={{
                    timeout: 500,
                }}>
                <Fade in={showModal ||show  || tab.show(record) || false}>
                    <Paper className={classes.paper} style={!md ? { width: '90%' } : {}}>
                        <Grid container justifyContent="flex-end">
                            {tab.disableCancelBtn ? (<CloseIcon onClick={closeModal} className={classes.closeIcon} />) : null}
                        </Grid>
                        <Tabs
                            value={tabValue}
                            onChange={handleChangeTab}
                            variant="fullWidth"
                            indicatorColor="primary"
                            textColor="primary">
                            {fields.map(s => (
                                <Tab key={s.name} label={s.name} />
                            ))}
                        </Tabs>
                        {tab && (
                            <form onSubmit={handleSubmit} className={classes.form} >
                                <Grid container className={classes.header} direction="column">
                                    {tab.message ? (
                                        <Grid className={classes.messageGrid}>
                                            <Typography component="span" className={classes.message}>
                                                {tab.message || ''}
                                            </Typography>
                                        </Grid>
                                    ) : null}
                                </Grid>
                                <Grid container className={classes.body}>
                                    <Grid item xs={12}>
                                        <Grid container spacing={3}>
                                            {(tab.columns || []).map((field, index) => {
                                                const InputComponent = ImplementationFor[field.type];
                                                return (
                                                    <Grid key={index} item xs={12}>
                                                        <Field
                                                            name={field.value}
                                                            label={field.label}
                                                            type="text"
                                                            metaData={metaData}
                                                            component={InputComponent}
                                                            required={field.required}
                                                            disabled={
                                                                field.disableOptons &&
                                                                field.disableOptons.edit
                                                            }
                                                            {...field}
                                                        />
                                                    </Grid>
                                                );
                                            })}
                                        </Grid>
                                    </Grid>
                                    {error ? (<Grid item xs={12} className={classes.error}>
                                        {' '}
                                        <Error errorMessage={error} />
                                    </Grid>) : null}
                                </Grid>
                                {notes ? (<Grid>
                                    <Typography component="span" className={classes.note}>
                                        {notes || ''}
                                    </Typography>
                                </Grid>) : null}
                                <Grid container justifyContent="flex-end" style={footerStyle} className={classes.footer}>
                                    {tab.confirmButton ? (
                                        <AlertDialog
                                            description={description}
                                            onConfirm={handleConfirmClose}
                                            onConfirmPopUpClose={true}
                                            btnLabel1="Yes"
                                            btnLabel2="No">
                                            {open => (<Button
                                                type="button"
                                                variant="contained"
                                                onClick={open}
                                                disabled={tab.enableSubmitBtn && (pristine || invalid)}
                                                color="primary"
                                                style={{marginRight : '15px'}}
                                                className={classes.button}>
                                                {(submitting && <ButtonSpinner />) || tab.btnLabel || 'submit'}
                                            </Button>)}
                                        </AlertDialog>
                                    ) : (<Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        disabled={tab.enableSubmitBtn && (pristine || invalid)}
                                        className={classes.button}
                                        style={{marginRight : '15px'}}
                                        onClick={!invalid && tab.onSubmitClose ? closeModal : null}>
                                        {(submitting && <ButtonSpinner />) || tab.btnLabel || 'submit'}
                                    </Button>)
                                    
                                    }
                                    {!tab.disableCancelBtn ||
                                        (tab.disableCancelBtn &&
                                            typeof tab.disableCancelBtn === 'function' &&
                                            tab.disableCancelBtn(record)) ||
                                        tab.disableCancelBtn ?
                                        (<Button
                                            type="button"
                                            variant="contained"
                                            onClick={closeModal}
                                            className={classes.button}>
                                            Cancel
                                        </Button>) : null}
                                </Grid>
                            </form>
                        )}
                    </Paper>
                </Fade>
            </Modal>
        </Grid>
    );
}

SettingsForm.propTypes = {
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'settingsForm',
    validate,
    enableReinitialize: true,
    touchOnChange: true,
})(SettingsForm);
