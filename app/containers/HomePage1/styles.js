import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  root: {
    height: '100vh',
    flexGrow: 1
  },
  component: {
    padding: '25px',
    background: 'white',
    height: '100%',
    display: 'flex', 
    flexDirection: 'column',
    justifyContent: 'space-between',
    boxShadow: '0px 0px 20px 10px #00000026'
  },
  FormPage: {
      display: 'flex',
      // alignItems: 'flex-end',
      // flexDirection: 'column',
      justifyContent: 'flex-end',
      backgroundImage: `url(${require('images/home/HomePage.jpg')})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundAttachment: 'fixed'
  }
}));


export default useStyles;
