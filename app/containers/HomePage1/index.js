/**
 * 
 * Home Page1
 * 
 */


import React, { memo, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import useStyles from './styles';
import { CssBaseline, Box, Grid } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { Helmet } from 'react-helmet';
import { selectLoggedIn, selectUser, selectError, selectSuccess, selectLoading, selectMetaData } from 'blocks/session/selectors';
import { logIn, signUp, forgotPassword, requestDemo, clearCache, verifyOtp, loadAppVersion } from 'blocks/session/actions';
import { ImplementationForm } from './utils';
import Copyright from 'components/Copyright';
import withQueryParams from 'react-router-query-params';

export function HomePage1(props) {
    const { error, location, dispatch, success, match, queryParams, payment, loading, metaData = {} } = props;// eslint-disable-line
    const { state = {} } = location;
    const { form, identifier, secret } = state;
    const classes = useStyles();
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const pageType = match.path.includes("signin") ? 'login' : match.path.includes("login") ? 'login' : match.path.includes("signup") ? 'register' : match.path.includes("forgot") ? 'forgot' : false;
    const formState = form || pageType || 'login';
    const Component = formState && ImplementationForm[formState];
    const componentMounted = useRef(true);
    const [pageLoader, setPageLoader] = useState(false);// eslint-disable-line

    useEffect(() => {
        // let mounted = true;
        setPageLoader(true);
        dispatch(loadAppVersion());
        setTimeout(() => setPageLoader(false), 3000);
        // return () => mounted = false;
        return () => { // This code runs when component is unmounted
            componentMounted.current = false; // (4) set it to false when we leave the page
        }
    }, []);

    let err = null;

    if ((error && error.login && error.login.response
        && error.login.response.data && error.login.response.data.error) || (error[formState])) {
        const data = (error && error.login && error.login.response && error.login.response.data) || error[formState];

        if ((typeof data.error === 'string') || (typeof data === 'string')) {
            err = data && data['error'] || data;
        } else {
            err = data['error'] && data['error'][Object.keys(data.error)[0]] && data['error'][Object.keys(data.error)[0]][0];
        }

    }


    const handleSubmit = (data, dispatch, { form }) => {
        if (formState === 'login') {
            dispatch(logIn(data.email, data.password, queryParams, form));
        } else if (formState === 'register') {
            dispatch(signUp(Object.assign({}, data, { role: 'physician' }), form));
        } else if (formState === 'forgot') {
            dispatch(forgotPassword(data, form));
        } else if (formState === 'requestDemo') {
            dispatch(requestDemo(data, form));
        } else if (formState === 'loginFailure' && identifier && secret) {
            dispatch(logIn(identifier, secret, queryParams, form));
        } else if(formState === 'verifyOtp' && identifier){
            dispatch(verifyOtp(identifier, data.verifyInput, form))
        }

    }

    const clearSessionCache = () => {
        dispatch(clearCache());
    }

    return (<Grid container component="main" className={classes.root}>
        <Helmet
            title="VettedClaims"
            meta={[
                { name: 'description', content: 'Home Page' },
            ]}
        />
        <Grid item xs={12} className={classes.FormPage}>
            <div className={classes.component} style={{ width: md ? `420px` : `100%` }}>
                {formState &&
                    <React.Fragment>
                        <Component
                            form={`${formState}Form`}
                            onSubmit={handleSubmit.bind(this)}
                            errorMessage={err}
                            clearCache={clearSessionCache}
                            success={success}
                            locationState={state}
                            metaData={metaData}
                            loading={loading} />
                        <Box mt={5}>
                            <Copyright textColor={formState === 'requestDemo' ? '#ffff' : '#7f7f7f'} />
                        </Box>
                    </React.Fragment> || null}
            </div>
        </Grid>
        <CssBaseline />
    </Grid>);
}

HomePage1.propTypes = {
    children: PropTypes.object,
    dispatch: PropTypes.func,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    history: PropTypes.object,
    loading: PropTypes.bool,
    location: PropTypes.object,
    loggedIn: PropTypes.bool,
    match: PropTypes.object,
    metaData: PropTypes.object,
    pathData: PropTypes.object,
    // queryParams: PropTypes.object,
    setQueryParams: PropTypes.func,
    success: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    user: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
    loggedIn: selectLoggedIn(),
    user: selectUser(),
    error: selectError(),
    success: selectSuccess(),
    loading: selectLoading(),
    metaData: selectMetaData()
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withConnect,
    memo,
    withQueryParams()
)(HomePage1);