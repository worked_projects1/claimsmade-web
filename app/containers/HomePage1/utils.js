

import LoginForm from 'components/LoginForm';
import RegisterForm from 'components/RegisterForm';
import ForgotPasswordForm from 'components/ForgotPasswordForm';
import LoginFailureForm from 'components/LoginFailureForm';
import VerificationForm from 'components/VerificationForm';

export const ImplementationForm = {
  login: LoginForm,
  register: RegisterForm,
  forgot: ForgotPasswordForm,
  loginFailure: LoginFailureForm,
  verifyOtp: VerificationForm
};