/*
* Modal Form
*/


import React, { useEffect, useState } from 'react';
import { Grid, Dialog, DialogContent, DialogTitle, Divider, Alert, Button, DialogActions } from '@mui/material';
import Styles from './styles';
import { useTheme } from '@mui/material/styles';
import CloseIcon from '@mui/icons-material/Close';
import Warning from '@mui/icons-material/Warning';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import RadioBoxField from '../../../Poc2Page/Components/RadioBoxField';
import InputField from 'components/InputField';
import { Field, reduxForm } from 'redux-form';
import ButtonSpinner from 'components/ButtonSpinner';

const CryptoJS = require("crypto-js");

const ModalForm = (props) => {
    const { title = "", component, close, open, loading, formValues, addedPolicies, policies, updateField, treatment, diagnosis, enableAlert, ind, dialogType, metaData, alertIcon, autoRow, handleSubmit, submitting, storeQuestions, initialValues, addOn, destroy, limit, records } = props;// eslint-disable-line
    const theme = useTheme();
    const classes = Styles();
    const [radio, setRadio] = useState([]);
    const [alertContent, setAlertContent] = useState("");
    const [alert, setAlertOpen] = useState(false);
    const [colorContent, setColor] = useState("#fff");
    const [icon, setIcon] = useState(false);
    const [toBeOpen, setToBeOpen] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);// eslint-disable-line

    useEffect(() => {
        if (!loading) {
            let policies1 = [];
            if (addedPolicies.length != 0) {
                const existPolicy = addedPolicies.filter(e => e.treatment == treatment);

                if (existPolicy.length == 0) {
                    policies1.push(policies["policy_doc"])
                } else {
                    policies1 = existPolicy.map(e => e["schema"])
                }
                const bytes = CryptoJS.AES.decrypt(policies1[0], 'K6*^)&b=087&H%K!s2A0');
                const secret = bytes.toString(CryptoJS.enc.Utf8);
                const schema = JSON.parse(secret);

                const matchedArray1 = schema.policies.filter(r => r.diagnosis['label-name'] == diagnosis)
                const starDiagnosis = schema.policies.filter(r => r.diagnosis['label-name'] == '*')

                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                    setAlertOpen(true);
                    if (formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert != 'error' && formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert != 'warning') {
                        setIcon(true);
                        setColor(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert || "primary");
                        setAlertContent(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alertContent || "No records");
                    } else {
                        setIcon(false);
                        setColor(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert || "primary");
                        setAlertContent(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alertContent || "No records");
                    }
                }
                if ((matchedArray1.length != 0) || (starDiagnosis.length != 0)) {
                    let diagnosis1 = {}
                    if (matchedArray1.length != 0) {
                        diagnosis1 = matchedArray1.find(e => e.questions)
                    } else if (starDiagnosis.length != 0) {
                        diagnosis1 = starDiagnosis.find(e => e.questions)
                    }
                    const questions = diagnosis1.questions
                    if (questions.length != 0) {
                        setAlertOpen(false)
                        setRadio([])
                        setRadio(questions.map((e, i) => i == 0 ? Object.assign({}, e, { editRecord: true }) : Object.assign({}, e, { editRecord: false })));
                        if (Object.keys(initialValues).length != 0) {
                            setRadio(questions.map((e, i) => initialValues[`questions${i}`] != undefined ? Object.assign({}, e, { editRecord: true }) : Object.assign({}, e, { editRecord: false })));
                            setAlertOpen(true);
                            if (formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert != 'error' && formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert != 'warning') {
                                setIcon(true);
                                setColor(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert || "primary");
                                setAlertContent(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alertContent || "No Record");
                            } else {
                                setIcon(false);
                                setColor(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert || "primary");
                                setAlertContent(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alertContent || "No record");
                            }

                        }
                        setToBeOpen(true)
                    } else {
                        setRadio([])
                        if (diagnosis1.recommendation == "deny") {
                            setToBeOpen(false);
                            if (diagnosis1.source == "mtus") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("error", `This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br /> Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(diagnosis1["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true);
                                } else {
                                    close(ind, addOn, treatment, false);
                                }

                            } else if (diagnosis1.source == "odg") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("error", `This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(diagnosis1["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true)
                                } else {
                                    close(ind, addOn, treatment, false)
                                }
                            }
                        } else if (diagnosis1.recommendation == "approve") {
                            setToBeOpen(false)
                            if (diagnosis1.source == "mtus") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("success", `This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(diagnosis1["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true)
                                } else {
                                    close(ind, addOn, treatment, false)
                                }
                            } else if (diagnosis1.source == "odg") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("success", `This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(diagnosis1["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true)
                                } else {
                                    close(ind, addOn, treatment, false)
                                }
                            }
                        }
                        else if (diagnosis1.recommendation == "no_recommendation") {
                            setToBeOpen(false)
                            if (diagnosis1.source == "mtus") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("warning", `Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided.</i> <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(diagnosis1["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true)
                                } else {
                                    close(ind, addOn, treatment, false)
                                }
                            } else if (diagnosis1.source == "odg") {
                                if (dialogType != "enteredForm") {
                                    enableAlert("warning", `Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided. <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(diagnosis1["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, false, addOn, false, treatment);
                                }
                                if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                                    setModalOpen(true)
                                } else {
                                    close(ind, addOn, treatment, false)
                                }
                            }
                        }
                    }
                }
                else {
                    setRadio([])
                    if (dialogType != "enteredForm") {
                        enableAlert("warning", `Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided.</i>`, ind, false, addOn, false, treatment);
                    }
                    if (dialogType == "enteredForm" || dialogType == "addOnEnteredForm") {
                        setModalOpen(true)
                    } else {
                        close(ind, addOn, treatment, false)
                    }
                }
            }
        }

        return () => destroy();
    }, [loading, treatment]);


    const handleRadio = (val) => {
        if (storeQuestions != undefined) {
            storeQuestions({ label: val.name, questionValue: val.value, index: ind, value: treatment, keyName: 'treatments', addOn: addOn, alert: "", alertContent: "", questions: true });
        }
        let policies1 = []
        const existPolicy = addedPolicies.filter(e => e.treatment == treatment);

        if (existPolicy.length == 0) {
            policies1.push(policies["policy_doc"])
        } else {
            policies1 = existPolicy.map(e => e["schema"])
        }
        const bytes = CryptoJS.AES.decrypt(policies1[0], 'K6*^)&b=087&H%K!s2A0');
        const secret = bytes.toString(CryptoJS.enc.Utf8);
        const schema = JSON.parse(secret);
        const selected1 = (schema.policies || []).filter(r => r.diagnosis['label-name'] == diagnosis)
        const starDiagnosis = (schema.policies || []).filter(r => r.diagnosis['label-name'] == '*')
        if (val.value == "deny") {
            setAlertOpen(true)
            setIcon(false)
            setColor("error")
            let obj = {}
            if (selected1.length != 0) {

                obj = selected1.find(e3 => true)// eslint-disable-line
            } else if (starDiagnosis.length != 0) {
                obj = starDiagnosis.find(e3 => true)// eslint-disable-line
            }
            if (obj.source == "mtus") {
                setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`)
                enableAlert("error", `This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            } else if (obj.source == "odg") {
                setAlertContent(`This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`)
                enableAlert("error", `This claim is likely to be denied (based on VettedClaims auto review against guidelines) <br /> <br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            }
            setRadio(radio.map((e, i) => (val.name.slice(-1) < i) ? Object.assign({}, e, { editRecord: false }) : Object.assign({}, e, { editRecord: true })))
        } else if (val.value == "approve") {
            setAlertOpen(true)
            setIcon(true)
            setColor("success")
            let obj = {}
            if (selected1.length != 0) {
                obj = selected1.find(e3 => true)// eslint-disable-line
            } else if (starDiagnosis.length != 0) {
                obj = starDiagnosis.find(e3 => true)// eslint-disable-line
            }
            if (obj.source == "mtus") {
                setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`);
                enableAlert("success", `This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from MTUS Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            } else if (obj.source == "odg") {
                setAlertContent(`This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`)
                enableAlert("success", `This claim is likely to be approved (based on VettedClaims auto review against guidelines) <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            }
            setRadio(radio.map((e, i) => (val.name.slice(-1) < i) ? Object.assign({}, e, { editRecord: false }) : Object.assign({}, e, { editRecord: true })))
        } else if (val.value == "no_recommendation") {
            setAlertOpen(true)
            setIcon(false)
            setColor("warning");
            let obj = {}
            if (selected1.length != 0) {
                obj = selected1.find(e3 => true)// eslint-disable-line
            } else if (starDiagnosis.length != 0) {
                obj = starDiagnosis.find(e3 => true)// eslint-disable-line
            }
            if (obj.source == "mtus") {
                setAlertContent(`Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided. <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`);
                enableAlert("warning", `Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided. <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["mtus-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            } else if (obj.source == "odg") {
                setAlertContent(`Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided. <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`)
                enableAlert("warning", `Guidelines do not cover this scenario <br /> <br /> <i>Workers comp guidelines by MTUS and ODG (Official Disability Guidelines) do not cover the requested treatment/service for the diagnosis provided. <br /><br />Relevant excerpt from ODG Guidelines: <i>${JSON.stringify(obj["odg-text"]).replace(/\\n/g, '<br />').replace(/['"]+/g, '').replace(/\\t/g, '&emsp;')}</i>`, ind, true, addOn, false, treatment)
            }
            setRadio(radio.map((e, i) => (val.name.slice(-1) < i) ? Object.assign({}, e, { editRecord: false }) : Object.assign({}, e, { editRecord: true })))
        } else {
            radio.map((e, i) => (val.name.slice(-1) < i) ? updateField("pocForm", `questions${i}`, "") : e)
            setRadio(radio.map((e, i) => e["#"] == parseInt(val.value) || e.editRecord ? Object.assign({}, e, { editRecord: true }) : Object.assign({}, e, { editRecord: false })))// eslint-disable-line
            setAlertOpen(false);
        }
    }


    return <Grid>
        {!loading && (dialogType == "addOn" || dialogType == "addOnEnteredForm") ?
            <Grid>
                <Grid container justify="space-between" className={classes.header1}>
                    <Grid item xs={10}>
                    </Grid>
                </Grid>
                {dialogType == "addOnEnteredForm" ?
                    <Grid>
                        {Object.keys(initialValues).length != 0 ?
                            <Grid>
                                <h4 className={classes.enterFormheader}>Confidence level for {records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'error' ? 'denial' : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'success' ? 'approval' : ''} is high based on the following inputs. </h4>
                            </Grid> :
                            <Grid>
                                {records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'error' ? <h4 className={classes.enterFormheader}>Requested input is unconditionally denied. </h4> : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'success' ? <h4 className={classes.enterFormheader}>Requested input is unconditionally approved.</h4> : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'warning' ? <h4 className={classes.enterFormheader}>Requested input is likely to be denied.
                                </h4> : null}
                            </Grid>}
                    </Grid> : null}

                <Grid className={classes.questions}>

                    {radio.length != 0 ? (radio || []).map((e, i) => e.editRecord ?
                        <Grid item xs={12} key={i}>
                            <Field name={`questions${i}`}
                                label={e.question}
                                type="check"
                                component={RadioBoxField}
                                disabled={Object.keys(initialValues).length != 0 && initialValues[`questions${i}`] || false}
                                defaultValue={Object.keys(initialValues).length != 0 && initialValues[`questions${i}`] || ""}
                                options={e["option-list"] || []}
                                onChange={handleRadio}
                            />
                        </Grid> : null) : null}
                    <Grid className={classes.hiddenField}>
                        <Field name={`alert`}
                            label={"Alert"}
                            type="text"
                            component={InputField}
                        />
                        <Field name={`alertContent`}
                            label={"Alert Content"}
                            type="text"
                            component={InputField}
                        />
                    </Grid>

                    {alert ? <Grid container justifyContent={'center'} className={classes.addonAlert}><div className={classes.alertDiv1}>
                        <Alert
                            iconMapping={{
                                success: icon ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                            }}
                            color={colorContent}
                            style={colorContent == 'error' ? { background: '#f5b9b9', padding: "6px 16px" } : colorContent == 'success' ? { background: '#dae7da', padding: "6px 16px" } : { padding: "6px 16px" }}
                            className={classes.alert}
                        >
                            <div className={classes.alertContent}>
                                <span dangerouslySetInnerHTML={{ __html: alertContent }} />
                            </div>
                        </Alert>
                    </div></Grid> : null}


                </Grid>
            </Grid> : null}
        {!loading && (toBeOpen || dialogType == "enteredForm") && dialogType != "addOn" && dialogType != "addOnEnteredForm" ? <Dialog
            open={open}
            //   maxWidth={"md"}
            sx={{ '& .MuiDialog-paper': { width: 850, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
            maxWidth="md"
            scroll={"body"}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description">
            <DialogTitle  >
                <Grid container justify="space-between" className={classes.header}>
                    <Grid item xs={10}>
                        <span dangerouslySetInnerHTML={{ __html: title }} className={classes.title} />
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'end' }}>
                        {<CloseIcon onClick={dialogType != "enteredForm" ? () => close(ind, addOn, treatment, true) : () => close(ind, addOn, treatment, true)} className={classes.closeIcon} />}
                    </Grid>
                </Grid>
            </DialogTitle>
            <Divider />
            <form name="pocForm" onSubmit={handleSubmit}>
                <DialogContent dividers className={classes.dialog} style={dialogType != "enteredForm" ? { height: '400px' } : { height: '400px' }}>
                    {dialogType == "enteredForm" ?
                        <Grid>
                            {Object.keys(initialValues).length != 0 ?
                                <Grid>
                                    <h4 className={classes.enterFormheader}>Confidence level for {records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'error' ? 'denial' : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'success' ? 'approval' : ''} is high based on the following inputs. </h4>
                                </Grid> :
                                <Grid>
                                    {records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'error' ? <h4 className={classes.enterFormheader}>Requested input is unconditionally denied. </h4> : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'success' ? <h4 className={classes.enterFormheader}>Requested input is unconditionally approved.</h4> : records.find((e) => e && e.index == ind && typeof (e.addOn) == typeof (addOn) && e.addOn == addOn).alert == 'warning' ? <h4 className={classes.enterFormheader}>Requested input is likely to be denied.
                                    </h4> : null}
                                </Grid>}
                        </Grid> : null}
                    {dialogType == "questions" || dialogType == "surgery" || dialogType == "enteredForm" ? <Grid className={classes.questions}>

                        {radio.length != 0 ? (radio || []).map((e, i) => e.editRecord ?
                            <Grid item xs={12} key={i}>
                                <Field name={`questions${i}`}
                                    label={e.question}
                                    type="check"
                                    component={RadioBoxField}
                                    disabled={Object.keys(initialValues).length != 0 && initialValues[`questions${i}`] || false}
                                    defaultValue={Object.keys(initialValues).length != 0 && initialValues[`questions${i}`] || ""}
                                    options={e["option-list"] || []}
                                    onChange={handleRadio}
                                />
                            </Grid> : null) : null}
                        <Grid className={classes.hiddenField}>
                            <Field name={`alert`}
                                label={"Alert"}
                                type="text"
                                component={InputField}
                            />
                            <Field name={`alertContent`}
                                label={"Alert Content"}
                                type="text"
                                component={InputField}
                            />
                        </Grid>

                        {alert ? <div className={classes.alertDiv}>
                            <Alert
                                iconMapping={{
                                    success: icon ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                                }}
                                color={colorContent}
                                style={colorContent == 'error' ? { background: '#f5b9b9', padding: "6px 16px" } : colorContent == 'success' ? { background: '#dae7da', padding: "6px 16px" } : { padding: "6px 16px" }}
                                className={classes.alert}
                            >
                                <div className={classes.alertContent}>
                                    <span dangerouslySetInnerHTML={{ __html: alertContent }} />
                                </div>
                            </Alert>
                        </div> : null}


                    </Grid> : null}

                </DialogContent >
                {dialogType != "enteredForm" && dialogType != "addOn" && dialogType != "questions" && ind != limit - 1 ?
                    <Grid>
                        <Divider />
                        <DialogActions>
                            <Grid container className={classes.footer}>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={records[ind] && (records[ind].treatments == undefined || records[ind].treatments != treatment || (records[ind].treatments && records[ind].treatments == treatment && records[ind].alert != "success")) || false}
                                    className={classes.submitBtn}>
                                    {submitting && <ButtonSpinner /> || 'Show Add-ons'}
                                </Button>
                            </Grid>
                        </DialogActions>
                    </Grid> : null}
            </form>
        </Dialog> : null}
    </Grid>
}

export default reduxForm({
    form: 'pocForm',
    enableReinitialize: true,
    touchOnChange: true
})(ModalForm);
