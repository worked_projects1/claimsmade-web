/**
 * 
 * Select Field
 * 
 */

import React, { useEffect, useState } from 'react';
import Styles from './styles';
import { FormControl, Select, MenuItem, ListSubheader, TextField, InputAdornment } from '@mui/material'; // eslint-disable-line
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';

export default function ({ input, label, required, metaData, options, variant, disabled, children, style, meta: { touched, error, warning }, defaultValue, removeIcon, selectedOther, settingOther, tothers }) {// eslint-disable-line
    const classes = Styles();
    const { name, value } = input;
    const { options1 = [], options2 = [] } = options
    const isPreDefinedSet1 = Array.isArray(options1);
    const isPreDefinedSet2 = Array.isArray(options2);
    const [searchText, setSearchText] = React.useState("")// eslint-disable-line
    const [selectValue, setSelectValue] = React.useState(false);

    const changeValue = (e) => {
        if (e.target.value == "others") {
            e.preventDefault();
            e.stopPropagation();
            setSelectValue(true);
        } else {
            input.onChange(e.target.value);

        }
    }

    useEffect(() => {
        setSelectValue(selectedOther);
    }, [selectedOther])

    const containsText = (option, searchText) =>
        option.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

    const displayedOptions2 = React.useMemo(
        () => options2.filter((option) => containsText(option.label, searchText)),
        [searchText]
    );
    const displayedOptions1 = React.useMemo(
        () => options1.filter((option) => containsText(option.label, searchText)),
        [searchText]
    );

    const [sorting, setSorting] = useState(false)

    return (
        <div className={classes.selectField} style={style || {}}>
            {children || ''}
            <FormControl variant="standard" size="small" sx={{ m: 0, minWidth: 140 }} className={classes.formControl}>

                <Select
                    name={name}
                    fullWidth
                    MenuProps={{ autoFocus: false, classes: { paper: classes.menuPaper } }}
                    style={{
                        width: "100%",
                    }}
                    // autoFocus={false}
                    disabled={disabled}
                    required={required}
                    value={value}
                    defaultValue={defaultValue}
                    onOpen={() => {
                        const data1 = (value != '') && options1.filter(val => val.value == value);
                        if (data1.length > 0) {
                            setSelectValue(false);
                        }
                    }}
                    onClose={(e) => {
                        setSearchText("");
                        setSorting(false);
                        if (e.target.value == undefined) {
                            const data = options2.filter(val => val.value == value);
                            if (data.length == 0) {
                                setSelectValue(false);
                            }
                        }
                    }}
                    //  input={<OutlinedInput notched label={label} />}
                    classes={{
                        select: classes.select
                    }}
                    sx={{
                        "& .MuiInput-input.MuiSelect-select": {
                            whiteSpace: 'pre-wrap',
                            maxWidth: "500px"
                        },
                        "& .MuiInputBase-input.Mui-disabled": {
                            WebkitTextFillColor: "#1d1e1c",
                        },
                    }}

                    IconComponent={removeIcon ? () => null : (props) => <ArrowDropDownIcon {...props} className={`material-icons ${props.className}`} />}// eslint-disable-line
                    onChange={changeValue} >

                    <ListSubheader>
                        <TextField
                            variant="standard"
                            autoFocus
                            sx={{ paddingTop: "3px" }}
                            placeholder="Type to search..."
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <ManageSearchIcon sx={{ color: '#bfbfbf' }} className={classes.search} />
                                    </InputAdornment>
                                )
                            }}
                            onClick={(e) => {
                                e.stopPropagation();
                                e.preventDefault();
                            }}
                            onChange={(e) => {
                                setSearchText(e.target.value);
                                setSorting(true);
                            }}
                            onKeyDown={(e) => {
                                if (e.key !== "Escape") {
                                    e.stopPropagation();
                                }
                            }}
                        />
                    </ListSubheader>
                    {!selectValue ? (typeof options1 === 'string' && metaData[options1] === undefined) ? <MenuItem value="">No options</MenuItem> : isPreDefinedSet1 ? ((sorting ? displayedOptions1 : options1) || []).map((opt, index) => <MenuItem
                        key={index}
                        divider={true}
                        style={{ whiteSpace: 'normal' }}
                        disabled={opt.disabled || false}
                        value={opt && opt.value || opt}>
                        {opt && opt.label || opt}
                    </MenuItem>) : (typeof options1 === 'string' && Object.keys(metaData[options1]).length === 0 || (typeof options1 === 'object' && sorting && displayedOptions1.length == 0)) ? <MenuItem value="">No options</MenuItem> : (metaData[options1] || []).map((opt, index) =>
                        <MenuItem
                            key={index}
                            divider={true}
                            style={{ whiteSpace: 'normal' }}
                            disabled={opt.disabled || false}
                            value={opt && opt.value != null
                                && opt.value || opt}>
                            {opt && opt.label != null && opt.label || opt}
                        </MenuItem>)
                        : null}
                    {!tothers && !selectValue ? <MenuItem
                        divider={true}
                        style={{ whiteSpace: 'normal', background: "#3a35352e" }}
                        // disabled={true}
                        button={false}
                        onClickCapture={(e) => {
                            e.stopPropagation();
                            setSelectValue(!selectValue);
                            settingOther(!selectValue);
                        }}
                        onKeyDown={(e) => e.stopPropagation()}
                        onMouseDown={(e) => e.stopPropagation()}


                        value="others">
                        Others
                    </MenuItem> : null}

                    {selectValue ? (typeof options2 === 'string' && metaData[options2] === undefined) ? <MenuItem value="">No options</MenuItem> : isPreDefinedSet2 ? (sorting ? displayedOptions2 : options2).map((opt, index) =>
                        <MenuItem
                            divider={true}
                            style={{ whiteSpace: 'normal' }}
                            key={index}
                            disabled={opt.disabled || false}
                            value={opt && opt.value || opt}>
                            {opt && opt.label || opt}</MenuItem>) : (typeof options2 === 'string' && Object.keys(metaData[options2]).length === 0 || (typeof options2 === 'object' && sorting && displayedOptions2.length == 0)) ? <MenuItem value="">No options</MenuItem> : (metaData[options2] || []).map((opt, index) => <MenuItem
                                divider={true}
                                style={{ whiteSpace: 'normal' }}
                                key={index}
                                disabled={opt.disabled || false}
                                value={opt && opt.value != null
                                    && opt.value || opt}>
                                {opt && opt.label != null && opt.label || opt}
                            </MenuItem>)
                        : null}
                </Select>
            </FormControl>
            <div className={classes.error}>
                {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
            </div>
        </div>
    )
}
