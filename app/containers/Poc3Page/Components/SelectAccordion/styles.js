

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  formControl: {
    width: '100%'
  },
  menuPaper: {
    width: '400px',
    maxHeight: '350px',
    marginLeft: '82px',
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    fontSize: '14px',
    '& label.Mui-focused': {
      color: '#1d1e1c',
    },
    '& label.Mui-visited': {
      color: '#1d1e1c',
    },
    '&.MuiFormLabel-root.Mui-focused': {
      borderColor: '#ccebf6',
      color: '#1d1e1c'
    },
    color: '#1d1e1c',
  },
  select: {
    '&:focus': {
      backgroundColor: 'transparent',
    },
    '&: -webkit-tap-highlight-color': 'transparent',
    '& :focused': {
      borderColor: '#ccebf6',
    },
    '& :before': {
      borderColor: '#ccebf6 !important',
    },
    '&:after': {
      borderColor: '#ccebf6',
    },
    '& .MuiInput-underline:after': {
      borderColor: '#ccebf6',
    },
  },
  selectField: {
    '& .MuiInput-underline:after': {
      borderBottom: '1px solid #ccebf6',
    },
    '& .MuiInput-underline:before': {
      borderBottomColor: '#ccebf6 !important',
    },
    '@global': {
      '.MuiOutlinedInput-notchedOutline': {
        border: 'none'
      }
    }
  },
  search: {
    width: '33px',
    height: '31px',
  }

}));


export default useStyles;