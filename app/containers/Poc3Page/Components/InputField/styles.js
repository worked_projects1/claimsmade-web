

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({// eslint-disable-line
  fieldColor: {
    "&.Mui-focused": {
      borderBottomColor: '#ccebf6',
    },
    '& :after': {
      borderBottomColor: '#ccebf6',
    },
    '& :before': {
      borderBottomColor: '#ccebf6 !important',
    },
    color: '#1d1e1c',
    '& label.Mui-focused': {
      color: '#1d1e1c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#ccebf6',
    },

  },
  input: {
    "&:-webkit-autofill": {
      WebkitBoxShadow: "0 0 0 1000px white inset"
    }
  },
  textSize: {
    fontSize: '14px',
  },
  error: {
    fontSize: '14px',
    color: 'red',
    marginTop: '5px'
  }
}));


export default useStyles;