
/***
 *
 * Input Field
 *
 */

 import React from 'react';
 import TextField from '@mui/material/TextField';
 import Styles from './styles';
 import {OutlinedInput} from '@mui/material';
 
 export default function ({ input, label, placeholder, autoFocus, type, disabled, prefix, style, variant, multiline, rows, rowMax, className, errorStyle, defaultBlur, meta: { touched, error, warning }, defaultValue }) {// eslint-disable-line
 
   const classes = Styles();
   const { name, value, onChange } = input;
 
   const InputChange = defaultBlur ? Object.assign({}, {
     onBlur: (e) => onChange(e.target.value),
     defaultValue: value || ''
   }) : Object.assign({}, {
     onChange: (e) => onChange(e.target.value),
     value: value || ''
   });

   if ( input.value === '' && defaultValue ) { // hack for redux form with material components
    input.onChange(String(defaultValue));
} 
 
   return (
     <div style={style || {}}>
       <TextField
         name={name}
         type={type}
         placeholder={placeholder}
         disabled={disabled}
         className={className || classes.fieldColor}
         fullWidth
         value={value || defaultValue || ''}
         defaultValue={defaultValue}
         variant={variant || "standard"}
         {...InputChange}
         input={<OutlinedInput notched label={label} />}
         InputProps={{ classes: { input: classes.input } }}
         autoFocus={autoFocus}
         multiline={multiline || false}
         rows={rows || 1} />
     </div>
 
   )
 }
 