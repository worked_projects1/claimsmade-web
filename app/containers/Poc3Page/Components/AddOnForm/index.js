
/*
* Add On Form
*/


import React, { useEffect, useRef, useState } from 'react';
import { Grid, Dialog, DialogContent, DialogTitle, Divider, Alert, Button, Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import Styles from './styles';
import { useTheme } from '@mui/material/styles';
import CloseIcon from '@mui/icons-material/Close';
import Warning from '@mui/icons-material/Warning';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import CheckBoxField from 'components/CheckboxField';
import { Field, reduxForm } from 'redux-form';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import Snackbar from '@mui/material/Snackbar';
// import Skeleton from '@mui/material/Skeleton';
import ModalForm from '../ModalForm';
import Spinner from 'components/Spinner';

function AddOnForm(props) {
    const { metaData, close, open, formValues, addedPolicies, policies, updateField, enableAlert, alertIcon, getPolicies, closeDialog, loading, showQuestAddOn, ind, title, closeAlertAddOn, showAlertAddOn, addOn, showAddOnPopUp, treatment, formSelected, dialogType, autoRow, storeQuestions, onSubmit, initialValues, showModal, closeModal, diagnosis, addOnTitle, limit, records, rfaOptions, bodySystemId, treatmentValue } = props;// eslint-disable-line
    const theme = useTheme();
    const classes = Styles();
    const [tableIndex, setIndex] = useState("");
    const [expanded, setExpanded] = React.useState("");
    let indLimit = (treatmentValue).findIndex((e, i) => addOn <= i && e && e.diagnosis == "" && e.treatments == "");

    const componentMounted = useRef(true);

    useEffect(() => {
        setIndex(ind);
        return () => { // This code runs when component is unmounted
            componentMounted.current = false; // (4) set it to false when we leave the page
        }
    }, []);

    let btn = {
        border: 'none',
        background: 'transparent',
        cursor: 'pointer'
    }
    const alertBox = records.filter((e) => e && typeof (addOn) == 'number' && addOn === e.addOn && e.bodySystemId == bodySystemId);
    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == bodySystemId && e1.type == "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type }));

    const handleChange = (i) => {
        setExpanded(`panel${i}`)
    }

    return <Grid>
        <Dialog
            open={open}
            //   maxWidth={"md"}
            sx={{ '& .MuiDialog-paper': { width: 850, [theme.breakpoints.up('md')]: { marginLeft: theme.spacing(11.5) } } }}
            maxWidth="md"
            scroll={"body"}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description">
            <DialogTitle  >
                <Grid container justify="space-between" className={classes.header}>
                    <Grid item xs={10}>
                        <span dangerouslySetInnerHTML={{ __html: addOnTitle }} className={classes.title} />
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'end' }}>
                        {<CloseIcon onClick={() => close(tableIndex)} className={classes.closeIcon} />}
                    </Grid>
                </Grid>
            </DialogTitle>
            <Divider />
            <DialogContent dividers className={classes.dialog} style={{ height: '500px' }}>
                <form className={classes.form}>
                    {loading ? <Spinner className={classes.spinner} /> : null}
                    <Grid>
                        <Grid className={classes.headerDiv}>
                            <Grid container>
                                <Grid item xs={6} md={6} className={classes.firstDiv}>Add-Ons</Grid>
                                <Grid item xs={6} md={6} className={classes.secDiv}>Confidence of Approval</Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    {(treatmentOpt.filter((ele, ind) => ind === treatmentOpt.findIndex(elem => elem.value === ele.value)).sort((a, b) => a.label.localeCompare(b.label)) || []).map((e, i) =>

                        <Accordion key={i} expanded={ expanded == `panel${i}` && i === ind && typeof (addOn) == 'number' && (showModal || dialogType == 'addOnEnteredForm')} classes={{ root: classes.acordion }} onChange={() => handleChange(i)}>
                            <AccordionSummary key={i} aria-controls={`panel${i}d-content`}  id={`panel${i}d-header`}
                            >
                                <Grid container key={i} className={classes.mainDiv}>
                                    <Grid key={i} item xs={6} md={6}>

                                        <Field
                                            key={i}
                                            name={e.value}
                                            label={e.label}
                                            value={(alertBox.length != 0 && alertBox.find((e2) => e2.index == i) != undefined)}
                                            type="text"
                                            component={CheckBoxField}
                                            disabled={loading}
                                            rfaFormRowLimit={!(indLimit != -1 && limit && indLimit < limit)}
                                            defaultValue={(alertBox.length != 0 && alertBox.find((e2) => e2.index == i) != undefined)}
                                            options={metaData.addOnOptions}
                                            onChange={(e1) => {
                                                if(!e1) setExpanded("");
                                                showQuestAddOn({
                                                treat: e.value,
                                                diag: "Surgery Add-on",
                                                label: e.label,
                                                index: i,
                                                addOn: tableIndex,
                                                checked: e1,
                                                bodySystem: bodySystemId
                                            })
                                        }}
                                        />
                                    </Grid>
                                    <Grid item xs={6} md={6}>
                                        {<div style={{ width: '100%', textAlign: 'center' }}>
                                            {alertBox.length != 0 && alertBox.find((e2) => e2.index == i) != undefined ? alertBox.find((e2) => e2.index == i).alert == "success" ?
                                                <Button style={btn} type="button" onClick={() => formSelected(alertBox.find((e2) => e2.index == i).index, i, alertBox.find((e2) => e2.index == i).addOn)}> <CheckCircleOutlineIcon sx={{ color: "green", fontSize: 26 }} /></Button>
                                                : alertBox.find((e2) => e2.index == i).alert == "error" ?
                                                    <Button style={btn} type="button" onClick={() => formSelected(alertBox.find((e2) => e2.index == i).index, i, alertBox.find((e2) => e2.index == i).addOn)}> <CancelOutlinedIcon sx={{ color: "#fb0404", fontSize: 26 }} /></Button>
                                                    : alertBox.find((e2) => e2.index == i).alert == "warning" ? <Button style={btn} type="button" onClick={() => formSelected(alertBox.find((e2) => e2.index == i).index, i, alertBox.find((e2) => e2.index == i).addOn)}><CancelOutlinedIcon sx={{ color: "#f38b06", fontSize: 26 }} /></Button> : "" : ""
                                            } </div>}
                                    </Grid>
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails key={i} >
                                {showModal ?
                                    <ModalForm
                                        title={(diagnosis == "Surgery Add-on") ?
                                            `<span style="font-weight: bold">Diagnosis : </span><span> Surgery Add Ons</span><br /><span style="font-weight: bold">Service/Good requested : </span><span>${e.label}</span>` :
                                            `<span style="font-weight: bold">Diagnosis : </span><span> ${diagnosis}</span><br /><span style="font-weight: bold">Service/Good requested : </span><span>${e.label}</span>`}
                                        treatment={treatment}
                                        diagnosis={diagnosis}
                                        close={closeModal}
                                        open={showModal}
                                        loading={loading}
                                        formValues={formValues}
                                        addedPolicies={addedPolicies}
                                        policies={policies}
                                        updateField={updateField}
                                        enableAlert={enableAlert}
                                        ind={ind}
                                        dialogType={dialogType}
                                        metaData={metaData}
                                        alertIcon={alertIcon}
                                        records={records}
                                        autoRow={autoRow}
                                        onSubmit={onSubmit}
                                        storeQuestions={storeQuestions}
                                        initialValues={initialValues}
                                        addOn={addOn}
                                    />
                                    : null}
                            </AccordionDetails>
                        </Accordion>

                    )}

                </form>
            </DialogContent >
            {!showModal && showAlertAddOn && records.findIndex((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn) != -1 && addOn != limit-1 ? <Grid>
                {/* <Divider /> */}
                <Snackbar open={showAlertAddOn} autoHideDuration={2000} onClose={closeAlertAddOn} className={classes.snackbar} sx={{ position: 'fixed', width: '60%' }} anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}>
                    <div className={classes.alertDiv}>
                        <Alert
                            iconMapping={{
                                success: (records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn).alert != 'error' && records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn).alert != 'warning') ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                            }}

                            color={records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn).alert}
                            style={records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn).alert == 'error' ? { background: '#f5b9b9' } : null}
                            className={classes.alert}
                        >
                            <div>
                                <span dangerouslySetInnerHTML={{ __html: (records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn == e.addOn).alertContent).substring(0, (records.find((e) => e && typeof (addOn) == 'number' && e.index == ind && addOn === e.addOn).alertContent).indexOf("<br /> <br />")) }} />
                            </div>
                        </Alert>
                    </div>
                </Snackbar>
            </Grid> :
            null}
        </Dialog>
    </Grid>
}

export default reduxForm({
    form: 'addOnForm',
    enableReinitialize: true,
    touchOnChange: true
})(AddOnForm);