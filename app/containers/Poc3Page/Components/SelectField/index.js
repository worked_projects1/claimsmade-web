/**
 * 
 * Select Field
 * 
 */

import React, { useState } from 'react';
import Styles from './styles';
import { FormControl, Select, MenuItem, ListSubheader, TextField, InputAdornment } from '@mui/material'; // eslint-disable-line
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import CircularProgress from '@mui/material/CircularProgress';


export default function ({ input, label, required, metaData, options, variant, disabled, children, style, meta: { touched, error, warning }, defaultValue, removeIcon, loadingRecord }) {// eslint-disable-line
    const classes = Styles();
    const { name, value } = input;
    const isPreDefinedSet = Array.isArray(options);
    const [searchText, setSearchText] = React.useState("");

    const changeValue = (e) => {
        input.onChange(e.target.value)
    }

    const containsText = (option, searchText) =>
        option.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

    const displayedOptions = React.useMemo(
        () => options.filter((option) => containsText(option.label, searchText)),
        [searchText]
    );

    const [sorting, setSorting] = useState(false)

    const selectOptions = (sorting ? displayedOptions : options)

    return (
        <div className={classes.selectField} style={style || {}}>
            {children || ''}
            <FormControl variant="standard" size="small" sx={{ m: 0, minWidth: 140 }} className={classes.formControl}>

                <Select
                    name={name}
                    fullWidth
                    MenuProps={{ autoFocus: false, classes: { paper: classes.menuPaper } }}
                    style={{
                        width: "100%",
                    }}
                    disabled={disabled}
                    required={required}
                    value={value}
                    defaultValue={defaultValue}
                    onClose={() => {
                        setSearchText("");
                        setSorting(false);
                    }}
                    classes={{
                        select: classes.select
                    }}

                    sx={{
                        "& .MuiInput-input.MuiSelect-select": {
                            whiteSpace: 'pre-wrap',
                            maxWidth: "500px"
                        },
                        "& .MuiInputBase-input.Mui-disabled": {
                            WebkitTextFillColor: "#1d1e1c",
                        },
                    }}

                    IconComponent={removeIcon ? () => null : (props) => <ArrowDropDownIcon {...props} className={`material-icons ${props.className}`} />}// eslint-disable-line
                    onChange={(e) => changeValue(e)} >

                    {!input.name.includes("bodySystemId") ? <ListSubheader>
                        <TextField
                            variant="standard"
                            autoFocus
                            sx={{ paddingTop: "3px" }}
                            placeholder="Type to search..."
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <ManageSearchIcon sx={{ color: '#bfbfbf' }} className={classes.search} />
                                    </InputAdornment>
                                )
                            }}
                            onClick={(e) => {
                                e.stopPropagation();
                                e.preventDefault();
                            }}
                            onChange={(e) => {
                                setSearchText(e.target.value);
                                setSorting(true);
                            }}
                            onKeyDown={(e) => {
                                if (e.key !== "Escape") {
                                    e.stopPropagation();
                                }
                            }}
                        />
                    </ListSubheader> : null}
                    {((typeof options === 'string' && metaData[options] === undefined) || (typeof options === 'object' && sorting && Object.keys(displayedOptions).length == 0) || (typeof options === 'object' && !sorting && !loadingRecord && Object.keys(selectOptions).length == 0)) ? <MenuItem value="" disabled>No options</MenuItem> : isPreDefinedSet ?
                        (selectOptions.length == 0 && loadingRecord) ? <MenuItem style={{ 'justifyContent': 'center' }}><CircularProgress size='1.5rem' /></MenuItem> :
                            (selectOptions).map((opt, index) =>
                                <MenuItem
                                    divider={true}
                                    style={{ whiteSpace: 'normal' }}
                                    key={index}
                                    disabled={opt.disabled || false}
                                    value={opt && opt.value || opt}>
                                    {opt && opt.label || opt}</MenuItem>) : ((typeof options === 'string' && Object.keys(metaData[options]).length === 0) || (typeof options === 'object' && sorting && Object.keys(displayedOptions).length == 0)) ? <MenuItem value="" disabled>No options</MenuItem> : (metaData[options] || []).map((opt, index) => <MenuItem
                                        divider={true}
                                        style={{ whiteSpace: 'normal' }}
                                        key={index}
                                        disabled={opt.disabled || false}
                                        value={opt && opt.value != null && opt.value || opt}>
                                        {opt && opt.label != null && opt.label || opt}
                                    </MenuItem>)
                    }
                </Select>
            </FormControl>
            <div className={classes.error}>
                {touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}
            </div>
        </div>
    )
}
