/**
 * 
 * Auto Complete Field
 * 
 */


import React, { useState, useEffect, useRef } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import Styles from './styles';
import { FormControl, TextField } from '@mui/material';

export default function ({ input, label, placeholder, metaData, options, disabled, style, meta: { touched, error, warning }, defaultValue, handleChange, handleInputChange }) {// eslint-disable-line
    const classes = Styles();
    const [open, setOpen] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [selectedValue, setSelectedValue] = useState("");
    const componentMounted = useRef(true);

    useEffect(() => {
        // input.onChange(defaultValue);
        // const val = options.find(e => e.value == defaultValue)
        // setInputValue(val && val.code || defaultValue);
        setInputValue(defaultValue);
        return () => { // This code runs when component is unmounted
            componentMounted.current = false; // (4) set it to false when we leave the page
        }
    }, [defaultValue]);


    const { id, name, value, onChange } = input;// eslint-disable-line
    const isPreDefinedSet = Array.isArray(options);
    const Options = isPreDefinedSet ? options : metaData[options] || [];

    const onBlur = (e) => {// eslint-disable-line
        onChange(e.target.value)
    }

    return (
        <div className={classes.selectField} style={style || {}}>
            <FormControl className={classes.formControl}>
                <Autocomplete
                    id={id}
                    name={name}
                    open={open}
                    onOpen={() => {
                        // only open when in focus and inputValue is not empty
                        if (inputValue) {
                            setOpen(true);
                        }
                    }}
                    onClose={() => setOpen(selectedValue && selectedValue.label != 'SeeMore' ? false : true)}
                    options={Options || []}
                    getOptionLabel={(option) => option && option.label || ''}
                    autoComplete
                    freeSolo
                    clearOnBlur={false}
                    includeInputInList
                    disabled={disabled}
                    inputValue={inputValue == 'SeeMore' ? '' : inputValue}
                    value={selectedValue}
                    //  onBlur={onBlur}
                    componentsProps={{
                        paper: {
                            sx: {
                                width: 300
                            }
                        }
                    }}
                    onInputChange={(e, value, reason) => {// eslint-disable-line
                        if (value == 'SeeMore' || reason != "reset") {

                            handleInputChange(value)
                            input.onChange(value);
                            setInputValue(value);
                            setOpen(true);
                        }
                        else {
                            setOpen(false);
                        }
                        // handleInputChange(value)
                        // only open when inputValue is not empty after the user typed something
                        if (!value) {
                            setOpen(false);
                        }
                        return value
                    }}
                    onChange={(event, value) => {
                        const selectedValue = value && value.value;
                        if (selectedValue && selectedValue != "SeeMore") {
                            handleChange(selectedValue);
                        }
                        setSelectedValue(value);
                    }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            className={classes.fieldColor}
                            label={label}
                            placeholder={placeholder}
                            multiline
                        />
                    )}
                />
            </FormControl>
            <div className={classes.error}>{touched && ((error && <span style={{ color: "red" }}>{error}</span>) || (warning && <span style={{ color: "red" }}>{warning}</span>))}</div>
        </div>
    )
}

