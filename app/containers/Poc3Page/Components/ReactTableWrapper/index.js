/**
 *
 * ReactTableWrapper
 *
 */

import React, { useEffect, useState } from 'react';
import {
    Grid, Table, TableBody, TableCell, TableHead, TableRow, Paper, TableContainer, Button, Tooltip
} from '@mui/material';
import InputField from '../InputField';
import SelectField from '../SelectField';
import SelectAccordion from '../SelectAccordion';
import { Field, reduxForm, FieldArray } from 'redux-form';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import AlertDialog from 'components/AlertDialog';
import SVG from 'react-inlinesvg';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import differenceWith from 'lodash/differenceWith';// eslint-disable-line
import AutoComplete from '../AutoComplete';
import { searchResult } from '../../../../blocks/poc2/remotes';
// import SelectBtn from '../SelectBtn';

const renderTable = ({ fields, columns, rows, metaData, limit, formValues, formModal, autoRow, showQuestions, defaultField, loading, getTreatmentDiagnosis, rfaOptions, bodySystemOptions, updateField, loadingRecord, enableDelete, favorites }) => {// eslint-disable-line

    const [bodySystemName, setBodySystemName] = useState();
    const [treatmentName, setTreatmentName] = useState([]); // eslint-disable-line
    const [selectValue, setSelectValue] = React.useState(false);// eslint-disable-line

    const [searchICDData, setSearchICDData] = useState([]);
    const [searchCPTData, setSearchCPTData] = useState([]);
    const [searchText, setSearchText] = useState();

    let cptData = [...new Map((searchCPTData.data && (searchCPTData.data).map((e) =>
        e && Object.assign({}, {
            label: e.cpt_code && `${e.cpt_code} - ${e.cpt_name}` || `${e.cpt_name}`,
            value: `${e.cpt_code}`,
            treatmentId: e.treatment_id
        })) || []).map(item =>
            [item['value'], item])).values()];

    let icdData = [...new Map((searchICDData.data && searchICDData.data.map((e) => e && Object.assign({}, {
        label: e.icd_code && `${e.icd_code} - ${e.icd_name}` || `${e.icd_name}`,
        value: `${e.icd_code}`,
        diagnosisId: e.diagnosis_id
    })) || []).map(item => [item['value'], item])).values()];


    if (searchCPTData && searchCPTData.count > 6 && searchCPTData.data && (searchCPTData.data).length < 7) {
        cptData.push(Object.assign({}, {
            label: 'SeeMore',
            value: `SeeMore`,
            treatmentId: searchCPTData.data[0].treatment_id
        }))
    }
    if (searchICDData && searchICDData.count > 6 && searchICDData.data && (searchICDData.data).length < 7) {
        icdData.push(Object.assign({}, {
            label: 'SeeMore',
            value: `SeeMore`,
            diagnosisId: searchICDData.data[0].diagnosis_id,
        }))
    }


    useEffect(() => {
        fields.removeAll();
        if (!loading) {
            if (defaultField && defaultField.length != 0) {
                defaultField.map((e) => fields.push(e));
            } else {
                rows.map(e => fields.push(e));
            }
        }
        // fields.push();
    }, [loading]);

    const settingOther = (val, index) => {
        const formRecord = formValues.form["equipments"] && formValues.form["equipments"][index] || false;
        updateField("reactTableForm", `equipments[${index}]`, {
            diagnosis: formRecord.diagnosis,
            icdCode: formRecord && formRecord.icdCode || "",
            treatments: formRecord.treatments,
            bodySystemId: formRecord.bodySystemId,
            cptCode: formRecord && formRecord.cptCode || "",
            other: val,
            alert: formRecord && formRecord.alert || "",
            alertContent: formRecord && formRecord.alertContent || "",
            addOn: formRecord && formRecord.addOn || false,
            otherInformation: formRecord && formRecord.otherInformation || ""
        })
    }

    let btn = {
        border: 'none',
        background: 'transparent',
        cursor: 'pointer'
    }

    const handleICDChange = (val, index) => {

        const ICD = icdData.filter((e) => e && e.value === val);
        const diagnosisId = ICD[0].diagnosisId || '';
        const other1Options = rfaOptions.filter((e1) => e1.diagnosisName != "*" && e1.diagnosisName != "NoDiagnosisFound" && e1.bodySystemId == formValues.form["equipments"][index].bodySystemId && e1.treatmentsId == formValues.form["equipments"][index].treatments && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.diagnosisName, label: e.diagnosisName, type: e.type })).sort((a, b) => a.label.localeCompare(b.label));
        // const other = vendors.filter((e) => e.value === ).length > 0;
        const selectedDiagnosis = rfaOptions.find((e) => e.diagnosisId == diagnosisId);
        const treatmentLabel = formValues.form && formValues.form["equipments"][index].treatments && rfaOptions.filter((e1) => e1.treatmentsId == formValues.form["equipments"][index].treatments && e1.type != "surgery-add-on").length != 0 && rfaOptions.filter((e1) => e1.treatmentsId == formValues.form["equipments"][index].treatments && e1.type != "surgery-add-on")[0].treatmentsName || "";

        const formRecord = formValues.form["equipments"] && formValues.form["equipments"][index] || false;

        formValues.form["equipments"][index].other = selectedDiagnosis && selectedDiagnosis != -1 && !(other1Options.filter((e) => e.value === selectedDiagnosis.diagnosisName).length > 0) || formRecord && formRecord.other || "";
        formValues.form["equipments"][index].icdCode = val || "";

        // updateField("reactTableForm", `equipments[${index}]`, {
        //     diagnosis: formRecord.diagnosis,
        //     icdCode: val || "",
        //     treatments: formRecord.treatments,
        //     bodySystemId: formRecord.bodySystemId,
        //     cptCode: formRecord && formRecord.cptCode || "",
        //     other: selectedDiagnosis && selectedDiagnosis != -1 && !(other1Options.filter((e) => e.value === selectedDiagnosis.diagnosisName).length > 0) || formRecord && formRecord.other || "",
        //     alert: formRecord && formRecord.alert || "",
        //     alertContent: formRecord && formRecord.alertContent || "",
        //     addOn: formRecord && formRecord.addOn || false,
        //     otherInformation: formRecord && formRecord.otherInformation || ""
        // });
        showQuestions({ treat: formRecord.treatments || "", diag: selectedDiagnosis && selectedDiagnosis != -1 && selectedDiagnosis.diagnosisName || formRecord.diagnosis, index: index, total: formRecord.length, bodySystem: formRecord.bodySystemId, label: treatmentLabel || '', addOn: false, other: selectValue })
    }

    const handleCPTChange = (val, index) => {

        const CPT = cptData.filter((e) => e && e.value === val);

        const treatmentID = CPT[0].treatmentId || '';

        const formRecord = formValues.form["equipments"] && formValues.form["equipments"][index] || false;
        updateField("reactTableForm", `equipments[${index}]`, {
            diagnosis: formRecord.diagnosis,
            icdCode: formRecord && formRecord.icdCode || "",
            treatments: treatmentID,
            bodySystemId: formRecord.bodySystemId,
            cptCode: val || "",
            other: formRecord && formRecord.other || "",
            alert: formRecord && formRecord.alert || "",
            alertContent: formRecord && formRecord.alertContent || "",
            addOn: formRecord && formRecord.addOn || false,
            otherInformation: formRecord && formRecord.otherInformation || ""
        });
    }

    const handleInputChange = async (value, index, name) => {

        if (value != 'SeeMore') {
            setSearchText(value)
        }
        const cptUrl = 'rest/treatments/getAllCPTCodesAndNames';

        const icdUrl = 'rest/treatments/getAllICDCodesAndNames';

        const body_system = formValues && formValues.form["equipments"] && formValues.form["equipments"][index].bodySystemId;

        if (body_system && value) {
            const ResultData = await searchResult(name === 'cptCode' ? cptUrl : name === 'icdCode' ? icdUrl : null, Object.assign({}, {
                body_system_id: body_system,
                search: value === 'SeeMore' ? searchText : value,
                see_more: value === 'SeeMore' ? true : false,
                Accept: 'application/json',
                'Content-Type': 'application/json',
            })).then((res) => {
                return res
            }).catch((err) => {
                console.log("err = ", err);
            })
            if (name === 'cptCode') {
                setSearchCPTData(ResultData)
            }
            else if (name === 'icdCode') {
                setSearchICDData(ResultData)
            }
        }
        else if (body_system && value === '') {
            name === 'cptCode' ? setSearchCPTData([]) : name === 'icdCode' ? setSearchICDData([]) : null
        }

    }

    const rowValue = (column, records, i) => columns.map((col, ind) => {
        if (col.type == "select") {

            const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type }));

            const treatmentAddOnOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.type == "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type }));

            //Uncommand it for favorite options 
            // ======================================================================================
            // const bodySystemFavorites = favorites.map((e) => {
            //     const filteredRecord = bodySystemOptions.filter(e1 => e1.value == e.body_system_id);
            //     return Object.assign({}, { label: filteredRecord.length && filteredRecord[0].label || "", value: e.body_system_id, index: filteredRecord.length && filteredRecord[0].index || "" })
            // });

            // const bodySystemNoFavorites = differenceWith(bodySystemOptions, bodySystemFavorites, function (arrVal, othVal) {
            //     return arrVal.value === othVal.value;
            // });

            // const treatmentFav = favorites.filter((e1) => e1.body_system_id == formValues.form["equipments"][i].bodySystemId)[0];
            // const treatmentFavorites = treatmentFav && treatmentFav.treatments && treatmentFav.treatments.map((e) => {
            //     return Object.assign({}, { label: e.name || "", value: e.id, type: e.type || "" })
            // }) || [];

            // const treatmentNoFavorites = differenceWith(treatmentOpt, treatmentFavorites, function (arrVal, othVal) {
            //     return arrVal.value === othVal.value;
            // });

            // const treatmentAddonFavorites = treatmentFav && treatmentFav.treatments && treatmentFav.treatments.map((e) => {
            //     return Object.assign({}, { label: e.name || "", value: e.id, type: e.type || "" })
            // }) || [];

            // const treatmentAddonNoFavorites = differenceWith(treatmentAddOnOpt, treatmentAddonFavorites, function (arrVal, othVal) {
            //     return arrVal.value === othVal.value;
            // });


            // const bodySystemOpt = Object.assign({}, { options1: bodySystemFavorites, options2: bodySystemNoFavorites });

            // const treatmentOptions = Object.assign({}, { options1: treatmentFavorites.filter((ele, ind) => ind === treatmentFavorites.findIndex(elem => elem.value === ele.value)).sort((a, b) => a.label.localeCompare(b.label)), options2: treatmentNoFavorites.filter((ele, ind) => ind === treatmentNoFavorites.findIndex(elem => elem.value === ele.value)).sort((a, b) => a.label.localeCompare(b.label)) });

            // const treatmentAddonOptions = Object.assign({}, { options1: treatmentAddonFavorites, options2: treatmentAddonNoFavorites });

            // const empty = { options1: [], options2: [] }
            // ====================================================================================
            //

            return <TableCell key={`${i}_${ind}`} sx={{
                '&.MuiTableCell-root': {
                    padding: '6px 8px',
                }
            }} align="center"><Field
                    name={`${column}.${col.value}`}
                    label={`diagnosis`}
                    type='text'
                    component={SelectField}
                    loadingRecord={loadingRecord}
                    removeIcon={(limit && limit <= i) || (col.value == 'treatments' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined)) || (col.value == 'diagnosis' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined)) || ((col.value == 'treatments' || col.value == "bodySystemId") && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].diagnosis == "Surgery Add-on"))}

                    // disabled={(limit && limit <= i) || (col.value == 'treatments' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined)) || (col.value == 'diagnosis' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined)) || ((col.value == 'treatments' || col.value == "bodySystemId") && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].diagnosis == "Surgery Add-on"))}

                    onChange={col.value == `bodySystemId` ? (value) => {
                        formValues.form['equipments'][i].treatments = '';
                        formValues.form['equipments'][i].alert = '';
                        formValues.form['equipments'][i].other = false;
                        formValues.form['equipments'][i].diagnosis = "";
                        getTreatmentDiagnosis(value);
                        setBodySystemName(value)
                        autoRow({ keyName: col.value, index: i, value: value })

                    } : (col.value == `treatments`) ? (value) => {
                        // formValues.form['equipments'][i].diagnosis = '';
                        formValues.form['equipments'][i].alert = '';
                        formValues.form['equipments'][i].other = false;
                        autoRow({ keyName: col.value, index: i, value: value, onChange: true })
                    } : (col.value == `diagnosis` && formValues.form["equipments"][i].treatments && col.value != `treatments`) ? ((e) => showQuestions({ treat: formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].treatments || "", diag: e, index: i, total: records.length, bodySystem: formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].bodySystemId || bodySystemName, label: metaData[col.options].find((e1) => e1 && e1.value == e) && metaData[col.options].find((e1) => e1 && e1.value == e).label || '', addOn: false })) : ((e) => autoRow({ keyName: col.value, index: i, value: e }))}
                    // Uncommand it for favorite
                    // ======================================
                    // options={(col.value == `treatments` && typeof (records[i].addOn) == 'number') ? (treatmentAddonOptions && treatmentAddonOptions || empty) : (col.value == `diagnosis` && typeof (records[i].addOn) == 'number') ? [{ label: 'Surgery Add-on', value: 'Surgery Add-on' }] : (col.value == `treatments`) ? rfaOptions.length > 0 && formValues.form && formValues.form["equipments"][i].bodySystemId && treatmentOptions || empty : (bodySystemOpt && bodySystemOpt || empty)}
                    // =======================================
                    //

                    // Command it for favorite
                    // =====================================
                    options={(col.value == `treatments` && typeof (records[i].addOn) == 'number') ? (treatmentAddOnOpt && treatmentAddOnOpt || []) : (col.value == `diagnosis` && typeof (records[i].addOn) == 'number') ? [{ label: 'Surgery Add-on', value: 'Surgery Add-on' }] : (col.value == `treatments`) ? rfaOptions.length > 0 && formValues.form && formValues.form["equipments"][i].bodySystemId && rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type })).filter((ele, ind) => ind === treatmentOpt.findIndex(elem => elem.value === ele.value)).sort((a, b) => a.label.localeCompare(b.label)) || [] : (bodySystemOptions && bodySystemOptions || [])}
                // =====================================
                />
            </TableCell>
        }

        else if (col.type == "selectAccordion") {

            let other2Options = []
            let other1Options = []

            const treatmentAddOnOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.type == "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type }));

            if (col.value == `diagnosis`) {
                other2Options = rfaOptions.filter((e1) => e1.diagnosisName != "*" && e1.diagnosisName != "NoDiagnosisFound" && e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.treatmentsId != formValues.form["equipments"][i].treatments && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.diagnosisName, label: e.diagnosisName, type: e.type })).sort((a, b) => a.label.localeCompare(b.label))

                other1Options = rfaOptions.filter((e1) => e1.diagnosisName != "*" && e1.diagnosisName != "NoDiagnosisFound" && e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.treatmentsId == formValues.form["equipments"][i].treatments && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.diagnosisName, label: e.diagnosisName, type: e.type })).sort((a, b) => a.label.localeCompare(b.label))

            }
            // if (col.value == `treatments` && typeof (records[i].addOn) == 'number'){
            //     other2Options = treatmentAddOnOpt && treatmentAddOnOpt || []
            // }
            if (col.value == `treatments` && typeof (records[i].addOn) != 'number') {
                other2Options = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type })).sort((a, b) => a.label.localeCompare(b.label)) || [];

                other1Options = formValues.form["equipments"][i].bodySystemId &&
                    formValues.form["equipments"][i].diagnosis ? rfaOptions.filter((e1) => e1 && e1.bodySystemId == formValues.form["equipments"][i].bodySystemId && e1.diagnosisName == formValues.form["equipments"][i].diagnosis && e1.type != "surgery-add-on").map((e) => Object.assign({}, { value: e.treatmentsId, label: e.treatmentsName, type: e.type })).sort((a, b) => a.label.localeCompare(b.label)) : []
            }


            const otherOptions = [...other2Options, ...other1Options];

            const optionForDiagnosis = (col.value == `diagnosis` && typeof (records[i].addOn) == 'number') ? Object.assign({}, { options1: [{ label: 'Surgery Add-on', value: 'Surgery Add-on' }] }, { options2: [] }) : (col.value == `treatments` && typeof (records[i].addOn) == 'number') ? Object.assign({}, { options1: treatmentAddOnOpt || [] }, { options2: [] }) : formValues.form && Object.assign({},
                {
                    options1: col.value == 'diagnosis' ? formValues && formValues.form["equipments"] && formValues.form["equipments"][i].treatments ? other1Options : otherOptions.filter((ele, ind) => ind === otherOptions.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label)) : col.value == 'treatments' ? formValues && formValues.form["equipments"] && formValues.form["equipments"][i].diagnosis ? other1Options : otherOptions.filter((ele, ind) => ind === otherOptions.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label)) : []
                },
                {
                    options2: otherOptions.filter((ele, ind) => ind === otherOptions.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label))
                }) || { options1: [], options2: [] };

            // console.log("22otherOptions", optionForDiagnosis)

            // Uncommand it for favorite
            // ======================================
            // const otherOptionNoDuplicate = otherOptions.filter((ele, ind) => ind === otherOptions.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label));

            // const diagnosisFav = favorites.filter((e1) => e1.body_system_id == formValues.form["equipments"][i].bodySystemId)[0];
            // const diagnosisFavorites = diagnosisFav && diagnosisFav.diagnoses && diagnosisFav.diagnoses.map((e) => {
            //     const filteredRecord = otherOptionNoDuplicate.find(e1 => e1.value == e);
            //     return Object.assign({}, { label: filteredRecord && filteredRecord.label || "", value: filteredRecord && filteredRecord.value || "", type: filteredRecord && filteredRecord.type || "" })
            // }) || [];

            // const diagnosisNoFavorites = differenceWith(other1Options, diagnosisFavorites, function (arrVal, othVal) {
            //     return arrVal.value === othVal.value;
            // });

            // const diagnosisNoOtherFavorites = differenceWith(otherOptionNoDuplicate, diagnosisFavorites, function (arrVal, othVal) {
            //     return arrVal.value === othVal.value;
            // });

            // const optionForDiagnosis = (col.value == `diagnosis` && typeof (records[i].addOn) == 'number') ? Object.assign({}, { options1: diagnosisFavorites }, { options2: [{ label: 'Surgery Add-on', value: 'Surgery Add-on' }] }, {
            //     options3: []
            // }) : formValues.form && formValues.form["equipments"][i].treatments && Object.assign({}, { options1: diagnosisFavorites }, { options2: diagnosisNoFavorites }, {
            //     options3: diagnosisNoOtherFavorites.filter((ele, ind) => ind === diagnosisNoOtherFavorites.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label))
            // }) || { options1: [], options2: [], options3: [] };
            // =======================================
            //

            const treatmentLabel = formValues.form && formValues.form["equipments"][i].treatments && rfaOptions.filter((e1) => e1.treatmentsId == formValues.form["equipments"][i].treatments && e1.type != "surgery-add-on").length != 0 && rfaOptions.filter((e1) => e1.treatmentsId == formValues.form["equipments"][i].treatments && e1.type != "surgery-add-on")[0].treatmentsName || ""

            return <TableCell key={`${i}_${ind}`} sx={{
                '&.MuiTableCell-root': {
                    padding: '6px 8px',
                }
            }} align="center"><Field
                    name={`${column}.${col.value}`}
                    label={`diagnosis`}
                    type='text'
                    // component={SelectBtn}
                    component={SelectAccordion}
                    removeIcon={((col.value == 'treatments' || col.value == 'diagnosis') && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined)) || ((col.value == 'diagnosis' || col.value == 'treatments') && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].diagnosis == "Surgery Add-on"))}

                    selectedDiagnosis={col.value == 'treatments' && formValues.form && formValues.form["equipments"][i] && rfaOptions.filter(e => formValues.form["equipments"][i].treatments == e.treatmentsId)[0] && rfaOptions.filter(e => formValues.form["equipments"][i].treatments == e.treatmentsId)[0].treatmentsName || col.value == 'diagnosis' && formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].diagnosis || ""}
                    // disabled={(col.value == 'treatments' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined)) || (col.value == 'diagnosis' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined)) || (col.value == 'diagnosis' && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].diagnosis == "Surgery Add-on"))}

                    disabled={((col.value == 'treatments' || col.value == 'diagnosis') && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined)) || ((col.value == 'diagnosis' || col.value == 'treatments') && formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].diagnosis == "Surgery Add-on"))}

                    settingOther={(val) => settingOther(val, i)}

                    selectedOther={formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].other || false}

                    onChange={col.value == `bodySystemId` ? (value) => {
                        setBodySystemName(value)
                    } : (col.value == `treatments`) ? (value) => {
                        setTreatmentName(value);
                        // formValues.form['equipments'][i].diagnosis = '';
                        // formValues.form['equipments'][i].alert = '';
                        // formValues.form['equipments'][i].other = false;
                        autoRow({ keyName: col.value, index: i, value: value, onChange: true })

                        const diagnoses = formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].diagnosis;
                        if (diagnoses) {
                            showQuestions({ treat: value || "", diag: records[i].diagnosis, index: i, total: records.length, bodySystem: records[i].bodySystemId || bodySystemName, label: treatmentLabel || '', addOn: false, other: selectValue })
                        }
                    } : (col.value == `diagnosis` && formValues.form["equipments"][i].treatments && col.value != `treatments`) ? ((e) => showQuestions({ treat: records[i].treatments || "", diag: e, index: i, total: records.length, bodySystem: records[i].bodySystemId || bodySystemName, label: treatmentLabel || '', addOn: false, other: selectValue })) : ((e) => autoRow({ keyName: col.value, index: i, value: e }))}

                    tothers={col.value == `diagnosis` && formValues.form && formValues.form["equipments"][i] && !formValues.form["equipments"][i].treatments ? true : col.value == `treatments` && formValues.form && formValues.form["equipments"][i] && !formValues.form["equipments"][i].diagnosis ? true : false}

                    options={((col.value == `diagnosis` || col.value == `treatments`) ? optionForDiagnosis : (bodySystemOptions && bodySystemOptions || []))}
                />
            </TableCell>
        }
        else if (col.type == "input") {
            const disableEnable = (formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined) && (formValues.form["equipments"][i].diagnosis == "" || formValues.form["equipments"][i].diagnosis == undefined));
            return <TableCell key={`${i}_${ind}`} sx={{
                '&.MuiTableCell-root': {
                    padding: '6px 8px',
                }
            }} align="center">{(col.value == "approval" && disableEnable && typeof (records[i].addOn) != 'number') ? <div></div> : ((col.value == "approval" && !disableEnable) || (column.value == "approval" && typeof (records[i].addOn) == 'number')) ? <div style={{ width: '100%', textAlign: 'center' }}>{records.length != 0 && records[i] != undefined ? records[i].alert == "success" ? <button style={btn} type="button" onClick={() => formModal(records[i].index, i, records[i].addOn)}>

                <CheckCircleOutlineIcon sx={{ color: "green", fontSize: 26 }} />
            </button> : records[i].alert == "error" ? <button style={btn} type="button" onClick={() => formModal(records[i].index, i, records[i].addOn)}>
                <CancelOutlinedIcon sx={{ color: "#fb0404", fontSize: 26 }} />
            </button> : records[i].alert == "warning" ? <button style={btn} type="button" onClick={() => formModal(records[i].index, i, records[i].addOn)}>
                <CancelOutlinedIcon sx={{ color: "#f38b06", fontSize: 26 }} />
            </button> : "" : ""} </div> : <Field
                name={`${column}.${col.value}`}
                label={`${col.label}`}
                type='input'
                component={InputField}
                disabled={(limit && limit <= i)}
                onChange={((e) => autoRow({
                    bodySystemName: formValues.form["equipments"][i].bodySystemId,
                    keyName: col.value, index: formValues.form && formValues.form["equipments"][i] && (typeof (formValues.form["equipments"][i].index) == 'number' ? formValues.form["equipments"][i].index : i),
                    value: e,
                    alert: formValues.form && formValues.form["equipments"][i].alert,
                    alertContent: formValues.form && formValues.form["equipments"][i].alertContent,
                    addOn: formValues.form && formValues.form["equipments"][i] && typeof (formValues.form["equipments"][i].addOn) == 'number' ? formValues.form["equipments"][i].addOn : false
                })
                )}
            />}
            </TableCell>
        } else if (col.type == "button") {
            const disableEnable = (formValues.form && formValues.form["equipments"][i] &&
                (formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined) &&
                (formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined) &&
                (formValues.form["equipments"][i].diagnosis == "" || formValues.form["equipments"][i].diagnosis == undefined));
            const data = formValues.form && formValues.form["equipments"][i] && (formValues.form["equipments"][i]) || {};
            return <TableCell key={`${i}_${ind}`} sx={{
                '&.MuiTableCell-root': {
                    padding: '6px 8px',
                }
            }} align="center">
                {!disableEnable ? <AlertDialog
                    description={"Do you want to delete this entry?"}
                    onConfirm={() => autoRow({ alert: data.alert, alertContent: data.alertContent, index: data.index, addOn: data.addOn, remove: true, treatments: data.treatments, keyName: "treatments" })}
                    onConfirmPopUpClose={true}
                    btnLabel1='Delete'
                    btnLabel2='Cancel' >
                    {(open) => <Button
                        style={{
                            justifyContent: 'right',
                            padding: '0px',
                            minWidth: 'fit-content',
                            cursor: disableEnable ? 'not-allowed' : 'pointer',
                            opacity: disableEnable ? 0.5 : 1
                        }}

                        disabled={disableEnable}
                        onClick={open}
                    ><SVG src={require('images/icons/delete.svg')} style={{ justifyContent: 'flex-end' }} /></Button>}
                </AlertDialog> : null}
            </TableCell>
        } else if (col.type == 'autoComplete') {
            return <TableCell key={`${i}_${ind}`} sx={{
                '&.MuiTableCell-root': {
                    padding: '6px 8px',
                }
            }} align="center"><Field
                    name={`${column}.${col.value}`}
                    label={``}
                    type='text'
                    component={AutoComplete}
                    loadingRecord={loadingRecord}

                    disabled={(formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].bodySystemId == "" || formValues.form["equipments"][i].bodySystemId == undefined) || (col.value === 'icdCode' && formValues.form["equipments"][i].treatments == "" || formValues.form["equipments"][i].treatments == undefined)}

                    defaultValue={col.value === 'cptCode' && formValues && formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].cptCode || col.value === 'icdCode' && formValues && formValues.form && formValues.form["equipments"][i] && formValues.form["equipments"][i].icdCode || ''}
                    handleInputChange={(value) => handleInputChange(value, i, col.value)}
                    options={col.value === 'cptCode' ? cptData : col.value === 'icdCode' ? icdData : []}
                    removeIcon={false}
                    handleChange={col.value === 'cptCode' ? (val) => handleCPTChange(val, i, col.value) : col.value === 'icdCode' ? (val) => handleICDChange(val, i, col.value) : null}
                />
            </TableCell>
        }
    })

    return (
        <TableContainer component={Paper} sx={{ boxShadow: 'none' }} >
            <Table
                size="small"
                aria-label="simple table"
                sx={{ minWidth: 600, "& th": { wordBreak: 'keep-all' } }}
            >
                <TableHead>
                    <TableRow>
                        {columns.map((col, ind) =>
                            <TableCell key={ind} align="center"
                                style={{ fontFamily: "MyriadPro-Semibold" }}
                                sx={{
                                    '&.MuiTableCell-root': {
                                        fontSize: '16px',
                                        color: '#1d1e1c',
                                        padding: '6px 8px',
                                        borderBottom: '1px solid rgba(224, 224, 224, 1)',
                                        fontWeight: 'normal'
                                    }
                                }}
                            >{col.label}
                                {(col.label == "Body System" || col.label == "Service/Good Requested (Required)" || col.label == "Diagnosis (Required)") ?
                                    <Tooltip
                                        arrow
                                        componentsProps={{
                                            tooltip: {
                                                sx: {
                                                    background: '#dddddd',
                                                    color: '#1d1e1c',
                                                    fontSize: '14px',
                                                    marginLeft: '5px !important',
                                                    '& .MuiTooltip-arrow': {
                                                        color: '#dddddd',
                                                        transform: 'translate(1px, 2px) !important'
                                                    }
                                                },
                                            },
                                        }}
                                         title={(col.label == "Body System") ? 'Currently, only four body systems are supported.' : col.label == "Service/Good Requested (Required)" ? 'List shows services covered in policies for the select body part.' : 'List shows diagnosis covered in policies for the requested service. Select “Others” for full list'}
                                        placement="right-start"
                                    >
                                        <InfoOutlinedIcon
                                            style={{
                                                color: '#3c89c9',
                                                height: '16px',
                                                cursor: 'pointer'
                                            }}
                                        />
                                    </Tooltip>
                                    : null
                                }</TableCell>)}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {fields.map((column, i) => {
                        return <TableRow key={i} hover={true}>

                            {rowValue(column, rows, i).map((e) =>
                                e)}
                        </TableRow>
                    }
                    )}
                </TableBody>
            </Table></TableContainer>)
}

function ReactTable(props) {
    const { name, records, columns, children, path, locationState = {}, metaData = {}, loading, formValues, showQuestions, settings, alertIcon, updateField, ind, initialValues, autoRow, formModal, submitting, handleSubmit, limit, destroy, initialData, getTreatmentDiagnosis, rfaOptions, bodySystemOpt, loadingRecord, getRfaRecords, enableDelete, favorites } = props;// eslint-disable-line
    const tableColumns = columns;
    const [valueBack, setValueBack] = useState({});

    useEffect(() => {
        return () => destroy();
    }, []);

    useEffect(() => {
        if (Object.keys(valueBack).length != 0) {
            autoRow(valueBack);
            getRfaRecords(valueBack);
        }
    }, [valueBack]);

    const bodySystemOptionsList = [
        { label: "Ankle and Foot Disorders (TBD)", value: "Ankle and Foot Disorders (TBD)", disabled: true },
        { label: "Elbow Disorders (TBD)", value: "Elbow Disorders (TBD)", disabled: true },
        { label: "Eye Disorders (TBD)", value: "Eye Disorders (TBD)", disabled: true },
        { label: "Hand, Wrist and Forearm Disorders (TBD)", value: "Hand, Wrist and Forearm Disorders (TBD)", disabled: true },
         { label: "Hip and Groin Disorders (TBD)", value: "Hip and Groin Disorders (TBD)", disabled: true },
        { label: "Interstitial Lung Disease (TBD)", value: "Interstitial Lung Disease (TBD)", disabled: true },
    ]

     const bodySystemOptions = [...(bodySystemOpt.filter((ele, ind) => ind === bodySystemOpt.findIndex(elem => elem.label === ele.label)).sort((a, b) => a.label.localeCompare(b.label)).filter(el => el.label == "Low Back" || el.label == "Shoulder Disorders" || el.label == "Neck and Upper Back" || el.label == "Knee Disorders")), ...bodySystemOptionsList];

    const addingRows = (e) => {
        setValueBack(e);

    }

    const showingQuestions = (e) => {
        showQuestions(e);
        const formRecord = formValues.form["equipments"][e.index] || false;
        updateField("reactTableForm", `equipments[${e.index}]`, {
            diagnosis: e.diag,
            icdCode: formRecord && formRecord.icdCode || "",
            treatments: e.treat,
            index: e.index,
            bodySystemId: e.bodySystem,
            cptCode: formRecord && formRecord.cptCode || "",
            other: formRecord.other,
            alert: formRecord && formRecord.alert || "",
            alertContent: formRecord && formRecord.alertContent || "",
            addOn: formRecord && formRecord.addOn || false,
            otherInformation: formRecord && formRecord.otherInformation || ""
        })
    }

    return (
        <Grid container>
            <Grid item xs={12} className={name}>
                <form onSubmit={handleSubmit}>
                    <FieldArray name="equipments" component={renderTable}
                        formValues={formValues}
                        columns={tableColumns}
                        rows={initialData}
                        metaData={metaData}
                        limit={limit}
                        formModal={formModal}
                        autoRow={addingRows}
                        showQuestions={showingQuestions}
                        defaultField={initialData}
                        loading={loading}
                        getTreatmentDiagnosis={getTreatmentDiagnosis}
                        updateField={updateField}
                        bodySystemOptions={bodySystemOptions}
                        rfaOptions={rfaOptions}
                        loadingRecord={loadingRecord}
                        enableDelete={enableDelete}
                        favorites={favorites}
                    />
                </form>
            </Grid>
        </Grid>
    );
}

 //  }
 
export default reduxForm({
    form: 'reactTableForm',
    enableReinitialize: true,
    touchOnChange: true
})(ReactTable);
