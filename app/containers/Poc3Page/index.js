/**
 * 
 * Poc3 Page
 * 
 */


import React, { useEffect, memo, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Grid, Alert, Button, AppBar, Snackbar } from '@mui/material';
import Styles from './styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import ReactTable from './Components/ReactTableWrapper';
// import PropTypes from 'prop-types';
import {
    selectUser, selectSessionExpand
} from 'blocks/session/selectors';
import { change } from 'redux-form';
import ModalForm from './Components/ModalForm'
import Warning from '@mui/icons-material/Warning';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import AddOnForm from './Components/AddOnForm';
import { handlePdf } from '../DwcPage/pdfmaker';
import Spinner from 'components/Spinner';
import { Link } from 'react-router-dom';
import AlertDialog from '../../components/AlertDialog';
import { FormattedMessage } from 'react-intl';
import schema from 'routes/schema';
import ModalRecordForm from 'components/ModalRecordForm';
import SnackbarComp from 'components/Snackbar';
import { getOffset, getDefaultHeaders } from 'utils/tools';
import moment from "moment";
import { getUploadPdfUrl, uploadingAttachment } from 'blocks/poc2/remotes';// eslint-disable-line
import DialogForm from '../../components/DialogForm';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import isEqual from 'lodash/isEqual';
const CryptoJS = require("crypto-js");
const base64 = require('base-64');
import ButtonSpinner from 'components/ButtonSpinner';
import { uploadFile } from 'utils/api';

/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 * @param {array} filterColumns 
 * @param {boolean} create 
 * @param {boolean} view 
 */
export default function (name, path, columns, actions, selectors, create, view) {// eslint-disable-line


    const {
        selectLoading,
        selectPocMetaData,
        selectError,
        selectPoc2PoliciesDoc,
        selectPoc2AddedDoc,
        selectPocAlert,
        selectTreatments,
        selectRecord,
        selectUpdate,
        selectPageLoader,
        selectUpdateError,
        selectAdministrator,
        selectAllTreatmentsAndDiagnosis,
        selectbodySystemNames,
        selectPhysicians
    } = selectors;

    /**
     * @param {object} props 
     * 
     */

    function Poc3Page(props) {
        const classes = Styles();
        const { dispatch, location = {}, history, loading, metaData = {}, queryParams, user, formValues, addedPolicies, policies, updateField, settings, record, match, update, pageLoader, expand, updateError, administrator, rfaOptions, bodySystemList, alerting, favorites } = props;// eslint-disable-line
        const { pathname } = location;
        const activeChildren = path !== pathname;
        const theme = useTheme();
        const entry = (formValues && formValues.errors && formValues.errors && formValues.errors.entry && formValues.errors.entry)
        const sm = useMediaQuery(theme.breakpoints.up('sm'));
        const sm1 = useMediaQuery(theme.breakpoints.down('sm'));
        const componentMounted = useRef(true);
        const [showModal, setModalOpen] = useState(false);
        const [alertContents, setAlertContent] = useState("");
        const [alerter, setAlertOpen] = useState(false);
        const [colorContent, setColor] = useState("#fff");
        const [icon, setIcon] = useState(false);
        const [title, setTitle] = useState(false);
        const [treatment, setTreatment] = useState("");
        const [diagnosis, setDiagnosis] = useState("");
        const [ind, setIndex] = useState("");
        const [dialogType, setType] = useState("");
        const [initialValue, setInitialValue] = useState({});
        const [showAddOn, setShowAddOn] = useState(false);
        const [addOn, setAddOn] = useState(false);
        const [addOnTitle, setAddOnTitle] = useState("");
        const [showAlertAddOn, setShowAlertAddOn] = useState(false);
        const [addOnModal, showAddOnModal] = useState(false);
        const [modalRecord, setModalRecord] = useState(false);
        // const [width, setWidth] = useState(window.innerWidth);// eslint-disable-line
        const [uploading, setUploading] = useState(false);
        const [uploadModal, setUploadModal] = useState(false);
        const [clearing, setClear] = useState(false);
        const [dialogError, setDialogError] = useState(false);
        const navbarWidth = expand ? 158 : 60;
        const successMessage = update && (update.updateSuccess || update.successUpload) || false;
        const errorMessage = update && (update.errorUpload) || updateError && updateError || false;
        const [adminAssignedValue, setAdminAssignedValue] = useState(null);// eslint-disable-line
        const generateRfaColumn = schema().generateRfa(user, record, formValues.errors, path, entry).columns;
        const casesColumns = schema().cases(user).columns;
        const dataLength = [1, 2, 3, 4, 5];
        const [bodySystemValue, setBodySystemValue] = useState();
        const bodySystemOpt = bodySystemList.map((e) => Object.assign({}, { label: e.name, value: e.id, index: e.index }));
        const data = dataLength.map((e, i) => Object.assign({}, {// eslint-disable-line
            diagnosis: "",
            icdCode: "",
            treatments: "",
            cptCode: "",
            otherInformation: "",
            addOn: false,
            alert: '',
            alertContent: '',
            other: false

        }));

        const [loadingRecord, isLoadingRecord] = useState(false);

        const [saveLoading, setSaveLoading] = useState(false);

        useEffect(() => {
            if (path == "/claims") {
                updateField('reactTableForm', 'equipments', data);
                dispatch(actions.clearRecord());
            }
            dispatch(actions.loadPocMetaData('Neck and Upper Back'));
            dispatch(actions.getAllBodySystems());


            if (path != "/claims") {
                dispatch(actions.loadRecord(match.params.id));
            }
            dispatch(actions.updatePoc3(false));
            // dispatch(actions.loadFavoritesList());

            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, []);

        useEffect(() => {
            updateField("dialogForm", "claim_admin_details", [administrator]);

        }, [administrator]);

        const handleClear = () => {
            updateField('reactTableForm', 'equipments', data)
            if (path != "/claims") {
                dispatch(actions.updateRecord(Object.assign({}, record['case_data'], { form_doc: null }), 'clearForm'));
            }
            setClear(true);
        }

        const physicianChange = (val) => {
            dispatch(actions.loadPhysician(val))
        }
        const AdministratorChange = (val) => {
            if (val != "others") {
                dispatch(actions.loadAdministrator(val))
            } else {
                if (!record.is_claims_admin && record.claims_admin_id) {
                    dispatch(actions.loadAdministrator(record.claims_admin_id));
                } else {
                    dispatch(actions.clearAdministrator({}));
                }
            }
        }
        const handleEditRecord = (data, dispatch, { form }) => {

            dispatch(actions.updateRecord(Object.assign({}, data, { form_doc: JSON.stringify(record['form_doc']) }), form));
            setModalRecord(false)

        };

        const loadCases = () => {
            dispatch(actions.setHeadersData(getDefaultHeaders()));
            dispatch(actions.loadRecords(true, queryParams));
        }

        const deleteRecord = () => {
            dispatch(actions.deleteRecord(record['case_data'].id));
        };

        const loadDialogForm = () => {
            if (!record.is_claims_admin && record.claims_admin_id != null) {
                dispatch(actions.loadAdministrator(record.claims_admin_id));
            }
            dispatch(actions.loadPocMetaData(''));
            if (user.role == "staff" && record.assignee_id) {
                dispatch(actions.loadPhysician(record.assignee_id));
            }
            setUploadModal(true);
        }

        let deleteConfirmation = `Are you sure you want to delete this ${name == 'rfaForm' ? 'RFA form' : name || ""}?`;

        let clearConfirmation = 'Do you want to clear all entries in this form?';

        const mergeUpload = (obj, buf) => {// eslint-disable-line
            (async () => {
                await getUploadPdfUrl(obj).then((result) => {
                    const { uploadURL, s3_file_key, public_url } = result;// eslint-disable-line
                    if (uploadURL != undefined) {
                        const ajax = new XMLHttpRequest();
                        ajax.addEventListener("error", () => setDialogError('Upload Failed'), false);
                        ajax.addEventListener("abort", () => setDialogError('Upload Failed'), false);
                        ajax.onreadystatechange = function () {
                            if (ajax.readyState == 4 || ajax.status == 200) {
                                window.open(public_url, '_blank');
                                setUploading(false);
                                dispatch(actions.rwcFormMergeSuccess({ successUpload: 'RFA Form generated' }));
                            }
                        };
                        ajax.open("PUT", uploadURL, true);
                        ajax.setRequestHeader("Content-Type", "application/pdf");
                        ajax.send(buf);
                    } else {
                        setDialogError('Upload Failed');
                    }
                })
            })();
        }

        async function generateRfa(files, data, settings, requestedTreatmentDoc, pdfArr, recordDuplicate) {// eslint-disable-line
            await files.getBlob((blob) => {
                (async () => {
                    await uploadingAttachment(Object.assign({}, {
                        "file_name": "rfa_form.pdf",
                        "path_identifier": "rfa_form",
                        "content_type": "application/pdf",
                        "rfa_form_id": record['case_data'] && record['case_data'].id || recordDuplicate && recordDuplicate.id || false
                    })).then(async (result) => {
                        if (result) {
                            return await uploadFile(result.uploadURL, blob, "application/pdf").then(async () => {
                                await dispatch(actions.rwcFormDownload({
                                    formPayLoad: Object.assign({}, Object.assign({}, recordDuplicate && { case_data: record['case_data'] || recordDuplicate } || recordDuplicate, { form_doc: JSON.stringify(requestedTreatmentDoc) }), { "id": record['case_data'] && record['case_data'].id || recordDuplicate.id, request_type: JSON.parse(data.request_type), "assignee_id": data.assignee_id && data.assignee_id || (user.role == 'staff' && settings.id && settings.id) || user.id, claims_admin_id: data.claims_admin_id == 'others' ? "" : data.claims_admin_id, claim_admin_details: data.claims_admin_id == 'others' && JSON.stringify(data.claim_admin_details[0]) || "" }, {
                                        "attachments": {
                                            "cover_page": (data.cover_page || []).map((e) => e.s3_file_key) || [],
                                            "clinical_notes": (data.clinical_notes || []).map((e) => e.s3_file_key) || [],
                                            "imaging_reports": (data.imaging_reports || []).map((e) => e.s3_file_key) || [],
                                            "supporting_docs": (data.supporting_docs || []).map((e) => e.s3_file_key) || [],
                                            "non_confirming_claims": (data.non_confirming_claims || []).map((e) => e.s3_file_key) || [],
                                        }
                                    }), mergePayLoad: Object.assign({}, { "id": record['case_data'] && record['case_data'].id || recordDuplicate && recordDuplicate.id, "fileUrls": [result.publicUrl, ...pdfArr] })
                                }, () => setUploading(false), () => {
                                    if (path == "/claims") {
                                        dispatch(actions.clearRecord());
                                    }
                                    setUploadModal(false)
                                }, () => setAdminAssignedValue(null)));
                            }).catch(err => dispatch(actions.updatePoc3({ errorUpload: err })))// eslint-disable-line
                        }
                    });
                })();
            });
        }
        // || (path == "/claims" && (!formValues.errors.patientInfo || (formValues.errors.patientInfo[0] && (!formValues.errors.patientInfo[0].name || !formValues.errors.patientInfo[0].date_of_injury || !formValues.errors.patientInfo[0].date_of_birth || !formValues.errors.patientInfo[0].claim_number))))
        async function uploadPdf(files, data, settings) {

            let base64 = '';
            let coverPagebese64 = (data.cover_page || []).map((e) => e.publicUrl);
            let clinicalNotesbese64 = (data.clinical_notes || []).map((e) => e.publicUrl);
            let imaageReportbese64 = (data.imaging_reports || []).map((e) => e.publicUrl);
            let supportingDocsbese64 = (data.supporting_docs || []).map((e) => e.publicUrl);
            let nonConfirmaingbese64 = (data.non_confirming_claims || []).map((e) => e.publicUrl);

            if (formValues.errors && (!formValues.errors.request_type || !formValues.errors.claims_admin_id || (user && user.role != "staff" && !formValues.errors.signature) || (user && user.role == "staff" && !formValues.errors.assignee_id))) {
                setDialogError(true);
                setUploading(false);
            }
            else if (formValues.errors.assignee_id && user && user.role == 'staff' && (!settings.hasAccess || (settings.hasAccess && (settings.signature == null || settings.signature == '')))) {
                if (settings && settings.hasAccess) {
                    if (settings.signature == null || settings.signature == '') {
                        setDialogError(`Selected Physician does not have digital signature saved in the system, please contact the physician to update the digital signature.`);
                        setUploading(false);
                    }
                }
                else {
                    setDialogError(`You are not authorized to generate forms on behalf of the selected physician. The physician can provide necessary authorization from VettedClaims website (under Users tab)`);
                    setUploading(false);
                }
            }
            else {
                const requestedTreatmentDoc = Object.assign({}, { treatments: formValues.form.equipments });
                let pdfArr = [];
                if (base64 != '') {
                    pdfArr.push(base64)
                }
                if (coverPagebese64.length != 0) {
                    pdfArr = [...pdfArr, ...coverPagebese64]
                }
                if (clinicalNotesbese64.length != 0) {
                    pdfArr = [...pdfArr, ...clinicalNotesbese64]
                }
                if (imaageReportbese64.length != 0) {
                    pdfArr = [...pdfArr, ...imaageReportbese64]
                }
                if (supportingDocsbese64.length != 0) {
                    pdfArr = [...pdfArr, ...supportingDocsbese64]
                }
                if (nonConfirmaingbese64.length != 0) {
                    pdfArr = [...pdfArr, ...nonConfirmaingbese64]
                }
                if (path == "/claims") {
                    await dispatch(actions.createRecord(data.patientInfo[0], "", (res) => generateRfa(files, data, settings, requestedTreatmentDoc, pdfArr, res)));
                    //    await generateRfa()
                } else {
                    await generateRfa(files, data, settings, requestedTreatmentDoc, pdfArr, record);
                }
                // files.open();
            }
        }

        const heads = [{
            diagnosis: 'Diagnosis\n(Required)',
            icdCode: 'ICD-Code\n(Required)',
            treatments: 'Service/Good Requested\n(Required)',
            cptCode: 'CPT/HCPCS Code (If known)',
            otherInformation: 'Other Information:(Frequency, Duration Quantity, etc.)',
        }];

        const splittedArray = (reqTreatment, origin) => {
            const resultArr = origin;
            if (reqTreatment.length > 5) {
                let subArr = [];
                let originalArr = reqTreatment.splice(0);
                resultArr.push([...heads, ...originalArr.filter((e, i) => (i < 5))]);
                subArr = originalArr.filter((e, i) => (i >= 5));
                if (subArr.length > 5) {
                    resultArr.push([...heads, ...subArr.filter((e, i) => (i < 5))]);
                    subArr = subArr.filter((e, i) => (i >= 5));
                } else {
                    if (subArr.length < 5) {
                        for (var i = subArr.length; i < 5; i++) {
                            subArr.push({
                                diagnosis: "",
                                icdCode: "",
                                treatments: "",
                                cptCode: "",
                                otherInformation: ""
                            });
                        }
                    }
                    resultArr.push([...heads, ...subArr]);
                    subArr = subArr.filter((e, i) => (i >= 5));
                }
                if (subArr.length != 0) {
                    splittedArray(subArr, resultArr);
                }

            } else {
                for (i = reqTreatment.length; i < 5; i++) {
                    reqTreatment.push({
                        diagnosis: "",
                        icdCode: "",
                        treatments: "",
                        cptCode: "",
                        otherInformation: ""
                    });
                }
                resultArr.push([...heads, ...reqTreatment]);
            }
            return resultArr

        }
        const handleEntryChange = (value) => {
            // let payloadData = [];
            if (value == 'new') {
                updateField("dialogForm", "patientInfo", [{}]);
                updateField("dialogForm", "existingPatients", '');
                updateField("dialogForm", "claims_admin_id", null);
                updateField("dialogForm", "claim_admin_details", [{}]);
                updateField("dialogForm", "assignee_id", null);
            }
        }


        function getExistPatient(data) {
            let payloadData = [];
            if (data) {
                const existingMetaData = metaData && metaData.existingPatients || [];
                const existPatient = existingMetaData.filter((e) =>
                    (e && e.value === data))
                if (existPatient.length != 0) {
                    payloadData[0] = Object.assign({}, {
                        "name": existPatient[0].name,
                        "date_of_injury": existPatient[0].date_of_injury,
                        "date_of_birth": existPatient[0].date_of_birth,
                        "claim_number": existPatient[0].claim_number,
                        "employer": existPatient[0].employer
                    });


                    if (user && user.role === 'staff' && existPatient && existPatient[0].assignee_id) {
                        dispatch(actions.loadPhysician(existPatient[0].assignee_id));
                    } else {
                        updateField("dialogForm", "assignee_id", null);
                    }


                    if (existPatient && existPatient[0].claims_admin_id) {
                        dispatch(actions.loadAdministrator(existPatient[0].claims_admin_id));
                    } else {
                        updateField("dialogForm", "claims_admin_id", null);
                    }

                }
                updateField("dialogForm", "patientInfo", payloadData);
            }
        }

        function handleOpen(condition, data) {
            const requestedTreatment = [];
            let j = 0;
            if (formValues.form.equipments.length != 0) {
                formValues.form.equipments.map((e, i) => {// eslint-disable-line
                    if (e.treatments != '') {
                        requestedTreatment.push({});
                        requestedTreatment[j].diagnosis = e.diagnosis;
                        requestedTreatment[j].icdCode = e.icdCode;
                        requestedTreatment[j].cptCode = e.cptCode;
                        requestedTreatment[j].otherInformation = e.otherInformation;
                        const selected = rfaOptions.filter(val => val.treatmentsId == e.treatments);
                        if (selected) {
                            requestedTreatment[j].treatments = selected[0].treatmentsName
                        }
                        j++;
                    }
                })
            }

            if (condition == 'showPdf') {
                const reqTreatment = [...requestedTreatment];
                const resArray = splittedArray(reqTreatment, []);
                // if (Object.keys(record).length != 0) {
                let Record = Object.assign({}, { ...settings });
                if (settings && settings.signature && settings.signature != '') {
                    const bytes = CryptoJS.AES.decrypt(settings.signature, settings.id);
                    let signature = bytes.toString(CryptoJS.enc.Utf8);
                    Record.signature = base64.decode(signature);
                }
                const value = {
                    "employeeInformation": record['case_data'] || data["patientInfo"][0], "physicianInformation": user.role == 'staff' && settings || metaData.patientDetails[0], "requestedTreatment": resArray, requestType: data.request_type ? JSON.parse(data.request_type) : {}, signature: user && user.role == "staff" && Record.signature || data.signature, date: moment().format("MM/DD/YYYY"), "adminInformation": data.claims_admin_id == 'others' && data.claim_admin_details[0] || administrator
                }

                setUploading(true);
                const getValue = handlePdf({ value: value });
                uploadPdf(getValue, data, Record);
                // }
                // else {
                //     const value = { "requestedTreatment": resArray, "physicianInformation": user.role == 'staff' && settings || metaData.patientDetails[0], requestType: user && user.role == "staff" && "" || data.request_type != "" ? JSON.parse(data.request_type) : {}, signature: data.signature, date: moment().format("MM/DD/YY"), seal1: base64String1, seal2: base64String2 }
                //     setUploading(true);

                //     const getValue1 = handlePdf({ value: value });
                //     uploadPdf(getValue1, data);
                // }
            } else if (condition == 'saveForm') {
                setSaveLoading(true);
                const requestedTreatmentDoc = Object.assign({}, { treatments: formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data });
                dispatch(actions.updateRecord(Object.assign({}, record['case_data'], { form_doc: JSON.stringify(requestedTreatmentDoc) }), 'saveForm', () => setSaveLoading(false)));
            }
        }

        function getPolicies(record, fun) {
            dispatch(actions.loadPoc2Doc(record, fun));
        }

        const closeAlertAddOn = () => {
            setShowAlertAddOn(false)
        }

        const closeAddOnModal = (ind, clear, enteredQuestions) => {// eslint-disable-line
            showAddOnModal(false);
        }

        const closeDialog = (ind1, addOn1, treatment1, questions) => {// eslint-disable-line
            let findRow = {};
            if (formValues.form && formValues.form["equipments"] && formValues.form["equipments"]) {
                findRow = (formValues.form && formValues.form["equipments"] && formValues.form["equipments"]).find((e) => (e && e.index == ind1 && e.treatments == treatment1));
            }

            if (findRow && (findRow.alert == "" || findRow.alert == undefined)) {
                autoRow({ keyName: "treatments", index: ind1, addOn: addOn && addOn || false, value: "", limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'], alert: "", alertContent: "", remove: true })
            }
            if (formValues.form && formValues.form["equipments"] && formValues.form["equipments"][ind1] && formValues.form["equipments"][ind1].alert == '' && formValues.form["equipments"][ind1].alertContent == '') {
                formValues.form["equipments"][ind1].diagnosis = '';

            }
            setModalOpen(false);
        }

        const showAddOnPopUp = (record, dispatch, { form }) => {// eslint-disable-line
            if (dialogType == "surgery" && Object.keys(formValues.form1).length != 0) {
                setTitle("Add Ons");
                setType("addOn");
                setAlertOpen(false);
                setModalOpen(false);
                setShowAlertAddOn(false);
                showAddOnModal(false);
                setAddOnTitle(title)
                setTimeout(() => setShowAddOn(true), 1000);
            }
        }
        const closed = (tableIndex) => {
            setShowAddOn(false);
            const treatmentValue = formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data;

            let newArray = treatmentValue;
            newArray = newArray.filter((e) => e && !(e.addOn === tableIndex && e.alert == "" && e.alertContent == ""));
            if (newArray.length < 5) {
                while (newArray.length < 5) {
                    newArray.push({
                        diagnosis: "",
                        icdCode: "",
                        treatments: "",
                        cptCode: "",
                        other: "",
                        alert: "",
                        alertContent: "",
                        otherInformation: "",
                        addOn: false
                    })
                }
            }
            updateField('reactTableForm', 'equipments', newArray);
        }

        const autoRow = (prop) => {
            const { keyName, index, value, addOn, alert, alertContent, label, questionValue, questions, other, remove, bodySystemName, onChange } = prop// eslint-disable-line
            const treatmentValue = formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data;

            if ((onChange || remove) && keyName == "treatments" && value != 'Surgery Add-on') {
                const data = treatmentValue.filter((val) => val.addOn === index);

                if (data && data.length > 0) {
                    treatmentValue.map((e, i) => {
                        if (e.addOn === index) {
                            treatmentValue[i].addOn = i;
                        }
                    })
                }

            }
            if (remove) {
                let newArray = treatmentValue;
                if (typeof (addOn) === 'number') {

                    let exist = newArray.findIndex((e) => e && e.index === index && e.addOn === addOn);
                    if (exist !== -1) {
                        let ind = (newArray).findIndex((e, i) => addOn <= i && e && e.diagnosis != "" && e.treatments != "");
                        if (ind !== -1 && exist !== newArray.length - 1) {
                            if (keyName == "treatments") {
                                const addOnArr = treatmentValue.map(e => e && e.addOn === addOn);
                                const addOnList = addOnArr.lastIndexOf(true);
                                const newArr = treatmentValue.filter((e) => !(e && addOn === e.addOn && index === e.index));
                                const addOnArr1 = treatmentValue.filter(e => e.addOn === addOn);
                                const addOnList1 = addOnArr1.length > 0 && addOnArr1.length;// eslint-disable-line

                                if (typeof (addOnList) === "number" && addOnList !== -1) {
                                    newArr.map((e, i) => {
                                        if (typeof (newArr[i].index) === "number" && typeof (newArr[i].addOn) !== "number") {

                                            newArr[i].index = i
                                        }
                                        if (newArr[i] && typeof (newArr[i].addOn) === "number" && newArr[i].addOn > addOnList) {
                                            if (typeof (addOnList) === "number" && addOnList !== -1) {
                                                newArr[i].addOn = newArr[i].addOn - 1;
                                            } else if (newArr[i].addOn > addOn) {
                                                newArr[i].addOn = newArr[i].addOn - 1;
                                            }
                                        }
                                    })
                                }

                                newArray = newArr;
                            } else {
                                newArray = newArray.filter((e) => !(e && e.index === index && e.addOn === addOn && e.diagnosis === value))
                            }
                        } else {
                            newArray[exist].addOn = false;
                            newArray[exist]["icdCode"] = ""
                            newArray[exist]["other"] = ""
                            newArray[exist]["cptCode"] = ""
                            newArray[exist]["treatments"] = ""
                            newArray[exist]["diagnosis"] = "";
                            newArray[exist]["alert"] = "";
                            newArray[exist]["alertContent"] = "";
                            newArray[exist]["otherInformation"] = "";
                            delete newArray[exist].index;
                        }
                    } else {
                        newArray = newArray.filter((e) => !(e && e.index === index && e.addOn === addOn))
                    }
                } else {

                    //Get AddOn Row's
                    const addOnArr = treatmentValue.map(e => e && e.addOn === index);
                    const addOnList = addOnArr.lastIndexOf(true);

                    // Filtered the form records except we deleted one's
                    const newArr = treatmentValue.filter((e) => !(e && e.index === index && e.addOn === addOn));

                    newArr.map((e, i) => {
                        if (newArr[i].index && typeof (newArr[i].addOn) !== "number") {
                            newArr[i].index = i
                        }
                        if (newArr[i] && typeof (newArr[i].addOn) === "number" && newArr[i].addOn > addOnList) {
                            if (typeof (addOnList) === "number" && addOnList != -1) {
                                newArr[i].addOn = newArr[i].addOn - 1;
                            } else if (newArr[i].addOn > index) {
                                newArr[i].addOn = newArr[i].addOn - 1;
                            }
                        }
                    })
                    // }
                    // else {
                    //     newArray.map((e, i) => {
                    //         newArray[i].index = i;
                    //     })

                    // }
                    // newArr = newArr.filter((e) => !(e && e.index == index && e.addOn === addOn))
                    // newArr[index].addOn = false;
                    // newArr[index]["icdCode"] = ""
                    // newArr[index]["other"] = ""
                    // newArr[index]["cptCode"] = ""
                    // newArr[index]["treatments"] = ""
                    // newArr[index]["diagnosis"] = "";
                    // newArr[index]["alert"] = "";
                    // newArr[index]["alertContent"] = "";
                    // newArr[index]["otherInformation"] = "";
                    // newArr[index].index = index;
                    // delete newArr[index].bodySystemId;
                    // updateField('reactTableForm', 'equipments', newArr);
                    newArray = newArr;
                }
                while (newArray.length < 5) {
                    newArray.push({
                        diagnosis: "",
                        icdCode: "",
                        treatments: "",
                        cptCode: "",
                        other: "",
                        alert: "",
                        alertContent: "",
                        otherInformation: "",
                        addOn: false
                    })
                }
                updateField('reactTableForm', 'equipments', newArray)
            }
            else if (typeof (prop.addOn) === "number") {
                const exist = treatmentValue.findIndex(e => e && e.index === prop.index && e.addOn === prop.addOn);
                if (exist != -1) {
                    treatmentValue[exist].alert = prop.alert;
                    treatmentValue[exist].alertContent = prop.alertContent;
                    treatmentValue[exist].index = prop.index;
                    treatmentValue[exist].addOn = prop.addOn;
                    treatmentValue[exist][prop.keyName] = prop.value;
                    treatmentValue[exist].bodySystemId = bodySystemName || bodySystemValue || treatmentValue[exist].bodySystemId;

                    if (questions) {
                        treatmentValue[exist].questions = treatmentValue[exist].questions && Object.assign({}, treatmentValue[exist].questions, { [label]: questionValue }) || Object.assign({}, { [label]: questionValue });
                    }
                } else {
                    let ind = (treatmentValue).findIndex((e, i) => addOn <= i && e && e.diagnosis == "" && e.treatments == "");
                    if (ind !== -1 && (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && ind <= (record['rfa_form_row_limit'] || user['rfa_form_row_limit'])) {
                        treatmentValue[ind]["index"] = index
                        treatmentValue[ind]["addOn"] = addOn
                        treatmentValue[ind][keyName] = value
                        treatmentValue[ind].alert = alert;
                        treatmentValue[ind].alertContent = alertContent;
                        treatmentValue[ind].bodySystemId = bodySystemValue;
                        if (questions) {
                            treatmentValue[ind].questions = treatmentValue[ind].questions && Object.assign({}, treatmentValue[ind].questions, { [label]: questionValue }) || Object.assign({}, { [label]: questionValue });
                        }
                        if (ind === treatmentValue.length - 1 && (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && ind <= (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) - 1) {
                            treatmentValue.push({
                                diagnosis: "",
                                icdCode: "",
                                treatments: "",
                                cptCode: "",
                                other: "",
                                alert: "",
                                alertContent: "",
                                otherInformation: "",
                                addOn: false
                            })
                        }
                    } else if ((record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && treatmentValue.length <= (record['rfa_form_row_limit'] || user['rfa_form_row_limit'])) {
                        treatmentValue.push({
                            diagnosis: "",
                            icdCode: "",
                            treatments: "",
                            cptCode: "",
                            other: "",
                            alert: "",
                            alertContent: "",
                            otherInformation: "",
                            addOn: false
                        })
                        treatmentValue[treatmentValue.length - 1]["index"] = index
                        treatmentValue[treatmentValue.length - 1]["addOn"] = addOn
                        treatmentValue[treatmentValue.length - 1][keyName] = value
                        treatmentValue[treatmentValue.length - 1].alert = alert;
                        treatmentValue[treatmentValue.length - 1].alertContent = alertContent;
                        treatmentValue[treatmentValue.length - 1].bodySystemId = bodySystemValue;
                        if (questions) {
                            treatmentValue[treatmentValue.length - 1].questions = treatmentValue[treatmentValue.length - 1].questions && Object.assign({}, treatmentValue[treatmentValue.length - 1].questions, { [label]: questionValue }) || Object.assign({}, { [label]: questionValue });
                        }
                        treatmentValue.push({
                            diagnosis: "",
                            icdCode: "",
                            treatments: "",
                            cptCode: "",
                            other: "",
                            alert: "",
                            alertContent: "",
                            otherInformation: "",
                            addOn: false
                        })
                    }

                }
            } else {
                const exist = treatmentValue.findIndex(e => e && e.index === index && e.addOn === false);
                if (exist != -1) {
                    treatmentValue[exist].alert = prop.alert || treatmentValue[exist].alert;
                    treatmentValue[exist].alertContent = prop.alertContent || treatmentValue[exist].alertContent;
                    treatmentValue[exist].index = index;
                    treatmentValue[exist].addOn = prop.addOn || false;

                    if (keyName === "diagnosis" && treatmentValue[exist].bodySystemId && treatmentValue[exist].treatments) {
                        const arr = rfaOptions.filter((e) => e.treatmentsId == treatmentValue[exist].treatmnents && e.bodySystemId == treatmentValue[exist].bodySystemId);
                        const checkOther = arr.find((e) => e.diagnosis == value);
                        console.log("arr1 = ",arr,"checkouther = ",checkOther);
                        treatmentValue[exist].other = checkOther ? false : true;
                    } else if(keyName === "treatments" && treatmentValue[exist].bodySystemId && treatmentValue[exist].diagnosis) {
                        const arr = rfaOptions.filter((e) => e.diagnosisName == treatmentValue[exist].diagnosis && e.bodySystemId == treatmentValue[exist].bodySystemId);
                        const checkOther = arr.find((e) => e.treatmentsId == value);
                        console.log("arr2 = ",arr,"checkouther = ",checkOther);
                        treatmentValue[exist].other = checkOther ? false : true;
                    }
                    treatmentValue[exist][prop.keyName] = prop.value;

                    if (questions) {
                        treatmentValue[exist].questions = treatmentValue[exist].questions && Object.assign({}, treatmentValue[exist].questions, { [label]: questionValue }) || Object.assign({}, { [label]: questionValue });
                    }
                } else if ((record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && index <= (record['rfa_form_row_limit'] || user['rfa_form_row_limit'])) {

                    treatmentValue[index][keyName] = value;
                    treatmentValue[index].alert = alert || "";
                    treatmentValue[index].alertContent = alertContent || "";
                    treatmentValue[index].index = index;
                    treatmentValue[index].addOn = addOn || false;
                    if (questions) {
                        treatmentValue[index].questions = treatmentValue[index].questions && Object.assign({}, treatmentValue[index].questions, { [label]: questionValue }) || Object.assign({}, { [label]: questionValue });
                    }
                }
                if (index === treatmentValue.length - 1 && typeof (addOn) !== 'number' && (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && index <= (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) - 1) {
                    treatmentValue.push({
                        diagnosis: "",
                        icdCode: "",
                        treatments: "",
                        cptCode: "",
                        other: "",
                        alert: "",
                        alertContent: "",
                        otherInformation: "",
                        addOn: false
                    })
                }
                if (keyName && keyName !== 'cptCode' && keyName !== "icdCode" && keyName !== "otherInformation") {

                    const addOnArr = treatmentValue.map(e => e && e.addOn === index);
                    const addOnList = addOnArr.lastIndexOf(true);
                    const newArr = treatmentValue;
                    const addOnArr1 = treatmentValue.filter(e => e.addOn === index);
                    const addOnList1 = addOnArr1.length > 0 && addOnArr1.length;// eslint-disable-line

                    if (typeof (addOnList) === "number" && addOnList !== -1) {
                        newArr.map((e, i) => {
                            if (newArr[i].index && typeof (newArr[i].addOn) !== "number") {
                                newArr[i].index = i
                            }
                            if (newArr[i] && typeof (newArr[i].addOn) === "number" && newArr[i].addOn > addOnList) {
                                // newArr[i].addOn = newArr[i].addOn - addOnList1
                            }
                            // }
                        })
                    }

                    if (newArr.length < 5) {
                        while (newArr.length < 5) {
                            newArr.push({
                                diagnosis: "",
                                icdCode: "",
                                treatments: "",
                                cptCode: "",
                                other: "",
                                alert: "",
                                alertContent: "",
                                otherInformation: "",
                                addOn: false
                            })
                        }
                    }
                    updateField('reactTableForm', 'equipments', newArr)
                }
                if (keyName && keyName === 'cptCode' || keyName === "icdCode" || keyName === "otherInformation") {
                    const selectedIndex = treatmentValue.findIndex((e) => index === e.index && addOn === e.addOn && e.treatments === "");
                    if (selectedIndex != -1 && value === "") {
                        updateField('reactTableForm', `equipments[${index}]`, {
                            diagnosis: "",
                            icdCode: "",
                            treatments: "",
                            cptCode: "",
                            other: false,
                            alert: "",
                            alertContent: "",
                            addOn: false,
                            otherInformation: ""
                        })
                    }
                }
            }
        }

        const showQuestions = (props) => {
            const { treat, diag, index, label, addOn, bodySystem } = props// eslint-disable-line
            const type = rfaOptions.filter((e) => e.treatmentsId == treat && e.bodySystemId == bodySystem)
            const bodySystemName = bodySystemOpt.find((e) => e.value == bodySystem)
            formValues.form[`equipments`][index].alert = '';
            formValues.form[`equipments`][index].alertContent = '';

            if (treat != "" && diag != "Surgery Add-on" && diag != "others") {
                if (type[0] && type[0].type == "surgery") {
                    setType("surgery")
                } else {
                    setType("questions")
                }
                const existPolicy = addedPolicies.filter(e => e.treatment == treat);
                setAlertOpen(false)
                if (existPolicy.length == 0) {
                    getPolicies(Object.assign({}, { "body_system": bodySystemName && bodySystemName.index || "neck-and-upper-back", "treatment_id": treat }), () => setModalOpen(true))
                } else {
                    setModalOpen(true);
                }
                if (typeof (addOn) == 'number') {
                    setAddOn(addOn)
                } else {
                    setAddOn(false)
                }

                setIndex(index);
                // setModalOpen(true);
                setInitialValue({});
                if (diag == "Surgery Add-on") {
                    setTitle(`<span style="font-weight: bold">Diagnosis : </span><span>Surgery Add Ons</span><br /><span style="font-weight: bold">Service/Good Requested : </span><span>${label}</span>`)
                } else {
                    setTitle(`<span style="font-weight: bold">Diagnosis : </span><span>${diag}</span><br /><span style="font-weight: bold">Service/Good Requested : </span><span>${label}</span>`)
                }
                setBodySystemValue(bodySystem);
                setTreatment(treat);
                setDiagnosis(diag);
            }
        }

        const showQuestAddOn = (props) => {
            const { treat, diag, index, addOn, checked, bodySystem } = props// eslint-disable-line
            const treatmentValue = formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data;
            let ind = (treatmentValue).findIndex((e, i) => addOn <= i && e && e.diagnosis == "" && e.treatments == "");

            if (ind != -1 && (record['rfa_form_row_limit'] || user['rfa_form_row_limit']) && ind < (record['rfa_form_row_limit'] || user['rfa_form_row_limit'])) {
                if (checked) {
                    const type = rfaOptions.filter((e) => e.treatmentsId == treat && e.bodySystemId == bodySystem);
                    const bodySystemName = bodySystemOpt.find((e) => e.value == bodySystem)
                    if (type[0] && type[0].type == "surgery") {
                        setType("surgery")
                    } else {
                        setType("questions")
                    }
                    const existPolicy = addedPolicies.filter(e => e.treatment == treat);
                    setAlertOpen(false)
                    if (existPolicy.length == 0) {
                        getPolicies(Object.assign({}, { "body_system": bodySystemName && bodySystemName.index || "neck-and-upper-back", "treatment_id": treat }), () => showAddOnModal(true));
                    } else {
                        showAddOnModal(true);
                    }
                    if (typeof (addOn) == 'number') {
                        setAddOn(addOn)
                    } else {
                        setAddOn(false)
                    }
                    setIndex(index);
                    // showAddOnModal(true);
                    setInitialValue({});
                    setTitle("Add Ons");
                    setType("addOn");
                    setBodySystemValue(bodySystem);
                    setTreatment(treat);
                    setDiagnosis(diag);
                } else {
                    if (typeof (addOn) == 'number') {
                        setAddOn(addOn)
                    } else {
                        setAddOn(false)
                    }
                    setShowAlertAddOn(false);
                    showAddOnModal(false);
                    const alertBox = formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter((e) => (e.index === index && e.addOn === addOn)) || [];
                    if (alertBox.length != 0) {
                        enableAddonAlert(alertBox[0].alert, alertBox[0].alertContent, alertBox[0].index, alertBox[0].addOn, true, treat)
                    }
                }
            } else {
                dispatch(actions.updatePoc3({ errorUpload: "You Reached Maximum Row Limits" }));
            }
        }

        const enableAddonAlert = (alert, alertContent, index, addOn, remove, treatment1) => {
            autoRow({ keyName: "treatments", index: index, value: treatment1, addOn: addOn, remove: remove, alert: alert, alertContent: alertContent, limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'] })
        }

        const enableAlert = (alert, alertContent, index, popup, addOn1, remove, treatment) => {
            if (!popup && typeof (addOn1) != 'number') {
                setAlertOpen(true);
                alert == "error" || alert == "warning" ? setIcon(false) : setIcon(true);
                setColor(alert);
                setAlertContent(alertContent)
            }
            autoRow({ keyName: "treatments", index: index, value: treatment, addOn: addOn1, limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'], alert: alert, alertContent: alertContent });
            if (diagnosis == "Surgery Add-on") {
                autoRow({ keyName: "diagnosis", index: index, value: diagnosis, addOn: addOn1, limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'], alert: alert, alertContent: alertContent });
            }
            if (dialogType == "addOn" && addOn === addOn1 && !popup) {
                setShowAlertAddOn(true);
                showAddOnModal(false);
            }
        }

        const formModal = (tableIndex, index1, addOn) => {
            let label = "";
            let initial = '';
            const addOnIndex = formValues.form && formValues.form["equipments"] && formValues.form["equipments"].findIndex((e) => e && e.index == tableIndex && e.addOn === addOn);
            const bodySystemName = bodySystemOpt.find((e) => e.value == formValues.form["equipments"][addOnIndex].bodySystemId)
            if (addOnIndex != -1) {
                if (typeof (addOn) == 'number') {
                    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][addOnIndex].bodySystemId && e1.type == "surgery-add-on" && e1.treatmentsId == formValues.form["equipments"][addOnIndex].treatments);
                    label = treatmentOpt && treatmentOpt[0].treatmentsName;

                    setIndex(tableIndex);
                    initial = formValues.form && formValues.form["equipments"] && formValues.form["equipments"][addOnIndex].questions || {};
                } else {
                    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][addOnIndex].bodySystemId && e1.type != "surgery-add-on" && e1.treatmentsId == formValues.form["equipments"][addOnIndex].treatments);
                    label = treatmentOpt && treatmentOpt[0].treatmentsName;
                    setIndex(tableIndex);
                    initial = formValues.form && formValues.form["equipments"] && formValues.form["equipments"][index1].questions || {};
                }
                setInitialValue(initial || {})
                setAlertOpen(false);
                setType("enteredForm");
                setAddOn(addOn)
                showAddOnModal(false);
                setShowAlertAddOn(false);
                setShowAddOn(false);
                setTitle(`<span style="font-weight: bold">Diagnosis : </span><span>${formValues.form["equipments"][addOnIndex].diagnosis}</span><br /><span style="font-weight: bold">Service/Good Requested : </span><span>${label}</span>`)
                setTreatment(formValues.form["equipments"][addOnIndex].treatments);
                setDiagnosis(formValues.form["equipments"][addOnIndex].diagnosis);
                if (name == 'rfaForm' && pathname.indexOf('poc3') > -1) {
                    const existPolicy = addedPolicies.filter(e => e.treatment == formValues.form["equipments"][addOnIndex].treatments);
                    setAlertOpen(false);
                    if (existPolicy.length == 0) {
                        getPolicies(Object.assign({}, { "body_system": bodySystemName && bodySystemName.index || "neck-and-upper-back", "treatment_id": formValues.form["equipments"][addOnIndex].treatments }), () => setModalOpen(true))
                    } else {
                        setModalOpen(true);
                    }
                } else {
                    setModalOpen(true);
                }
            }
        }

        const formSelected = (tableIndex, index1, addOn) => {
            let label = "";
            let initial = '';
            const alertIndex = formValues.form && formValues.form["equipments"] && formValues.form["equipments"].findIndex((e) => e && e.index == index1 && e.addOn === addOn);

            if (alertIndex != -1) {
                if (typeof (addOn) == 'number') {
                    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][alertIndex].bodySystemId && e1.type == "surgery-add-on" && e1.treatmentsId == formValues.form["equipments"][alertIndex].treatments);
                    label = treatmentOpt && treatmentOpt[0].treatmentsName;
                    setIndex(index1);
                    initial = formValues.form["equipments"][alertIndex] && formValues.form["equipments"][alertIndex].questions || {}
                } else {
                    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == formValues.form["equipments"][alertIndex].bodySystemId && e1.type != "surgery-add-on" && e1.treatmentsId == formValues.form["equipments"][alertIndex].treatments);
                    label = treatmentOpt && treatmentOpt[0].treatmentsName;
                    setIndex(index1);
                    initial = formValues.form["equipments"][alertIndex] && formValues.form["equipments"][alertIndex].questions || {};
                }
                setInitialValue(initial || {})
                setAlertOpen(false);
                setType("addOnEnteredForm");
                setAddOn(addOn)
                setShowAlertAddOn(false);
                setTitle(`<span style="font-weight: bold">Diagnosis : </span><span>${formValues.form["equipments"][alertIndex].diagnosis}</span><br /><span style="font-weight: bold">Service/Good Requested : </span><span>${label}</span>`)
                setTreatment(formValues.form["equipments"][alertIndex].treatments);
                setDiagnosis(formValues.form["equipments"][alertIndex].diagnosis);
                showAddOnModal(true);
            }
        }

        const storeQuestions = (prop) => {
            const { label, questionValue, index, addOn, remove, alertContent, keyName, value, questions, alert } = prop;

            if (!isNaN(questionValue) && typeof (addOn) != 'number') {
                formValues.form['equipments'][index].alert = '';
                formValues.form['equipments'][index].alertContent = '';
                autoRow({ keyName: keyName, index: index, value: value, addOn: addOn, remove: remove, alert: alert, alertContent: alertContent, limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'], questions: questions, questionValue: questionValue, label: label })
            } else {
                autoRow({ keyName: keyName, index: index, value: value, addOn: addOn, remove: remove, alert: alert, alertContent: alertContent, limit: record['rfa_form_row_limit'] || user['rfa_form_row_limit'], questions: questions, questionValue: questionValue, label: label })
            }
        }

        const getTreatmentDiagnosisList = (id) => {
            isLoadingRecord(true)
            const Data = rfaOptions.filter((ele, ind) => ind === rfaOptions.findIndex(elem => elem.bodySystemId === ele.bodySystemId)).sort((a, b) => a.bodySystemName.localeCompare(b.bodySystemName));
            const existedData = Data.map(val => val.bodySystemId);
            if (Data.length > 0) {
                if (!existedData.includes(id)) {
                    dispatch(actions.getRfaFormDetail(id, () => isLoadingRecord(false)));
                }
            } else {
                dispatch(actions.getRfaFormDetail(id, () => isLoadingRecord(false)));
            }

        }

        return <Grid container name={"poc3Page"}>
            {path != "/claims" ? <AppBar container justifyContent='flex-end' id="VettedClaims-rfaForm-controller" className={classes.appbar} style={{ top: `${getOffset('VettedClaims-header-toolbar1') + ((getOffset('VettedClaims-header-toolbar2') / 2) - getOffset('VettedClaims-rfaForm-controller') / 2)}px`, width: `calc(100% - ${navbarWidth}px) `, left: `${navbarWidth}px` }}>
                <Grid container justifyContent='flex-end'
                    className={classes.headerButton}>
                    <Grid item style={{ alignSelf: 'center', marginRight: '13px' }}>
                        <Link to={{ pathname: `/rfaForms`, state: history.location.state }}>
                            <Button className={classes.headerBtn} onClick={loadCases}>Back</Button>
                        </Link>
                    </Grid>
                    <Grid item style={{ alignSelf: 'center' }}>
                        <AlertDialog
                            description={deleteConfirmation}
                            onConfirm={deleteRecord}
                            onConfirmPopUpClose={true}
                            btnLabel1='Delete'
                            btnLabel2='Cancel' >
                            {(open) => <span className={classes.delete} onClick={open}><FormattedMessage {...({
                                id: `app.containers.Poc3PAge.delete`,
                                defaultMessage: 'Delete',
                            })} /></span>}
                        </AlertDialog>
                    </Grid>
                </Grid>
            </AppBar> : null}
            {path != "/claims" ? <Grid container justifyContent='flex-start' style={name == 'rfaForm' && pathname.indexOf('poc3') > -1 ? {
                position: 'relative', boxShadow: '0px 0px 8px 1px lightgrey',
                borderRadius: '8px', padding: '8px 16px'
            } : {}}>
                {record['case_data'] != undefined ? <Grid item xs={12} >
                    <div>
                        <Grid container justifyContent={'space-between'}>
                            <Grid item md={4} xs={12} style={{ alignSelf: 'center' }}>
                                <Grid container justifyContent={sm1 ? 'space-between' : 'flex-start'}>
                                    <Grid item style={{ alignSelf: 'center' }}>
                                        <h3 className={classes.section_tittle}>Patient Info

                                        </h3>
                                    </Grid>
                                    <Grid item style={{ alignSelf: 'center' }}>
                                        <span><EditOutlinedIcon className={classes.icons} onClick={() => setModalRecord(true)} /></span>
                                    </Grid>
                                </Grid>


                            </Grid>

                            {modalRecord ? <ModalRecordForm
                                title={`Patient Info`}
                                fields={casesColumns.filter(_ => _.editRecord && _.type != 'radio')}
                                initialValues={Object.assign({}, record['case_data'] && record['case_data'] || {})}
                                form={'modalRecord'}
                                btnLabel="Update"
                                enableSubmitBtn
                                name={name}
                                path={path}
                                onClose={() => setModalRecord(false)}
                                metaData={metaData}
                                className={classes.editForm}
                                onSubmit={handleEditRecord}
                                show={modalRecord}
                                error={updateError}
                            />

                                : null}
                        </Grid>
                        <Grid container className={classes.patientFirstDiv}>
                            <Grid item md={4} xs={12} className={classes.patientSecDiv}>
                                <span style={{ fontFamily: "MyriadPro-Semibold" }}> Name: </span>
                                <span>{record['case_data'].name && record['case_data'].name || ''}</span>
                            </Grid>
                            <Grid item md={4} xs={12} className={classes.patientSecDiv}>
                                <span style={{ fontFamily: "MyriadPro-Semibold" }}> Date of Injury: </span>
                                <span>{record['case_data'].date_of_injury && record['case_data'].date_of_injury || ''}</span>
                            </Grid>
                            <Grid item md={4} xs={12} className={classes.patientSecDiv}>
                                <span style={{ fontFamily: "MyriadPro-Semibold" }}> Date of Birth: </span>
                                <span>{record['case_data'].date_of_birth && record['case_data'].date_of_birth || ''}</span>
                            </Grid>
                            <Grid item md={4} xs={12} className={classes.patientSecDiv}>
                                <span style={{ fontFamily: "MyriadPro-Semibold" }}> Claim Number: </span>
                                <span>{record['case_data'].claim_number && record['case_data'].claim_number || ''}</span>
                            </Grid>
                            <Grid item md={4} xs={12} className={classes.patientSecDiv}>
                                <span style={{ fontFamily: "MyriadPro-Semibold" }}> Employer: </span>
                                <span>{record['case_data'].employer && record['case_data'].employer || ''}</span>
                            </Grid>
                        </Grid>
                    </div>
                </Grid> : null}
            </Grid> : null}

            <Grid container justifyContent={'space-between'} style={{ marginTop: path != "/claims" ? '24px' : '0px' }}>
                <Grid item xs={12} md={4} style={{ alignSelf: 'center' }}>
                    {name == 'rfaForm' && pathname.indexOf('poc3') > -1 ? <h3 className={classes.section_tittle}>Treatment Request</h3> : null}
                </Grid>
                <Grid item xs={12} md={8} style={{ alignSelf: 'center', width: '100%' }}>
                    <Grid container justifyContent={'flex-end'}>
                        {(location.state && location.state.createPoc3 && clearing) || (clearing && formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0) || formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "") != 0 || (!location.state && !clearing) ?
                            <Grid item className={classes.addBtn1}>
                                <AlertDialog
                                    description={clearConfirmation}
                                    onConfirm={handleClear}
                                    onConfirmPopUpClose={true}
                                    btnLabel1='Clear'
                                    btnLabel2='Cancel' >
                                    {(open) => <Button disabled={formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0} style={(formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0) && { opacity: '0.5' } || {}} className={classes.btn1} onClick={open}><FormattedMessage {...({
                                        id: `app.containers.Poc3PAge.clear`,
                                        defaultMessage: 'Clear',
                                    })} /></Button>}
                                </AlertDialog>
                            </Grid> : null}
                        {name == 'rfaForm' && pathname.indexOf('poc3') > -1 ?
                            <Grid item className={classes.addBtn}>
                                <Button
                                    className={classes.btn}
                                    disabled={((formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0 || formValues.form && formValues.form["equipments"] && record.form_doc != null && typeof record.form_doc == 'object' && isEqual(formValues.form["equipments"], record.form_doc.treatments))) ? true : false}
                                    onClick={() => handleOpen('saveForm')}

                                    style={((formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0 || formValues.form && formValues.form["equipments"] && record.form_doc != null && typeof record.form_doc == 'object' && isEqual(formValues.form["equipments"], record.form_doc.treatments))) ? { opacity: '0.5', width: sm1 ? '100%' : '85px' } : { width: sm1 ? '100%' : '85px' }} >
                                    {saveLoading ? <ButtonSpinner style={{ minWidth: 'auto' }} /> : 'Save'}
                                </Button>
                            </Grid> : null}
                        <Grid item className={classes.addBtn}>
                            <Button className={classes.btn} disabled={formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0 ? true : false} onClick={() => loadDialogForm()} style={formValues.form && formValues.form["equipments"] && formValues.form["equipments"].filter(e => !e.alert == "").length == 0 ? { opacity: '0.5' } : null}>Generate DWC RFA</Button>

                        </Grid>
                        {(loading || pageLoader) && dialogType != 'addOn' ? <Spinner className={classes.spinner} /> : null}
                    </Grid>
                </Grid>
            </Grid>
            {uploadModal ?
                <DialogForm
                    initialValues={path == "/claims" ? Object.assign({}, {
                        signature: user && user.signature || '',
                        cover_page: [],
                        clinical_notes: [],
                        imaging_reports: [],
                        supporting_docs: [],
                        non_confirming_claims: []
                    }) : Object.assign({}, {
                        signature: user && user.signature || '',
                        request_type: record['request_type'] && JSON.stringify(record['request_type']) || '',
                        claims_admin_id: record.claims_admin_id && (!record.is_claims_admin) ? "others" : record['claims_admin_id'] && record['claims_admin_id'] || null,
                        claim_admin_details: (!record.is_claims_admin) && formValues.claims_admin_id == "others" ? [administrator] : formValues.claims_admin_id == "others" && [] || [administrator],
                        assignee_id: record['assignee_id'] && record['assignee_id'] || '',
                        cover_page: record.attachments && record.attachments.cover_page && (record.attachments.cover_page.map((e) => Object.assign({}, { s3_file_key: e }))) || [],
                        clinical_notes: record.attachments && record.attachments.clinical_notes && (record.attachments.clinical_notes.map((e) => Object.assign({}, { s3_file_key: e }))) || [],
                        imaging_reports: record.attachments && record.attachments.imaging_reports && (record.attachments.imaging_reports.map((e) => Object.assign({}, { s3_file_key: e }))) || [],
                        supporting_docs: record.attachments && record.attachments.supporting_docs && (record.attachments.supporting_docs.map((e) => Object.assign({}, { s3_file_key: e }))) || [],
                        non_confirming_claims: record.attachments && record.attachments.non_confirming_claims && (record.attachments.non_confirming_claims.map((e) => Object.assign({}, { s3_file_key: e }))) || []
                    })}
                    title="Generate DWC RFA Form"
                    fields={generateRfaColumn}
                    formValues={formValues}
                    administrator={administrator}
                    settings={settings}
                    path={path}
                    form={`dialogForm`}
                    btnLabel="Generate"
                    show={uploadModal}
                    closeAlert={() => setDialogError(false)}
                    user={user}
                    formError={dialogError ? (typeof (dialogError) == 'string' ? dialogError : formValues.errors
                    ) : false}
                    record={path == "/claims" && {} || record}
                    enableScroll={classes.bodyScroll}
                    enableSubmitBtn
                    physicianDetailChange={physicianChange}
                    AdminstratorDetailChange={AdministratorChange}
                    metaData={metaData}
                    onClose={() => {
                        if (path == "/claims") {
                            dispatch(actions.clearRecord());
                        }
                        setUploadModal(false); setDialogError(false); setAdminAssignedValue(null);
                    }}
                    className={classes.signatureForm}
                    loading={uploading}
                    onSubmit={(data) => handleOpen('showPdf', data)}
                    updateField={updateField}
                    getExistPatient={getExistPatient}
                    handleEntryChange={handleEntryChange}
                /> : null}
            <Grid container name={"Poc3Table"} direction={!sm ? "column-reverse" : "row"} wrap={!sm ? "nowrap" : "wrap"}>
                <Grid item xs={12} md={activeChildren ? 12 : 12} className={classes.tableData}>

                    <div className={classes.table} style={{marginTop: path == "/claims" ? "25px" : "20px"}}>
                        <ReactTable
                            columns={columns}
                            initialData={formValues.form && formValues.form["equipments"] && formValues.form["equipments"].length != 0 && formValues.form["equipments"] || record['form_doc'] && record['form_doc'] != null && record['form_doc'].treatments || data}
                            children={activeChildren}// eslint-disable-line
                            path={path}
                            name={name}
                            history={history}
                            locationState={location.state}
                            metaData={metaData}
                            updateField={updateField}
                            loading={pageLoader}
                            formValues={formValues}
                            showQuestions={(props) => showQuestions(props)}
                            rfaOptions={rfaOptions}
                            ind={ind}
                            autoRow={autoRow}
                            formModal={formModal}
                            enableDelete={enableAddonAlert}
                            storeQuestions={storeQuestions}
                            limit={record['rfa_form_row_limit'] || user['rfa_form_row_limit'] || -1}
                            form={'reactTableForm'}
                            getTreatmentDiagnosis={getTreatmentDiagnosisList}
                            bodySystemOpt={bodySystemOpt}
                            loadingRecord={loadingRecord}
                            getRfaRecords={(e) => dispatch(actions.getAllRFARecords(e))}
                            favorites={favorites}
                        />
                    </div>
                </Grid>
                {showModal ? <Grid item xs={12} md={6}>

                    <ModalForm
                        title={title}
                        treatment={treatment}
                        diagnosis={diagnosis}
                        close={closeDialog}
                        open={showModal}
                        loading={loading}
                        formValues={formValues}
                        addedPolicies={addedPolicies}
                        policies={policies}
                        updateField={updateField}
                        enableAlert={enableAlert}
                        ind={ind}
                        dialogType={dialogType}
                        metaData={metaData}
                        records={formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data}
                        autoRow={autoRow}
                        limit={record['rfa_form_row_limit'] || user['rfa_form_row_limit'] || -1}
                        onSubmit={(record, dispatch, { form }) => showAddOnPopUp(record, dispatch, { form })}
                        storeQuestions={storeQuestions}
                        initialValues={initialValue}
                        addOn={addOn}
                    />

                </Grid> : null}


                {
                    showAddOn ?
                        <AddOnForm
                            close={closed}
                            treatment={treatment}
                            diagnosis={diagnosis}
                            open={showAddOn}
                            closeDialog={closeDialog}
                            loading={loading}
                            formValues={formValues}
                            updateField={updateField}
                            enableAlert={enableAlert}
                            showModal={addOnModal}
                            dialogType={dialogType}
                            records={formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data}
                            autoRow={autoRow}
                            limit={record['rfa_form_row_limit'] || user['rfa_form_row_limit'] || -1}
                            onSubmit={(record, dispatch, { form }) => showAddOnPopUp(record, dispatch, { form })}
                            storeQuestions={storeQuestions}
                            initialValues={initialValue}
                            bodySystemId={bodySystemValue}
                            rfaOptions={rfaOptions}
                            metaData={metaData}
                            title={title}
                            addedPolicies={addedPolicies}
                            policies={policies}
                            ind={ind}
                            showQuestAddOn={showQuestAddOn}
                            closeModal={closeAddOnModal}
                            getPolicies={getPolicies}
                            showAlertAddOn={showAlertAddOn}
                            closeAlertAddOn={closeAlertAddOn}
                            addOn={addOn}
                            formSelected={formSelected}
                            addOnTitle={addOnTitle}
                            treatmentValue={formValues.form && formValues.form["equipments"] && formValues.form["equipments"] || data}
                        /> : null
                }

                {/* This snackbar shows message of matched treament and diagnosis result  */}
                {alerter && !showModal ? <Snackbar open={alerter} onClose={() => {
                    setAlertOpen(false);
                    const treatmentOpt = rfaOptions.filter((e1) => e1.bodySystemId == bodySystemValue && e1.type == "surgery-add-on");
                    if (dialogType == "surgery" && formValues.form && formValues.form["equipments"] && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == 'boolean') && formValues.form["equipments"].find((e) => e && e.index == ind && typeof (e.addOn) == 'boolean').alert == 'success' && treatmentOpt.length != 0) {
                        setTitle("Add Ons");
                        setType("addOn");
                        setAddOnTitle(title)
                        setShowAddOn(true);
                        setShowAlertAddOn(false);
                    }
                }} autoHideDuration={2000} className={classes.snackbar} sx={{ position: 'fixed', width: '60%' }} anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}>
                    {alerter && !showModal ? <div className={classes.alertDiv}>
                        <Alert
                            iconMapping={{
                                success: icon ? <CheckCircleOutlineIcon fontSize="inherit" /> : <Warning fontSize="inherit" />,
                            }}

                            color={colorContent}
                            style={colorContent == 'error' ? { background: '#f5b9b9' } : null}
                            className={classes.alert}
                        >
                            <div>
                                <span dangerouslySetInnerHTML={{ __html: alertContents.substring(0, alertContents.indexOf("<br /> <br />")) }} />
                            </div>
                        </Alert>
                    </div> : null}
                </Snackbar> : null}

                {/* This snackbar shows success and error message of save form and generate form submit result */}
                {successMessage || errorMessage ? <SnackbarComp show={successMessage || errorMessage ? true : false} text={successMessage || errorMessage} severity={successMessage ? 'success' : errorMessage ? 'error' : ''} handleClose={() => dispatch(actions.updatePoc3(false))} /> : null}
            </Grid>
        </Grid>
    }

    const mapStateToProps = createStructuredSelector({

        loading: selectLoading(),
        favorites: selectTreatments(),
        metaData: selectPocMetaData(),
        error: selectError(),
        user: selectUser(),
        policies: selectPoc2PoliciesDoc(),
        addedPolicies: selectPoc2AddedDoc(),
        settings: selectPhysicians(),
        administrator: selectAdministrator(),
        record: selectRecord(),
        update: selectUpdate(),
        expand: selectSessionExpand(),
        pageLoader: selectPageLoader(),
        updateError: selectUpdateError(),
        rfaOptions: selectAllTreatmentsAndDiagnosis(),
        bodySystemList: selectbodySystemNames(),
        alerting: selectPocAlert(),
        formValues: (state) => {
            return {
                form: state.form && state.form.reactTableForm && state.form.reactTableForm.values || false,
                form1: state.form && state.form.pocForm && state.form.pocForm.values || false,
                form2: state.form && state.form.addOnForm && state.form.addOnForm.values || false,
                errors: state.form && state.form.dialogForm && state.form.dialogForm.values || false,

            }
        },
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch,
            updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );


    return compose(
        withConnect,
        memo
    )(Poc3Page);

}