import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    root: {
      height: '100vh',
      flexGrow: 1
    },
    component: {
      padding: '35px',
      background: 'white',
      display: 'flex',
      flexDirection: 'column',
      boxShadow: '0px 0px 20px 10px #00000026',
      marginTop: `${theme.spacing(7)}`,
      alignItems: 'center'
    },
    FormPage: {
      display: 'flex',
      justifyContent: 'center',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    btn: {
      width: '75%',
      backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
      boxShadow: 'none',
      color: '#fff !important'
    }
  })
});


export default useStyles;
