/**
 *
 * Dwc Page
 *
 */


import React, { memo, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import useStyles from './styles';
import { Grid, Button } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { Helmet } from 'react-helmet';
import { handlePdf } from './pdfmaker';
import {
    selectUser,
} from 'blocks/session/selectors';
import { getPatientDetails } from 'blocks/poc2/remotes.js';

export default function (name, path, columns, create, view) {// eslint-disable-line

    function CwcPage(props) {
        const { location = {}, record, metaData, dispatch, formRecord = {}, match, queryParams, form, formValues, user } = props;// eslint-disable-line
        const classes = useStyles();
        const theme = useTheme();
        const md = useMediaQuery(theme.breakpoints.up('md'));
        const componentMounted = useRef(true);


        useEffect(() => {
            // let mounted = true;
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [])

        const value = {// eslint-disable-line
            requestType: { newRequest: true },
            employeeInformation: {
                name: "SenthilNathan",
                dob: "27/5/1997",
                doi: "26/6/2022",
                claimNumber: "#1",
                employer: "Junior Developer"
            },
            physicianInformation: {
                name: "Sai Ganesh",
                practiceName: "Miotiv",
                contactName: "Sai",
                address: "86, square colony, chennai",
                city: "New York",
                state: "Chennai",
                phone: "8877665599",
                npi: "345",
                fax: "213445",
                speciality: "Dentist",
                email: "senthilnathan.rifluxyss@gmail.com",
                zip: "12345"
            },
            adminInformation: {
                name: "VettedClaims",
                address: "v.o.c street, johnson colony, chennai",
                city: "Los Angels",
                state: "California",
                phone: "3355667711",
                fax: "534341",
                email: "selva@rifluxyss.com",
                zip: "54321"
            },
            requestedTreatment: [
                {
                    diagnosis: "BioFeedBack",
                    icd: "2021",
                    request: "service",
                    code: "3201",
                    other: '50HZ'
                },
                {
                    diagnosis: "Cryptotherapy",
                    icd: "1567",
                    request: "good",
                    code: "2201",
                    other: '50HZ'
                },
                {
                    diagnosis: "Discography",
                    icd: "4512",
                    request: "good",
                    code: "3342",
                    other: '20HZ'
                }
            ],
            date: "22/6/2022",
            uroResponse: "modified",
            authNumber: "$%6fgd*^7",
            authDate: "23/6/2022",
            authName: "senthil",
            phone: "9976516400",
            fax: "66773434",
            email: "senthil@gmail.com",
            comments: "this is testing only"
        }

        const handleOpen = () => {
            getPatientDetails(user).then((result) => {
                handlePdf({value: Array.isArray(result) && result[0] || {}});
                // handlePdf({ value: value });
                // pdfDocGenerator.open();
            }).catch(e => Promise.reject(e))

        }

        return (<Grid container>
            <Helmet
                title="VettedClaims"
                meta={[
                    { name: 'description', content: 'Insurance Page' },
                ]}
            />
            <Grid item xs={12} className={classes.FormPage}>
                <div className={classes.component} style={{ width: md ? `420px` : `100%` }}>
                    <h3>DWC Form</h3>
                    <Button className={classes.btn} onClick={handleOpen}>Download DWC Form RFA</Button>
                </div>
            </Grid>
        </Grid>);
    }

    const mapStateToProps = createStructuredSelector({
        user: selectUser()
    });


    function mapDispatchToProps(dispatch) {
        return {
            dispatch
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(CwcPage);
}