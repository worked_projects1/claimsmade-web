
/**
 * 
 * Edit Record Page
 * 
 */


import React, { useState, useEffect, useRef, memo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import CreateTreatmentForm from 'components/CreateTreatmentForm';
import moment from 'moment';
import { formValueSelector, change } from 'redux-form';
import { Grid } from '@mui/material';
const CryptoJS = require("crypto-js");

import {
    selectUser,
} from 'blocks/session/selectors';


/**
 * @param {string} name 
 * @param {string} path 
 * @param {array} columns 
 * @param {object} actions 
 * @param {object} selectors 
 */
export default function (name, path, columns, actions, selectors, decoratorView) {// eslint-disable-line

    const { selectRecord, selectRecordsMetaData, selectRecords } = selectors;

    function handleCreate(user, entry, id, record, dispatch, { form }) { // eslint-disable-line

        if (entry == "manual") {
            var policies = record.policy == "yes" && record.policies.map((e) => e != undefined).includes(true) ? (record.policies || []).map((policy) => {
                return Object.assign({}, {// eslint-disable-line
                    "source": policy.source,
                    "diagnosis": {
                        "index-name": policy.diagnosis.trim().replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                        "label-name": policy.diagnosis.trim()
                    },
                    "recommendation": policy.recommendation != "askaquestion" ? policy.recommendation : "1",
                    "questions": policy.recommendation == "askaquestion" && policy.questions.length != 0 && policy.questions.find(e => true) != undefined ?// eslint-disable-line
                        policy.questions.map((question, i) =>
                            Object.assign({}, { "#": (i + 1).toString(), "question": question.questionsText, "option-list": [{ "option": "yes", "action": question.yes == "nextQuestion" ? (i + 1 + 1).toString() : question.yes }, { "option": "no", "action": question.no == "nextQuestion" ? (i + 1 + 1).toString() : question.no }] }))
                        : [],
                    ...(policy["mtus-text"] != undefined) && { "mtus-text": policy["mtus-text"] },
                    ...(policy["odg-text"] != undefined) && { "odg-text": policy["odg-text"] },
                }) || ""
            }) : [];

            const jsonObj = {
                "treatment": {
                    "index-name": record['treatment'].trim().replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                    "label-name": record['treatment'].trim(),
                    "treatment-type": record['treatment-type']
                },
                "body-system": {
                    "index-name": record['body-system'].replace(' - ', '-').replace(/\s+/g, '-').toLowerCase(),
                    "label-name": record['body-system']
                },
                "version": {
                    "mtus": record['mtus'] ? (!isNaN(Date.parse(record['mtus']))) && moment(record['mtus']).format('MM/DD/YY') || record['mtus'] : '',
                    "odg": record['odg'] ? (!isNaN(Date.parse(record['odg']))) && moment(record['odg']).format('MM/DD/YY') || record['odg'] : '',
                    "policy-doc": moment().format('MM/DD/YY'),
                    "created-by": user.name,
                    "comments": record['comments'] ? record['comments'] : ''
                },
                "policies": policies.filter(e => e != ""),
                "references": {
                    "mtus-link": record["mtus-link"] ? record["mtus-link"] : "",
                    "odg-link": record["odg-link"] ? record["odg-link"] : ""
                }
            }

            if (user.email == "siva@miotiv.com") {
                dispatch(actions.updateRecord(Object.assign({}, { doc: jsonObj, entry: entry, user: false, id: id }), form))
            } else {
                dispatch(actions.createRecord(Object.assign({}, { doc: jsonObj, entry: entry, user: true, id: id }), form))
            }
        } else {
            if (user.email == "siva@miotiv.com") {
                dispatch(actions.updateRecord({ doc: record.doc, entry: entry, user: false, id: id }, form))
            } else {
                dispatch(actions.createRecord({ doc: record.doc, entry: entry, user: true, id: id }, form))
            }
        }

    }


    /**
     * 
     * @param {object} props 
     */
    function EditTreatmentPage(props) {
        const { location = {}, record, records, metaData, dispatch, formValues, match, queryParams, user, initialize, updateField } = props;// eslint-disable-line
        const [loading, setLoading] = useState(true);
        const componentMounted = useRef(true);

        let schema = {}
        if (record["policy_doc"]) {
            var bytes = CryptoJS.AES.decrypt(record["policy_doc"], 'K6*^)&b=087&H%K!s2A0');
            var secret = bytes.toString(CryptoJS.enc.Utf8);
            schema.obj = JSON.parse(secret);
        }

        const fields = (typeof columns === 'function' ? columns(Object.assign({}, { process: "Update", updateField: updateField, policy: schema.obj && schema.obj.policies && schema.obj.policies.map((e, i) => e != undefined).includes(true) && "yes" || "no" })).columns : columns).filter(_ => formValues.form.entry == undefined ? _.value == "entry" : user.email == "siva@miotiv.com" ? (formValues.form.entry == "manual" ? _.value == "entry" || _.value != "doc" : formValues.form.entry == "fileUpload" ? _.value == "entry" || _.value == "doc" : _.editRecord && !_.edit) : (_.value != "entry" && _.value != "doc"));// eslint-disable-line

        const rec = {
            "body-system": (schema.obj && schema.obj["body-system"] && (schema.obj["body-system"])["label-name"] || ""),
            "treatment": (schema.obj && schema.obj["treatment"] && (schema.obj["treatment"])["label-name"] || ""),
            "treatment-type": (schema.obj && schema.obj["treatment"] && (schema.obj["treatment"])["treatment-type"] || ""),
            "mtus": (schema.obj && schema.obj["version"] && (schema.obj["version"])["mtus"] || ""),
            "odg": (schema.obj && schema.obj["version"] && (schema.obj["version"])["odg"] || ""),
            "comments": (schema.obj && schema.obj["version"] && (schema.obj["version"])["comments"] || ""),
            "mtus-link": (schema.obj && schema.obj["references"] && (schema.obj["references"])["mtus-link"] || ""),
            "odg-link": (schema.obj && schema.obj["references"] && (schema.obj["references"])["odg-link"] || ""),
            "policy": schema.obj && schema.obj.policies && schema.obj.policies.map((e, i) => e != undefined).includes(true) && "yes" || "no",// eslint-disable-line
            "policies": schema.obj && schema.obj.policies && schema.obj.policies.map((e, i) => Object.assign({},// eslint-disable-line
                {
                    "source": e["source"],
                    "recommendation": e.recommendation != "deny" && e.recommendation != "approve" && e.recommendation != "no_recommendation" ? "askaquestion" : e.recommendation,
                    "mtus-text": e["mtus-text"],
                    "odg-text": e["odg-text"],
                    "diagnosis": e.diagnosis["label-name"],
                    // "question": e.questions && e.questions.length != 0 && e.questions.map((question, ind) => question != undefined).includes(true) && "yes" || "no",// eslint-disable-line
                    "questions": e.questions && e.questions.map((question, ind) => Object.assign({},// eslint-disable-line
                        {
                            "questionsText": question.question,
                            "yes": question['option-list'] && isNaN(question['option-list'].find((option, index) => option.option == "yes").action) && question['option-list'].find((option, index) => option.option == "yes").action || "nextQuestion" || "",// eslint-disable-line
                            "no": question['option-list'] && isNaN(question['option-list'].find((option, index) => option.option == "no").action) && question['option-list'].find((option, index) => option.option == "no").action || "nextQuestion" || ""// eslint-disable-line
                        })
                    ) || [],
                })

            ) || []
        }

        useEffect(() => {
            // let mounted = true;
            dispatch(actions.loadRecord(match.params.id));
            dispatch(actions.loadRecords(false, queryParams));
            setLoading(true);
            setTimeout(() => setLoading(false), 200);
            // return () => mounted = false;
            return () => { // This code runs when component is unmounted
                componentMounted.current = false; // (4) set it to false when we leave the page
            }
        }, [match.params.id]);


        return (<div>
            {name == "treatments" ? <Grid style={{ fontWeight: 'bold' }}>{record.name}</Grid> : null}
            {!loading && record && Object.keys(record).length > 0 ?
                <CreateTreatmentForm
                    initialValues={rec}
                    name={name}
                    path={path}
                    form={`createTreatmentForm`}
                    metaData={metaData}
                    fields={fields}
                    onSubmit={handleCreate.bind(this, user, formValues.form.entry, record.id)}
                    locationState={location.state}
                    formValues={formValues}
                    submitButtonName={"Update"}
                    user={user}
                    updateField={updateField}
                /> : 'Loading...'}
        </div>)

    }

    EditTreatmentPage.propTypes = {
        children: PropTypes.object,
        dispatch: PropTypes.func,
        formRecord: PropTypes.object,
        history: PropTypes.object,
        location: PropTypes.object,
        match: PropTypes.object,
        metaData: PropTypes.object,
        record: PropTypes.object
    };

    const mapStateToProps = createStructuredSelector({
        record: selectRecord(),
        metaData: selectRecordsMetaData(),
        user: selectUser(),
        formValues: (state, props) => {// eslint-disable-line
            return {
                Policies: formValueSelector('createTreatmentForm')(state, 'policies'),
                form: (state.form.createTreatmentForm && state.form.createTreatmentForm.values) || false,
            }
        },
        records: selectRecords()
    });

    function mapDispatchToProps(dispatch) {
        return {
            dispatch,
            updateField: (form, field, newValue) => dispatch(change(form, field, newValue)),
        };
    }

    const withConnect = connect(
        mapStateToProps,
        mapDispatchToProps,
    );

    return compose(
        withConnect,
        memo
    )(EditTreatmentPage);

}