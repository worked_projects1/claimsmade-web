/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NotFoundPage';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: '404 - Not Found',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'This is not found page.',
  },
  message: {
    id: `${scope}.message`,
    defaultMessage: 'Cannot find the page you’re looking for. Please check the URL and try again',
  },
  notFound: {
    id: `${scope}.notFound`,
    defaultMessage: '404',
  },
  expired: {
    id: `${scope}.expired`,
    defaultMessage: 'Session Expired',
  },
  reLogin: {
    id: `${scope}.reLogin`,
    defaultMessage: 'Please re-login to renew your session',
  },
  backToHome: {
    id: `${scope}..backToHome`,
    defaultMessage: 'Go back to home',
  },
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the NotFoundPage container!',
  },
});
