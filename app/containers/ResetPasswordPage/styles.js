

import { makeStyles } from '@mui/styles';
import { createTheme } from '@mui/material/styles';

const useStyles = makeStyles(() => {
  const theme = createTheme();
  return ({
    submit: {
      margin: theme.spacing(3, 0, 2),
      backgroundImage: 'linear-gradient(90deg, #009cd2, #00ca1f)',
      textTransform: 'none',
      color: '#fff',
      '& span': {
        color: '#fff',
      },
    },
    section: {
      fontFamily: 'MyriadPro-Semibold',
      textAlign: 'center',
      marginTop: '60px'
    },
    title: {
      '& span': {
        fontWeight: '900',
      },
      textAlign: 'center',
      marginTop: '40px',
      fontFamily: 'MyriadPro-Bold',
    },

    resetGrid: {
      textAlign: 'center',
      marginTop: '20px'
    },
    resetTitle: {
      fontSize: '18px',
      fontFamily: 'MyriadPro-Semibold',
      textTransform: 'none'
    },
    copyright: {
      '& p': {
        color: 'rgb(127, 127, 127)',
      },
      ['@media (min-width:460px)']: { 
        position: 'absolute',
        bottom: '4%',   
      },
      color: 'rgb(127, 127, 127)',
    },
  })
});


export default useStyles;